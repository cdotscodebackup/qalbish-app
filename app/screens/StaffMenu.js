import React, {useState, useEffect} from 'react';
import {
    Alert,
    Image,
    KeyboardAvoidingView,
    StyleSheet, SafeAreaView,
    Text, SectionList,
    Platform,
    TouchableOpacity, ScrollView,
    StatusBar, TouchableHighlight, TouchableWithoutFeedback,
    View,
} from 'react-native';
import {FlatList, TextInput} from 'react-native-gesture-handler';
import TextStyles from '../Styles/TextStyles';
import dimen from '../Styles/Dimen';
import {
    _storeData, clearAllData,
    console_log,
    errorAlert, getData,
    getDataWithoutToken,
    getDataWithToken, getObject,
    isNetConnected,
    saveObject,
    successAlert,
} from '../Classes/auth';

import {
    data_key,
    general_network_error,
    login_url,
    logout_url,
    orders_url,
    referal_key,
    referral_discount_key,
    store_order_url,
    token_key,
    update_profile_image_url,
    web_view_url,
} from '../Classes/UrlConstant';
import {Loader} from '../Components/Loader';

import Colors from '../Styles/Colors';
import AppText from '../Components/AppText';
import MyImageButton, {MyImage} from '../Components/MyImageButton';
import MyBackgroundImage from '../Components/MyBackgroundImage';
import ImagePicker from 'react-native-image-picker';


export const StaffMenu = ({navigation}) => {


    const [isloading, setIsLoading] = useState(false);
    const [userData, setUserData] = useState({});
    const [userName, setName] = useState('');
    const [userEmail, setEmail] = useState('');


    useEffect(() => {
        updateUserData();

    }, []);
    const updateUserData = () => {
        getObject(data_key)
            .then((res) => {
                setUserData(res);
                setName(res.name);
                setEmail(res.email);

            })
            .catch((err) => {
                console_log(err);
            });
    };


    const logoutPressed = () => {
        isNetConnected()
            .then(() => {
                setIsLoading(true);
                let formData = {
                    // 'phone_number':phone,
                    // 'password':password,
                };
                getDataWithToken(logout_url, 'POST', {})
                    .then((response) => {
                        setIsLoading(false);
                        console_log('logout response ' + JSON.stringify(response));
                        clearAllData();
                        navigation.navigate('AuthStack');

                    })
                    .catch((err) => {
                        setIsLoading(false);
                        console_log('error: ' + err);
                        clearAllData();
                        navigation.navigate('AuthStack');
                    });
            })
            .catch((err) => {
                errorAlert(general_network_error);
                console_log(err);
            });
    };

    const submitOrders = () => {
        isNetConnected()
            .then(() => {
                setIsLoading(true);
                let orderObject = {
                    'id': 1,
                    'name': 'azeem chodhry',
                };
                let formData = {
                    'finalOrder': JSON.stringify(orderObject),
                };
                getDataWithToken(store_order_url, 'POST', formData)
                    .then((response) => {
                        setIsLoading(false);
                        console_log(response);

                    })
                    .catch((err) => {
                        setIsLoading(false);
                        console_log(err);

                    });
            })
            .catch((err) => {
                errorAlert(general_network_error);
            });
    };


    const profilePressed = () => {
        navigation.navigate('Profile');
    };
    const changePassword = () => {
        navigation.navigate('ChangePassword');
    };
    const servicesPressed = () => {
        navigation.navigate('Home');
    };
    const bookingsPressed = () => {
        navigation.navigate('Bookings');
    };
    const referralPressed = () => {
        navigation.navigate('MyReferrals');
    };
    const contactUsPressed = () => {
        // submitOrders();
        navigation.navigate('ContactUs');
    };


    const changeProfileImage = () => {

        const options = {
            title: 'Select Profile Picture',
            // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        ImagePicker.launchImageLibrary(options, (response) => {
            // console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = {uri: response.uri};
                let type = response.type.split('/')[1] !== undefined ? response.type.split('/')[1] : '';
                changeProfileImageApi(response.uri, type);
            }
        });

    };

    const changeProfileImageApi = (path, type) => {
        if (path === '') {
            errorAlert('Select image');
            return;
        }
        isNetConnected()
            .then(() => {
                setIsLoading(true);

                const data = new FormData();
                data.append('image', {
                    uri: path,
                    type: 'image/jpeg',
                    name: 'userPhoto' + new Date().getTime() + '.' + type,  //Math.random()
                });
                getData(update_profile_image_url, 'POST', data)
                    .then((response) => {
                        setIsLoading(false);
                        if (response.status === true) {
                            saveObject(data_key, response.data).then((res) => {
                                updateUserData();
                            })
                                .catch((err) => console_log(err));
                        } else {
                            errorAlert('Image size must be less than 2MB.');
                        }
                    })
                    .catch((err) => {
                        setIsLoading(false);
                        errorAlert('Error updating profile picture.');
                        console_log('error: ' + err);
                    });
            })
            .catch((err) => {
                errorAlert(general_network_error);
            });
    };


    const MenuItem = (props) => {
        const [isctive, setIsActive] = useState(false);
        const onPressItem = () => {
            props.onPress();
        };

        return (
            <TouchableWithoutFeedback onPress={onPressItem} underlayColor={Colors.primary_color}
                                      onPressIn={() => setIsActive(true)} onPressOut={() => setIsActive(false)}
            >
                <View style={[styles.menuItemContainer,
                    isctive ?
                        {backgroundColor: Colors.primary_color} : {backgroundColor: Colors.app_background_color}]}>
                    <MyImage source={props.image}
                             tintColor={isctive ? Colors.app_background_color : Colors.primary_color}
                             imageContainerStyle={styles.menuItemImageContainer}/>

                    {isctive && <AppText style={[styles.menuItemNameText, {color: Colors.app_background_color}]}>
                        {props.text}
                    </AppText>}
                    {!isctive && <AppText style={[styles.menuItemNameText, {color: Colors.primary_color}]}>
                        {props.text}
                    </AppText>}
                </View>
            </TouchableWithoutFeedback>
        );
    };


    return (
        <MyBackgroundImage>
            <ScrollView style={styles.scrollViewContainer}>
                <View style={styles.scrollViewInnerContainer}>
                    <View style={{backgroundColor: Colors.primary_color}}>
                        <TouchableOpacity onPress={changeProfileImage} style={styles.profileImageContainer}>
                            {userData.profile_pic !== undefined &&
                            <MyImage source={{uri: userData.profile_pic}}
                                // source={require('../Asset/vendor.png')}
                                     imageContainerStyle={styles.profileImage}/>}
                        </TouchableOpacity>
                        <AppText style={styles.userNameText}>
                            {userName}
                        </AppText>
                        <AppText style={[styles.emailText]}>
                            {userEmail}
                        </AppText>
                    </View>
                    <View style={styles.menuItemsContainer}>

                        <MenuItem onPress={profilePressed} image={require('../Asset/user_profile.png')}
                                  text={'My Profile'}/>
                        <MenuItem onPress={changePassword} image={require('../Asset/change_password.png')}
                                  text={'Change Password'}/>
                        <MenuItem onPress={servicesPressed} image={require('../Asset/staff_orders.png')}
                                  text={'Orders'}/>
                        <MenuItem onPress={bookingsPressed} image={require('../Asset/staff_processing.png')}
                                  text={'Processing'}/>
                        {/*<MenuItem onPress={referralPressed} image={require('../Asset/my_referals.png')} text={'My referrals'}/>*/}
                        <MenuItem onPress={contactUsPressed} image={require('../Asset/contact_us.png')}
                                  text={'Contact us'}/>

                        <MenuItem onPress={logoutPressed} image={require('../Asset/logout.png')} text={'Log out'}/>


                    </View>
                </View>
            </ScrollView>
            {isloading && <Loader/>}
        </MyBackgroundImage>
    );

};

const styles = StyleSheet.create({
    scrollViewContainer: {flex: 1, marginBottom: dimen.bottom_tab_height},
    scrollViewInnerContainer: {flex: 1, justifyContent: 'flex-start', alignItems: 'stretch'},

    profileImageContainer: {alignSelf: 'center', overflow: 'hidden', borderRadius: 50, width: 100, height: 100},
    profileImage: {
        width: 100,
        height: 100,
        overflow: 'hidden',
        borderRadius: 50,
        backgroundColor: Colors.image_background_color,
        alignSelf: 'center',
        // borderWidth: 1,
        // borderColor:Colors.app_background_color,
    },
    userNameText: {
        alignSelf: 'center',
        color: Colors.app_background_color,
        fontSize: 14,
        marginTop: dimen.home_item_padding,
    },
    emailText: {
        alignSelf: 'center',
        color: Colors.app_background_color,
        fontSize: 11,
        marginBottom: dimen.home_item_padding,
    },

    menuItemContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingLeft: dimen.app_padding,
        paddingRight: dimen.app_padding,
        paddingBottom: 10,
        paddingTop: 10,
    },
    menuItemsContainer: {
        paddingTop: dimen.app_padding,
        paddingBottom: dimen.app_padding,
    },
    menuItemImageContainer: {
        width: 22,
        height: 22,
        overflow: 'hidden',
        alignSelf: 'center',
    },
    menuItemNameText: {
        alignSelf: 'center',
        color: Colors.primary_color,
        fontSize: 18,
        marginLeft: 10,
    },

});
