import React from 'react';
import {
    StyleSheet,
    Text,
    View,
} from 'react-native';

import MyCarousel from '../Components/Carousel';
import dimen from '../Styles/Dimen';
import Colors from '../Styles/Colors';

const IntroSlides =()=>{
    {
        return (
            <View style={styles.mainContainer}>
                <View style={styles.welcomeContainer}>
                    <Text style={styles.welcomeText}>
                        Welcome to Insto App.
                    </Text>
                </View>
                <View style={styles.carousalContainer}>
                    <MyCarousel/>
                </View>

                <View style={styles.getStartedContainer}>
                    <Text style={styles.getStartedText}>
                        Get Started
                    </Text>
                </View>
            </View>
        );
    }

}
const styles= StyleSheet.create({
    mainContainer:{
        flex: 1,
        justifyContent:'flex-start',
        alignItems:'stretch',

    },
    welcomeContainer:{
        alignItems: 'center',
        marginTop:dimen.app_padding,
    },
    welcomeText:{
       fontSize:18,
        // fontStyle:'bold'
    },
    getStartedContainer:{
        alignItems: 'center',
        marginBottom:dimen.app_padding,
        marginLeft:dimen.app_padding,
        marginRight:dimen.app_padding,
    },
    getStartedText:{

        width:'100%',
        textAlign:'center',

        padding: dimen.app_padding,
        backgroundColor: Colors.primary_color,
        borderRadius: 7
    },
    carousalContainer:{

        flex: 1,
        // backgroundColor:'green',
        margin: dimen.app_padding,
        padding:dimen.app_padding,
        paddingBottom:0

    }
})

export default IntroSlides;

