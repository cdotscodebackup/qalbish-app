import React, {useState, useEffect} from 'react';
import {
    ActivityIndicator, Alert,
    Image,
    StatusBar, TouchableOpacity, Text,
    View,
} from 'react-native';
import {console_log, getObject} from '../Classes/auth';
import {data_key, token_key, user_key} from '../Classes/UrlConstant';
import Colors from "../Styles/Colors";


const Authentication = ({navigation}) => {

    useEffect(() => {
        // navigation.navigate('CustomerDashboard')
        // return;

        getObject(token_key)
            .then((res) => {
                // console_log('token: '+res);
                if (!res) {
                    navigation.navigate('AuthStack')
                } else {
                    getObject(data_key)
                        .then((res) => {
                            // console_log(res)
                            if (res.user_type === 'customer') {
                                navigation.navigate('CustomerDashboard')
                            } else if (res.user_type === 'staff') {
                                navigation.navigate('StaffDashboard')
                            } else {
                                navigation.navigate('AuthStack')
                            }
                        })
                        .catch((err) => {
                            navigation.navigate('AuthStack')
                        })
                }
            })
            .catch(() => {
                navigation.navigate('AuthStack')
            });
    });

    return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center',backgroundColor:Colors.app_background_color}}>
            {/*<Image source={require('../Asset/bg.png')}*/}
            {/*       style={{flex: 1, position: 'absolute', resizeMode: 'repeat'}}/>*/}
            <ActivityIndicator/>
            <StatusBar barStyle="default"/>
        </View>
    );
};

export default Authentication;
