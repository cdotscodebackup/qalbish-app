import React, {useState, useEffect} from 'react';
import {
    Alert,
    Image,
    KeyboardAvoidingView,
    StyleSheet, SafeAreaView,
    Text, SectionList,
    Platform,
    TouchableOpacity, ScrollView,
    StatusBar,
    View,
} from 'react-native';
import {FlatList, TextInput} from 'react-native-gesture-handler';
import TextStyles from '../Styles/TextStyles';
import dimen from '../Styles/Dimen';
import {
    _storeData, console_log, successAlert,
} from '../Classes/auth';

import {
    web_view_url,
} from '../Classes/UrlConstant';
import {Loader} from '../Components/Loader';

import NetInfo from '@react-native-community/netinfo';
import Colors from '../Styles/Colors';
import AppText from '../Components/AppText';
import {CategoriesCard} from '../Components/Categories';
import MyImageButton, {MyImage} from '../Components/MyImageButton';
import MyBackgroundImage from '../Components/MyBackgroundImage';
import MyStrings from '../Classes/MyStrings';
import MyCarousel from '../Components/Carousel';
import BannerHome from '../Components/BannerHome';
import {ProductsCard} from '../Components/ProductsCard';
import {OffersCard} from '../Components/OffersCard';
import SearchField from '../Components/SearchComponent';
import TopBar from '../Components/TopBar';
import {ProductOffersCard} from '../Components/ProductOffersCard';
import App from '../../App';
import {FeatureCard, FeatureCardProductOffers} from '../Components/FeatureCard';


export const ProductOffers = ({navigation}) => {

    const [categories, setCategories] = useState([{'id': 1},{'id': 2}]);
    const [isloading, setIsLoading] = useState(false);
    const [add, setAdd] = useState('Instalment.pk add here');
    const [email, setEmail] = useState('');


    useEffect(() => {
        setAdd('Instalment.pk new add here');
    });

    const subCatPress = (item) => {
        navigation.navigate('ProductListS');
    };
    const locationButtonPressed = (item) => {
        // navigation.navigate('ProductListS');
    };
    const goback = () => {
        navigation.goBack();
    };
    const renderSubCat = (cat) => {
        const txtContainer = cat.map(
            (i, j) =>
                <FeatureCardProductOffers subCatPress={subCatPress} data={i}/>,
        );
        return (
            <View style={styles.subCategoriesMainContainer}>
                <View style={styles.featureContainer}>
                    {txtContainer}
                </View>
            </View>
        );

    };


    const ProductListHeader = () => {
        return (
            <View style={styles.productDetailsContainer}>
                <View style={styles.imageContainerProductDetail}>
                    <MyImage source={require('../Asset/car.png')}
                             imageContainerStyle={styles.productImage}/>
                </View>
                <View style={styles.productDetailsTextContainer}>
                    <AppText style={styles.productName}>
                        Microsoft surface Book 2 15 i7 Quad Core 8th Generation
                    </AppText>
                    <View style={styles.feacturesContainer}>
                        <AppText style={styles.featureHeaderTextStyle}>Features</AppText>
                        {renderSubCat(categories)}
                    </View>
                </View>

            </View>
        );
    };


    const FlatListHeader = () => {
        return (
            <View style={{flex: 1}}>
                {banner()}
                <View style={styles.subCategoriesMainContainer}>
                    <View style={styles.categoryHeader}>
                        <View style={styles.headerStartStyle}/>
                        <AppText style={styles.headerTextStyle}>
                            Laptops
                        </AppText>
                        <View style={styles.filterIconContainer}>
                            <MyImageButton onPress={locationButtonPressed}
                                           source={require('../Asset/filter.png')}
                                           imageContainerStyle={styles.filterIconStyle}/>
                            <MyImageButton onPress={locationButtonPressed}
                                           source={require('../Asset/sort.png')}
                                           imageContainerStyle={styles.filterIconStyle}/>

                        </View>
                    </View>
                </View>
            </View>
        );
    };


    return (
        <MyBackgroundImage>
            <TopBar title={'Offers'}/>
            <SearchField/>
            <ScrollView style={styles.scrollViewContainer}>
                <View style={styles.scrollViewInnerContainer}>

                    <ProductListHeader/>


                    <FlatList
                        style={styles.productListStyle}
                        data={categories}
                        keyExtractor={(item, index) => item.id.toString()}
                        // contentContainerStyle={{margin:dimen.app_padding}}
                        numColumns={1}
                        bounces={false}
                        renderItem={({item}) => (<ProductOffersCard subCatPress={subCatPress} data={item}/>)}
                        // ListHeaderComponent={() => {return (<FlatListHeader/>)}}
                    />

                </View>
            </ScrollView>


        </MyBackgroundImage>
    );

};

const styles = StyleSheet.create({
    dashboardButtonText: {fontFamily: 'roboto-bold', fontSize: 16},
    dashboardButton: {flex: 1, padding: 8, justifyContent: 'center', alignItems: 'center'},
    dashboardButtonContainer: {
        borderWidth: 1,
        borderColor: Colors.border_color,
        borderRadius: dimen.border_radius,
        overflow: 'hidden',
        backgroundColor: Colors.home_widget_background_color,
        margin: dimen.app_padding,
        justifyContent: 'space-evenly',
        alignItems: 'flex-start',
        flexDirection: 'row',

    },
    bannerAdStyle: {
        height: 90, backgroundColor: Colors.primary_color,
        padding: 20, margin: dimen.app_padding,
        justifyContent: 'center',
    },
    flatlistStyle: {
        // width: '100%',
        // backgroundColor: Colors.home_widget_background_color,
        // marginBottom: dimen.bottom_margin_for_bottom_menu + 20,
        padding: dimen.app_padding,
    },
    productFlatList: {
        width: '100%',
        backgroundColor: Colors.primary_color,
        // marginBottom: dimen.bottom_margin_for_bottom_menu + 20,
        // padding: dimen.app_padding,
    },
    flatlistContainerStyle: {
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        backgroundColor: Colors.primary_dark_color,
    },
    emptyComponentStyle: {
        justifyContent: 'flex-start',
        alignItems: 'center',
        // backgroundColor:Colors.primary_dark_color
    },


    //banner
    carousalContainer: {
        height: 130,
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: Colors.home_widget_background_color,
        // margin: dimen.app_padding,
        // padding:dimen.app_padding,
        // paddingBottom:0

    },
    scrollViewContainer: {flex: 1, marginBottom: dimen.bottom_tab_height},
    scrollViewInnerContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'stretch',
    },
    productListStyle: {
        flex: 1,
        padding: dimen.app_padding - dimen.home_item_padding,
    },

    //product details
    productDetailsContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        padding: dimen.app_padding,
    },
    imageContainerProductDetail: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 5,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: Colors.border_color,
        backgroundColor: Colors.home_widget_background_color,
    },
    productName: {
        fontSize: 16,
        fontWeight: 'bold',
        color: Colors.primary_color,
        textAlign: 'center',
    },
    productDetailsTextContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexGrow: 1,
        padding: dimen.app_padding,
    },


    headerTextStyle: {
        fontSize: 16,
        marginLeft: 15,
        color: Colors.button_text_color,
        textAlign: 'center',
    },
    filterIconContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 15,
    },
    filterIconStyle: {
        width: 20,
        height: 20,
        marginLeft: 10,
    },
    headerStartStyle: {
        width: 8,
        borderTopRightRadius: 4,
        borderBottomRightRadius: 4,
        position: 'absolute',
        height: '100%',
        left: 0,
    },
    subCategoryContainer: {
        flexDirection: 'row',
        marginTop: 20,
        marginBottom: 20,
        marginLeft: 10,
        marginRight: 10,
        flexWrap: 'wrap',
        alignContent: 'flex-start',
    },


    //product header
    productHeaderContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        padding: 12,

    },
    productImageContainer: {
        height: 50,
        width: 50,


    },
    productImage: {
        height: 140,
        width: 140,
    },
    subCategoriesMainContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },
    featureContainer: {
        flexDirection: 'row',
        // marginTop: 20,
        // marginBottom: 20,
        marginLeft: 10,
        marginRight: 10,
        flexWrap: 'wrap',
        alignContent: 'flex-start',
    },
    featureHeaderTextStyle: {
        fontSize: 11,
        color: Colors.primary_color,
        fontWeight: 'bold',
        alignSelf: 'center',
        textAlign: 'center',
        marginBottom: 0,
        marginTop: 3,
    },
    feacturesContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        borderWidth: 1,
        borderColor: Colors.light_border_color,
        borderRadius: 5,
        margin: dimen.small_padding,
    },



});


