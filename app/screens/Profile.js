import React, {useState, useEffect} from 'react';
import {
    Alert,
    Image,
    KeyboardAvoidingView,
    StyleSheet, SafeAreaView,
    Text, SectionList,
    Platform,
    TouchableOpacity, ScrollView,
    StatusBar, TouchableHighlight, TouchableWithoutFeedback,
    View,
} from 'react-native';
import dimen from '../Styles/Dimen';
import {
    clearAllData,
    console_log,
    errorAlert, getDataWithoutToken,
    getDataWithToken, getObject,
    isNetConnected, saveObject,
} from '../Classes/auth';

import {
    area_url,
    cities_url,
    data_key, general_network_error,
    login_url, logout_url, referal_key, referral_discount_key, register_url, token_key, update_profile_url,
    web_view_url,
} from '../Classes/UrlConstant';
import {Loader} from '../Components/Loader';

import Colors from '../Styles/Colors';
import AppText from '../Components/AppText';
import MyImageButton, {MyImage} from '../Components/MyImageButton';
import MyBackgroundImage from '../Components/MyBackgroundImage';
import TopBar from '../Components/TopBar';
import {TextInput} from "react-native-gesture-handler";
import MyStrings from '../Classes/MyStrings';
import {DropDown} from '../Components/DropDown';
import MyTextButton from '../Components/MyTextButton';
import {InputFieldStyle} from '../Styles/TextStyles';

export const Profile = ({navigation}) => {

    const [isLoading, setIsLoading] = useState(false);
    const [userData, setUserData] = useState({});


    const [fname, setFname] = useState('');
    const [lname, setLname] = useState('');
    const [email, setEmail] = useState('');
    const [gender, setGender] = useState('');
    const [age, setAge] = useState('');

    const [phone, setPhone] = useState('');
    const [cnic, setCnic] = useState('');
    const [password, setPassword] = useState('');
    const [cPassword, setCPassword] = useState('');
    const [address, setAddress] = useState('');

    const [city, setCity] = useState('');

    const [area, setArea] = useState('');



    const parseError=(error)=>{
        var m = error;
        var keys = [];
        for (var k in m) keys.push(k);

        var mforuser = '';
        // console.log('key 0 ' + (m[keys[0]]))

        for (var k in keys)
            mforuser = mforuser + m[keys[k]] + '\n'

        return mforuser;
    }

    const editProfile = () => {
        if (email=== '' ) {
            errorAlert('Email is required.');
            return;
        }

        if (phone=== '' ) {
            errorAlert('Phone number is required.');
            return;
        }
        if (phone.length!== 11) {
            errorAlert('Phone number must be of 11 characters.');
            return;
        }

        if (address=== '' ) {
            errorAlert('Address is required.');
            return;
        }


        isNetConnected()
            .then(() => {
                setIsLoading(true);
                let formData = {
                    // 'first_name': fname,
                    // 'last_name': lname,
                    // 'city_id': selectedCity.id,
                    // 'area_id': selectedArea.id,
                    // 'age': selectedAge.title,
                    'phone_number': phone,
                    // 'cnic': cnic,
                    // 'gender': selectedGender.id,
                    'address': address,
                    'email': email,
                    // 'password': password,
                    // 'password_confirmation': cPassword,
                    // 'referral_code': referalCode,
                };
                getDataWithToken(update_profile_url, 'POST', formData)
                    .then((res) => {
                        setIsLoading(false);
                        // let res = JSON.parse(response);
                        console_log(res)
                        if (res.message){
                            saveObject(data_key, res.data).then(()=>{}).catch(()=>{})
                            Alert.alert('Success',res.message,
                                [
                                    // {
                                    //     text: 'Cancel',
                                    //     onPress: () => console.log('Cancel Pressed'),
                                    //     style: 'cancel',
                                    // },
                                    {text: 'Okay',onPress: () => navigation.pop()},
                                ],
                                {cancelable: false},)
                        } else{
                            console_log(res)
                            errorAlert(parseError(res.error))
                        }


                    })
                    .catch((err) => {
                        setIsLoading(false);
                        console_log('error: ' + err);
                        // errorAlert('Wrongng Credentials')
                    });
            })
            .catch((err) => {
                errorAlert(general_network_error);
            });
    };



    useEffect(() => {
        updateProfileFromLocal();
    }, []);
    const updateProfileFromLocal=()=>{
        getObject(data_key)
            .then((res) => {
                console_log(res);
                setUserData(res);
                setFname(res.first_name)
                setLname(res.last_name)
                setEmail(res.email)
                setGender(res.gender)
                setAge(res.age)
                setPhone(res.phone_number)
                setCnic(res.cnic)
                setAddress(res.address)
                setCity(res.city_name)
                setArea(res.area_name)

            })
            .catch((err) => {
                console_log(err);
            });
    }

    return (
        <MyBackgroundImage>
            <TopBar title={'Profile'} backButton={() => navigation.pop()}/>
            <View style={{flex:1,}}>
            <ScrollView style={styles.scrollViewContainer}>
                <View style={styles.scrollViewInnerContainer}>

                    {false && <View style={{backgroundColor: Colors.app_background_color}}>
                        <View style={{height:70,backgroundColor:Colors.primary_color}}></View>
                        <View style={{height:70,backgroundColor:Colors.primary_color}}>
                        </View>

                        <View style={{position:'absolute',height:140,elevation:30, top:35,borderRadius:dimen.border_radius, justifyContent:'flex-end', alignItems:'center',paddingBottom:dimen.app_padding*2,
                            backgroundColor:Colors.app_background_color,left:dimen.app_padding , right:dimen.app_padding}}>
                         <AppText style={styles.userNameText}>
                            {userData.name}
                        </AppText>
                        <AppText style={[styles.emailText]}>
                            {userData.email}
                        </AppText>
                        </View>

                        <View style={[styles.profileImageContainer,{position:'absolute',elevation:40,top:5,backgroundColor:Colors.app_background_color}]}>
                            <MyImage //source={{uri: userData.profile_pic}}
                                source={require('../Asset/user_icon.png')}
                                imageContainerStyle={styles.profileImage}/>
                        </View>
                    </View>}

                    <View style={styles.inputContainer}>

                        <View style={styles.emailOrPhoneContainer}>
                            <TextInput onChangeText={(email) => setFname(email)}
                                       value={fname}
                                       editable={false}
                                       placeholder={MyStrings.firstname}
                                       autoCapitalize='none'
                                       style={InputFieldStyle.inputField}/>
                        </View>

                        <View style={[styles.emailOrPhoneContainer, {marginTop: 10}]}>
                            <TextInput onChangeText={(email) => setLname(email)}
                                       value={lname}
                                       editable={false}
                                       placeholder={MyStrings.lastname}
                                       autoCapitalize='none'
                                       style={InputFieldStyle.inputField}/>
                        </View>

                        <View style={[styles.emailOrPhoneContainer, {marginTop: 10}]}>
                            <TextInput onChangeText={(email) => setEmail(email)}
                                       value={email}
                                       editable={true}
                                       placeholder={MyStrings.email}
                                       keyboardType={'email-address'}
                                       autoCapitalize='none'
                                       style={InputFieldStyle.inputField}/>
                        </View>

                        <View style={[styles.emailOrPhoneContainer, {marginTop: 10}]}>
                            <AppText style={styles.dropDownText}>{gender}</AppText>
                        </View>

                        <View style={[styles.emailOrPhoneContainer, {marginTop: 10}]}>
                            <AppText style={styles.dropDownText} >{age}</AppText>
                        </View>

                        <View style={[styles.emailOrPhoneContainer, {marginTop: 10}]}>
                            <TextInput onChangeText={(email) => setPhone(email)}
                                       value={phone}
                                       placeholder={MyStrings.mobile_number}
                                       autoCapitalize='none'
                                       maxLength={11}
                                       keyboardType={'phone-pad'}
                                       style={InputFieldStyle.inputField}/>
                        </View>

                        <View style={[styles.emailOrPhoneContainer, {marginTop: 10}]}>
                            <TextInput onChangeText={(email) => setCnic(email)}
                                       value={cnic}
                                       editable={false}
                                       placeholder={MyStrings.cnic}
                                       autoCapitalize='none'
                                       keyboardType={'numeric'}
                                       style={InputFieldStyle.inputField}/>
                        </View>

                        <View style={[styles.emailOrPhoneContainer, {marginTop: 10}]}>
                            <TextInput onChangeText={(email) => setAddress(email)}
                                       value={address}
                                       editable={true}
                                       placeholder={MyStrings.address}
                                       autoCapitalize='none'
                                       style={InputFieldStyle.inputField}/>
                        </View>

                        <View style={[styles.emailOrPhoneContainer, {marginTop: 10}]}>
                            <AppText style={styles.dropDownText}>{city}</AppText>
                        </View>

                        <View style={[styles.emailOrPhoneContainer, {marginTop: 10}]}>
                            <AppText style={styles.dropDownText}>{area}</AppText>
                        </View>

                        <MyTextButton onPress={editProfile}
                                      buttonText={'Save Changes'}
                                      buttonContainerStyle={[styles.loginButton, {marginTop: 30}]}/>

                    </View>



                </View>
            </ScrollView>
            {isLoading && <Loader/>}
            </View>
        </MyBackgroundImage>
    );

};

const styles = StyleSheet.create({
    scrollViewContainer: {flex: 1, },
    scrollViewInnerContainer: {flex: 1, justifyContent: 'flex-start', alignItems: 'stretch'},

    profileImageContainer: {alignSelf:'center',overflow: 'hidden',borderRadius: 35, width: 70, height: 70},
    profileImage: {
        width: 60,
        height: 60,
        overflow: 'hidden',
        borderRadius: 50,
        alignSelf: 'center',
        borderWidth: 1,
    },
    userNameText: {
        alignSelf: 'center',
        color: Colors.primary_color,
        fontSize: 16,
        marginTop: dimen.home_item_padding,
    },
    emailText: {
        alignSelf: 'center',
        color: Colors.normal_text_color,
        fontSize: 14,
        marginBottom: dimen.home_item_padding,
    },

    menuItemContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingLeft: dimen.app_padding,
        paddingRight: dimen.app_padding,
        paddingBottom: 10,
        paddingTop: 10,
    },
    menuItemsContainer: {
        paddingTop: dimen.app_padding,
        paddingBottom: dimen.app_padding,
    },
    menuItemImageContainer: {
        width: 22,
        height: 22,
        overflow: 'hidden',
        alignSelf: 'center',
    },
    menuItemNameText: {
        alignSelf: 'center',
        color: Colors.primary_color,
        fontSize: 18,
        marginLeft: 10,
    },


    inputContainer: {
        margin:dimen.app_padding,
        // marginTop: 60,
        flexGrow: 1,
        justifyContent:'center',


    },
    emailOrPhoneContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        borderColor: Colors.primary_color,
        backgroundColor: Colors.app_background_color,
        borderWidth: 1,
        // borderRadius: dimen.login_border_radius,
        paddingRight: dimen.app_padding,
        paddingLeft: dimen.app_padding,



    },
    dropDownText:{
        fontSize:13,
        marginTop:dimen.app_padding,
        marginBottom :dimen.app_padding,
        color:Colors.not_active_text_color,
        marginLeft: dimen.app_padding
    },
    inputField:{
        flexGrow: 1,
    }



});


