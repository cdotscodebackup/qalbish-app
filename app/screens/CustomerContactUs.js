import React, {useState, useEffect} from 'react';
import {
    Alert,
    Image,
    KeyboardAvoidingView,
    StyleSheet, SafeAreaView,
    Text, SectionList,
    Platform,
    TouchableOpacity, ScrollView,
    StatusBar,
    View, Animated, Share, Linking,
} from 'react-native';
import {FlatList, TextInput} from 'react-native-gesture-handler';
import TextStyles from '../Styles/TextStyles';
import dimen from '../Styles/Dimen';
import {
    _storeData, console_log, saveObject, successAlert, getObject, errorAlert,
} from '../Classes/auth';

import {
    data_key,
    referal_key, referral_discount_key, settings_key,
    web_view_url,
} from '../Classes/UrlConstant';
import Colors from '../Styles/Colors';
import AppText from '../Components/AppText';
import MyImageButton, {MyImage} from '../Components/MyImageButton';
import MyBackgroundImage from '../Components/MyBackgroundImage';

import TopBar from '../Components/TopBar';
import MyTextButton from '../Components/MyTextButton';


export const CustomerContactUs = ({navigation}) => {


    const [phone, setPhone] = useState('');
    const [email, setEmail] = useState('');
    const [whatsapp, setWhatsapp] = useState('');

    useEffect(() => {
        getObject(settings_key)
            .then((res) => {
                console_log(res);

                res.forEach((item) => {
                    if (item.name === 'Helpline Support') {
                        setPhone(item.value);
                    }
                    if (item.name === 'Email Support') {
                        setEmail(item.value);
                    }
                    if (item.name === 'Whatsapp') {
                        setWhatsapp(item.value);
                    }
                });
            })
            .catch((err) => {
                console_log(err);
            });
    }, []);


    const makeCall = () => {
        if (phone !== '') {
            Linking.canOpenURL('tel:' + phone)
                .then(res => {
                    console_log(res)

                    Linking.openURL('tel:' + phone)
                        .then(res => console_log(res))
                        .catch(err => console_log(err));
                })
                .catch(err => {
                    console_log(err);
                });

        }
    };
    const makeEmail = () => {
        if (email !== '') {
            Linking.canOpenURL('tel:' + phone)
                .then(res => {
                    console_log(res)
                    Linking.openURL('mailto:' + email)
                        .then(res => console_log(res))
                        .catch(err => console_log(err));
                })
                .catch(err => {
                    console_log(err);
                });
        }
    };
    const messageWhatsapp = () => {
        if (whatsapp !== '') {
            console_log(whatsapp);
        }
        Linking.openURL(whatsapp).then((res) => {
            console_log(res);
        }).catch((err) => {
            console_log(err);
        });
    };

    return (
        <MyBackgroundImage>
            <TopBar title={'Contact Us'} backButton={() => navigation.pop()}/>
            <View style={styles.mainContainer}>
                <ScrollView>
                    <View>
                        <View style={styles.headerImageContainer}>
                            <MyImage source={require('../Asset/logo.png')}
                                     imageContainerStyle={styles.imageStyle}/>
                        </View>
                        {/*<AppText style={styles.serviceNameHeading}>*/}
                        {/*    Contact us on the our contact details given below.*/}
                        {/*</AppText>*/}

                        <TouchableOpacity onPress={makeCall} style={styles.callButtonContainer}>
                            <MyImage source={require('../Asset/call_contact_us.png')}
                                     imageContainerStyle={styles.callImageStyle}/>
                            <AppText style={styles.callTextStyle}>{phone}</AppText>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={makeEmail} style={[styles.callButtonContainer, {marginTop: 0}]}>
                            <MyImage source={require('../Asset/email_contact_us.png')}
                                     imageContainerStyle={styles.callImageStyle}/>
                            <AppText style={styles.callTextStyle}>{email}</AppText>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={messageWhatsapp}
                                          style={[styles.callButtonContainer, {marginTop: 0}]}>
                            <MyImage source={require('../Asset/whatsapp_icon.png')}
                                     imageContainerStyle={styles.callImageStyle}/>
                            <AppText style={styles.callTextStyle}>Whatsapp</AppText>
                        </TouchableOpacity>

                    </View>
                </ScrollView>
            </View>

        </MyBackgroundImage>
    );

};

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'stretch',
    },
    headerImageContainer: {
        height: 150,
        width: '100%',
        marginTop: 10,
    },
    imageStyle: {
        resizeMode: 'contain',
    },
    callImageStyle: {
        resizeMode: 'contain',
        width: 25,
        height: 25,
    },
    serviceNameHeading: {
        fontSize: 16,
        color: Colors.normal_text_color,
        margin: dimen.app_padding,
    },
    referCodeText: {
        fontSize: 24,
        color: Colors.primary_color,
        textAlign: 'center',
    },
    referCodeContainer: {
        marginLeft: dimen.app_padding * 2,
        marginRight: dimen.app_padding * 2,
        marginTop: dimen.app_padding,
        marginBottom: dimen.app_padding,
        borderWidth: 1,
        borderColor: Colors.primary_color,
        paddingLeft: dimen.app_padding,
        paddingRight: dimen.app_padding,
    },
    callButtonContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.primary_color,
        margin: dimen.app_padding,
    },
    callTextStyle: {
        color: Colors.app_background_color,
        fontSize: 14,
        // fontWeight:'bold',
        margin: dimen.app_padding,

    },

});

