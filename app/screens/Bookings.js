import React, {useState, useEffect} from 'react';
import {
    Alert,
    Image,
    KeyboardAvoidingView,
    StyleSheet, SafeAreaView,
    Text, SectionList,
    Platform,
    TouchableOpacity, ScrollView, RefreshControl,
    StatusBar,
    View,
} from 'react-native';
import {FlatList, TextInput} from 'react-native-gesture-handler';
import TextStyles from '../Styles/TextStyles';
import dimen from '../Styles/Dimen';
import {
    _storeData, console_log, errorAlert, getDataWithToken, getObject, isNetConnected, saveObject, successAlert,
} from '../Classes/auth';

import {
    general_network_error, orders_url,
    services_url,
    web_view_url,
} from '../Classes/UrlConstant';
import Colors from '../Styles/Colors';
import AppText from '../Components/AppText';
import MyBackgroundImage from '../Components/MyBackgroundImage';
import TopBar from '../Components/TopBar';
import {OrderItemCard} from '../Components/OrderItemCard';
import {EmptyList} from '../Components/Loader';

export const Bookings = ({navigation}) => {

    const [categories, setCategories] = useState([{'id': 1},
        {'id': 2}, {'id': 3}]);
    const [isLoading, setIsLoading] = useState(false);
    const [ordersList, setOrderList] = useState([]);
    const [filteredOrdersList, setfilteredOrderList] = useState([]);
    const [newOrderSelected, setNewOrderSelected] = useState(true);

    useEffect(() => {
        getAllOrders();
    }, []);

    //for updating user information if updated
    useEffect(() => {
        let lis = navigation.addListener(
            'willFocus',
            () => {

                getAllOrders();
                // console_log('in listener'+newOrderSelected)

            });

        return function cleanup() {
            lis.remove();
        };
    }, []);
    const newOrdersPressed = () => {
        setNewOrderSelected(true);
        setfilteredOrderList(ordersList.filter((item) => item.order_status !== 'completed'));
        saveObject('selectedButton','new order').then().catch()

    };
    const oldOrdersPressed = () => {
        setNewOrderSelected(false);
        setfilteredOrderList(ordersList.filter((item) => item.order_status === 'completed'));
        saveObject('selectedButton','old order').then().catch()

    };
    const orderPressed = (item) => {
        navigation.navigate('OrderDetails', {'order': item});

    };


    const getAllOrders = () => {
        // console_log(newOrderSelected)
        let newButton='';
        getObject('selectedButton').then(res=>{
            console_log(res)
            if (!res){
                newButton= 'new order';
                setNewOrderSelected(true)
            }else{
                if (res==='new order'){
                    newButton= 'new order';
                    setNewOrderSelected(true)

                } else{
                    newButton= 'old order';
                    setNewOrderSelected(false)

                }
            }
        })
        // console_log(newButton)

        isNetConnected()
            .then(() => {
                setIsLoading(true);
                getDataWithToken(orders_url, 'GET', {})
                    .then((response) => {
                        setIsLoading(false);
                        setOrderList(response.data);
                        // if (newOrderSelected)
                        if (newButton==='new order')
                            setfilteredOrderList(response.data.filter((item) => item.order_status !== 'completed'));
                        else
                            setfilteredOrderList(response.data.filter((item) => item.order_status === 'completed'));
                    })
                    .catch((err) => {
                        setIsLoading(false);
                        console_log(err);

                    });
            })
            .catch((err) => {
                errorAlert(general_network_error);
            });
    };


    return (
        <MyBackgroundImage>
            <TopBar title={'Bookings'}   GoToCart={() => {
                navigation.navigate('CartDetails')
            }}/>
            <View style={{
                margin: dimen.app_padding,
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'flex-start',
            }}>

                <TouchableOpacity
                    style={[TextStyles.buttonStyle, {
                        paddingTop: 5,
                        paddingBottom: 5
                    }, newOrderSelected ? styles.selectedButtonStyle : styles.unSelectedButtonStyle, {marginRight: 5}]}
                    onPress={newOrdersPressed}>
                    {newOrderSelected && <AppText style={{fontSize: 15, color: Colors.primary_color}}>New</AppText>}
                    {!newOrderSelected &&
                    <AppText style={{fontSize: 15, color: Colors.app_background_color}}>New</AppText>}
                </TouchableOpacity>

                <TouchableOpacity
                    style={[TextStyles.buttonStyle, {
                        paddingTop: 5,
                        paddingBottom: 5
                    }, !newOrderSelected ? styles.selectedButtonStyle : styles.unSelectedButtonStyle, {marginRight: 5}]}
                    onPress={oldOrdersPressed}>
                    {!newOrderSelected &&
                    <AppText style={{fontSize: 15, color: Colors.primary_color}}>History</AppText>}
                    {newOrderSelected &&
                    <AppText style={{fontSize: 15, color: Colors.app_background_color}}>History</AppText>}
                </TouchableOpacity>

                {/*<MyTextButton onPress={newOrdersPressed}*/}
                {/*buttonText={'New'}*/}
                {/*buttonTextStyle={newOrderSelected?{color:Colors.primary_color}:{}}*/}
                {/*buttonContainerStyle={[newOrderSelected?styles.selectedButtonStyle:styles.unSelectedButtonStyle,{marginRight:5,}]}/>*/}
                {/*<MyTextButton onPress={oldOrdersPressed}*/}
                {/*buttonText={'History'}*/}
                {/*buttonContainerStyle={[!newOrderSelected?styles.selectedButtonStyle:styles.unSelectedButtonStyle,{marginLeft:5,}]}/>*/}
            </View>

            <View style={styles.scrollViewInnerContainer}>
                <FlatList
                    style={styles.productListStyle}
                    data={filteredOrdersList}
                    keyExtractor={(item, index) => item.id.toString()}
                    // contentContainerStyle={{margin:dimen.app_padding}}
                    numColumns={1}
                    bounces={false}
                    renderItem={({item}) => (
                        <OrderItemCard productOffersPress={() => orderPressed(item)} data={item}/>)}
                    // ListHeaderComponent={() => {return (<FlatListHeader/>)}}
                    ListEmptyComponent={<EmptyList reload={getAllOrders}/>}

                    refreshControl={
                        <RefreshControl
                            refreshing={isLoading}
                            onRefresh={getAllOrders}
                        />
                    }
                />
            </View>
            {/*{isLoading && <Loader />}*/}
        </MyBackgroundImage>
    );

};

const styles = StyleSheet.create({
    dashboardButtonText: {fontFamily: 'roboto-bold', fontSize: 16},
    dashboardButton: {flex: 1, padding: 8, justifyContent: 'center', alignItems: 'center'},
    dashboardButtonContainer: {
        borderWidth: 1,
        borderColor: Colors.border_color,
        borderRadius: dimen.border_radius,
        overflow: 'hidden',
        backgroundColor: Colors.home_widget_background_color,
        margin: dimen.app_padding,
        justifyContent: 'space-evenly',
        alignItems: 'flex-start',
        flexDirection: 'row',

    },
    bannerAdStyle: {
        height: 90, backgroundColor: Colors.primary_color,
        padding: 20, margin: dimen.app_padding,
        justifyContent: 'center',
    },
    flatlistStyle: {
        // width: '100%',
        // backgroundColor: Colors.home_widget_background_color,
        // marginBottom: dimen.bottom_margin_for_bottom_menu + 20,
        padding: dimen.app_padding,
    },
    productFlatList: {
        width: '100%',
        backgroundColor: Colors.primary_color,
        // marginBottom: dimen.bottom_margin_for_bottom_menu + 20,
        // padding: dimen.app_padding,
    },
    flatlistContainerStyle: {
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        backgroundColor: Colors.primary_dark_color,
    },
    emptyComponentStyle: {
        justifyContent: 'flex-start',
        alignItems: 'center',
        // backgroundColor:Colors.primary_dark_color
    },


    //banner
    carousalContainer: {
        height: 130,
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: Colors.home_widget_background_color,
        // margin: dimen.app_padding,
        // padding:dimen.app_padding,
        // paddingBottom:0

    },

    scrollViewContainer: {flex: 1, marginBottom: dimen.bottom_tab_height},
    scrollViewInnerContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        marginBottom: dimen.bottom_tab_height,
    },
    productListStyle: {
        flex: 1,
        padding: dimen.app_padding - dimen.home_item_padding,
        marginBottom: 10,
    },

    //sub categories
    subCategoriesMainContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'stretch',
    },
    categoryHeader: {
        height: 40,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.primary_color,

    },
    headerTextStyle: {
        fontSize: 16,
        marginLeft: 15,
        color: Colors.button_text_color,
    },
    filterIconContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 15,
    },
    filterIconStyle: {
        width: 20,
        height: 20,
        marginLeft: 10,
    },
    headerStartStyle: {
        width: 8,
        borderTopRightRadius: 4,
        borderBottomRightRadius: 4,
        backgroundColor: Colors.header_start_text,
        position: 'absolute',
        height: '100%',
        left: 0,
    },
    subCategoryContainer: {
        flexDirection: 'row',
        marginTop: 20,
        marginBottom: 20,
        marginLeft: 10,
        marginRight: 10,
        flexWrap: 'wrap',
        alignContent: 'flex-start',
    },


    selectedButtonStyle: {
        backgroundColor: Colors.app_background_color,
        borderColor: Colors.primary_color,
        borderWidth: 1,
        width: 100,
    },
    unSelectedButtonStyle: {
        backgroundColor: Colors.primary_color,
        borderColor: Colors.primary_color,
        borderWidth: 1,
        width: 100,

    },


});
