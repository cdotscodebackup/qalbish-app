import React, {useState, useEffect} from 'react';
import {
    Alert,
    Image,
    KeyboardAvoidingView,
    StyleSheet, SafeAreaView,
    Text, SectionList,
    Platform,
    TouchableOpacity, ScrollView, RefreshControl,
    StatusBar,
    View,
} from 'react-native';
import {FlatList, TextInput} from 'react-native-gesture-handler';
import TextStyles from '../Styles/TextStyles';
import dimen from '../Styles/Dimen';
import {
    _storeData, console_log, errorAlert, getDataWithToken, getObject, isNetConnected, saveObject, successAlert,
} from '../Classes/auth';

import {
    api_key,
    general_network_error, last_location_time, on_off_key, orders_url,
    services_url, staff_all_orders_url, staff_save_lat_lon_url,
    web_view_url,
} from '../Classes/UrlConstant';
import Colors from '../Styles/Colors';
import AppText from '../Components/AppText';
import MyBackgroundImage from '../Components/MyBackgroundImage';
import TopBar from '../Components/TopBar';
import {StaffOrderItemCard} from '../Components/StaffOrderItemCard';
import {EmptyList} from '../Components/Loader';

import {PermissionsAndroid} from 'react-native';
import Geolocation from '@react-native-community/geolocation';
import Geocoder from 'react-native-geocoding';
import {MyImage} from '../Components/MyImageButton';


const initGeoCoding = () => {
    // Initialize the module (needs to be done only once)
    Geocoder.init(api_key); // use a valid API key

};

export const StaffOrders = ({navigation}) => {


    const [isLoading, setIsLoading] = useState(false);
    const [onOff, setOnOff] = useState(false);
    const [ordersList, setOrderList] = useState([]);
    const [filteredOrdersList, setFilteredOrderList] = useState([]);
    const [locationPermission, setLocationPermission] = useState(false);
    const [currentLocation, setCurrentLocation] = useState('');
    const [currentLocationLat, setCurrentLocationLat] = useState('');
    const [currentLocationLong, setCurrentLocationLong] = useState('');

    useEffect(() => {
        getObject(on_off_key)
            .then((res) => {
                if (!res) {
                } else {
                    setOnOff(res);
                }
            })
            .catch(err => console_log(err));

        initGeoCoding();
    }, []);

    useEffect(() => {
        getAllOrders();

    }, []);

    useEffect(() => {
        let listener = navigation.addListener(
            'willFocus',
            () => {
                getAllOrders();
                saveLatLong();
            });
        return function cleanup() {
            listener.remove();
        };
    }, []);

    useEffect(() => {
        // if (onOff) {
        requestLocationPermission();
        // }
    }, []);

    useEffect(() => {
        saveObject(on_off_key, onOff).then().catch(err => console_log(err));
        if (onOff) {
            requestLocationPermission();
        }
    }, [onOff]);

    useEffect(() => {
        if (locationPermission) {
            getCurrentLocation();
        }
    }, [locationPermission]);

    const updateAddress = (lat, long) => {
        Geocoder.from(lat, long)
            .then(json => {
                if (json.results !== null && json.results.length > 0) {
                    // var addressComponent = json.results[0].address_components[0];
                    let addressComponent = json.results[0];
                    // console.log(json);
                    setCurrentLocation(addressComponent.formatted_address);
                }
            })
            .catch(error => {
                console_log(error);
                if (error.code === 0 || error.code === 1) {
                    console_log(error.message);
                } else {
                    console_log(error.origin.error_message);
                }
            });
    };

    const requestLocationPermission = () => {
        if (Platform.OS !== 'ios') {
            PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
                .then((status) => {
                    if (!status) {
                        PermissionsAndroid.request(
                            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                            {
                                title: 'Location permission',
                                message: 'Qalbish needs access to location services',
                                buttonNeutral: 'Ask Me Later',
                                buttonNegative: 'Cancel',
                                buttonPositive: 'OK',
                            },
                        )
                            .then((res) => {
                                if (res === 'granted') {
                                    setLocationPermission(true);
                                }
                            })
                            .catch((err) => {
                                console_log(err);
                            });
                    } else {
                        setLocationPermission(true);
                    }
                })
                .catch((err) => {
                    console_log(err);
                });
        }
        if (Platform.OS === 'ios') {
            getCurrentLocation();
        }
    };

    const getCurrentLocation = () => {
        if (Platform.OS !== 'ios') {

            if (locationPermission) {
                Geolocation.getCurrentPosition(info => {
                    if (info.coords !== undefined) {
                        setCurrentLocationLat(info.coords.latitude);
                        setCurrentLocationLong(info.coords.longitude);
                        updateAddress(info.coords.latitude, info.coords.longitude);
                    }
                });
            } else {
                requestLocationPermission();
            }
        } else {
            Geolocation.getCurrentPosition(info => {
                if (info.coords !== undefined) {
                    setCurrentLocationLat(info.coords.latitude);
                    setCurrentLocationLong(info.coords.longitude);
                    updateAddress(info.coords.latitude, info.coords.longitude);
                }
            });
        }

    };


    const orderPressed = (item) => {
        navigation.navigate('StaffOrderNew', {'order': item});
    };

    const saveLatLong = () => {
        getObject(on_off_key)
            .then((res) => {
                if (!res) {
                } else {
                    setOnOff(res);
                    if (res) {
                        saveLatLongApi();
                    }
                }
            })
            .catch(err => console_log(err));
    };
    const saveLatLongApi = () => {
        isNetConnected()
            .then(() => {
                let formData = {
                    // staff_id: 52,
                    latitude: currentLocationLat,
                    longitude: currentLocationLong,
                };
                setIsLoading(true);
                getDataWithToken(staff_save_lat_lon_url, 'POST', formData)
                    .then((response) => {
                        setIsLoading(false);
                        console_log(response);
                    })
                    .catch((err) => {
                        setIsLoading(false);
                        console_log(err);

                    });
            })
            .catch((err) => {
                errorAlert(general_network_error);
            });
    };


    const getAllOrders = () => {
        isNetConnected()
            .then(() => {
                setIsLoading(true);
                getDataWithToken(staff_all_orders_url, 'GET', {})
                    .then((response) => {
                        setIsLoading(false);
                        // setOrderList(response.data);
                        setFilteredOrderList(response.data.filter((item) => item.staff_status === 'pending' || item.staff_status === 'completed'));
                    })
                    .catch((err) => {
                        setIsLoading(false);
                        console_log(err);

                    });
            })
            .catch((err) => {
                errorAlert(general_network_error);
            });
    };
    const updateOnOff = () => {
        setOnOff(!onOff);
        console_log(onOff);
    };
    const pressOn = () => {
        setOnOff(true);
        saveObject(on_off_key, true).then().catch(err => console_log(err));

        saveLatLong();


    };
    const pressOff = () => {

        setOnOff(false);
        saveObject(on_off_key, false).then().catch(err => console_log(err));


    };


    return (
        <MyBackgroundImage>
            <TopBar title={'Orders'}/>

            <View style={styles.locationContainer}>
                <View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center'}}>
                    <MyImage source={require('../Asset/location.png')}
                             tintColor={Colors.app_background_color}
                             imageContainerStyle={styles.locationIcon}
                    />
                    <AppText numberOfLines={2} style={{flexShrink: 1, color: Colors.app_background_color}}>
                        {currentLocation}
                    </AppText>
                </View>

                <View //onPress={updateOnOff}
                    style={styles.onOffButtonContainer}>
                    {onOff &&
                    <TouchableOpacity onPress={pressOff} style={styles.buttonContainer1}>
                        <AppText style={[styles.onTextStyle, {marginLeft: 5}]}>OFF</AppText>
                        <View style={styles.roundButton}/>
                    </TouchableOpacity>}
                    {!onOff &&
                    <TouchableOpacity onPress={pressOn} style={styles.buttonContainer1}>
                        <View style={styles.roundButton}/>
                        <AppText style={[styles.onTextStyle, {marginRight: 5}]}>ON</AppText>
                    </TouchableOpacity>}
                </View>
            </View>


            <View style={styles.scrollViewInnerContainer}>
                <FlatList
                    style={styles.productListStyle}
                    data={filteredOrdersList}
                    keyExtractor={(item, index) => item.id.toString()}
                    numColumns={1}
                    bounces={false}
                    renderItem={({item}) => (
                        <StaffOrderItemCard productOffersPress={() => orderPressed(item)} data={item}/>)}
                    ListEmptyComponent={<EmptyList reload={getAllOrders}/>}

                    refreshControl={
                        <RefreshControl
                            refreshing={isLoading}
                            onRefresh={getAllOrders}
                        />
                    }
                />
            </View>
        </MyBackgroundImage>
    );

};

const styles = StyleSheet.create({
    dashboardButtonText: {fontFamily: 'roboto-bold', fontSize: 16},
    dashboardButton: {flex: 1, padding: 8, justifyContent: 'center', alignItems: 'center'},
    dashboardButtonContainer: {
        borderWidth: 1,
        borderColor: Colors.border_color,
        borderRadius: dimen.border_radius,
        overflow: 'hidden',
        backgroundColor: Colors.home_widget_background_color,
        margin: dimen.app_padding,
        justifyContent: 'space-evenly',
        alignItems: 'flex-start',
        flexDirection: 'row',

    },
    bannerAdStyle: {
        height: 90, backgroundColor: Colors.primary_color,
        padding: 20, margin: dimen.app_padding,
        justifyContent: 'center',
    },
    flatlistStyle: {
        // width: '100%',
        // backgroundColor: Colors.home_widget_background_color,
        // marginBottom: dimen.bottom_margin_for_bottom_menu + 20,
        padding: dimen.app_padding,
    },
    productFlatList: {
        width: '100%',
        backgroundColor: Colors.primary_color,
        // marginBottom: dimen.bottom_margin_for_bottom_menu + 20,
        // padding: dimen.app_padding,
    },
    flatlistContainerStyle: {
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        backgroundColor: Colors.primary_dark_color,
    },
    emptyComponentStyle: {
        justifyContent: 'flex-start',
        alignItems: 'center',
        // backgroundColor:Colors.primary_dark_color
    },


    //banner
    carousalContainer: {
        height: 130,
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: Colors.home_widget_background_color,
        // margin: dimen.app_padding,
        // padding:dimen.app_padding,
        // paddingBottom:0

    },

    scrollViewContainer: {flex: 1, marginBottom: dimen.bottom_tab_height},
    scrollViewInnerContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        marginBottom: dimen.bottom_tab_height,
    },
    productListStyle: {
        flex: 1,
        padding: dimen.app_padding - dimen.home_item_padding,
        marginBottom: 10,
    },

    //sub categories
    subCategoriesMainContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'stretch',
    },
    categoryHeader: {
        height: 40,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.primary_color,

    },
    headerTextStyle: {
        fontSize: 16,
        marginLeft: 15,
        color: Colors.button_text_color,
    },
    filterIconContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 15,
    },
    filterIconStyle: {
        width: 20,
        height: 20,
        marginLeft: 10,
    },
    headerStartStyle: {
        width: 8,
        borderTopRightRadius: 4,
        borderBottomRightRadius: 4,
        backgroundColor: Colors.header_start_text,
        position: 'absolute',
        height: '100%',
        left: 0,
    },
    subCategoryContainer: {
        flexDirection: 'row',
        marginTop: 20,
        marginBottom: 20,
        marginLeft: 10,
        marginRight: 10,
        flexWrap: 'wrap',
        alignContent: 'flex-start',
    },


    selectedButtonStyle: {
        backgroundColor: Colors.app_background_color,
        borderColor: Colors.primary_color,
        borderWidth: 1,
        width: 100,
    },
    unSelectedButtonStyle: {
        backgroundColor: Colors.primary_color,
        borderColor: Colors.primary_color,
        borderWidth: 1,
        width: 100,

    },

    locationContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 10,
        backgroundColor: Colors.primary_color,
    },

    onOffButtonContainer: {
        backgroundColor: Colors.app_background_color,
        height: 20,
        overflow: 'hidden',
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        // flexGrow:1,


    },
    locationIcon: {width: 20, height: 20, resizeMode: 'contain'},
    onTextStyle: {
        fontSize: 16,
        color: Colors.primary_color,
    },
    roundButton: {
        width: 20,
        height: 20,
        backgroundColor: Colors.primary_color,
        borderWidth: 1,
        borderRadius: 10,
        borderColor: Colors.app_background_color,
    },

    buttonContainer1: {
        flexDirection: 'row',
        justifyContent: 'flex-end',

    },

});
