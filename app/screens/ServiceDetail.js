import React, {useState, useEffect, useCallback, useContext} from 'react';
import {
    Alert,
    Image,
    KeyboardAvoidingView,
    StyleSheet, SafeAreaView, Dimensions,
    Text,
    Platform, Animated,
    TouchableOpacity, ScrollView, TouchableWithoutFeedback,
    StatusBar, SectionList,
    View,
} from 'react-native';
import {FlatList, TextInput} from 'react-native-gesture-handler';
import dimen from '../Styles/Dimen';
import {
    _storeData, console_log, errorAlert, getObject, saveObject, successAlert,
} from '../Classes/auth';

import {EmptyList, Loader} from '../Components/Loader';

import Colors from '../Styles/Colors';
import AppText from '../Components/AppText';
import {CategoriesCard} from '../Components/Categories';
import MyImageButton, {MyImage} from '../Components/MyImageButton';
import MyBackgroundImage from '../Components/MyBackgroundImage';
import TopBar from '../Components/TopBar';

import MyTextButton from '../Components/MyTextButton';
import {MenuItemCard} from '../Components/MenuItemCard';
import {CartProvider, MyContext} from '../Classes/AppContext';

import {ScrollableTabView, DefaultTabBar, ScrollableTabBar} from '@valdio/react-native-scrollable-tabview';
import {CartItemCard} from '../Components/CartItemCard';
import BottomUpPanel from '../Components/BottomUpPanel';
import {cart_key, services_key} from '../Classes/UrlConstant';
import objectDestructuringEmpty from '@babel/runtime/helpers/esm/objectDestructuringEmpty';


const ServiceDetail = ({navigation}) => {
    const {width, height} = Dimensions.get('window');


    const [categories, setCategories] = useState([{'id': 1}, {'id': 2}, {'id': 3}]);
    const [cartItems, setCartItems] = useState([]);
    const [cartItems2, setCartItems2] = useState({});
    const [selectedService, setSelectedService] = useState(2);
    const [isLoading, setIsLoading] = useState(false);
    const [renderLoading, setRenderLoading] = useState(false);
    const [scrollView, setScrollView] = useState(false);
    const [showCart, setShowCart] = useState(false);


    const [sService, setSService] = useState({});
    const [sCategories, setSCategories] = useState([]);
    const [allService, setAllService] = useState([]);

    const [savedCart, setSavedCart] = useState([]);


    const [cartButtonActive, setCartButtonActive] = useState(false);

    const [cartCount, setCartCount] = useState(0);
    const [cartTotal, setCartTotal] = useState(0);
    const [refresh, setRefresh] = useState(false);


    useEffect(() => {
        getObject((cart_key)).then((res) => {
            if (!res) {
            } else {
                setCartCount(res.length);
            }
        });
        calculateCart();


    }, []);
    const calculateCart = () => {
        getObject(cart_key)
            .then((res) => {
                if (!res) {
                } else {
                    let t = 0;
                    res.forEach((item) => {
                        t = t + ((parseInt(item.price) * item.quantity));
                        item.addOns.forEach((ite) => {
                            t = t + ((parseInt(ite.price) * ite.quantity));
                        });
                    });
                    setCartTotal(t);
                    let length = 0;
                    res.forEach((item) => {
                        length = length + 1;
                        item.addOns.forEach((ite) => {
                            length = length + 1;
                        });

                    });
                    setCartCount(length);
                }
            })
            .catch((err) => console_log(err));
    };


    let scrollRef = null;

    const myContext = useContext(MyContext);

    useEffect(() => {
        setRenderLoading(true);
    }, [allService]);

    useEffect(() => {
        setSavedCart([]);

    }, [sService]);

    useEffect(() => {
        setRenderLoading(true);

    }, [savedCart]);

    useEffect(() => {
        myContext.services.forEach((it) => {
            if (it.id === myContext.selectedService) {
                setSService(it);
                setSCategories(it.sub_categories);
            }
        });
        setAllService(myContext.services);

        getObject(services_key)
            .then((res) => {
                setAllService(res);
            })
            .catch((err) => {
                console_log(err);
            });

    }, []);

    useEffect(() => {
        getObject(cart_key)
            .then((res) => {
                if (!res) {
                } else {
                    console_log('cart');
                    console_log(res);
                }
            })
            .catch((err) => {
                console_log(err);
            });
    }, []);


    const productOffersPress = (item) => {
        alert('product pressed');
    };
    const getAllCategories =
        useCallback(
            (item) => {

                calculateCart();

            },
            [allService],
        );

    const menuItemSelected = (item) => {
        console_log(item);
        // setCartItems2({'id':1})
    };

    const [, updateState] = React.useState();
    const forceUpdate = React.useCallback(() => updateState({}), []);

    const reviewOrder = (item) => {
        getObject(cart_key)
            .then((res) => {
                // console_log(res);
                if (!res || res.length === 0) {
                    errorAlert('No items added to cart.');
                } else {
                    // errorAlert('Empty cart')
                    navigation.navigate('CartDetails');
                }
            })
            .catch((res) => console_log(err));
    };
    const renderCategoryNew = (CATEGORY) => {
        return CATEGORY.map((item, i) => {
            return (
                <View tabLabel={item.name} key={i}>

                    {false && <View style={{flex: 1, backgroundColor: Colors.app_background_color}}>
                        <AppText style={styles.serviceNameHeading}>{item.name}</AppText>
                        <View style={{flex: 1}}>
                            <FlatList
                                style={styles.menuItemList}
                                data={item.sub_categories}
                                keyExtractor={(item, index) => item.id.toString()}
                                // contentContainerStyle={{margin:dimen.app_padding}}
                                numColumns={1}
                                bounces={false}
                                renderItem={({item}) => (
                                    <MenuItemCard data={item}
                                        // check={findCheck(item)}
                                    />)}
                                // ListHeaderComponent={() => {return (<FlatListHeader/>)}}
                                ListEmptyComponent={<EmptyList/>}
                            />
                        </View>
                    </View>}

                </View>
                //       style={selectedService === 0 ? styles.serviceNameSelectedBottomLine : {}}>
                //     {selectedService === i &&
                //     <View style={styles.serviceNameSelectedBottomLine}/>}
                // </View>
            );
        });

    };
    const renderCategory = (CATEGORY) => {
        return CATEGORY.map((item, i) => {
            // console_log(item);
            return (
                <TouchableOpacity
                    key={i}
                    onPress={() => {
                        setSelectedService(i);
                        // scrollRef.scrollTo({x: 200});
                        scrollRef.scrollToIndex({index: 4, animated: true});
                    }}>
                    <AppText style={[styles.serviceNameText]}>
                        {item.name}
                    </AppText>
                    {selectedService === i &&
                    <View style={styles.serviceNameSelectedBottomLine}/>}
                </TouchableOpacity>
            );
        });
    };

    const ServicesItem = (props) => {
        const [selectedServiceItem, setSelectedServiceItem] = useState(false);
        const myContext = useContext(MyContext);
        // console_log(myContext.selectedService);

        return (
            <TouchableOpacity
                onPress={() => {

                    setSelectedServiceItem(!selectedServiceItem);
                    myContext.selectedService = props.item.id;
                    console_log(myContext.selectedService);
                    // setSService(myContext.services.find((item)=>item.id===selectedService))
                    setSService(myContext.services.find((item) => item.id === myContext.selectedService));
                    // setSelectedService(props.index);
                    scrollRef.scrollToIndex({index: props.index, animated: true});

                }}>

                <AppText style={[styles.serviceNameText]}>
                    {props.item.name}
                </AppText>

                {selectedServiceItem && //selectedServiceItem &&
                <View style={styles.serviceNameSelectedBottomLine}/>}

            </TouchableOpacity>
        );
    };

    const ServicesHeader = (props) => {
        return (
            <View style={{}}>
                <View style={styles.headerImageContainer}>
                    <MyImage
                        source={{uri: sService.image}}
                        // source={require('../Asset/sevice_image.jpg')}
                        imageContainerStyle={styles.imageStyle}/>
                </View>
                {false && <View style={styles.servicesScrollbar}>
                    <MyImage source={require('../Asset/left_arrow.png')}
                             tintColor={Colors.button_text_color}
                             imageContainerStyle={[styles.dropdownIcon, {marginRight: 10}]}/>
                    <ScrollView
                        horizontal
                        // pagingEnabled
                        showsHorizontalScrollIndicator={false}
                        contentContainerStyle={{
                            justifyContent: 'center',
                        }}
                        ref={node => {
                            (scrollRef = node);
                        }}
                    >
                        <View style={styles.serviceNameItemContainer}>
                            {renderCategory(props.servicesArray)}
                        </View>
                    </ScrollView>
                    <MyImage source={require('../Asset/right_arrow.png')}
                             tintColor={Colors.button_text_color}
                             imageContainerStyle={[styles.dropdownIcon, {marginLeft: 10}]}/>
                </View>}

                <View style={styles.servicesScrollbar}>
                    <MyImage source={require('../Asset/left_arrow.png')}
                             tintColor={Colors.button_text_color}
                             imageContainerStyle={[styles.dropdownIcon, {marginRight: 10}]}/>
                    <FlatList
                        style={{backgroundColor: Colors.primary_color, padding: 10}}
                        contentContainerStyle={styles.serviceNameItemContainer}
                        ref={node => {
                            (scrollRef = node);
                        }}

                        data={props.servicesArray}
                        keyExtractor={(item, index) => item.id.toString()}
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        // extraData={myContext.selectedService}
                        bounces={false}
                        renderItem={({item, index}) => (
                            <ServicesItem item={item} index={index} selectedItem={myContext.selectedService}/>
                        )}
                        // ListHeaderComponent={() => {return (<FlatListHeader/>)}}
                    />
                    <MyImage source={require('../Asset/right_arrow.png')}
                             tintColor={Colors.button_text_color}
                             imageContainerStyle={[styles.dropdownIcon, {marginLeft: 10}]}/>
                </View>


                <AppText style={styles.serviceNameHeading}>Service Name</AppText>
            </View>
        );
    };
    const ServicesHeaderNew = (props) => {


        useEffect(() => {
            props.setRendered();
        }, []);

        return (
            <View style={{}}>
                <View //onPress={press}
                    style={styles.headerImageContainer}>
                    <MyImage source={{uri: sService.image}}
                        // source={require('../Asset/sevice_image.jpg')}
                             imageContainerStyle={styles.imageStyle}/>
                </View>

                <TouchableOpacity onPress={() => {

                    Alert.alert(
                        '',
                        sService.description,
                        [
                            {
                                text: 'Done',
                                onPress: () => {
                                    // console.log('Open pressed')
                                    // saveObject(cart_key, []);
                                    // navigation.popToTop();
                                },
                            },
                        ],
                        {cancelable: false});

                }}
                                  style={styles.descriptionContainer}>
                    <View style={{flexShrink:1}}>
                        <AppText style={{}} numberOfLines={1}>
                            {sService.description}
                        </AppText>
                    </View>
                    <View style={{flexGrow:1,alignItems:'flex-end'}}>
                        <MyImage source={require('../Asset/down.png')}
                                 tintColor={Colors.primary_color}
                                 imageContainerStyle={[styles.dropdownIcon, {paddingRight: 5}]}/>
                    </View>

                </TouchableOpacity>
                <View style={styles.servicesScrollbar}>
                    <MyImage source={require('../Asset/left_arrow.png')}
                             tintColor={Colors.button_text_color}
                             imageContainerStyle={[styles.dropdownIcon, {marginLeft: 0}]}/>

                    <ScrollableTabView initialPage={myContext.selectedServiceIndex}
                                       prerenderingSiblingsNumber={2}
                                       onChangeTab={
                                           (i, j) => {
                                               let ssService = myContext.services.find((item, index) => {
                                                   return i.i === index;
                                               });
                                               setSService(ssService);
                                               setSCategories(ssService.sub_categories);
                                               myContext.selectedServiceIndex = i.i;
                                           }
                                       }
                                       onScroll={(i) => {
                                           // console_log(i)
                                           // if (i%1===0){
                                           //     setSService(myContext.services.find((item, index) => {
                                           //         return i === index;
                                           //     }));
                                           //     myContext.selectedServiceIndex = i;
                                           // }

                                       }}
                                       locked={false}
                                       showsHorizontalScrollIndicator={false}
                                       tabBarUnderlineStyle={{backgroundColor: Colors.app_background_color}}
                                       tabBarBackgroundColor={Colors.primary_color}
                                       tabBarActiveTextColor={Colors.app_background_color}
                                       tabBarInactiveTextColor={Colors.app_background_color}
                                       style={{flex: 1, backgroundColor: Colors.app_background_color}}
                        // tabBarTextStyle={{fon
                        // tSize:15,borderBottomWidth:2,borderColor:Colors.app_background_color,fontWeight:'normal'}}
                                       tabBarTextStyle={{fontSize: 12}}
                                       renderTabBar={() => <ScrollableTabBar/>}>
                        {renderCategoryNew(props.servicesArray)}
                    </ScrollableTabView>

                    <MyImage source={require('../Asset/right_arrow.png')}
                             tintColor={Colors.button_text_color}
                             imageContainerStyle={[styles.dropdownIcon, {marginRight: 0}]}/>
                </View>
            </View>
        );
    };

    const checkEqual = (prevProps, nextProps) => {
        console_log(prevProps);
        console_log(nextProps);

        return true;
    };
    const MemoChild = React.memo(ServicesHeaderNew, checkEqual);

    //region myanimation
    const [bounceValue, setBounceValue] = useState(new Animated.Value(dimen.orderPopupHeight));
    const [show, setShow] = useState(false);
    const toggleBottomView = () => {
        let toValue = dimen.orderPopupHeight;

        if (!show) {
            toValue = 0;
        }
        Animated.spring(bounceValue, {toValue: toValue, velocity: 3, tension: 2, friction: 8}).start();

        setShow(!show);
    };
    //endregion
    //region bottom pannel animation
    const renderBottomUpPanelContent = () =>
        <View>
            <FlatList
                style={styles.cartItemList}
                data={categories}
                keyExtractor={(item, index) => item.id.toString()}
                numColumns={1}
                bounces={false}
                renderItem={({item}) => (
                    <CartItemCard productOffersPress={productOffersPress} data={item}/>)}
                ListEmptyComponent={<EmptyList/>}
            />
        </View>;

    const renderBottomUpPanelIcon = () =>
        <MyImage source={require('../Asset/down.png')}
                 imageContainerStyle={[styles.dropdownIcon, {marginLeft: 5}]}
        />;


    let config = {
        position: {
            max: height,
            start: height - 380, //start height
            end: height - height * 0.7,
            min: height - height * 0.7,
            animates: [
                () => _animatedHeight,
            ],
        },
        width: {
            end: width,
            start: width,
        },
        height: {
            end: height,
            start: 380,       // start height
        },

    };


    const _animatedHeight = new Animated.Value(showCart ? config.height.end : config.height.start);

    const _animatedPosition = new Animated.Value(showCart
        ? config.position.end
        : config.position.start);

    let animatedHeight = _animatedHeight.interpolate({
        inputRange: [config.position.end, config.position.start],
        outputRange: [config.height.end, config.height.start],
    });


    const open = () => {
        setShowCart(true);

        Animated.timing(_animatedPosition, {
            toValue: config.position.end,
            duration: 600,
        }).start();


    };

    const close = () => {
        // this._scrollView.scrollTo({y: 0});
        Animated.timing(_animatedPosition, {
            toValue: config.position.start,
            duration: 600,
        }).start(() => setShowCart(false));

    };

    const toggle = () => {
        if (!showCart) {
            open();
        } else {
            close();
        }
    };
    //endregion


    return (
        <MyBackgroundImage>
            <TopBar title={'Services'} GoToCart={() => {
                navigation.navigate('CartDetails');
            }}
                    backButton={() => navigation.pop()}
                // GoToCart={()=>{navigation.navigate('CartDetails')}}
                // cartCount={cartCount}
            />
            <View style={{flex: 1}}>
                {renderLoading &&
                <View style={styles.mainContainer}>
                    <ScrollView>
                        <View>
                            <ServicesHeaderNew servicesArray={allService} onSelection={getAllCategories}
                                               setRendered={() => {
                                                   setRefresh(false);
                                               }}/>
                            <View
                                style={{
                                    flex: 1,
                                    backgroundColor: Colors.app_background_color,
                                    marginBottom: dimen.app_padding,
                                    marginTop: dimen.app_padding,
                                }}>

                                <AppText style={styles.serviceNameHeading}>{sService.name}</AppText>
                                <View style={{flex: 1}}>
                                    <FlatList
                                        style={styles.menuItemList}
                                        extraData={{refresh}}
                                        // key={{refresh}}
                                        data={sService.sub_categories}
                                        keyExtractor={(item, index) => item.id.toString()}
                                        // contentContainerStyle={{margin:dimen.app_padding}}
                                        numColumns={1}
                                        bounces={false}
                                        renderItem={({item}) => (
                                            <MenuItemCard
                                                menuSelected={getAllCategories}
                                                data={item}
                                                check={false}
                                            />)}
                                        // ListHeaderComponent={() => {return (<FlatListHeader/>)}}
                                        ListEmptyComponent={<EmptyList text={'No items available.'}/>}
                                    />
                                </View>
                            </View>
                        </View>
                        {/*{refresh && <Loader/>}*/}

                    </ScrollView>

                    <View style={{margin: dimen.app_padding, marginBottom: 0}}>
                        {false && <Animated.View style={[{backgroundColor: 'red'}, {height: animatedHeight}]}>
                            <Animated.View
                                style={[styles.orderSummaryPopupContainer, {
                                    // transform: [{translateY: bounceValue}]
                                    transform: [{translateY: _animatedPosition}, {translateX: 0}],

                                }]}>
                                <TouchableWithoutFeedback onPress={toggle} style={styles.cartPopUpHeadingContainer}>
                                    <AppText style={styles.cartPopUpHeadingText}>Order Summary</AppText>
                                    {/*<MyImage source={require('../Asset/down.png')}*/}
                                    {/*         imageContainerStyle={[styles.dropdownIcon, {marginLeft: 5}]}*/}
                                    {/*/>*/}
                                </TouchableWithoutFeedback>

                                <View style={styles.cartItemsContainer}/>

                                <FlatList
                                    style={styles.cartItemList}
                                    data={categories}
                                    keyExtractor={(item, index) => item.id.toString()}
                                    numColumns={1}
                                    bounces={false}
                                    renderItem={({item}) => (
                                        <CartItemCard productOffersPress={productOffersPress} data={item}/>)}
                                    ListEmptyComponent={<EmptyList text={'No item available.'}/>}
                                />

                            </Animated.View>
                        </Animated.View>}

                        {/*<TouchableOpacity onPress={toggle} style={styles.orderSummaryContainer}>*/}
                        {/*    <AppText>Total Rs. 2000</AppText>*/}
                        {/*    <View style={styles.cartSummaryContainer}>*/}
                        {/*        <AppText>Order Summary</AppText>*/}
                        {/*        <MyImage source={require('../Asset/up_arrow.png')}*/}
                        {/*                 imageContainerStyle={[styles.dropdownIcon, {marginLeft: 5}]}*/}
                        {/*        />*/}
                        {/*    </View>*/}
                        {/*</TouchableOpacity>*/}


                        <BottomUpPanel
                            total={cartTotal}
                            updateParent={(cartLength) => {
                                if (cartLength !== cartCount) {
                                    setCartCount(cartLength);
                                    // setRefresh(true)
                                    let newSS = sService.sub_categories.map((i) => {
                                        // i.shouldUpdate=true;
                                        return i;
                                    });
                                    setSService(newSS);
                                    calculateCart();
                                }
                            }}
                            parentCartCount
                            icon={renderBottomUpPanelIcon}
                            topEnd={height - height * 0.5}
                            startHeight={30}
                            headerText={'Order Summary'}
                            headerTextStyle={{
                                color: Colors.normal_text_color,
                                fontSize: 15,
                                // backgroundColor:'red'
                            }}
                            bottomUpSlideBtn={{
                                display: 'flex',
                                alignSelf: 'flex-start',
                                alignItems: 'center',
                            }}
                        />

                        <MyTextButton onPress={reviewOrder}
                                      buttonText={'Go To Cart'}
                                      activeOpacity={1}
                                      buttonContainerStyle={[styles.loginButton, styles.goToCartButtonStyle]}/>

                        <View style={styles.bottomViewToHideBackground}/>

                    </View>
                </View>}

                {isLoading && <Loader/>}
                {!renderLoading && <Loader/>}
            </View>
        </MyBackgroundImage>
    );
};

export default React.memo(ServiceDetail);


const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'stretch',
        // marginBottom: dimen.bottom_tab_height,
    },
    servicesScrollbar: {
        // height: 40,
        backgroundColor: Colors.primary_color,
        // paddingLeft: dimen.app_padding,
        // paddingRight: dimen.app_padding,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        // marginBottom: dimen.app_padding * 3,

    },
    headerImageContainer: {
        height: 150,
        width: '100%',
        marginTop: 10,
    },
    imageStyle: {
        resizeMode: 'cover',
        backgroundColor: Colors.image_background_color
        ,
    },
    dropdownIcon: {
        width: 10,
        height: 10,
        resizeMode: 'contain',
    },
    orderSummaryContainer: {
        borderWidth: 1,
        borderColor: Colors.primary_color,
        backgroundColor: Colors.app_background_color,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        // margin: dimen.app_padding,
        marginBottom: 0,
        padding: 10,


    },
    serviceNameItemContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    serviceNameText: {
        fontSize: 18,
        // color: this.state.currentIndex === i ? "#F08C4F" : "white",
        paddingHorizontal: 10,
        color: Colors.button_text_color,
    },
    serviceNameSelectedBottomLine: {
        width: '100%',
        height: 1,
        backgroundColor: Colors.app_background_color,
        marginLeft: 10,
        marginRight: 10,
        alignSelf: 'center',

    },
    serviceNameHeading: {
        fontSize: 18,
        fontWeight: 'bold',
        color: Colors.normal_text_color,
        margin: dimen.app_padding,
        marginTop: 0,
    },

    menuItemList: {
        flex: 1,
        padding: dimen.app_padding - dimen.home_item_padding,
        borderWidth: 1,
        borderColor: Colors.primary_color,
        marginLeft: dimen.app_padding,
        marginRight: dimen.app_padding,
    },

    cartItemList: {
        flex: 1,
        // padding: dimen.home_item_padding,
        // backgroundColor:'red'

    },


    orderSummaryPopupContainer: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: Colors.app_background_color,
        borderWidth: 1,
        borderColor: Colors.primary_color,
        height: dimen.orderPopupHeight,

    },

    cartPopUpHeadingContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingLeft: dimen.app_padding,
        paddingRight: dimen.app_padding,
        paddingTop: dimen.home_item_padding,
        paddingBottom: dimen.home_item_padding,
    },
    cartPopUpHeadingText: {
        fontSize: 13,
        fontWeight: 'bold',
    },
    cartItemsContainer: {
        marginLeft: dimen.app_padding,
        marginRight: dimen.app_padding,
        height: 1,
        backgroundColor: Colors.image_background_color,
    },
    cartSummaryContainer: {flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center'},
    goToCartButtonStyle: {
        marginTop: 0,
        marginBottom: 0,
    },
    bottomViewToHideBackground: {
        height: dimen.app_padding,
        width: '100%',
        backgroundColor: Colors.app_background_color,
    },

    descriptionContainer:{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: dimen.app_padding,
    }


});































