import React, {useState, useEffect} from 'react';
import {
    Alert,
    Image,
    KeyboardAvoidingView,
    StyleSheet, SafeAreaView,
    Text, SectionList,
    Platform,
    TouchableOpacity, ScrollView,
    StatusBar,
    View,
} from 'react-native';
import {FlatList, TextInput} from 'react-native-gesture-handler';
import TextStyles from '../Styles/TextStyles';
import dimen from '../Styles/Dimen';
import {
    _storeData, console_log, successAlert,
} from '../Classes/auth';

import {
    web_view_url,
} from '../Classes/UrlConstant';
import {Loader} from '../Components/Loader';

import NetInfo from '@react-native-community/netinfo';
import Colors from '../Styles/Colors';
import AppText from '../Components/AppText';
import {CategoriesCard} from '../Components/Categories';
import MyImageButton, {MyImage} from '../Components/MyImageButton';
import MyBackgroundImage from '../Components/MyBackgroundImage';
import MyStrings from '../Classes/MyStrings';
import MyCarousel from '../Components/Carousel';
import BannerHome from '../Components/BannerHome';
import {ProductsCard} from '../Components/ProductsCard';
import {OffersCard} from '../Components/OffersCard';
import SearchField from '../Components/SearchComponent';
import TopBar from '../Components/TopBar';
import {OrderItemCard} from '../Components/OrderItemCard';
import {MyReferralsCard} from '../Components/MyReferralsCard';


export const MyReferrals = ({navigation}) => {

    const [categories, setCategories] = useState([{'id': 1},
        {'id': 2}, {'id': 3}]);
    const [isloading, setIsLoading] = useState(false);
    const [add, setAdd] = useState('Instalment.pk add here');
    const [email, setEmail] = useState('');

    const subCatPress = (item) => {
        navigation.navigate('ProductListS');
    };
    const productOffersPress = (item) => {
        navigation.navigate('OrderDetails');
    };
    const locationButtonPressed = (item) => {
        // navigation.navigate('ProductListS');
    };
    const goback = () => {
        navigation.goBack();
    };
    const renderSubCat = (cat) => {
        const txtContainer = cat.map(
            (i, j) =>
                <ProductsCard subCatPress={subCatPress} data={i}/>,
        );
        return (
            <View style={styles.subCategoriesMainContainer}>
                <View style={styles.subCategoryContainer}>
                    {txtContainer}
                </View>
            </View>
        );

    };

    const ProductListHeader = () => {
        return (
            <View style={styles.subCategoriesMainContainer}>
                <View style={styles.categoryHeader}>
                    <View style={styles.headerStartStyle}/>
                    <AppText style={styles.headerTextStyle}>
                        My Offers
                    </AppText>

                </View>
            </View>
        );
    };

    const FlatListHeader = () => {
        return (
            <View style={{flex: 1}}>
                {banner()}
                <View style={styles.subCategoriesMainContainer}>
                    <View style={styles.categoryHeader}>
                        <View style={styles.headerStartStyle}/>
                        <AppText style={styles.headerTextStyle}>
                            Laptops
                        </AppText>
                        <View style={styles.filterIconContainer}>
                            <MyImageButton onPress={locationButtonPressed}
                                           source={require('../Asset/filter.png')}
                                           imageContainerStyle={styles.filterIconStyle}/>
                            <MyImageButton onPress={locationButtonPressed}
                                           source={require('../Asset/sort.png')}
                                           imageContainerStyle={styles.filterIconStyle}/>

                        </View>
                    </View>
                </View>
            </View>
        );
    };


    return (
        <MyBackgroundImage>
            <TopBar title={'My Referrals'}/>
            <View style={styles.scrollViewInnerContainer}>
                <FlatList
                    style={styles.productListStyle}
                    data={categories}
                    keyExtractor={(item, index) => item.id.toString()}
                    // contentContainerStyle={{margin:dimen.app_padding}}
                    numColumns={1}
                    bounces={false}
                    renderItem={({item}) => (<MyReferralsCard productOffersPress={productOffersPress} data={item}/>)}
                    // ListHeaderComponent={() => {return (<FlatListHeader/>)}}
                />
            </View>
        </MyBackgroundImage>
    );

};

const styles = StyleSheet.create({
    dashboardButtonText: {fontFamily: 'roboto-bold', fontSize: 16},
    dashboardButton: {flex: 1, padding: 8, justifyContent: 'center', alignItems: 'center'},
    dashboardButtonContainer: {
        borderWidth: 1,
        borderColor: Colors.border_color,
        borderRadius: dimen.border_radius,
        overflow: 'hidden',
        backgroundColor: Colors.home_widget_background_color,
        margin: dimen.app_padding,
        justifyContent: 'space-evenly',
        alignItems: 'flex-start',
        flexDirection: 'row',

    },
    bannerAdStyle: {
        height: 90, backgroundColor: Colors.primary_color,
        padding: 20, margin: dimen.app_padding,
        justifyContent: 'center',
    },
    flatlistStyle: {
        // width: '100%',
        // backgroundColor: Colors.home_widget_background_color,
        // marginBottom: dimen.bottom_margin_for_bottom_menu + 20,
        padding: dimen.app_padding,
    },
    productFlatList: {
        width: '100%',
        backgroundColor: Colors.primary_color,
        // marginBottom: dimen.bottom_margin_for_bottom_menu + 20,
        // padding: dimen.app_padding,
    },
    flatlistContainerStyle: {
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        backgroundColor: Colors.primary_dark_color,
    },
    emptyComponentStyle: {
        justifyContent: 'flex-start',
        alignItems: 'center',
        // backgroundColor:Colors.primary_dark_color
    },


    //banner
    carousalContainer: {
        height: 130,
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: Colors.home_widget_background_color,
        // margin: dimen.app_padding,
        // padding:dimen.app_padding,
        // paddingBottom:0

    },

    scrollViewContainer: {flex: 1, marginBottom: dimen.bottom_tab_height},
    scrollViewInnerContainer: {flex: 1, justifyContent: 'flex-start', alignItems: 'stretch',marginBottom:dimen.bottom_tab_height},
    productListStyle: {
        flex: 1,
        padding: dimen.app_padding - dimen.home_item_padding,
    },

    //sub categories
    subCategoriesMainContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'stretch',
    },
    categoryHeader: {
        height: 40,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.primary_color,

    },
    headerTextStyle: {
        fontSize: 16,
        marginLeft: 15,
        color: Colors.button_text_color,
    },
    filterIconContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 15,
    },
    filterIconStyle: {
        width: 20,
        height: 20,
        marginLeft: 10,
    },
    headerStartStyle: {
        width: 8,
        borderTopRightRadius: 4,
        borderBottomRightRadius: 4,
        backgroundColor: Colors.header_start_text,
        position: 'absolute',
        height: '100%',
        left: 0,
    },
    subCategoryContainer: {
        flexDirection: 'row',
        marginTop: 20,
        marginBottom: 20,
        marginLeft: 10,
        marginRight: 10,
        flexWrap: 'wrap',
        alignContent: 'flex-start',
    },


});
