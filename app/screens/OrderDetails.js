import React, {useState, useEffect} from 'react';
import {
    Alert,
    Image,
    KeyboardAvoidingView,
    StyleSheet, SafeAreaView,
    Text, SectionList,
    Platform,
    TouchableOpacity, ScrollView,
    StatusBar,
    View,
} from 'react-native';
import {FlatList, TextInput} from 'react-native-gesture-handler';
import TextStyles from '../Styles/TextStyles';
import dimen from '../Styles/Dimen';
import Colors from '../Styles/Colors';
import AppText from '../Components/AppText';
import {CategoriesCard} from '../Components/Categories';
import MyImageButton, {MyImage} from '../Components/MyImageButton';
import MyBackgroundImage from '../Components/MyBackgroundImage';

import TopBar from '../Components/TopBar';
import {OrderItemCard} from '../Components/OrderItemCard';
import {StatusView} from '../Components/StatusView';
import {Rating, AirbnbRating} from 'react-native-ratings';
import MyTextButton from '../Components/MyTextButton';
import {console_log, errorAlert, getDataWithToken, isNetConnected, simpleAlert} from '../Classes/auth';
import {general_network_error, get_staff_lat_lon_url, orders_url, staff_rating_url} from '../Classes/UrlConstant';
import {Loader} from '../Components/Loader';

export const OrderDetails = ({navigation}) => {

    const [categories, setCategories] = useState([{'id': 1},
        {'id': 2}, {'id': 3}]);
    const [isLoading, setIsLoading] = useState(false);
    const [order, setOrder] = useState({});
    const [orderStaff, setOrderStaff] = useState(null);

    const [timeRating, setTimeRating] = useState(0);
    const [profRating, setProfRating] = useState(0);
    const [serviceRating, setServiceRating] = useState(0);

    const [ratingStatus, setRatingStatus] = useState(false);
    const [averageRating, setAverageRating] = useState(0);


    useEffect(() => {
        let order = navigation.getParam('order');
        // console_log(order)
        setOrder(order);
        if (order.staff !== null) {
            setOrderStaff(order.staff);
        }
        if (order.rating !== null) {
            setRatingStatus(true)
            // console_log(order.rating)
            setAverageRating();
            let ave = (parseInt(order.rating.time) +
                parseInt(order.rating.professionalism) +
                parseInt(order.rating.professionalism)) / 3
            // console_log(ave)
            setAverageRating(Math.round(ave * 100) / 100)


        }
    }, []);

    useEffect(() => {
        // console_log(orderStaff);
    }, [orderStaff]);

    const backPress = () => {
        navigation.pop();
    };

    const trackStaff = () => {
        getLatLong();
    };


    const submitRating = () => {
        isNetConnected()
            .then(() => {
                if (timeRating === 0 || profRating === 0 || serviceRating === 0) {
                    errorAlert('Rate staff.')
                    return;
                }
                setIsLoading(true);
                let formData = {
                    order_id: order.id,
                    time: timeRating,
                    professionalism: profRating,
                    service: serviceRating
                }
                getDataWithToken(staff_rating_url, 'POST', formData)
                    .then((response) => {
                        setIsLoading(false);
                        console_log(response)
                        if (response.status !== undefined && response.status === true) {
                            setRatingStatus(true)
                            simpleAlert(response.message)
                        }

                    })
                    .catch((err) => {
                        setIsLoading(false);
                        console_log(err);

                    });
            })
            .catch((err) => {
                errorAlert(general_network_error);
            });
    };
    const getLatLong = () => {
        isNetConnected()
            .then(() => {
                setIsLoading(true);
                let formData = {
                    staff_id: order.staff.id,
                }
                getDataWithToken(get_staff_lat_lon_url, 'POST', formData)
                    .then((response) => {
                        setIsLoading(false);
                        if (response.data !== undefined) {
                            navigation.navigate('TrackStaff', {'data': response.data})
                        }
                        console_log(response)

                    })
                    .catch((err) => {
                        setIsLoading(false);
                        console_log(err);

                    });
            })
            .catch((err) => {
                errorAlert(general_network_error);
            });
    };

    return (
        <MyBackgroundImage>
            <TopBar title={'Order Details'} backButton={backPress}/>
            <View style={{flex: 1,}}>
                <ScrollView style={{flex: 1}}>
                    <View style={styles.scrollViewInnerContainer}>
                        <View style={styles.containerStyle}>
                            <View style={styles.nameContainer}>
                                <AppText style={styles.headingTextStyle} numberOfLines={1}>
                                    Order Id:
                                </AppText>
                                <AppText style={[styles.dataSubTextStyle, {marginTop: 5}]} numberOfLines={1}>
                                    {order.order_id}
                                </AppText>
                            </View>
                            <View style={styles.dateTimeContainer}>
                                <AppText style={styles.headingTextStyle} numberOfLines={1}>
                                    Date/Time
                                </AppText>
                                <AppText style={[styles.dataSubTextStyle, {marginTop: 5}]} numberOfLines={2}>
                                    {order.order_date}
                                </AppText>
                                <AppText style={[styles.dataSubTextStyle]} numberOfLines={1}>
                                    {order.order_time}
                                </AppText>
                            </View>
                        </View>
                        <View style={styles.containerStyle}>
                            <View style={styles.nameContainer}>
                                <AppText style={styles.headingTextStyle} numberOfLines={1}>
                                    Address
                                </AppText>
                                <AppText style={[styles.dataSubTextStyle, {marginTop: 5}]} numberOfLines={4}>
                                    {order.full_address}
                                </AppText>
                            </View>
                            <View style={styles.dateTimeContainer}>
                                <AppText style={styles.headingTextStyle} numberOfLines={1}>
                                    Phone Number
                                </AppText>
                                <AppText style={[styles.dataSubTextStyle, {marginTop: 5}]} numberOfLines={1}>
                                    {order.phone_number}
                                </AppText>
                            </View>
                        </View>

                        {order.alternate_address!==undefined && order.alternate_address!== '' &&
                        <View style={styles.containerStyle}>
                            <View style={styles.nameContainer}>
                                <AppText style={styles.headingTextStyle} numberOfLines={1}>
                                   Alternate Address
                                </AppText>
                                <AppText style={[styles.dataSubTextStyle, {marginTop: 5}]} numberOfLines={4}>
                                    {order.alternate_address}
                                </AppText>
                            </View>
                        </View>
                        }

                        <StatusView status={order.order_status}/>

                        {order.order_progress_status === 'on-my-way' &&
                        <MyTextButton onPress={trackStaff}
                                      buttonText={'Track Staff'}
                                      buttonTextStyle={{fontSize: 12}}
                                      buttonContainerStyle={{
                                          alignSelf: 'center',
                                          padding: 10,
                                          margin: dimen.home_item_padding,
                                      }}/>
                        }


                        {orderStaff !== null &&
                        <View style={styles.staffContainer}>
                            <View style={styles.profileImageContainer}>
                                <MyImage
                                    source={{uri: orderStaff.profile_pic}}
                                    imageContainerStyle={styles.profileImage}/>
                            </View>
                            <AppText style={[styles.headingTextStyle, {marginLeft: dimen.app_padding}]}
                                     numberOfLines={1}>
                                {orderStaff.name}
                            </AppText>
                        </View>
                        }

                        <View style={styles.reviewOrderContainer}>
                            <AppText style={[styles.reviewOrderTextStyle]}>
                                Review Order
                            </AppText>
                            <AppText style={[styles.reviewOrderTextStyle]}>
                                Rs. {order.grand_total}
                            </AppText>

                        </View>

                        {ratingStatus &&  order.rating !== null &&
                        <Rating
                            count={5}
                            startingValue={averageRating}
                            fractions={2}
                            tintColor={Colors.app_background_color}
                            style={{backgroundColor: Colors.app_background_color}}
                            imageSize={30}
                        />}


                        {order.order_status === 'completed' && !ratingStatus &&
                        <View style={styles.ratingContainer}>
                            <AppText style={[styles.headingTextStyle, {marginLeft: dimen.app_padding}]}
                                     numberOfLines={1}>
                                Time
                            </AppText>
                            <AirbnbRating
                                count={5}
                                defaultRating={0}
                                onFinishRating={(rating) => setTimeRating(rating)}
                                showRating={false}
                                size={20}
                            />
                            <AppText style={[styles.headingTextStyle, {marginLeft: dimen.app_padding}]}
                                     numberOfLines={1}>
                                Professionalism
                            </AppText>
                            <AirbnbRating
                                count={5}
                                defaultRating={0}
                                onFinishRating={(rating) => setProfRating(rating)}
                                showRating={false}
                                size={20}
                            />
                            <AppText style={[styles.headingTextStyle, {marginLeft: dimen.app_padding}]}
                                     numberOfLines={1}>
                                Services
                            </AppText>
                            <AirbnbRating
                                count={5}
                                defaultRating={0}
                                onFinishRating={(rating) => setServiceRating(rating)}
                                showRating={false}
                                size={20}
                            />

                            <MyTextButton onPress={submitRating}
                                          buttonText={'Submit Rating'}
                                          buttonTextStyle={{fontSize: 12}}
                                          buttonContainerStyle={{
                                              alignSelf: 'center',
                                              padding: 10,
                                              margin: dimen.home_item_padding,
                                          }}/>

                        </View>
                        }

                    </View>
                </ScrollView>
                {isLoading && <Loader/>}
            </View>
        </MyBackgroundImage>
    );

};

const styles = StyleSheet.create({

    scrollViewInnerContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        marginBottom: dimen.bottom_tab_height,
    },
    containerStyle: {
        // flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        // borderWidth: 1,borderColor: 'black'
        margin: dimen.home_item_padding,
        padding: dimen.home_item_padding,
    },
    nameContainer: {
        flex: 2,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        padding: 7,
    },

    dateTimeContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-start',
        padding: 7,
        paddingLeft: 0,
        // backgroundColor:'yellow'

    },
    headingTextStyle: {
        color: Colors.normal_text_color,
        fontSize: 12,
        fontWeight: 'bold',
    },
    dataSubTextStyle: {
        color: Colors.normal_text_color,
        fontSize: 12,
    },

    textStyle: {
        fontSize: 10,
        color: Colors.primary_color,
        fontWeight: 'bold',
        alignSelf: 'center',
        textAlign: 'center',
    },
    ratingContainer: {
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        margin: dimen.app_padding,
        // backgroundColor:'red'
    },
    staffContainer: {
        // flex:1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        padding: dimen.app_padding,
    },
    profileImageContainer: {alignSelf: 'center', overflow: 'hidden', borderRadius: 25, width: 50, height: 50},
    profileImage: {
        width: 50,
        height: 50,
        overflow: 'hidden',
        borderRadius: 25,
        alignSelf: 'center',
        borderWidth: 1,

        borderColor: Colors.image_background_color,
    },
    reviewOrderContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        backgroundColor: Colors.primary_color,
        margin: dimen.app_padding,
        padding: dimen.app_padding - dimen.home_item_padding,
    },
    ratingOrderContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        margin: dimen.app_padding,
        padding: dimen.app_padding - dimen.home_item_padding,
    },
    reviewOrderTextStyle: {
        color: Colors.app_background_color,
        fontSize: 12,
    },


});
















