import React, {useState, useEffect, useCallback} from 'react';
import {
    Alert,
    Image,
    KeyboardAvoidingView,
    StyleSheet, SafeAreaView,
    Text,
    Platform, Animated,
    TouchableOpacity, ScrollView, TouchableWithoutFeedback,
    StatusBar, SectionList,
    View,
} from 'react-native';
import dimen from '../Styles/Dimen';
import {EmptyList, Loader} from '../Components/Loader';
import Colors from '../Styles/Colors';
import MyBackgroundImage from '../Components/MyBackgroundImage';
import TopBar from '../Components/TopBar';


import MyTextButton from '../Components/MyTextButton';
import {CurrentOrderCard, CustomerComponent, DriverComponent} from '../Components/StaffComponents';
import {console_log, errorAlert, getDataWithToken, isNetConnected} from '../Classes/auth';
import {general_network_error, staff_all_orders_url, update_new_order_status_url} from '../Classes/UrlConstant';


const StaffOrderNew = ({navigation}) => {


    const [isLoading, setIsLoading] = useState(false);
    const [order, setOrder] = useState(null);


    useEffect(() => {
        let ord = navigation.getParam('order');
        setOrder(ord);

    }, []);


    const acceptOrder = () => {
        acceptOrderApi(order.id, 'accepted');
    };
    const rejectOrder = () => {
        acceptOrderApi(order.id, 'rejected');
    };

    const acceptOrderApi = (orderId, status) => {
        isNetConnected()
            .then(() => {
                setIsLoading(true);
                let formData = {
                    'order_id': orderId,
                    'order_staff_status': status,
                };
                console_log('formData')
                console_log(formData)
                getDataWithToken(update_new_order_status_url, 'POST', formData)
                    .then((response) => {
                        setIsLoading(false);
                        // console_log('accept order status');
                        // console_log(response);
                        navigation.pop();
                        // navigation.navigate('StaffProcessing');

                    })
                    .catch((err) => {
                        setIsLoading(false);
                        console_log(err);

                    });
            })
            .catch((err) => {
                errorAlert(general_network_error);
            });
    };


    return (
        <MyBackgroundImage>
            <TopBar title={'Appointment Details'} backButton={() => navigation.pop()}/>
            <View style={{flex:1}}>
                <ScrollView style={{flex: 1}}>
                    <View>
                        <CustomerComponent data={order}/>
                        <CurrentOrderCard data={order}/>
                        <DriverComponent data={order}/>

                        {order !== null && order.staff_status=== 'pending' &&
                        <View>
                            <MyTextButton onPress={acceptOrder}
                                          buttonText={'Accept'}
                                          buttonTextStyle={{fontSize: 17}}
                                          buttonContainerStyle={[styles.loginButton, {
                                              margin: dimen.app_padding,
                                              marginBottom: 0,
                                              backgroundColor: Colors.current_order_color,
                                          }]}/>

                            <MyTextButton onPress={rejectOrder}
                                          buttonText={'Reject'}
                                          buttonTextStyle={{fontSize: 17}}
                                          buttonContainerStyle={[styles.loginButton, {
                                              margin: dimen.app_padding,
                                              backgroundColor: Colors.canceled_color,
                                          }]}/>
                        </View>}


                    </View>
                </ScrollView>
                {isLoading && <Loader/>}
            </View>

        </MyBackgroundImage>
    );

};

export default (StaffOrderNew);


const styles = StyleSheet.create({
    mainContainer: {
        // flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        margin: dimen.app_padding,
        borderWidth: 1,
        borderColor: Colors.primary_color,

        // marginBottom: dimen.bottom_tab_height,
    },
    servicesScrollbar: {
        height: 40,
        backgroundColor: Colors.primary_color,
        paddingLeft: dimen.app_padding,
        paddingRight: dimen.app_padding,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',

    },
    headerImageContainer: {
        height: 150,
        width: '100%',
        marginTop: 10,
    },
    imageStyle: {
        resizeMode: 'cover',
    },
    dropdownIcon: {
        width: 10,
        height: 10,
        resizeMode: 'contain',
    },
    orderSummaryContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        // margin: dimen.app_padding,
        marginBottom: 0,
        padding: 10,

    },
    serviceNameItemContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    serviceNameText: {
        fontSize: 18,
        // color: this.state.currentIndex === i ? "#F08C4F" : "white",
        paddingHorizontal: 10,
        color: Colors.button_text_color,
    },
    serviceNameSelectedBottomLine: {
        width: '80%',
        height: 1,
        backgroundColor: Colors.button_text_color,
        marginLeft: 10,
        marginRight: 10,
        alignSelf: 'center',
    },
    serviceNameHeading: {
        fontSize: 18,
        fontWeight: 'bold',
        color: Colors.normal_text_color,
        margin: dimen.app_padding,
    },

    menuItemList: {
        flex: 1,
        padding: dimen.app_padding - dimen.home_item_padding,
        borderWidth: 1,
        borderColor: Colors.primary_color,
        marginLeft: dimen.app_padding,
        marginRight: dimen.app_padding,
        marginTop: dimen.app_padding,
    },
    orderSummaryPopupContainer: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: Colors.app_background_color,
        borderWidth: 1,
        borderColor: Colors.primary_color,
        height: dimen.orderPopupHeight,

    },

    staffContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        padding: dimen.app_padding,
    },
    profileImageContainer: {alignSelf: 'center', overflow: 'hidden', borderRadius: 25, width: 50, height: 50},
    profileImage: {
        width: 50,
        height: 50,
        overflow: 'hidden',
        borderRadius: 25,
        alignSelf: 'center',
        borderWidth: 1,
    },

    orderItemsMainContainer: {
        borderWidth: 1,
        borderColor: Colors.primary_color,
        margin: dimen.app_padding,
        padding: dimen.home_item_padding,
        paddingRight: dimen.home_item_padding,
    },


});































