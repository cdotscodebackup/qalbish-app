import React, {useState, useEffect, useCallback, useContext} from 'react';
import {
    Alert,
    Image,
    KeyboardAvoidingView,
    StyleSheet, SafeAreaView,
    Text,
    Platform,
    TouchableOpacity, ScrollView,
    StatusBar, SectionList,
    View, RefreshControl,
} from 'react-native';
import {FlatList, TextInput} from 'react-native-gesture-handler';
import TextStyles from '../Styles/TextStyles';
import dimen from '../Styles/Dimen';
import {
    _storeData,
    clearAllData,
    console_log,
    errorAlert,
    getDataWithToken,
    getObject,
    isNetConnected,
    saveObject,
    successAlert,
} from '../Classes/auth';

import {
    cart_key,
    general_network_error,
    logout_url, services_key, services_url,
    web_view_url,
} from '../Classes/UrlConstant';
import {EmptyList, Loader} from '../Components/Loader';

import Colors from '../Styles/Colors';
import MyBackgroundImage from '../Components/MyBackgroundImage';
import TopBar from '../Components/TopBar';
import {addFriend, changeName, getCategoriesActionCreator, getUser} from '../Actions/actions';
import {shallowEqual, useDispatch, useSelector} from 'react-redux';
import {ServicesCard} from '../Components/ServicesCard';
import {MyContext} from '../Classes/AppContext';


const Services = ({navigation}) => {


    const [categories, setCategories] = useState([{'id': 1},
        {'id': 2}, {'id': 3}, {'id': 4}]);
    const [isLoading, setIsLoading] = useState(false);
    const [services, setServices] = useState([]);

    const [cartCount, setCartCount] = useState(0);
    const [refresh, setRefresh] = useState(false);


    useEffect(() => {
        getObject((cart_key)).then((res) => {
            if (!res) {
            } else {
                setCartCount(res.length)
            }
        })
        getAllServices();
    }, []);

    useEffect(() => {
        let listener = navigation.addListener(
            'willFocus',
            () => {
                getAllServices2();
                // saveObject(cart_key,[]);
                getObject((cart_key)).then((res) => {
                    if (!res) {
                    } else {
                        setCartCount(res.length)
                    }
                })
            });
        return function cleanup() {
            listener.remove();
        };
    }, []);

    const appContext = MyContext;
    const myContext = useContext(MyContext);

    const servicePressed = (item) => {
        myContext.selectedService = item.id;
        myContext.services.forEach((ite, index) => {
            if (ite.id === item.id) {
                myContext.selectedServiceIndex = index;
                navigation.navigate('ServiceDetail', {'selectedService': item.id, 'index': index});
            }

        });

    };
    const getAllServices = () => {
        isNetConnected()
            .then(() => {
                setIsLoading(true);
                getDataWithToken(services_url, 'GET', {})
                    .then((response) => {
                        setIsLoading(false);
                        // console_log(response)

                        let data = [];

                        if (response.packageInformation !== undefined && response.packageInformation !== null) {

                            let packagesList = [];
                            if (response.packages !== undefined && response.packages !== null) {
                                response.packages.forEach((pItem) => {

                                    // finding add ons / package Services
                                    let packageServices=[];
                                    if (pItem.package_services !== undefined) {
                                        pItem.package_services.forEach((pi)=>{
                                            if (pi.package_sub_category !== undefined && pi.package_sub_category !== null){
                                                pi.package_sub_category.forEach((pSubCatItem)=>{

                                                    if (pSubCatItem.sub_category !== undefined && pSubCatItem.sub_category !== null){
                                                        packageServices.push(pSubCatItem.sub_category);
                                                    }

                                                })
                                            }
                                        })
                                    }




                                    let packageItem = {
                                        'id': pItem.id,
                                        'name': pItem.name,
                                        'type': pItem.type,
                                        'price': pItem.total_price,
                                        'addOns':packageServices

                                    }
                                    packagesList.push(packageItem)
                                })
                            }

                            let packageObject = {
                                'id': 1,
                                'name': response.packageInformation.name,
                                'thumbnail': response.packageInformation.thumbnail,
                                'image': response.packageInformation.image,
                                'description': response.packageInformation.package_descritpipn,
                                'sub_categories':packagesList
                            }

                            console_log(response.packageInformation.name)

                            data = [packageObject]

                            data = data.concat(response.data);

                        }

                        myContext.services = data;
                        // myContext.services = [...response.data];
                        myContext.discounts = response.discounts;
                        myContext.selectedService = 1;
                        myContext.cartvaluefromContext = 0
                        setServices(myContext.services);
                        saveObject(services_key, myContext.services)
                            .then(() => {
                            })
                            .catch((err) => {
                                console_log(err);
                            });

                    })
                    .catch((err) => {
                        setIsLoading(false);
                        console_log(err);

                    });
            })
            .catch((err) => {
                errorAlert(general_network_error);
                console_log(err);
            });
    };
    const getAllServices2 = () => {
        isNetConnected()
            .then(() => {
                // setIsLoading(true);
                getDataWithToken(services_url, 'GET', {})
                    .then((response) => {
                        // setIsLoading(false);

                        let data = [];

                        if (response.packageInformation !== undefined && response.packageInformation !== null) {

                            let packagesList = [];
                            if (response.packages !== undefined && response.packages !== null) {
                                response.packages.forEach((pItem) => {

                                    // finding add ons / package Services
                                    let packageServices=[];
                                    if (pItem.package_services !== undefined) {
                                        pItem.package_services.forEach((pi)=>{
                                            if (pi.package_sub_category !== undefined && pi.package_sub_category !== undefined){
                                                pi.package_sub_category.forEach((pSubCatItem)=>{

                                                    if (pSubCatItem.sub_category !== undefined && pSubCatItem.sub_category !== null){
                                                        packageServices.push(pSubCatItem.sub_category);
                                                    }

                                                })
                                            }
                                        })
                                    }




                                    let packageItem = {
                                        'id': pItem.id,
                                        'name': pItem.name,
                                        'type': pItem.type,
                                        'price': pItem.total_price,
                                        'addOns':packageServices

                                    }

                                    // console_log('package services')
                                    // console_log(packageItem)

                                    packagesList.push(packageItem)
                                })
                            }

                            let packageObject = {
                                'id': 1,
                                'name': response.packageInformation.name,
                                'thumbnail': response.packageInformation.thumbnail,
                                'sub_categories':packagesList
                            }

                            // console_log('package services')
                            // console_log(packageObject.sub_categories)

                            data = [packageObject]

                            data = data.concat(response.data);

                        }

                        // myContext.services = data;
                        // myContext.services = [...response.data];
                        myContext.discounts = response.discounts;
                        // myContext.selectedService = 1;
                        // myContext.cartvaluefromContext = 0
                        // setServices(myContext.services);
                        // saveObject(services_key, myContext.services)
                        //     .then(() => {
                        //     })
                        //     .catch((err) => {
                        //         console_log(err);
                        //     });

                    })
                    .catch((err) => {
                        // setIsLoading(false);
                        console_log(err);

                    });
            })
            .catch((err) => {
                errorAlert(general_network_error);
                console_log(err);
            });
    };


    return (
        <MyBackgroundImage>
            <TopBar title={'Services'} GoToCart={() => {
                navigation.navigate('CartDetails')
            }} />
            <View style={{flex: 1}}>
                <View style={styles.scrollViewInnerContainer}>
                    <FlatList
                        style={styles.servicesList}
                        data={services}

                        keyExtractor={(item, index) => item.id.toString()}
                        contentContainerStyle={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            paddingTop: dimen.app_padding,
                            paddingBottom: dimen.app_padding,
                        }}
                        numColumns={2}
                        bounces={false}
                        renderItem={({item}) => (<ServicesCard productOffersPress={servicePressed} data={item}/>)}
                        // refreshControl={
                        //     <RefreshControl
                        //         refreshing={isLoading}
                        //         onRefresh={getAllServices}
                        //     />
                        // }
                        ListEmptyComponent={<EmptyList reload={getAllServices}/>}

                        // ListHeaderComponent={() => {return (<FlatListHeader/>)}}
                    />
                </View>
                {isLoading && true && <Loader/>}
            </View>
        </MyBackgroundImage>

    );

};

export default (Services);


const styles = StyleSheet.create({
    dashboardButtonText: {fontFamily: 'roboto-bold', fontSize: 16},
    dashboardButton: {flex: 1, padding: 8, justifyContent: 'center', alignItems: 'center'},
    dashboardButtonContainer: {
        borderWidth: 1,
        borderColor: Colors.border_color,
        borderRadius: dimen.border_radius,
        overflow: 'hidden',
        backgroundColor: Colors.home_widget_background_color,
        margin: dimen.app_padding,
        justifyContent: 'space-evenly',
        alignItems: 'flex-start',
        flexDirection: 'row',

    },
    bannerAdStyle: {
        height: 90, backgroundColor: Colors.primary_color,
        padding: 20, margin: dimen.app_padding,
        justifyContent: 'center',
    },
    flatlistStyle: {
        // width: '100%',
        // backgroundColor: Colors.home_widget_background_color,
        marginBottom: dimen.bottom_margin_for_bottom_menu + 10,
        padding: dimen.app_padding,
    },
    flatlistContainerStyle: {
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        // backgroundColor:Colors.primary_dark_color
    },
    emptyComponentStyle: {
        justifyContent: 'flex-start',
        alignItems: 'center',
        // backgroundColor:Colors.primary_dark_color
    },

    //top bar
    topContainer: {
        height: 40,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.primary_color,
    },
    locationButtonContainer: {
        width: 12,
        height: 12,
        marginLeft: 10,
    },
    topHeading: {
        fontSize: 18,
        color: Colors.button_text_color,
    },
    locationHeading: {
        fontSize: 11,
        color: Colors.button_text_color,
    },
    locationContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        position: 'absolute',
        left: dimen.app_padding,
        right: dimen.app_padding,
    },

    //search bar
    searchContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    searchbarContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginLeft: dimen.app_padding * 3,
        marginRight: dimen.app_padding * 3,
        borderWidth: 1,
        borderRadius: 5,
        borderColor: Colors.light_border_color,
        backgroundColor: Colors.home_widget_background_color,
    },
    searchIcon: {
        width: 12,
        height: 12,
        margin: 5,
        marginLeft: 15,
    },
    searchField: {
        fontSize: 13,
        padding: 5,
        flexGrow: 1,
        color: Colors.light_border_color,
    },
    halfView: {
        height: 18,
        width: '100%',
        backgroundColor: Colors.primary_color,
        position: 'absolute',
        top: 0, left: 0, right: 0,

    },

    //banner
    carousalContainer: {
        height: 130,
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: Colors.home_widget_background_color,
        // margin: dimen.app_padding,
        // padding:dimen.app_padding,
        // paddingBottom:0

    },

    //sub categories
    subCategoriesMainContainer: {
        flex: 1,
    },
    categoryHeader: {
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.primary_color,

    },
    headerTextStyle: {
        fontSize: 16,
        color: Colors.button_text_color,
    },
    headerStartStyle: {
        width: 8,
        borderTopRightRadius: 4,
        borderBottomRightRadius: 4,
        backgroundColor: Colors.header_start_text,
        position: 'absolute',
        height: '100%',
        left: 0,
    },
    subCategoryContainer: {
        flexDirection: 'row',
        marginTop: 20,
        marginBottom: 20,
        marginLeft: 10,
        marginRight: 10,
        flexWrap: 'wrap',
        alignContent: 'flex-start',
    },

    scrollViewInnerContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        marginBottom: dimen.bottom_tab_height,
    },
    servicesList: {
        flex: 1,
        // justifyContent:'center',
        // alignItems:'center'
        // padding: dimen.app_padding - dimen.home_item_padding,
        // paddingTop:dimen.app_padding,
        // paddingBottom:dimen.app_padding,
    },


});































