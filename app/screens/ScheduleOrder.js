import React, {useState, useEffect, useCallback} from 'react';
import {
    StyleSheet, Alert,
    ScrollView, TouchableOpacity,Platform,
    View, Image,
} from 'react-native';
import {FlatList, TextInput} from 'react-native-gesture-handler';
import dimen from '../Styles/Dimen';

import {Loader} from '../Components/Loader';

import Colors from '../Styles/Colors';
import AppText from '../Components/AppText';
import MyBackgroundImage from '../Components/MyBackgroundImage';

import TopBar from '../Components/TopBar';

import MyTextButton from '../Components/MyTextButton';
import {DropDown, DropDownTime} from '../Components/DropDown';
import {
    console_log,
    errorAlert,
    getDataWithToken,
    getObject,
    isNetConnected,
    saveObject,
    successAlert,
} from '../Classes/auth';
import {cart_key, data_key, general_network_error, store_order_url, user_key} from '../Classes/UrlConstant';
import RNDateTimePicker from '@react-native-community/datetimepicker';
import MyStrings from '../Classes/MyStrings';
import {InputFieldStyle} from '../Styles/TextStyles';

const ScheduleOrder = ({navigation}) => {

    const [isLoading, setIsLoading] = useState(false);
    const [order, setOrder] = useState({});


    const [date1, setDate1] = useState('13/03/2020');
    const [time, setTime] = useState('--:-- --');
    const [mobile, setMobile] = useState('');
    const [address, setAddress] = useState('');
    const [alternateAddress, setAlternateAddress] = useState('');
    const [alternatAddressCheck, setAlternateAddressCheck] = useState(false);


    const [city, setCity] = useState('');
    const [area, setArea] = useState('');
    const [cityid, setCityid] = useState('');
    const [areaid, setAreaid] = useState('');
    const [notes, setNotes] = useState('');

    const [coupon, setCoupon] = useState(null);
    const [subtotal, setSubtotal] = useState(0);
    const [total, setTotal] = useState(0);


    const [persons, setPersons] = useState([{'id': 0, 'title': '1'}, {'id': 1, 'title': '2'},
        {'id': 2, 'title': '3'}, {'id': 3, 'title': '4'}, {'id': 4, 'title': '5'}, {'id': 5, 'title': '6'},
        {'id': 6, 'title': '7'}, {'id': 6, 'title': '8'}, {'id': 6, 'title': '9'}, {'id': 6, 'title': '10'}]);
    const [selectedPerson, setSelectedPerson] = useState(persons[0]);

    const [hours, setHours] = useState([{'id': 0, 'title': '1'}, {'id': 1, 'title': '2'},
        {'id': 2, 'title': '3'}, {'id': 3, 'title': '4'}, {'id': 4, 'title': '5'}, {'id': 5, 'title': '6'},
        {'id': 6, 'title': '7'}, {'id': 7, 'title': '8'}, {'id': 8, 'title': '9'}, {'id': 9, 'title': '10'},
        {'id': 10, 'title': '11'}, {'id': 11, 'title': '12'}]);
    const [selectedHours, setSelectedHours] = useState(hours[0]);

    const [minuts, setMinutes] = useState([{'id': 0, 'title': '00'}, {'id': 1, 'title': '15'},
        {'id': 2, 'title': '30'}, {'id': 3, 'title': '45'}]);
    const [selectedMinutes, setSelectedMinutes] = useState(minuts[0]);

    const [ampm, setAmpm] = useState([{'id': 0, 'title': 'AM'}, {'id': 1, 'title': 'PM'}]);
    const [selectedAmpm, setSelectedAmpm] = useState(ampm[0]);


    //region picker
    const [date, setDate] = useState(new Date());
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);

    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        setDate(currentDate);
        // console_log(currentDate);
        updateDate(currentDate);
    };

    const updateDate = (currentDate) => {

        let today = new Date();
        let hou = currentDate.getHours();
        if (today.getDate() === currentDate.getDate()) {
            hou = hou + 2;
        }
        if (hou > 12) {
            setSelectedAmpm(ampm[1]);
            setTime((hou - 12) + ':' + mints + ' ' + 'PM');
            hou = hou - 12;
        } else {
            setSelectedAmpm(ampm[0]);
            setTime((hou) + ':' + mints + ' ' + 'AM');
        }
        setSelectedHours(hours.find((h) => {
            return h.title === hou.toString();
        }));
        let mints = currentDate.getMinutes();

        if (mints > 0 && mints < 15) {
            setSelectedMinutes(minuts[0]);
        } else if (mints > 15 && mints < 30) {
            setSelectedMinutes(minuts[1]);
        } else if (mints > 30 && mints < 45) {
            setSelectedMinutes(minuts[2]);
        } else if (mints > 45 && mints < 59) {
            setSelectedMinutes(minuts[3]);
        }

        setTime(selectedHours.title + ':' + selectedMinutes.title + ' ' + selectedAmpm.title);


        // setSelectedMinutes(minuts[2])

    };

    const showMode = currentMode => {
        setShow(true);
        setMode(currentMode);
    };

    const showDatepicker = () => {
        showMode('date');
    };

    const showTimepicker = () => {
        showMode('time');
    };

    //endregion


    useEffect(() => {
        getObject(data_key)
            .then((res) => {
                if (!res) {
                } else {
                    setMobile(res.phone_number);
                    setAddress(res.address);
                    setCity(res.city_name);
                    setArea(res.area_name);

                    setCityid(res.city_id);
                    setAreaid(res.area_id);

                }
            })
            .catch((err) => {
                console_log(err);
            });

        getObject(cart_key)
            .then((res) => {
                if (!res) {
                } else {
                    // setOrder(res);
                    let services = res.map((item) => {
                        let addOns = item.addOns.map((aItem) => {
                            return (
                                {
                                    'id': aItem.id,
                                    'name': aItem.name,
                                    'amount': aItem.price,
                                    'duration': 0,
                                    'quantity': aItem.quantity,
                                    'subcategoryId': aItem.sub_category_id,
                                    'isSelected': true,
                                }
                            );
                        });
                        return (
                            {
                                'id': item.id,
                                'name': item.name,
                                'amount': parseInt(item.price),
                                'duration': 0,
                                'quantity': item.quantity,
                                'type': item.type,
                                'subcategoryId': item.service_id,
                                'isSelected': true,
                                'addOns': addOns,
                            }
                        );
                    });
                    setOrder(services);

                }
            })
            .catch((err) => {
                console_log(err);
            });

        updateDate(new Date());


    }, []);
    useEffect(() => {
        let order = navigation.getParam('order');
        setTotal(order.total);
        setSubtotal(order.subtotal);
        if (order.coupon !== '') {

            setCoupon(order.coupon);
        }
    }, []);


    const reviewOrder = (item) => {
        if (selectedAmpm.title === 'AM') {
            if (parseInt(selectedHours.title) < 9) {
                errorAlert('Operating hours are between 9:30 AM to 8:00 PM');
                return;
            } else if (parseInt(selectedHours.title) === 9) {
                if ((selectedMinutes.title) === '00' || (selectedMinutes.title) === '15') {
                    errorAlert('Operating hours are between 9:30 AM to 8:00 PM');
                    return;
                }
            }
        } else {
            if (parseInt(selectedHours.title) >= 8) {
                if (parseInt(selectedHours.title) === 8 && (selectedMinutes.title) === '00') {

                } else {
                    errorAlert('Operating hours are between 9:30 AM to 8:00 PM');
                    return;
                }

            }
        }
        let today = new Date();
        if (date.getDate() === today.getDate()) {
            // console_log('today');
            let currentSelectedHours = parseInt(selectedHours.title);
            let currentSelectedMinutes = parseInt(selectedMinutes.title);
            if (selectedAmpm.title === 'PM') {
                currentSelectedHours = currentSelectedHours + 12;
            }

            if (currentSelectedHours >= today.getHours() + 2) {
                if (currentSelectedHours === today.getHours() + 2) {
                    if (currentSelectedMinutes < today.getMinutes()) {
                        errorAlert('You cannot order before 2 hours from current time.');
                        return;
                    }
                }
            } else {
                errorAlert('You cannot order before 2 hours from current time.');
                return;
            }
        }


        let timeToSend = selectedHours.title + ':' + selectedMinutes.title + ' ' + selectedAmpm.title;

        isNetConnected()
            .then(() => {
                let orderObject = {
                    'services': order,

                    'noOfPeople': selectedPerson.title,
                    'specialInstructions': notes,
                    'requestedDatetime': (date.getDate()) + '/' + (date.getMonth() + 1) + '/' + date.getFullYear(),
                    'time': timeToSend,
                    'subtotal': subtotal,
                    'totalPrice': total,
                    'coupon_code': coupon,
                    'alternateAddress': alternateAddress,
                    'timeZone': 'Asia/Karachi',

                };
                console_log(orderObject);
                // return ;
                let formData = {
                    'finalOrder': JSON.stringify(orderObject),
                };

                setIsLoading(true);
                getDataWithToken(store_order_url, 'POST', formData)
                    .then((response) => {
                        setIsLoading(false);
                        console_log(response);
                        if (response.status === true) {
                            Alert.alert(
                                response.message,
                                response.message2,
                                [
                                    {
                                        text: 'Okay',
                                        onPress: () => {
                                            // console.log('Open pressed')
                                            saveObject(cart_key, []);
                                            navigation.popToTop();
                                        },
                                    },
                                ],
                                {cancelable: false});
                        } else {
                            errorAlert('Some error occurred.');
                        }
                    })
                    .catch((err) => {
                        setIsLoading(false);
                        console_log(err);

                    });
            })
            .catch((err) => {
                errorAlert(general_network_error);
            });
    };


    const alternatAddressCheckPress = () => {
        setAlternateAddressCheck(!alternatAddressCheck);
    };

    const hideDatePickerIOS =() => {
        setShow(false);
    }

    return (
        <MyBackgroundImage>
            <TopBar title={'Order Details'} backButton={() => navigation.pop()}/>
            <View style={styles.mainContainer} mainContainer>
                <ScrollView style={styles.scrollContainer}>

                    <View style={styles.inputFieldMainContainer}>
                        <AppText style={styles.inputFieldHeading}>
                            Date
                        </AppText>
                        <View style={styles.inputContainer}>
                            {/*<TextInput onChangeText={(email) => setDate(email)}*/}
                            {/*           value={date}*/}
                            {/*           placeholder={'dd/mm/yyyy'}*/}
                            {/*           autoCapitalize='none'*/}
                            {/*           style={styles.inputField}/>*/}
                            <TouchableOpacity style={{flex: 1}} onPress={showDatepicker}>
                                <AppText style={styles.dropDownText}>
                                    {date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear()}
                                </AppText>
                            </TouchableOpacity>

                        </View>
                        {show && Platform.OS==='ios' &&(
                            <View style={styles.datePickerContainerIOS}>
                                <TouchableOpacity onPress={hideDatePickerIOS}
                                                  style={styles.dateDoneContainer}>
                                    <AppText style={{color: Colors.primary_color, fontSize: 16}}>Done</AppText>
                                </TouchableOpacity>
                                <RNDateTimePicker
                                    testID="dateTimePicker"
                                    timeZoneOffsetInMinutes={0}
                                    value={date}
                                    mode={mode}
                                    minuteInterval={15}
                                    is24Hour={false}
                                    minimumDate={new Date()}
                                    display="default"
                                    onChange={onChange}
                                />
                            </View>
                        )}

                        {show && Platform.OS!== 'ios' &&(
                                <RNDateTimePicker
                                    testID="dateTimePicker"
                                    timeZoneOffsetInMinutes={0}
                                    value={date}
                                    mode={mode}
                                    minuteInterval={15}
                                    is24Hour={false}
                                    minimumDate={new Date()}
                                    display="default"
                                    onChange={onChange}
                                />
                        )}
                    </View>

                    <View style={styles.inputFieldMainContainer}>
                        <AppText style={styles.inputFieldHeading}>
                            Time (Operating hours are between 9:30 AM to 8:00PM)
                        </AppText>
                        <View style={styles.inputContainerTime}>
                            {false && <TouchableOpacity style={{flex: 1}} onPress={showTimepicker}>
                                <AppText style={styles.dropDownText}>{time}</AppText>
                            </TouchableOpacity>}
                            <DropDownTime itemsArray={hours}
                                          selectedItem={selectedHours}
                                          setSelectedItems={setSelectedHours}
                            />
                            <DropDownTime itemsArray={minuts}
                                          selectedItem={selectedMinutes}
                                          setSelectedItems={setSelectedMinutes}
                            />
                            <DropDownTime itemsArray={ampm}
                                          selectedItem={selectedAmpm}
                                          setSelectedItems={setSelectedAmpm}
                            />
                        </View>
                    </View>

                    <View style={styles.inputFieldMainContainer}>
                        <AppText style={styles.inputFieldHeading}>
                            Mobile Number
                        </AppText>
                        <View style={styles.inputContainer}>
                            <TextInput onChangeText={(email) => setMobile(email)}
                                       value={mobile}
                                       editable={false}
                                       placeholder={'mobile'}
                                       autoCapitalize='none'
                                       style={InputFieldStyle.inputField}/>
                        </View>
                    </View>

                    <View style={styles.inputFieldMainContainer}>
                        <AppText style={styles.inputFieldHeading}>
                            Address
                        </AppText>
                        <View style={styles.inputContainer}>
                            <TextInput onChangeText={(email) => setAddress(email)}
                                       value={address}
                                       editable={false}
                                       placeholder={'Address'}
                                       autoCapitalize='none'
                                       style={InputFieldStyle.inputField}/>
                        </View>
                    </View>


                    <View style={styles.alternateAddressContainer}>
                        <View style={styles.checkBoxMainContainer}>
                            {!alternatAddressCheck &&
                            <TouchableOpacity onPress={alternatAddressCheckPress}>
                                <View style={styles.checboxContainer}>
                                </View>
                            </TouchableOpacity>
                            }
                            {alternatAddressCheck &&
                            <TouchableOpacity onPress={alternatAddressCheckPress}>
                                <View style={styles.checkboxContainerNotselected}>
                                    <Image source={require('../Asset/tick.png')}
                                           style={styles.tickImageStyle}/>
                                </View>
                            </TouchableOpacity>
                            }
                        </View>

                        <AppText style={[styles.inputFieldHeading]}>
                            Use alternate address for this order
                        </AppText>
                    </View>


                    {alternatAddressCheck &&
                    <View style={styles.inputFieldMainContainer}>
                        <AppText style={styles.inputFieldHeading}>
                            Alternate Address
                        </AppText>
                        <View style={styles.inputContainer}>
                            <TextInput onChangeText={(email) => setAlternateAddress(email)}
                                       value={alternateAddress}
                                       multiline={true}
                                       numberOfLines={3}
                                       returnKeyType="done"
                                       blurOnSubmit={true}
                                       placeholder={'Alternate address for this order'}
                                       autoCapitalize='none'
                                       style={styles.inputFieldnotes}/>
                        </View>
                    </View>}

                    <View style={styles.inputFieldMainContainer}>
                        <AppText style={styles.inputFieldHeading}>
                            City
                        </AppText>
                        <View style={styles.inputContainer}>
                            <TextInput onChangeText={(email) => setCity(email)}
                                       value={city}
                                       editable={false}
                                       placeholder={'email'}
                                       autoCapitalize='none'
                                       style={InputFieldStyle.inputField}/>
                        </View>
                    </View>

                    <View style={styles.inputFieldMainContainer}>
                        <AppText style={styles.inputFieldHeading}>
                            Area
                        </AppText>
                        <View style={styles.inputContainer}>
                            <TextInput onChangeText={(email) => setArea(email)}
                                       value={area}
                                       editable={false}
                                       placeholder={'email'}
                                       autoCapitalize='none'
                                       style={InputFieldStyle.inputField}/>
                        </View>
                    </View>

                    <View style={styles.inputFieldMainContainer}>
                        <AppText style={styles.inputFieldHeading}>
                            Number of People
                        </AppText>
                        <DropDown itemsArray={persons}
                                  selectedItem={selectedPerson}
                                  setSelectedItems={setSelectedPerson}
                        />
                    </View>

                    <View style={styles.inputFieldMainContainer}>
                        <AppText style={styles.inputFieldHeading}>
                            Notes
                        </AppText>
                        <View style={styles.inputContainer}>
                            <TextInput onChangeText={(email) => setNotes(email)}
                                       value={notes}
                                       multiline={true}
                                       numberOfLines={3}
                                       returnKeyType="done"
                                       blurOnSubmit={true}
                                       placeholder={'notes...'}
                                       autoCapitalize='none'
                                       style={styles.inputFieldnotes}/>
                        </View>
                    </View>

                    {/*<MyTextButton onPress={reviewOrder}*/}
                    {/*              buttonText={'Schedule Order'}*/}
                    {/*              buttonContainerStyle={[{*/}
                    {/*                  marginTop: dimen.app_padding,*/}
                    {/*                  marginBottom: dimen.app_padding,*/}
                    {/*              }]}/>*/}

                    <View style={{flexDirection: 'row', justifyContent: 'space-evenly', margin: dimen.app_padding}}>
                        <View style={{flex: 1, backgroundColor: Colors.primary_color, marginRight: 5}}>
                            <MyTextButton onPress={() => {
                                saveObject(cart_key, []);
                                navigation.popToTop();
                            }}
                                          buttonText={'Cancel Order'}
                                          buttonContainerStyle={[{
                                              margin: 0,
                                              marginBottom: 0,
                                          }]}/>
                        </View>
                        <View style={{flex: 1, backgroundColor: Colors.primary_color, marginLeft: 5}}>
                            <MyTextButton onPress={reviewOrder}
                                          buttonText={'Schedule Order'}
                                          buttonContainerStyle={[{
                                              margin: 0, alignSelf: 'stretch',
                                              marginBottom: 0,
                                          }]}/>
                        </View>
                    </View>

                </ScrollView>
            </View>

            {isLoading && <Loader/>}
        </MyBackgroundImage>
    );

};

export default (ScheduleOrder);

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'stretch',
        // marginBottom: dimen.bottom_tab_height,
    },
    scrollContainer: {
        // flex: 1,
        paddingLeft: dimen.app_padding,
        paddingRight: dimen.app_padding,
        marginTop: dimen.app_padding,
        // marginBottom:dimen.app_padding,

        // paddingTop:dimen.app_padding,
        paddingBottom: dimen.app_padding,


    },
    container: {
        flex: 1,
        // backgroundColor: Colors.app_background_color,
        margin: dimen.signup_margin,
        justifyContent: 'flex-start',
        alignItems: 'stretch',

    },

    inputContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        borderColor: Colors.primary_color,
        backgroundColor: Colors.app_background_color,
        borderWidth: 1,
        paddingRight: dimen.app_padding,
        paddingLeft: dimen.app_padding,


    },
    inputContainerTime: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        borderColor: Colors.primary_color,
        backgroundColor: Colors.app_background_color,
        borderWidth: 1,
        paddingRight: dimen.app_padding,
        paddingLeft: dimen.app_padding,


    },
    inputFieldMainContainer: {
        marginLeft: dimen.app_padding,
        marginRight: dimen.app_padding,
        marginTop: 10,

    },
    userIconStyle: {
        width: 12,
        height: 12,
    },
    inputField: {
        marginRight: dimen.app_padding,
        marginLeft: dimen.home_item_padding,
        fontSize: 15,
        flexGrow: 1,


    }, inputFieldnotes: {
        marginRight: dimen.app_padding,
        marginLeft: dimen.home_item_padding,
        fontSize: 15,
        flexGrow: 1,
        textAlignVertical: 'top',
        height: 80,


    },
    inputFieldHeading: {
        fontSize: 14,
        color: Colors.primary_color,
        marginBottom: 5,

    },
    dropDownText: {
        fontSize: 14,
        marginTop: dimen.app_padding,
        marginBottom: dimen.app_padding,
        color: Colors.normal_text_color,
        marginLeft: 10,
    },


    alternateAddressContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginTop: 10,
        marginBottom: 5,
        marginLeft: dimen.app_padding,
    },
    termsContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        flex: 1,
        flexWrap: 'wrap',
    },
    checboxContainer: {
        width: 20,
        height: 20,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        // borderRadius: 5,
        borderColor: Colors.primary_color,
    },
    checkboxContainerNotselected: {
        width: 20,
        height: 20,
        // borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.primary_color,
    },
    tickImageStyle: {width: 10, height: 10, alignSelf: 'center', resizeMode: 'contain'},
    checkBoxMainContainer: {marginRight: 10, justifyContent: 'center', alignItems: 'center'},
    dateDoneContainer:{
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        padding: dimen.home_item_padding,
        marginTop: 5,
        marginRight: 5,
    },
    datePickerContainerIOS:{
        marginTop: 10,
        marginBottom: 10,
        borderWidth: 1,
        borderColor: Colors.primary_color,
    },

});


