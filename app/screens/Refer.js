import React, {useState, useEffect} from 'react';
import {
    Alert,
    Image,
    KeyboardAvoidingView,
    StyleSheet, SafeAreaView,
    Text, SectionList,
    Platform,
    TouchableOpacity, ScrollView,
    StatusBar,
    View, Animated,Share,
} from 'react-native';
import {FlatList, TextInput} from 'react-native-gesture-handler';
import TextStyles from '../Styles/TextStyles';
import dimen from '../Styles/Dimen';
import {
    _storeData, console_log, saveObject, successAlert,getObject
} from '../Classes/auth';

import {
    data_key,
    referal_key, referral_discount_key,
    web_view_url,
} from '../Classes/UrlConstant';
import {Loader} from '../Components/Loader';

import NetInfo from '@react-native-community/netinfo';
import Colors from '../Styles/Colors';
import AppText from '../Components/AppText';
import {CategoriesCard} from '../Components/Categories';
import MyImageButton, {MyImage} from '../Components/MyImageButton';
import MyBackgroundImage from '../Components/MyBackgroundImage';
import MyStrings from '../Classes/MyStrings';
import MyCarousel from '../Components/Carousel';
import BannerHome from '../Components/BannerHome';
import {ProductsCard} from '../Components/ProductsCard';
import {OffersCard} from '../Components/OffersCard';
import TopBar from '../Components/TopBar';
import {ProductOffersCard} from '../Components/ProductOffersCard';
import {MenuItemCard} from '../Components/MenuItemCard';
import MyTextButton from '../Components/MyTextButton';


export const Refer = ({navigation}) => {
    const [discountText,setDiscountText] = useState('')
    const [referalCode,setReferalCode] = useState('')
    const [referalCodeText,setReferalCodeText] = useState('')

    useEffect(()=>{
        getObject(data_key)
            .then((res)=>{
                setReferalCode(res.referral_code)
                let referMessage= 'Join me on Qalbish and get 15% off your first order: ' +
                    'For Website Registration use this link https://qalbish.pk/register/'+
                    res.referral_code +'  and For App Registration use this Referral Code: '+res.referral_code;
                setReferalCodeText(referMessage)
            })
            .catch((err)=>{
                console_log(err)
            })

        getObject(referral_discount_key)
            .then((res)=>{
                setDiscountText(res)
                // console_log(res)
            })
            .catch((err)=>{
                console_log(err)
            })
    },[])




    const shareCode = () => {
        // navigation.goBack();
    };
   const onShare = async () => {
        try {
            const result = await Share.share({
                message: referalCodeText ,
            });

            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            alert(error.message);
        }
    };


    return (
        <MyBackgroundImage>
            <TopBar title={'Refer'} GoToCart={() => {
                navigation.navigate('CartDetails')
            }}/>
            <View style={styles.mainContainer}>
                <ScrollView>
                    <View>
                        <View style={styles.headerImageContainer}>
                            <MyImage source={require('../Asset/sevice_image.jpg')}
                                     imageContainerStyle={styles.imageStyle}/>
                        </View>
                        <AppText style={styles.serviceNameHeading}>
                            Refer a friend and get 15% of your next order
                        </AppText>

                        <View style={styles.referCodeContainer}>
                            <AppText style={styles.referCodeText}>
                                {referalCode}
                            </AppText>
                        </View>
                        <MyTextButton onPress={onShare}
                                      buttonText={'Share Code'}
                                      buttonContainerStyle={[{
                                          margin: dimen.app_padding,
                                          // marginBottom:0
                                      }]}/>
                    </View>
                </ScrollView>
            </View>

        </MyBackgroundImage>
    );

};

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'stretch',
        // marginBottom: dimen.bottom_tab_height,
    },
    headerImageContainer: {
        height: 150,
        width: '100%',
        marginTop: 10,
    },
    imageStyle: {
        resizeMode: 'cover',
    },
    serviceNameHeading: {
        fontSize: 18,
        color: Colors.normal_text_color,
        margin: dimen.app_padding,
    },
    referCodeText: {
        fontSize: 24,
        color: Colors.primary_color,


        textAlign: 'center',

    },
    referCodeContainer: {
        marginLeft: dimen.app_padding * 2,
        marginRight: dimen.app_padding * 2,
        marginTop: dimen.app_padding,
        marginBottom: dimen.app_padding,

        borderWidth: 1,
        borderColor: Colors.primary_color,

        paddingLeft: dimen.app_padding,
        paddingRight: dimen.app_padding,
    },

});

