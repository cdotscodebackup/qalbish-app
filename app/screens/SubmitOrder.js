import React, {useState, useEffect, useCallback, useContext} from 'react';
import {
    Alert,
    Image,
    KeyboardAvoidingView,
    StyleSheet, SafeAreaView,
    Text,
    Platform, Animated,
    TouchableOpacity, ScrollView, TouchableWithoutFeedback,
    StatusBar, SectionList,
    View,
} from 'react-native';
import {FlatList, TextInput} from 'react-native-gesture-handler';
import dimen from '../Styles/Dimen';
import {EmptyList, Loader} from '../Components/Loader';
import Colors from '../Styles/Colors';
import AppText from '../Components/AppText';
import MyBackgroundImage from '../Components/MyBackgroundImage';
import TopBar from '../Components/TopBar';


import MyTextButton from '../Components/MyTextButton';
import {MenuItemCard} from '../Components/MenuItemCard';
import {CartItemCard} from '../Components/CartItemCard';
import {console_log, errorAlert, getDataWithToken, getObject, isNetConnected, saveObject} from '../Classes/auth';
import {cart_key, coupon_apply_url, general_network_error, services_key, services_url} from '../Classes/UrlConstant';
import {useReduxContext} from 'react-redux/lib/hooks/useReduxContext';
import {MyContext} from '../Classes/AppContext';
import MyStrings from '../Classes/MyStrings';


const SubmitOrder = ({navigation}) => {


    const [categories, setCategories] = useState([]);


    const [selectedService, setSelectedService] = useState(0);
    const [isLoading, setIsLoading] = useState(false);

    const [coupon, setCoupon] = useState('');
    const [total, setTotal] = useState(0);

    const myContext = useContext(MyContext);

    useEffect(() => {
        getObject(cart_key)
            .then((res) => {
                if (!res) {
                } else {
                    setCategories(res);
                    let t= 0;
                    res.forEach((item)=>{
                        t= t + ((parseInt(item.price) * item.quantity))
                           item.addOns.forEach((ite)=>{
                               t=t+ ((parseInt(ite.price) * ite.quantity))
                           })
                        console_log(total)
                    })
                    setTotal(t)
                }
            });
        console_log(myContext.discounts);


    }, []);


    const productOffersPress = (item) => {
        navigation.navigate('ProductOffers');
    };
    const reviewOrder = (item) => {
        navigation.navigate('ScheduleOrder');
    };
    const applyCoupon = (item) => {
        if (coupon=== ''){
            errorAlert('Enter coupon code.')
            return;
        }
        applyCouponApi();

    };
    const applyCouponApi = () => {
        isNetConnected()
            .then(() => {
                setIsLoading(true);
                let data={
                    'coupon_code':coupon,
                    'sub_total':12312,
                }
                getDataWithToken(coupon_apply_url, 'POST', {})
                    .then((response) => {
                        // setIsLoading(false);
                        // myContext.services = [...response.data];
                        // myContext.discounts = response.discounts;
                        // myContext.selectedService = 1;
                        // setServices(myContext.services);
                        // saveObject(services_key, response.data)
                        //     .then(()=>{})
                        //     .catch((err)=>{console_log(err)})

                    })
                    .catch((err) => {
                        setIsLoading(false);
                        console_log(err);

                    });
            })
            .catch((err) => {
                errorAlert(general_network_error);
                console_log(err);
            });
    };


    const renderCategory = (CATEGORY) => {
        return CATEGORY.map((item, i) => {
            return (
                <TouchableOpacity
                    key={i}
                    onPress={() => {
                        setSelectedService(i);
                    }}>
                    <AppText style={[styles.serviceNameText]}>
                        {item}
                    </AppText>
                    {selectedService === i &&
                    <View style={styles.serviceNameSelectedBottomLine}/>}
                </TouchableOpacity>
            );
        });
    };

    return (
        <MyBackgroundImage>
            <TopBar title={'Services'} backButton={() => navigation.pop()}/>
            <View style={styles.mainContainer}>

                <FlatList
                    style={styles.cartItemList}
                    data={categories}
                    keyExtractor={(item, index) => item.id.toString()}
                    // contentContainerStyle={{margin:dimen.app_padding}}
                    numColumns={1}
                    bounces={false}
                    renderItem={({item}) => (
                        <CartItemCard productOffersPress={productOffersPress} data={item}/>)}
                    // ListHeaderComponent={() => {return (<FlatListHeader/>)}}
                    ListEmptyComponent={<EmptyList/>}
                    ListFooterComponent={
                        <View>
                            {myContext.discounts.coupon_discount !== undefined && myContext.discounts.coupon_discount !== null &&
                            <View style={styles.orderSummaryContainer}>
                                <View style={styles.emailOrPhoneContainer}>
                                    <TextInput onChangeText={(c) => setCoupon(c)}
                                               value={coupon}
                                               placeholder={'coupon'}
                                               // maxLength={10}
                                               autoCapitalize='none'
                                               style={styles.inputField}/>
                                </View>
                                <TouchableOpacity onPress={applyCoupon} style={{backgroundColor:Colors.primary_color,padding:7,}}>
                                    <AppText style={{color:Colors.app_background_color,fontSize:10,}}>Apply</AppText>

                                </TouchableOpacity>
                            </View>}
                        </View>
                    }

                />

                <View style={{}}>
                    {myContext.discounts.first_order_discount !== undefined && myContext.discounts.first_order_discount !== null &&
                    <View style={styles.orderSummaryContainer}>
                        <AppText>First Order Discount </AppText>
                        <AppText>2000</AppText>
                    </View>}
                    {myContext.discounts.membership_discount !== undefined && myContext.discounts.membership_discount !== null &&
                    <View style={styles.orderSummaryContainer}>
                        <AppText>Membership Discount </AppText>
                        <AppText>2000</AppText>
                    </View>}
                    {myContext.discounts.referral_discount !== undefined && myContext.discounts.referral_discount !== null &&
                    <View style={styles.orderSummaryContainer}>
                        <AppText>Referral Discount </AppText>
                        <AppText>2000</AppText>
                    </View>}
                    <View style={styles.orderSummaryContainer}>
                        <AppText>Total </AppText>
                        <AppText>{total}</AppText>
                    </View>
                    <MyTextButton onPress={reviewOrder}
                                  buttonText={'Schedule Order'}
                                  buttonContainerStyle={[styles.loginButton, {
                                      margin: 0,
                                      marginBottom: 0,
                                  }]}/>

                </View>

            </View>

            {isLoading && <Loader/>}
        </MyBackgroundImage>
    );

};

export default (SubmitOrder);


const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'stretch',
        margin: dimen.app_padding,
        borderWidth: 1,
        borderColor: Colors.primary_color,

        // marginBottom: dimen.bottom_tab_height,
    },
    servicesScrollbar: {
        height: 40,
        backgroundColor: Colors.primary_color,
        paddingLeft: dimen.app_padding,
        paddingRight: dimen.app_padding,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',

    },
    headerImageContainer: {
        height: 150,
        width: '100%',
        marginTop: 10,
    },
    imageStyle: {
        resizeMode: 'cover',
    },
    dropdownIcon: {
        width: 10,
        height: 10,
        resizeMode: 'contain',
    },
    orderSummaryContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        // margin: dimen.app_padding,
        marginBottom: 0,
        padding: 10,

    },
    serviceNameItemContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    serviceNameText: {
        fontSize: 18,
        // color: this.state.currentIndex === i ? "#F08C4F" : "white",
        paddingHorizontal: 10,
        color: Colors.button_text_color,
    },
    serviceNameSelectedBottomLine: {
        width: '80%',
        height: 1,
        backgroundColor: Colors.button_text_color,
        marginLeft: 10,
        marginRight: 10,
        alignSelf: 'center',
    },
    serviceNameHeading: {
        fontSize: 18,
        fontWeight: 'bold',
        color: Colors.normal_text_color,
        margin: dimen.app_padding,
    },

    menuItemList: {
        flex: 1,
        padding: dimen.app_padding - dimen.home_item_padding,
        borderWidth: 1,
        borderColor: Colors.primary_color,
        marginLeft: dimen.app_padding,
        marginRight: dimen.app_padding,
        marginTop: dimen.app_padding,
    },


    orderSummaryPopupContainer: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: Colors.app_background_color,
        borderWidth: 1,
        borderColor: Colors.primary_color,
        height: dimen.orderPopupHeight,

    },
    emailOrPhoneContainer: {
        // flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        borderColor: Colors.primary_color,
        backgroundColor: Colors.app_background_color,
        borderWidth: 1,
        // borderRadius: dimen.login_border_radius,
        paddingRight: dimen.app_padding,
        paddingLeft: dimen.app_padding,
    },
    inputField: {
        // marginRight: dimen.app_padding,
        // marginLeft: dimen.app_padding - 4,
        fontSize: 9,
        padding:0,
        // flexGrow: 1,

    },


});































