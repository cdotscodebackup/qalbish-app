import React, {useState, useEffect} from 'react';
import {
    Alert,
    Image,
    KeyboardAvoidingView,
    StyleSheet, SafeAreaView,
    Text, SectionList,
    Platform,
    TouchableOpacity, ScrollView, RefreshControl,
    StatusBar,
    View,
} from 'react-native';
import {FlatList, TextInput} from 'react-native-gesture-handler';
import TextStyles from '../Styles/TextStyles';
import dimen from '../Styles/Dimen';
import {
    _storeData, console_log, errorAlert, getDataWithToken, isNetConnected, successAlert,
} from '../Classes/auth';

import {
    general_network_error, orders_url,
    services_url, staff_all_orders_url,
    web_view_url,
} from '../Classes/UrlConstant';
import Colors from '../Styles/Colors';
import AppText from '../Components/AppText';
import MyBackgroundImage from '../Components/MyBackgroundImage';
import TopBar from '../Components/TopBar';
import {OrderItemCard} from '../Components/OrderItemCard';
import {EmptyList} from '../Components/Loader';
import {StaffOrderItemCard} from '../Components/StaffOrderItemCard';

export const StaffProcessing = ({navigation}) => {

    const [isLoading, setIsLoading] = useState(false);
    const [ordersList, setOrderList] = useState([]);
    const [filteredOrdersList, setFilteredOrderList] = useState([]);

    useEffect(() => {
        getAllOrders();
    }, []);

    useEffect(() => {
       let listener = navigation.addListener(
            'willFocus',
            () => {
                getAllOrders();
            });

        return function cleanup() {
            listener.remove();
        };

    }, []);
    const orderPressed = (item) => {
        navigation.navigate('StaffOrderProcessing', {'order': item});

    };


    const getAllOrders = () => {
        isNetConnected()
            .then(() => {
                setIsLoading(true);
                getDataWithToken(staff_all_orders_url, 'GET', {})
                    .then((response) => {
                        setIsLoading(false);
                        // setOrderList(response.data);
                        setFilteredOrderList(response.data.filter((item) => item.staff_status === 'accepted'));
                        // setFilteredOrderList(response.data);
                    })
                    .catch((err) => {
                        setIsLoading(false);
                        console_log(err);
                    });
            })
            .catch((err) => {
                errorAlert(general_network_error);
            });
    };


    return (
        <MyBackgroundImage>
            <TopBar title={'Bookings'}/>
            <View style={styles.scrollViewInnerContainer}>
                <FlatList
                    style={styles.productListStyle}
                    data={filteredOrdersList}
                    keyExtractor={(item, index) => item.id.toString()}
                    numColumns={1}
                    bounces={false}
                    renderItem={({item}) => (
                        <StaffOrderItemCard productOffersPress={() => orderPressed(item)} data={item}/>)}
                    ListEmptyComponent={<EmptyList reload={getAllOrders}/>}
                    refreshControl={
                        <RefreshControl
                            refreshing={isLoading}
                            onRefresh={getAllOrders}
                        />
                    }
                />
            </View>
        </MyBackgroundImage>
    );

};

const styles = StyleSheet.create({
    dashboardButtonText: {fontFamily: 'roboto-bold', fontSize: 16},
    dashboardButton: {flex: 1, padding: 8, justifyContent: 'center', alignItems: 'center'},
    dashboardButtonContainer: {
        borderWidth: 1,
        borderColor: Colors.border_color,
        borderRadius: dimen.border_radius,
        overflow: 'hidden',
        backgroundColor: Colors.home_widget_background_color,
        margin: dimen.app_padding,
        justifyContent: 'space-evenly',
        alignItems: 'flex-start',
        flexDirection: 'row',

    },
    bannerAdStyle: {
        height: 90, backgroundColor: Colors.primary_color,
        padding: 20, margin: dimen.app_padding,
        justifyContent: 'center',
    },
    flatlistStyle: {
        // width: '100%',
        // backgroundColor: Colors.home_widget_background_color,
        // marginBottom: dimen.bottom_margin_for_bottom_menu + 20,
        padding: dimen.app_padding,
    },
    productFlatList: {
        width: '100%',
        backgroundColor: Colors.primary_color,
        // marginBottom: dimen.bottom_margin_for_bottom_menu + 20,
        // padding: dimen.app_padding,
    },
    flatlistContainerStyle: {
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        backgroundColor: Colors.primary_dark_color,
    },
    emptyComponentStyle: {
        justifyContent: 'flex-start',
        alignItems: 'center',
        // backgroundColor:Colors.primary_dark_color
    },


    //banner
    carousalContainer: {
        height: 130,
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: Colors.home_widget_background_color,
        // margin: dimen.app_padding,
        // padding:dimen.app_padding,
        // paddingBottom:0

    },

    scrollViewContainer: {flex: 1, marginBottom: dimen.bottom_tab_height},
    scrollViewInnerContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        marginBottom: dimen.bottom_tab_height,
    },
    productListStyle: {
        flex: 1,
        padding: dimen.app_padding - dimen.home_item_padding,
        marginBottom: 10,
    },

    //sub categories
    subCategoriesMainContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'stretch',
    },
    categoryHeader: {
        height: 40,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.primary_color,

    },
    headerTextStyle: {
        fontSize: 16,
        marginLeft: 15,
        color: Colors.button_text_color,
    },
    filterIconContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 15,
    },
    filterIconStyle: {
        width: 20,
        height: 20,
        marginLeft: 10,
    },
    headerStartStyle: {
        width: 8,
        borderTopRightRadius: 4,
        borderBottomRightRadius: 4,
        backgroundColor: Colors.header_start_text,
        position: 'absolute',
        height: '100%',
        left: 0,
    },
    subCategoryContainer: {
        flexDirection: 'row',
        marginTop: 20,
        marginBottom: 20,
        marginLeft: 10,
        marginRight: 10,
        flexWrap: 'wrap',
        alignContent: 'flex-start',
    },


    selectedButtonStyle: {
        backgroundColor: Colors.app_background_color,
        borderColor: Colors.primary_color,
        borderWidth: 1,
        width: 100,
    },
    unSelectedButtonStyle: {
        backgroundColor: Colors.primary_color,
        borderColor: Colors.primary_color,
        borderWidth: 1,
        width: 100,

    },


});
