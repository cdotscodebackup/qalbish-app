import React, {useState, useEffect, useCallback, useContext} from 'react';
import {
    Alert,
    Image,
    KeyboardAvoidingView,
    StyleSheet, SafeAreaView,
    Text,
    Platform, Animated,
    TouchableOpacity, ScrollView, TouchableWithoutFeedback,
    StatusBar, SectionList,
    View,
} from 'react-native';
import {FlatList, TextInput} from 'react-native-gesture-handler';
import dimen from '../Styles/Dimen';
import {EmptyList, Loader} from '../Components/Loader';
import Colors from '../Styles/Colors';
import AppText from '../Components/AppText';
import MyBackgroundImage from '../Components/MyBackgroundImage';
import TopBar from '../Components/TopBar';


import MyTextButton from '../Components/MyTextButton';
import {CartItemCard} from '../Components/CartItemCard';
import {
    console_log,
    errorAlert,
    getDataWithToken,
    getObject,
    isNetConnected, saveObject,
} from '../Classes/auth';
import {
    cart_key,
    coupon_apply_url, data_key,
    general_network_error,

} from '../Classes/UrlConstant';
import {MyContext} from '../Classes/AppContext';
import {InputFieldStyle} from '../Styles/TextStyles';


const CartDetails = ({navigation}) => {


    const [categories, setCategories] = useState([]);


    const [selectedService, setSelectedService] = useState(0);
    const [isLoading, setIsLoading] = useState(false);

    const [couponToSend, setCouponToSend] = useState('');
    const [coupon, setCoupon] = useState('');
    const [shouldApplyCoupon, setShouldApplyCoupon] = useState(true);
    const [total, setTotal] = useState(0);
    const [grandTotal, setGrandTotal] = useState(0);
    const [grandGrandTotal, setGrandGrandTotal] = useState(0);

    const [deliveryCharges, setDeliveryCharges] = useState('0');

    const [couponDiscount, setCouponDiscount] = useState(null);
    const [membershipDiscount, setMembershipDiscount] = useState(null);
    const [firstOrderDiscount, setFirstOrderDiscount] = useState(null);
    const [referralDiscount, setReferralDiscount] = useState(null);

    const myContext = useContext(MyContext);

    useEffect(() => {
        getObject(cart_key)
            .then((res) => {
                if (!res) {
                } else {
                    setCategories(res);
                    let t = 0;
                    res.forEach((item) => {
                        t = t + ((parseInt(item.price) * item.quantity));
                        item.addOns.forEach((ite) => {
                            t = t + ((parseInt(ite.price) * ite.quantity));
                        });
                    });

                    setTotal(t);
                    let totalValue = t;

                    if (myContext.discounts) {
                        if (myContext.discounts.membership_discount !== undefined && myContext.discounts.membership_discount !== null) {
                            if (myContext.discounts.membership_discount.type === 'percentage') {
                                let percent = myContext.discounts.membership_discount.percent_off;
                                let percentAmount = totalValue * (percent / 100);
                                setMembershipDiscount(Math.floor(percentAmount));
                                totalValue = totalValue - percentAmount;
                            } else {
                                let percent = myContext.discounts.membership_discount.value;
                                setMembershipDiscount(Math.floor(percent));
                                totalValue = totalValue - percent;
                            }
                        }

                        if (myContext.discounts.feferral_discount !== undefined && myContext.discounts.feferral_discount !== null) {
                            if (myContext.discounts.feferral_discount.type === 'percentage') {

                                let percent = myContext.discounts.feferral_discount.percent_off;
                                let percentAmount = totalValue * (percent / 100);
                                setReferralDiscount(Math.floor(percentAmount));
                                totalValue = totalValue - percentAmount;

                            } else {

                                let percent = myContext.discounts.feferral_discount.value;
                                setReferralDiscount(Math.floor(percent));
                                totalValue = totalValue - percent;
                            }
                        }
                        if (myContext.discounts.first_order_discount !== undefined && myContext.discounts.first_order_discount !== null) {
                            if (myContext.discounts.first_order_discount.type === 'percentage') {

                                let percent = myContext.discounts.first_order_discount.percent_off;
                                let percentAmount = totalValue * (percent / 100);
                                setFirstOrderDiscount(Math.floor(percentAmount));
                                totalValue = totalValue - percentAmount;

                            } else {

                                let percent = myContext.discounts.first_order_discount.value;
                                setFirstOrderDiscount(Math.floor(percent));
                                totalValue = totalValue - percent;

                            }
                        }
                    }
                    setGrandTotal(totalValue);
                }
            })
            .catch((err) => console_log(err))
        ;
        getObject(data_key)
            .then((res) => {
                if (!res) {
                } else {
                    setDeliveryCharges(res.delivery_charges);
                }
            })
            .catch((err) => console_log(err));
        // applyDiscounts()


    }, []);

    useEffect(() => {
    }, [total]);

    useEffect(() => {
        setGrandGrandTotal(grandTotal + parseInt(deliveryCharges));
    }, [grandTotal, deliveryCharges]);


    const productOffersPress = (item) => {
        navigation.navigate('ProductOffers');
    };
    const scheduleOrder = (item) => {
        if (total < 1000) {
            errorAlert('Minimum Order is Rs.1000/-')
            return;
        }
        navigation.navigate('ScheduleOrder',
            {
                'order': {
                    'total': grandGrandTotal,
                    'subtotal': total,
                    'coupon': couponToSend,
                },
            });
    };
    const parseError = (error) => {
        var m = error;
        var keys = [];
        for (var k in m) keys.push(k);
        console_log(keys)

        var mforuser = '';
        // console.log('key 0 ' + (m[keys[0]]))

        for (var k in keys)
            mforuser = mforuser + m[keys[k]] + '\n'

        return mforuser;
    }

    const applyCoupon = (item) => {
        if (coupon === '') {
            errorAlert('Enter coupon code.');
            return;
        }

        isNetConnected()
            .then(() => {
                setIsLoading(true);
                let data = {
                    'coupon_code': coupon,
                    'sub_total': grandTotal,
                };
                getDataWithToken(coupon_apply_url, 'POST', data)
                    .then((response) => {
                        setIsLoading(false)
                        setCoupon('')
                        console_log(response)
                        if (response.data !== undefined) {
                            setShouldApplyCoupon(false)
                            if (response.data.type === 'percentage') {
                                let percent = response.data.discount;
                                let percentAmount = percent//totalValue * (percent / 100);
                                setCouponDiscount(percentAmount);
                                // totalValue = totalValue - percentAmount;
                                setGrandTotal(grandTotal - percentAmount)
                            } else {
                                console_log('fixed price coupon')
                                console_log(response.data.fixed_price)
                                let amount = response.data.fixed_price !== null ? response.data.fixed_price : 0;
                                setCouponDiscount(amount);
                                setGrandTotal(grandTotal - amount)
                            }
                        } else if (response.error !== undefined) {
                            errorAlert(parseError(response.error))
                            setCouponToSend('')
                        } else if (response.message !== undefined) {
                            errorAlert(response.message)
                            setCouponToSend('')
                        }


                    })
                    .catch((err) => {
                        setIsLoading(false);
                        console_log(err);

                    });
            })
            .catch((err) => {
                errorAlert(general_network_error);
                console_log(err);
            });
    };


    return (
        <MyBackgroundImage>
            <TopBar title={'Services'} backButton={() => navigation.pop()}/>
            <View style={styles.mainContainer}>

                <FlatList
                    style={styles.cartItemList}
                    data={categories}
                    keyExtractor={(item, index) => item.id.toString()}
                    numColumns={1}
                    bounces={false}
                    renderItem={({item}) => (
                        <CartItemCard productOffersPress={productOffersPress} data={item}/>)}
                    ListEmptyComponent={<EmptyList text={'Cart is empty'}/>}
                    ListFooterComponent={
                        <View>
                            {myContext.discounts.coupon_discount !== undefined &&
                            myContext.discounts.coupon_discount !== null &&
                            myContext.discounts.coupon_discount === true &&
                            shouldApplyCoupon &&
                            <View style={styles.orderSummaryContainer}>
                                <View style={styles.emailOrPhoneContainer}>
                                    <TextInput onChangeText={(c) => {
                                        setCoupon(c);
                                        setCouponToSend(c);
                                    }}
                                               value={coupon}
                                               placeholder={'coupon'}
                                               // maxLength={10}
                                               autoCapitalize='none'
                                               style={InputFieldStyle.couponInputField}/>
                                </View>
                                <TouchableOpacity onPress={applyCoupon}
                                                  style={{backgroundColor: Colors.primary_color, padding: 7}}>
                                    <AppText style={{color: Colors.app_background_color, fontSize: 10}}>Apply</AppText>

                                </TouchableOpacity>
                            </View>}
                        </View>
                    }

                />

                <View style={{}}>

                    <View style={styles.orderSummaryContainer}>
                        <AppText>Total </AppText>
                        <AppText>Rs. {total}</AppText>
                    </View>

                    {myContext.discounts.first_order_discount !== undefined && myContext.discounts.first_order_discount !== null &&
                    <View style={styles.orderSummaryContainer}>
                        <AppText>First Order Discount </AppText>
                        <AppText>-Rs. {firstOrderDiscount}</AppText>
                    </View>}
                    {myContext.discounts.membership_discount !== undefined && myContext.discounts.membership_discount !== null &&
                    <View style={styles.orderSummaryContainer}>
                        <AppText>Membership Discount </AppText>
                        <AppText>-Rs. {membershipDiscount}</AppText>
                    </View>}
                    {myContext.discounts.referral_discount !== undefined && myContext.discounts.referral_discount !== null &&
                    <View style={styles.orderSummaryContainer}>
                        <AppText>Referral Discount </AppText>
                        <AppText>-Rs. {referralDiscount}</AppText>
                    </View>}


                    {couponDiscount !== null && <View style={styles.orderSummaryContainer}>
                        <AppText>Coupon Discount </AppText>
                        <AppText>-Rs. {couponDiscount}</AppText>
                    </View>}

                    <View style={styles.orderSummaryContainer}>
                        <AppText>Delivery Charges </AppText>
                        <AppText>+Rs. {deliveryCharges}</AppText>
                    </View>

                    <View style={styles.orderSummaryContainer}>
                        <AppText>Grand Total</AppText>
                        <AppText>Rs. {grandGrandTotal}</AppText>
                    </View>



                </View>


            </View>

            <View style={{flexDirection: 'row', justifyContent: 'space-evenly',margin:dimen.app_padding,marginTop:0,}}>
                <View style={{flex:1,backgroundColor:Colors.primary_color,marginRight:5,}}>
                    <MyTextButton onPress={()=>{
                        saveObject(cart_key,[]);
                        navigation.popToTop();
                    }}
                                  buttonText={'Cancel Order'}
                                  buttonContainerStyle={[{
                                      margin: 0,
                                      marginBottom: 0,
                                  }]}/>
                </View>
                <View style={{flex:1,backgroundColor:Colors.primary_color,marginLeft:5}}>
                    <MyTextButton onPress={scheduleOrder}
                                  buttonText={'Schedule Order'}
                                  buttonContainerStyle={[{
                                      margin: 0, alignSelf: 'stretch',
                                      marginBottom: 0,
                                  }]}/>
                </View>
            </View>


            {isLoading && <Loader/>}
        </MyBackgroundImage>
    );

};

export default (CartDetails);


const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'stretch',
        margin: dimen.app_padding,
        borderWidth: 1,
        borderColor: Colors.primary_color,

        // marginBottom: dimen.bottom_tab_height,
    },
    servicesScrollbar: {
        height: 40,
        backgroundColor: Colors.primary_color,
        paddingLeft: dimen.app_padding,
        paddingRight: dimen.app_padding,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',

    },
    headerImageContainer: {
        height: 150,
        width: '100%',
        marginTop: 10,
    },
    imageStyle: {
        resizeMode: 'cover',
    },
    dropdownIcon: {
        width: 10,
        height: 10,
        resizeMode: 'contain',
    },
    orderSummaryContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        // margin: dimen.app_padding,
        marginBottom: 0,
        padding: 10,

    },
    serviceNameItemContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    serviceNameText: {
        fontSize: 18,
        // color: this.state.currentIndex === i ? "#F08C4F" : "white",
        paddingHorizontal: 10,
        color: Colors.button_text_color,
    },
    serviceNameSelectedBottomLine: {
        width: '80%',
        height: 1,
        backgroundColor: Colors.button_text_color,
        marginLeft: 10,
        marginRight: 10,
        alignSelf: 'center',
    },
    serviceNameHeading: {
        fontSize: 18,
        fontWeight: 'bold',
        color: Colors.normal_text_color,
        margin: dimen.app_padding,
    },

    menuItemList: {
        flex: 1,
        padding: dimen.app_padding - dimen.home_item_padding,
        borderWidth: 1,
        borderColor: Colors.primary_color,
        marginLeft: dimen.app_padding,
        marginRight: dimen.app_padding,
        marginTop: dimen.app_padding,
    },


    orderSummaryPopupContainer: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: Colors.app_background_color,
        borderWidth: 1,
        borderColor: Colors.primary_color,
        height: dimen.orderPopupHeight,

    },
    emailOrPhoneContainer: {
        // flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        borderColor: Colors.primary_color,
        backgroundColor: Colors.app_background_color,
        borderWidth: 1,
        // borderRadius: dimen.login_border_radius,
        paddingRight: dimen.app_padding,
        paddingLeft: dimen.app_padding,
    },
    inputField: {
        // marginRight: dimen.app_padding,
        // marginLeft: dimen.app_padding - 4,
        fontSize: 9,
        padding: 0,
        // flexGrow: 1,
    },


});































