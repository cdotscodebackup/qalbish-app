import React, {useState, useEffect} from 'react';
import {
    Alert,
    Image,
    KeyboardAvoidingView,
    StyleSheet, SafeAreaView,
    Text, SectionList,
    Platform,
    TouchableOpacity, ScrollView,
    StatusBar,
    View,
} from 'react-native';
import {FlatList, TextInput} from 'react-native-gesture-handler';
import TextStyles from '../Styles/TextStyles';
import dimen from '../Styles/Dimen';
import Colors from '../Styles/Colors';
import AppText from '../Components/AppText';
import {CategoriesCard} from '../Components/Categories';
import MyImageButton, {MyImage} from '../Components/MyImageButton';
import MyBackgroundImage from '../Components/MyBackgroundImage';

import TopBar from '../Components/TopBar';
import {OrderItemCard} from '../Components/OrderItemCard';
import {StatusView} from '../Components/StatusView';
import {Rating, AirbnbRating} from 'react-native-ratings';
import MyTextButton from '../Components/MyTextButton';
import {console_log, errorAlert, getDataWithToken, isNetConnected} from '../Classes/auth';
import {general_network_error, get_staff_lat_lon_url, orders_url, staff_rating_url} from '../Classes/UrlConstant';
import {Loader} from '../Components/Loader';
import MapView from 'react-native-maps';
import {Marker} from 'react-native-maps';

export const TrackStaff = ({navigation}) => {

    const [isLoading, setIsLoading] = useState(false);
    const [order, setOrder] = useState({});
    const [orderStaff, setOrderStaff] = useState(null);
    const [lat, setLat] = useState(0.0);
    const [lon, setLon] = useState(0.0);
    const [latlong, setLatlong] = useState({latitude:0.0, longitude:0.0});


    useEffect(() => {
        let data = navigation.getParam('data');
        console_log(data)
        setLat(parseFloat(data.latitude));
        setLon(parseFloat(data.longitude))
        setLatlong({latitude:parseFloat(data.latitude),longitude: parseFloat(data.longitude)})


    }, []);


    const backPress = () => {
        navigation.pop();
    };

    const trackStaff = () => {
        getLatLong();
    };
    const getLatLong = () => {
        isNetConnected()
            .then(() => {
                setIsLoading(true);
                let formData = {
                    staff_id: 52,
                }
                getDataWithToken(get_staff_lat_lon_url, 'POST', formData)
                    .then((response) => {
                        setIsLoading(false);
                        // console_log(response)

                    })
                    .catch((err) => {
                        setIsLoading(false);
                        console_log(err);

                    });
            })
            .catch((err) => {
                errorAlert(general_network_error);
            });
    };

    if (lat===0.0)
        return <View></View>
    else
        return (
            <View style={{ left:0, right: 0, top:0, bottom: 0, position: 'absolute' }}>
                <MapView
                    style={{ left:0, right: 0, top:0, bottom: 0, position: 'absolute' }}
                    initialRegion={{
                        latitude: lat,
                        longitude: lon,
                        latitudeDelta: 0.0222,
                        longitudeDelta: 0.0121,
                    }}
                >
                    {
                        <Marker
                            style={{width:40,height:40} }
                            coordinate={latlong}
                            title={'Qalbish'}
                            description={'Qalbish staff is on its way to you.'}
                            // image={require('../Asset/location.png')}
                        />
                    }

                </MapView>
                <TopBar title={'Track Staff'} backButton={backPress}/>
            </View>
        // </MyBackgroundImage>
    );

};

const styles = StyleSheet.create({

    scrollViewInnerContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        marginBottom: dimen.bottom_tab_height,
    },
    containerStyle: {
        // flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        // borderWidth: 1,borderColor: 'black'
        margin: dimen.home_item_padding,
        padding: dimen.home_item_padding,
    },
    nameContainer: {
        flex: 2,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        padding: 7,
    },

    dateTimeContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-start',
        padding: 7,
        paddingLeft: 0,
        // backgroundColor:'yellow'

    },
    headingTextStyle: {
        color: Colors.normal_text_color,
        fontSize: 12,
        fontWeight: 'bold',
    },
    dataSubTextStyle: {
        color: Colors.normal_text_color,
        fontSize: 12,
    },

    textStyle: {
        fontSize: 10,
        color: Colors.primary_color,
        fontWeight: 'bold',
        alignSelf: 'center',
        textAlign: 'center',
    },
    ratingContainer: {
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        margin: dimen.app_padding,
        // backgroundColor:'red'
    },
    staffContainer: {
        // flex:1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        padding: dimen.app_padding,
    },
    profileImageContainer: {alignSelf: 'center', overflow: 'hidden', borderRadius: 25, width: 50, height: 50},
    profileImage: {
        width: 50,
        height: 50,
        overflow: 'hidden',
        borderRadius: 25,
        alignSelf: 'center',
        borderWidth: 1,

        borderColor: Colors.image_background_color,
    },
    reviewOrderContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        backgroundColor: Colors.primary_color,
        margin: dimen.app_padding,
        padding: dimen.app_padding - dimen.home_item_padding,
    },
    reviewOrderTextStyle: {
        color: Colors.app_background_color,
        fontSize: 12,
    },


});
















