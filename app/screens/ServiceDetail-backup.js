import React, {useState, useEffect, useCallback, useContext} from 'react';
import {
    Alert,
    Image,
    KeyboardAvoidingView,
    StyleSheet, SafeAreaView, Dimensions,
    Text,
    Platform, Animated,
    TouchableOpacity, ScrollView, TouchableWithoutFeedback,
    StatusBar, SectionList,
    View,
} from 'react-native';
import {FlatList, TextInput} from 'react-native-gesture-handler';
import dimen from '../Styles/Dimen';
import {
    _storeData, console_log, successAlert,
} from '../Classes/auth';

import {EmptyList, Loader} from '../Components/Loader';

import Colors from '../Styles/Colors';
import AppText from '../Components/AppText';
import {CategoriesCard} from '../Components/Categories';
import MyImageButton, {MyImage} from '../Components/MyImageButton';
import MyBackgroundImage from '../Components/MyBackgroundImage';
import TopBar from '../Components/TopBar';

import MyTextButton from '../Components/MyTextButton';
import {MenuItemCard} from '../Components/MenuItemCard';
import {MyContext} from '../Classes/AppContext';

import {ScrollableTabView, DefaultTabBar, ScrollableTabBar} from '@valdio/react-native-scrollable-tabview';
import {CartItemCard} from '../Components/CartItemCard';
import BottomUpPanel from '../Components/BottomUpPanel';
import * as Easing from 'react-native/Libraries/Animated/src/Easing';

const ServiceDetail = ({navigation}) => {


    const [categories, setCategories] = useState([{'id': 1}, {'id': 2}]);
    const [cartItems, setCartItems] = useState([]);
    const CATEGORY = [
        'Dresses',
        'Shoes',
        'Shorts',
        'Skirts',
        'Dresses',
        'Shoes',
        'Shorts',
        'Skirts',
    ];
    const [selectedService, setSelectedService] = useState(2);
    const [isLoading, setIsLoading] = useState(false);
    const [renderLoading, setRenderLoading] = useState(false);
    const [scrollView, setScrollView] = useState(false);
    const [showCart, setShowCart] = useState(false);


    const {width,height} = Dimensions.get('window');
    const DATA = ['Some component', 'Some component', 'Some component', 'Some component', 'Some component', 'Some component', 'Some component', 'Some component', 'Some component', 'Some component', 'Some component', 'Some component', 'Some component', 'Some component', 'Some component', 'Some component'];


    const [sService, setSService] = useState({});

    let scrollRef = null;

    const myContext = useContext(MyContext);
    // console_log(myContext.services);

    useEffect(() => {
        setRenderLoading(true);
    }, []);

    useEffect(() => {
        // console_log("selected service ");
        // console_log(sService);

    }, [sService]);

    useEffect(() => {
        console_log(myContext.selectedService);
        // console_log(setSService(myContext.services.find( (item) => item.id === myContext.selectedService)))
        setSService(myContext.services[0]);
        // console_log(sService);

    }, []);

    const subCatPress = (item) => {
        // incrementCounter(dispatch)
        // changeNamecallback(dispatch)
        // changeUserCallback()
        // getAllCategories();
        navigation.navigate('ProductList');

    };
    const RenderSubCat = (cat) => {
        const txtContainer = cat.map(
            (i, j) =>
                <CategoriesCard subCatPress={subCatPress} data={i}/>,
        );
        return (
            <View style={styles.subCategoriesMainContainer}>
                <View style={styles.categoryHeader}>
                    <View style={styles.headerStartStyle}/>
                    <AppText style={styles.headerTextStyle}>
                        Mobile and Digital
                    </AppText>
                </View>
                <View style={styles.subCategoryContainer}>
                    {txtContainer}
                </View>
            </View>
        );
    };
    const subcategoriesComponent = (cat) => {
        const subcategories = cat.map((i, j) => {
            return (<CategoriesCard subCatPress={subCatPress} data={i}/>);
        });
        return (subcategories);
    };
    const CategoriesComponent = (props) => {
        if (props.data.length === 0) {
            return (
                <View><AppText>No Categories found</AppText></View>
            );
        } else {
            const allCategories = props.data.map((item, index) => {
                return (
                    <View style={styles.subCategoriesMainContainer}>
                        <View style={styles.categoryHeader}>
                            <View style={styles.headerStartStyle}/>
                            <AppText style={styles.headerTextStyle}>
                                {item.catagory_name}
                            </AppText>
                        </View>
                        <View style={[styles.subCategoryContainer]}>
                            {subcategoriesComponent(item.sub_catagory)}
                        </View>
                    </View>
                );
            });
            return (allCategories);
        }
    };
    const productOffersPress = (item) => {
        navigation.navigate('ProductOffers');
    };
    const reviewOrder = (item) => {
        navigation.navigate('CartDetails');
    };
    const renderCategoryNew = (CATEGORY) => {
        return CATEGORY.map((item, i) => {
            // console_log(selectedService);
            return (
                <View tabLabel={item.name}>
                    <View style={{flex: 1, backgroundColor: Colors.app_background_color}}>
                        <AppText style={styles.serviceNameHeading}>Service Name</AppText>
                        <View style={{flex: 1}}>
                            <FlatList
                                style={styles.menuItemList}
                                data={item.sub_categories}
                                keyExtractor={(item, index) => item.id.toString()}
                                // contentContainerStyle={{margin:dimen.app_padding}}
                                numColumns={1}
                                bounces={false}
                                renderItem={({item}) => (
                                    <MenuItemCard productOffersPress={productOffersPress} data={item}/>)}
                                // ListHeaderComponent={() => {return (<FlatListHeader/>)}}
                                ListEmptyComponent={<EmptyList/>}

                            />
                        </View>


                    </View>
                </View>
                //       style={selectedService === 0 ? styles.serviceNameSelectedBottomLine : {}}>
                //     {selectedService === i &&
                //     <View style={styles.serviceNameSelectedBottomLine}/>}
                // </View>
            );
        });
    };
    const TabLabelComponent = (props) => {
        return (
            <View>
                <AppText style={[styles.serviceNameText]}>
                    {props.name}
                </AppText>
                {selectedService === props.id &&
                <View style={styles.serviceNameSelectedBottomLine}/>}
            </View>
        );
    };
    const renderCategory = (CATEGORY) => {
        return CATEGORY.map((item, i) => {
            // console_log(item);
            return (
                <TouchableOpacity
                    key={i}
                    onPress={() => {
                        setSelectedService(i);
                        // scrollRef.scrollTo({x: 200});
                        scrollRef.scrollToIndex({index: 4, animated: true});
                    }}>
                    <AppText style={[styles.serviceNameText]}>
                        {item.name}
                    </AppText>
                    {selectedService === i &&
                    <View style={styles.serviceNameSelectedBottomLine}/>}
                </TouchableOpacity>
            );
        });
    };

    const ServicesItem = (props) => {
        const [selectedServiceItem, setSelectedServiceItem] = useState(false);
        const myContext = useContext(MyContext);
        // console_log(myContext.selectedService);

        return (
            <TouchableOpacity
                onPress={() => {

                    setSelectedServiceItem(!selectedServiceItem);
                    myContext.selectedService = props.item.id;
                    console_log(myContext.selectedService);
                    // setSService(myContext.services.find((item)=>item.id===selectedService))
                    setSService(myContext.services.find((item) => item.id === myContext.selectedService));
                    // setSelectedService(props.index);
                    scrollRef.scrollToIndex({index: props.index, animated: true});

                }}>

                <AppText style={[styles.serviceNameText]}>
                    {props.item.name}
                </AppText>

                {selectedServiceItem && //selectedServiceItem &&
                <View style={styles.serviceNameSelectedBottomLine}/>}

            </TouchableOpacity>
        );
    };

    const ServicesHeader = (props) => {
        return (
            <View style={{}}>
                <View style={styles.headerImageContainer}>
                    <MyImage
                        source={{uri: sService.image}}
                        // source={require('../Asset/sevice_image.jpg')}
                        imageContainerStyle={styles.imageStyle}/>
                </View>
                {false && <View style={styles.servicesScrollbar}>
                    <MyImage source={require('../Asset/left_arrow.png')}
                             tintColor={Colors.button_text_color}
                             imageContainerStyle={[styles.dropdownIcon, {marginRight: 10}]}/>
                    <ScrollView
                        horizontal
                        // pagingEnabled
                        showsHorizontalScrollIndicator={false}
                        contentContainerStyle={{
                            justifyContent: 'center',
                        }}
                        ref={node => {
                            (scrollRef = node);
                        }}
                    >
                        <View style={styles.serviceNameItemContainer}>
                            {renderCategory(props.servicesArray)}
                        </View>
                    </ScrollView>
                    <MyImage source={require('../Asset/right_arrow.png')}
                             tintColor={Colors.button_text_color}
                             imageContainerStyle={[styles.dropdownIcon, {marginLeft: 10}]}/>
                </View>}

                <View style={styles.servicesScrollbar}>
                    <MyImage source={require('../Asset/left_arrow.png')}
                             tintColor={Colors.button_text_color}
                             imageContainerStyle={[styles.dropdownIcon, {marginRight: 10}]}/>
                    <FlatList
                        style={{backgroundColor: Colors.primary_color, padding: 10}}
                        contentContainerStyle={styles.serviceNameItemContainer}
                        ref={node => {
                            (scrollRef = node);
                        }}

                        data={props.servicesArray}
                        keyExtractor={(item, index) => item.id.toString()}
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        // extraData={myContext.selectedService}
                        bounces={false}
                        renderItem={({item, index}) => (
                            <ServicesItem item={item} index={index} selectedItem={myContext.selectedService}/>
                        )}
                        // ListHeaderComponent={() => {return (<FlatListHeader/>)}}
                    />
                    <MyImage source={require('../Asset/right_arrow.png')}
                             tintColor={Colors.button_text_color}
                             imageContainerStyle={[styles.dropdownIcon, {marginLeft: 10}]}/>
                </View>


                <AppText style={styles.serviceNameHeading}>Service Name</AppText>
            </View>
        );
    };
    const ServicesHeaderNew = (props) => {
        return (
            <View style={{}}>
                <View style={styles.headerImageContainer}>
                    <MyImage source={require('../Asset/sevice_image.jpg')}
                             imageContainerStyle={styles.imageStyle}/>
                </View>
                <View style={styles.servicesScrollbar}>
                    {/*<MyImage source={require('../Asset/left_arrow.png')}*/}
                    {/*tintColor={Colors.button_text_color}*/}
                    {/*imageContainerStyle={[styles.dropdownIcon, {marginLeft: 0}]}/>*/}
                    <ScrollableTabView initialPage={0}
                                       prerenderingSiblingsNumber={2}
                                       showsHorizontalScrollIndicator={false}
                                       tabBarUnderlineStyle={{backgroundColor: Colors.app_background_color}}
                                       tabBarBackgroundColor={Colors.primary_color}
                                       tabBarActiveTextColor={Colors.app_background_color}
                                       tabBarInactiveTextColor={Colors.app_background_color}
                                       style={{flex: 1, backgroundColor: Colors.app_background_color}}
                        // tabBarTextStyle={{fon
                        // tSize:15,borderBottomWidth:2,borderColor:Colors.app_background_color,fontWeight:'normal'}}
                                       tabBarTextStyle={{fontSize: 12}}
                                       renderTabBar={() => <ScrollableTabBar/>}>
                        {renderCategoryNew(props.servicesArray)}
                    </ScrollableTabView>
                    {/*<MyImage source={require('../Asset/right_arrow.png')}*/}
                    {/*tintColor={Colors.button_text_color}*/}
                    {/*imageContainerStyle={[styles.dropdownIcon, {marginRight: 0}]}/>*/}
                </View>
            </View>
        );
    };


    const [bounceValue, setBounceValue] = useState(new Animated.Value(dimen.orderPopupHeight));
    const [show, setShow] = useState(false);
    const toggleBottomView = () => {
        let toValue = dimen.orderPopupHeight;

        if (!show) {
            toValue = 0;
        }
        Animated.spring(bounceValue, {toValue: toValue, velocity: 3, tension: 2, friction: 8}).start();

        setShow(!show);
    };


    const renderBottomUpPanelContent = () =>
        <View>
            <FlatList style={{backgroundColor: 'black', opacity: 0.7, flex: 1}}
                      data={DATA}
                      renderItem={({item}) =>
                          <Text style={{color: 'white', padding: 20}}>{item}</Text>
                      }
            />
        </View>;

    const renderBottomUpPanelIcon = () =>
        <MyImage source={require('../Asset/down.png')}
                 imageContainerStyle={[styles.dropdownIcon, {marginLeft: 5}]}
        />;


    let config = {
        position: {
            max: height,
            start: height - 80, //start height
            end: height - height * 0.7,
            min: height - height * 0.7,
            animates: [
                () => _animatedHeight
            ]
        },
        width: {
            end: width,
            start: width
        },
        height:{
            end: height,
            start: 80       // start height
        }

    };

    const _animatedHeight = new Animated.Value(showCart ? config.height.end : config.height.start);

    const _animatedPosition = new Animated.Value(showCart
        ? config.position.end
        : config.position.start);





    const open = () => {
        setShowCart(true);

        Animated.timing(_animatedPosition, {
            toValue: config.position.end,
            duration: 600,
        }).start();


    };

    const close = () => {
        // this._scrollView.scrollTo({y: 0});
        Animated.timing(_animatedPosition, {
            toValue: config.position.start,
            duration: 600,
        }).start(() => setShowCart(false));

    };

    const toggle = () => {
        if (!showCart) {
            this.open();
        } else {
            this.close();
        }
    };


    return <MyBackgroundImage>
        <TopBar title={'Services'} backButton={() => navigation.pop()}/>
        {renderLoading &&
        <View style={styles.mainContainer}>
            <ScrollView>
                <ServicesHeaderNew servicesArray={myContext.services}/>

                {false && <View style={{flex: 1}}>
                    <FlatList
                        style={styles.menuItemList}
                        data={sService.sub_categories}
                        keyExtractor={(item, index) => item.id.toString()}
                        // contentContainerStyle={{margin:dimen.app_padding}}
                        numColumns={1}
                        bounces={false}
                        renderItem={({item}) => (
                            <MenuItemCard productOffersPress={productOffersPress} data={item}/>)}
                        // ListHeaderComponent={() => {return (<FlatListHeader/>)}}
                    />
                </View>}
            </ScrollView>
            <View style={{margin: dimen.app_padding, marginBottom: 0}}>


                <Animated.View style={[styles.orderSummaryPopupContainer, {transform: [{translateY: bounceValue}]}]}>
                    <TouchableOpacity onPress={toggleBottomView} style={styles.cartPopUpHeadingContainer}>
                        <AppText style={styles.cartPopUpHeadingText}>Order Summary</AppText>
                        <MyImage source={require('../Asset/down.png')}
                                 imageContainerStyle={[styles.dropdownIcon, {marginLeft: 5}]}
                        />
                    </TouchableOpacity>

                    <View style={styles.cartItemsContainer}/>

                    <FlatList
                        style={styles.cartItemList}
                        data={myContext.cart}
                        keyExtractor={(item, index) => item.id.toString()}
                        numColumns={1}
                        bounces={false}
                        renderItem={({item}) => (
                            <CartItemCard productOffersPress={productOffersPress} data={item}/>)}
                        ListEmptyComponent={<EmptyList/>}
                    />

                </Animated.View>


                <TouchableOpacity onPress={toggleBottomView} style={styles.orderSummaryContainer}>
                    <AppText>Total Rs. 2000</AppText>
                    <View style={styles.cartSummaryContainer}>
                        <AppText>Order Summary</AppText>
                        <MyImage source={require('../Asset/up_arrow.png')}
                                 imageContainerStyle={[styles.dropdownIcon, {marginLeft: 5}]}
                        />
                    </View>
                </TouchableOpacity>
                <MyTextButton onPress={reviewOrder}
                              buttonText={'Go To Cart'}
                              buttonContainerStyle={[styles.loginButton, styles.goToCartButtonStyle]}/>
                <View style={styles.bottomViewToHideBackground}/>

                {false && <BottomUpPanel
                    content={renderBottomUpPanelContent}
                    icon={renderBottomUpPanelIcon}
                    topEnd={height - height * 0.8}
                    startHeight={80}
                    headerText={'List of items'}
                    headerTextStyle={{
                        color: 'white',
                        fontSize: 15,
                    }}
                    bottomUpSlideBtn={{
                        display: 'flex',
                        alignSelf: 'flex-start',
                        backgroundColor: 'black',
                        alignItems: 'center',
                        borderTopColor: 'grey',
                        borderTopWidth: 5,
                    }}
                />}

            </View>
        </View>}

        {isLoading && <Loader/>}
        {!renderLoading && <Loader/>}
    </MyBackgroundImage>;

};

export default (ServiceDetail);


const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'stretch',
        // marginBottom: dimen.bottom_tab_height,
    },
    servicesScrollbar: {
        // height: 40,
        backgroundColor: Colors.primary_color,
        // paddingLeft: dimen.app_padding,
        // paddingRight: dimen.app_padding,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',

    },
    headerImageContainer: {
        height: 150,
        width: '100%',
        marginTop: 10,
    },
    imageStyle: {
        resizeMode: 'cover',
        backgroundColor: Colors.image_background_color
        ,
    },
    dropdownIcon: {
        width: 10,
        height: 10,
        resizeMode: 'contain',
    },
    orderSummaryContainer: {
        borderWidth: 1,
        borderColor: Colors.primary_color,
        backgroundColor: Colors.app_background_color,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        // margin: dimen.app_padding,
        marginBottom: 0,
        padding: 10,


    },
    serviceNameItemContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    serviceNameText: {
        fontSize: 18,
        // color: this.state.currentIndex === i ? "#F08C4F" : "white",
        paddingHorizontal: 10,
        color: Colors.button_text_color,
    },
    serviceNameSelectedBottomLine: {
        width: '100%',
        height: 1,
        backgroundColor: Colors.app_background_color,
        marginLeft: 10,
        marginRight: 10,
        alignSelf: 'center',

    },
    serviceNameHeading: {
        fontSize: 18,
        fontWeight: 'bold',
        color: Colors.normal_text_color,
        margin: dimen.app_padding,
    },

    menuItemList: {
        flex: 1,
        padding: dimen.app_padding - dimen.home_item_padding,
        borderWidth: 1,
        borderColor: Colors.primary_color,
        marginLeft: dimen.app_padding,
        marginRight: dimen.app_padding,
    },

    cartItemList: {
        flex: 1,
        // padding: dimen.home_item_padding,
        // backgroundColor:'red'

    },


    orderSummaryPopupContainer: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: Colors.app_background_color,
        borderWidth: 1,
        borderColor: Colors.primary_color,
        height: dimen.orderPopupHeight,

    },

    cartPopUpHeadingContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingLeft: dimen.app_padding,
        paddingRight: dimen.app_padding,
        paddingTop: dimen.home_item_padding,
        paddingBottom: dimen.home_item_padding,
    },
    cartPopUpHeadingText: {
        fontSize: 13,
        fontWeight: 'bold',
    },
    cartItemsContainer: {
        marginLeft: dimen.app_padding,
        marginRight: dimen.app_padding,
        height: 1,
        backgroundColor: Colors.image_background_color,
    },
    cartSummaryContainer: {flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center'},
    goToCartButtonStyle: {
        marginTop: 0,
        marginBottom: 0,
    },
    bottomViewToHideBackground: {
        height: dimen.app_padding,
        width: '100%',
        backgroundColor: Colors.app_background_color,
    },


});































