import React, {useState} from 'react';
import {
    Alert,
    Image,
    Text,
    Platform,
    TouchableOpacity, ScrollView,
    View, StyleSheet,
} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import TextStyles from '../../Styles/TextStyles';
import dimen from '../../Styles/Dimen';

import MyTextButton, {MyTextButtonSimple} from '../../Components/MyTextButton';
import Colors from '../../Styles/Colors';
import MyBackgroundImage from '../../Components/MyBackgroundImage';
import MyStrings from '../../Classes/MyStrings';
import TopBar from '../../Components/TopBar';
import {console_log, errorAlert, getDataWithToken, isNetConnected} from '../../Classes/auth';
import {change_password_url, coupon_apply_url, general_network_error} from '../../Classes/UrlConstant';
import {Loader} from '../../Components/Loader';

const ChangePassword = ({navigation}) => {
    const [isLoading, setIsLoading] = useState(false);
    const [oldPassword, setOldPassword] = useState('');
    const [password, setPassword] = useState('');
    const [newPassword, setNewPassword] = useState('');


    const changePassword = () => {
        if (oldPassword === '' || password === '' || newPassword === '') {
            errorAlert('Enter all fields.');
            return;
        }
        if (password.length < 6) {
            errorAlert('Password must be minimum 6 characters.');
            return;
        }

        if (password !== newPassword) {
            errorAlert('New password and confirm password does not match.');
            return;
        }


        isNetConnected()
            .then(() => {
                setIsLoading(true);
                let data = {
                    'current-password': oldPassword,
                    'new-password': password,
                    'new-password-confirm': newPassword,
                };
                getDataWithToken(change_password_url, 'POST', data)
                    .then((response) => {
                        setIsLoading(false);
                        console_log(response);
                        if (response.error !== undefined) {
                            Alert.alert(response.error);
                        }
                        if (response.status !== undefined) {
                            if (response.status === 'error') {
                                errorAlert(response.message);
                            } else if (response.status === 'success') {
                                Alert.alert('Success', response.message,
                                    [{text: 'Okay', onPress: () => navigation.pop()}],
                                    {cancelable: false});
                            }

                        }


                    })
                    .catch((err) => {
                        setIsLoading(false);
                        console_log(err);

                    });
            })
            .catch((err) => {
                errorAlert(general_network_error);
                console_log(err);
            });
    };


    return (
        <MyBackgroundImage>
            <TopBar title={'Change Password'} backButton={() => navigation.pop()}/>
            <ScrollView style={[styles.scrollContainer]}>
                <View style={{}}>
                    <View style={styles.container}>
                        {/*<SignUpLogo/>*/}
                        <View style={styles.inputContainer}>
                            {/*<AppText style={styles.fpassStyle}>*/}
                            {/*    {MyStrings.createNP}*/}
                            {/*</AppText>*/}
                            {/*<AppText style={[styles.fpassStyle2]}>*/}
                            {/*    {MyStrings.newPassToContinue}*/}
                            {/*</AppText>*/}
                            <View style={styles.emailOrPhoneContainer}>
                                {/*<MyImage source={require('../../Asset/user_icon.png')}*/}
                                {/*         imageContainerStyle={styles.userIconStyle}/>*/}
                                <TextInput onChangeText={(email) => setOldPassword(email)}
                                           value={oldPassword}
                                           placeholder={'Old Password'}
                                           autoCapitalize='none'
                                           secureTextEntry={true}
                                           style={styles.inputField}/>
                            </View>

                            <View style={styles.emailOrPhoneContainer}>
                                {/*<MyImage source={require('../../Asset/user_icon.png')}*/}
                                {/*         imageContainerStyle={styles.userIconStyle}/>*/}
                                <TextInput onChangeText={(email) => setPassword(email)}
                                           value={password}
                                           placeholder={MyStrings.newPassPlaceholder}
                                           autoCapitalize='none'
                                           secureTextEntry={true}
                                           style={styles.inputField}/>
                            </View>

                            <View style={styles.emailOrPhoneContainer}>
                                {/*<MyImage source={require('../../Asset/user_icon.png')}*/}
                                {/*         imageContainerStyle={styles.userIconStyle}/>*/}
                                <TextInput onChangeText={(email) => setNewPassword(email)}
                                           value={newPassword}
                                           placeholder={MyStrings.rePassPlaceholder}
                                           autoCapitalize='none'
                                           secureTextEntry={true}
                                           style={styles.inputField}/>
                            </View>

                            <MyTextButton onPress={changePassword}
                                          buttonText={'Submit'}
                                          buttonContainerStyle={[styles.loginButton, {marginTop: 35}]}/>

                        </View>

                    </View>

                </View>
            </ScrollView>
            {isLoading && <Loader/>}
        </MyBackgroundImage>
    );

};
export default ChangePassword;
const styles = StyleSheet.create({
    scrollContainer: {
        flex: 1,
        // backgroundColor: 'red',
    },
    container: {
        flex: 1,
        flexGrow: 1,
        // backgroundColor: Colors.app_background_color,
        flexDirection: 'column',
        margin: dimen.signup_margin,
        justifyContent: 'space-between',
        alignItems: 'stretch',

    },
    crossContain: {
        alignItems: 'flex-end',
    },
    loginorContain: {
        marginTop: dimen.app_padding * 2,
        alignItems: 'center',
    },
    byInstalmentText: {
        fontSize: 12,
        alignItems: 'center',
        color: Colors.light_text_login,
    },
    loginLogoButtonContainer: {
        width: 154,
        height: 56,
    },
    crossButtonContainer: {
        width: 20,
        height: 20,
    },
    inputContainer: {
        marginTop: 50,
        flexGrow: 1,

    },
    emailOrPhoneContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        borderColor: Colors.primary_color,
        backgroundColor: Colors.app_background_color,
        borderWidth: 1,
        // borderRadius: dimen.login_border_radius,
        paddingRight: dimen.app_padding,
        paddingLeft: dimen.app_padding,
        marginTop: 10,


    },
    userIconStyle: {
        width: 12,
        height: 12,
    },
    inputField: {
        marginRight: dimen.app_padding,
        marginLeft: dimen.app_padding - 4,
        fontSize: 13,
        flexGrow: 1,

    },
    fpassStyle: {
        flex: 1,
        alignSelf: 'center',
        fontSize: 14,
        color: Colors.dark_text_login,


    },
    fpassStyle2: {
        flex: 1,
        alignSelf: 'center',
        fontSize: 10,
        marginLeft: 20,
        marginRight: 20,
        color: Colors.light_text_login,

    },


});

