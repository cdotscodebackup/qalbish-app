import React, {useState} from 'react';
import {
    Alert,
    Image,
    Text,
    Platform,
    TouchableOpacity, ScrollView,
    View, StyleSheet,
} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import TextStyles from '../../Styles/TextStyles';
import dimen from '../../Styles/Dimen';
import {
    _storeData,
} from '../../Classes/auth';

import {
    internetError, main_url, web_view_url, web_view_url_key, webview_web_url,
} from '../../Classes/UrlConstant';
import {Loader} from '../../Components/Loader';

import NetInfo from '@react-native-community/netinfo';
import SplashScreen from 'react-native-splash-screen';
import MyImageButton, {MyImage} from '../../Components/MyImageButton';
import AppText from '../../Components/AppText';
import MyTextButton, {MyTextButtonSimple} from '../../Components/MyTextButton';
import Colors from '../../Styles/Colors';
import MyStrings from '../../Classes/MyStrings';
import MyBackgroundImage from '../../Components/MyBackgroundImage';
import SignUpLogo, {SignUpCross} from '../../Components/SignUpLogo';

const SignUp = ({navigation}) => {
    const [isLoading, setIsLoading] = useState(false);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const loginPressed = () => {
        Alert.alert('login pressed');
    };
    const crossPress= () => {
        navigation.pop()
    };
    const signUpPress= () => {
        navigation.navigate('PhoneVarification')
    };
    const backPressed = () => {
        navigation.goBack();
    };
    const signupUsingPhone = () => {
        // navigation.navigate('Signup');
    };
    const forgetPassword = () => {
        Alert.alert('login pressed');
    };

    return (
        <MyBackgroundImage>
            <ScrollView style={[styles.scrollContainer]} >
                <View style={{}} >
                    <View style={styles.container}>
                        <SignUpCross  onPress={crossPress}/>
                        <SignUpLogo/>

                        <View style={styles.inputContainer}>
                            <View style={styles.emailOrPhoneContainer}>
                                <MyImage source={require('../../Asset/user_icon.png')}
                                         imageContainerStyle={styles.userIconStyle}/>
                                <TextInput onChangeText={(email) => setEmail({email})}
                                           value={email}
                                           placeholder={MyStrings.login_phone_email_placeholder}
                                           autoCapitalize='none'
                                           style={styles.inputField}/>
                            </View>

                            <MyTextButton onPress={signUpPress}
                                          buttonText={MyStrings.login_button}
                                          buttonContainerStyle={[styles.loginButton, {marginTop: 15}]}/>

                            <AppText style={[styles.byInstalmentText, styles.orTextStyle]}>
                                {MyStrings.orText}
                            </AppText>

                            <View style={styles.quickConnectContainer}>
                                <View style={styles.line}/>

                                <AppText style={[styles.byInstalmentText, styles.quickConnectText]}>
                                    {MyStrings.quickConnect}
                                </AppText>

                                <View style={styles.line}/>
                            </View>


                            {/*<View style={styles.signUpContainer}>*/}
                                {/*<AppText style={styles.dontHaveAccountText}>*/}
                                    {/*{MyStrings.dontHaveAccount}*/}
                                {/*</AppText>*/}
                                {/*<AppText style={styles.signUpTextStyle}>*/}
                                    {/*{MyStrings.signup}*/}
                                {/*</AppText>*/}
                            {/*</View>*/}

                        </View>

                    </View>
                    <View style={styles.policyDisclaimerContainer}>
                        <AppText style={styles.disclaimerTextStyle}>
                            {MyStrings.disclaimer1}
                        </AppText>
                        <AppText style={styles.termsStyle}>
                            {MyStrings.terms}
                        </AppText>
                        <AppText style={styles.disclaimerTextStyle}>
                            {MyStrings.and}
                        </AppText>
                        <AppText style={styles.termsStyle}>
                            {MyStrings.privacy}
                        </AppText>
                    </View>
                </View>
            </ScrollView>
        </MyBackgroundImage>
    );

};
export default SignUp;
const styles = StyleSheet.create({
    scrollContainer: {
        flex: 1,
        // backgroundColor: 'red',
    },
    container: {
        flex: 1,
        flexGrow: 1,
        // backgroundColor: Colors.app_background_color,
        flexDirection: 'column',
        margin: dimen.signup_margin,
        justifyContent: 'space-between',
        alignItems: 'stretch',

    },
    crossContain: {
        alignItems: 'flex-end',
    },
    loginorContain: {
        marginTop: dimen.app_padding * 2,
        alignItems: 'center',
    },
    byInstalmentText: {
        fontSize: 12,
        alignItems: 'center',
        color: Colors.light_text_login,
    },
    loginLogoButtonContainer: {
        width: 154,
        height: 56,
    },
    crossButtonContainer: {
        width: 20,
        height: 20,
    },
    inputContainer: {
        marginTop: 50,
        flexGrow: 1,
    },
    emailOrPhoneContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        borderColor: Colors.signup_input_text_background_color,
        backgroundColor: Colors.signup_input_text_background_color,
        borderWidth: 1,
        borderRadius: dimen.login_border_radius,
        paddingRight: dimen.app_padding,
        paddingLeft: dimen.app_padding,


    },
    userIconStyle: {
        width: 12,
        height: 12,
    },
    inputField: {
        marginRight: dimen.app_padding,
        marginLeft: dimen.app_padding - 4,
        fontSize: 13,
        flexGrow:1,

    },
    orTextStyle: {
        marginTop: 30,
        marginBottom: 30,
        flex: 1,
        alignSelf: 'center',
    },
    quickConnectText: {
        // backgroundColor:'green'
        marginRight: 10,
        marginLeft: 10,

    },
    line: {
        height: 1,
        width: 30,
        flexGrow: 1,
        alignSelf: 'center',
        backgroundColor: Colors.border_color,

    },
    quickConnectContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',

    },
    loginButton: {
        marginTop: dimen.app_padding,

    },
    socialContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        margin: dimen.app_padding,

    },
    signUpContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        marginBottom: 20,
    },
    dontHaveAccountText: {
        fontSize: 16,
        alignItems: 'center',
        color: Colors.light_text_login,
    },

    signUpTextStyle: {
        fontSize: 18,
        alignItems: 'center',
        alignSelf: 'center',
        color: Colors.primary_color,
        borderBottomColor: Colors.primary_color,
        borderBottomWidth: 1,
        fontWeight: 'bold',
    },
    policyDisclaimerContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        flexWrap: 'wrap',
        padding: 10,
        alignSelf: 'flex-end',
        marginRight:dimen.signup_margin,
        marginLeft:dimen.signup_margin,
        // backgroundColor:'red'

    },
    disclaimerTextStyle: {
        fontSize: 9,
        alignItems: 'center',
        color: Colors.light_text_login,
    },
    termsStyle: {
        fontSize: 9,
        alignItems: 'center',
        alignSelf: 'center',
        color: Colors.primary_color,
        borderBottomColor: Colors.primary_color,
        borderBottomWidth: 1,
        fontWeight: 'bold',
    },

});

