import React, {useState,useEffect} from 'react';
import {
    Alert,
    Image,
    Text,
    Platform,
    TouchableOpacity, ScrollView,
    View, StyleSheet,
} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import TextStyles, {InputFieldStyle} from '../../Styles/TextStyles';
import dimen from '../../Styles/Dimen';

import MyImageButton, {MyImage} from '../../Components/MyImageButton';
import AppText from '../../Components/AppText';
import MyTextButton, {MyTextButtonSimple} from '../../Components/MyTextButton';
import Colors from '../../Styles/Colors';
import MyBackgroundImage from '../../Components/MyBackgroundImage';
import {SignUpCross} from '../../Components/SignUpLogo';
import SignUpLogo from '../../Components/SignUpLogo';
import MyStrings from '../../Classes/MyStrings';
import {console_log, errorAlert, getDataWithoutToken, isNetConnected, parseError} from "../../Classes/auth";
import {general_network_error, set_new_password_url, verify_otp_url} from "../../Classes/UrlConstant";
import {Loader} from "../../Components/Loader";

const NewPassword = ({navigation}) => {
    const [isLoading, setIsLoading] = useState(false);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [rePassword, setRePassword] = useState('');
    const [phone, setPhone] = useState('');


    const loginPressed = () => {
       // navigation.navigate('Login')
    };

    useEffect(()=>{
        let ph=navigation.getParam('phone')
        setPhone(ph)

    },[])

    const setNewPassword = () => {
        if (password.length < 6) {
            errorAlert('Password must be of minimum 6 characters.');
            return;
        }
        if (password !== rePassword) {
            errorAlert('Password and confirm password must match');
            return;
        }
        console_log()
        isNetConnected()
            .then(() => {
                setIsLoading(true);
                let formData = {
                    'phone_number': phone,
                    'password': password,
                    'password_confirmation': rePassword,
                };
                getDataWithoutToken(set_new_password_url, 'POST', formData)
                    .then((response) => {
                        setIsLoading(false);
                        let res = JSON.parse(response);
                        console_log(res)

                        if (res.status === 'success') {
                            Alert.alert(
                                'Success',
                                res.message,
                                [
                                    {
                                        text: 'okay',
                                        onPress: () => {
                                            navigation.popToTop();
                                        },
                                    },
                                ],
                                {cancelable: false},
                            );

                        } else if (res.error !== undefined) {
                            console_log((res.error));
                            errorAlert(parseError(res.error));
                        }

                    })
                    .catch((err) => {
                        setIsLoading(false);
                        console_log('error: ' + err);
                        // errorAlert('Wrongng Credentials')
                    });
            })
            .catch((err) => {
                errorAlert(general_network_error);
            });
    };


    return (
        <MyBackgroundImage>
            <ScrollView style={[styles.scrollContainer]}>
                <View style={{}}>
                    <View style={styles.container}>
                        <SignUpCross onPress={()=>navigation.popToTop() }/>
                        <SignUpLogo/>

                        <View style={styles.inputContainer}>
                            {/*<AppText style={styles.fpassStyle}>*/}
                            {/*    {MyStrings.createNP}*/}
                            {/*</AppText>*/}
                            <AppText style={[styles.fpassStyle2]}>
                                {MyStrings.newPassToContinue}
                            </AppText>
                            <View style={styles.emailOrPhoneContainer}>
                                {/*<MyImage source={require('../../Asset/user_icon.png')}*/}
                                {/*         imageContainerStyle={styles.userIconStyle}/>*/}
                                <TextInput onChangeText={(email) => setPassword(email)}
                                           value={password}
                                           placeholder={MyStrings.newPassPlaceholder}
                                           autoCapitalize='none'
                                           secureTextEntry={true}
                                           style={InputFieldStyle.inputField}/>
                            </View>

                            <View style={styles.emailOrPhoneContainer}>
                                {/*<MyImage source={require('../../Asset/user_icon.png')}*/}
                                {/*         imageContainerStyle={styles.userIconStyle}/>*/}
                                <TextInput onChangeText={(email) => setRePassword(email)}
                                           value={rePassword}
                                           placeholder={MyStrings.rePassPlaceholder}
                                           autoCapitalize='none'
                                           secureTextEntry={true}
                                           style={InputFieldStyle.inputField}/>
                            </View>

                            <MyTextButton onPress={setNewPassword}
                                          buttonText={'Submit'}
                                          buttonContainerStyle={[styles.loginButton, {marginTop: 35}]}/>

                        </View>

                    </View>

                </View>
            </ScrollView>

            {isLoading && <Loader />}
        </MyBackgroundImage>
    );

};
export default NewPassword;
const styles = StyleSheet.create({
    scrollContainer: {
        flex: 1,
        // backgroundColor: 'red',
    },
    container: {
        flex: 1,
        flexGrow: 1,
        // backgroundColor: Colors.app_background_color,
        flexDirection: 'column',
        margin: dimen.signup_margin,
        justifyContent: 'space-between',
        alignItems: 'stretch',

    },
    crossContain: {
        alignItems: 'flex-end',
    },
    loginorContain: {
        marginTop: dimen.app_padding * 2,
        alignItems: 'center',
    },
    byInstalmentText: {
        fontSize: 12,
        alignItems: 'center',
        color: Colors.light_text_login,
    },
    loginLogoButtonContainer: {
        width: 154,
        height: 56,
    },
    crossButtonContainer: {
        width: 20,
        height: 20,
    },
    inputContainer: {
        marginTop: 50,
        flexGrow: 1,

    },
    emailOrPhoneContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        borderColor: Colors.primary_color,
        backgroundColor: Colors.app_background_color,
        borderWidth: 1,
        // borderRadius: dimen.login_border_radius,
        paddingRight: dimen.app_padding,
        paddingLeft: dimen.app_padding,
        marginTop: 10,


    },
    userIconStyle: {
        width: 12,
        height: 12,
    },
    inputField: {
        marginRight: dimen.app_padding,
        marginLeft: dimen.app_padding - 4,
        fontSize: 13,
        flexGrow: 1,

    },
    fpassStyle: {
        flex: 1,
        alignSelf: 'center',
        fontSize: 14,
        color: Colors.dark_text_login,


    },
    fpassStyle2: {
        flex: 1,
        alignSelf: 'center',
        fontSize: 10,
        marginLeft: 20,
        marginRight: 20,
        color: Colors.light_text_login,

    },


});

