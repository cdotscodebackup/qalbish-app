import React, {useState}  from 'react';
import {
    Alert,
    Image,
    Text,
    Platform,
    TouchableOpacity, ScrollView,
    View, StyleSheet,
} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import dimen from '../../Styles/Dimen';

import MyImageButton, {MyImage} from '../../Components/MyImageButton';
import AppText from '../../Components/AppText';
import MyTextButton, {MyTextButtonSimple} from '../../Components/MyTextButton';
import Colors from '../../Styles/Colors';
import MyBackgroundImage from '../../Components/MyBackgroundImage';
import {SignUpCross} from '../../Components/SignUpLogo';
import SignUpLogo from '../../Components/SignUpLogo';
import MyStrings from '../../Classes/MyStrings';
import {
    console_log,
    errorAlert,
    getDataWithoutToken,
    isNetConnected,
    parseError,
    saveObject,
    successAlert,
} from '../../Classes/auth';
import {
    data_key, general_network_error, get_otp_url,
    login_url,
    referal_key,
    referral_discount_key,
    settings_key,
    token_key,
} from '../../Classes/UrlConstant';
import {Loader} from '../../Components/Loader';
import {InputFieldStyle} from '../../Styles/TextStyles';

const ForgetPassword = ({navigation}) => {
    const [isLoading, setIsLoading]=useState(false);
    const [phone, setPhone]=useState('');

    const loginPressed  = () => {
        if (phone === '') {
            errorAlert('Enter email and password');
            return;
        }
        if (phone.length !== 11) {
            errorAlert('Phone number must be of 11 characters.');
            return;
        }
        isNetConnected()
            .then(() => {
                setIsLoading(true);
                let formData = {
                    'phone_number': phone,
                    // 'password': password,
                };
                getDataWithoutToken(get_otp_url, 'POST', formData)
                    .then((response) => {
                        setIsLoading(false);
                        let res = JSON.parse(response);
                        console_log(res)

                        if (res.data !== undefined) {
                            Alert.alert(
                                'Success',
                                res.message,
                                [
                                    {
                                        text:'okay',
                                        onPress:()=>{navigation.navigate('PhoneVarification',{'phone':phone})}
                                    }
                                ],
                            {cancelable:false}
                            )

                        } else if (res.error !== undefined) {
                            console_log((res.error));
                            errorAlert(parseError(res.error));
                        }

                    })
                    .catch((err) => {
                        setIsLoading(false);
                        console_log('error: ' + err);
                        // errorAlert('Wrongng Credentials')
                    });
            })
            .catch((err) => {
                errorAlert(general_network_error);
            });
    };


    return (
       <MyBackgroundImage>

           <ScrollView style={[styles.scrollContainer]} >
               <View style={{}} >
                   <View style={styles.container}>
                       <SignUpCross  onPress={()=>navigation.pop()}/>
                       <SignUpLogo/>

                       <View style={styles.inputContainer}>
                           <AppText style={styles.fpassStyle}>
                               {MyStrings.forgot_password}
                           </AppText>
                           <AppText style={[styles.fpassStyle2]}>
                               {MyStrings.otptext}
                           </AppText>
                           <AppText style={[styles.fpassStyle2]}>
                               {MyStrings.otptext2}
                           </AppText>
                           <View style={styles.emailOrPhoneContainer}>
                               {/*<MyImage source={require('../../Asset/user_icon.png')}*/}
                               {/*         imageContainerStyle={styles.userIconStyle}/>*/}
                               <TextInput onChangeText={(email) => setPhone(email)}
                                          value={phone}
                                          placeholder={MyStrings.login_phone_placeholder}
                                          autoCapitalize='none'
                                          maxLength={11}
                                          keyboardType={'phone-pad'}
                                          style={InputFieldStyle.inputField}/>
                           </View>

                           <MyTextButton onPress={loginPressed}
                                         buttonText={'Send'}
                                         buttonContainerStyle={[styles.loginButton, {marginTop: 35}]}/>

                       </View>

                   </View>

               </View>
           </ScrollView>
           {isLoading && <Loader/>}
       </MyBackgroundImage>

   );

};
export default ForgetPassword ;
const styles = StyleSheet.create({
    scrollContainer: {
        flex: 1,
        // backgroundColor: 'red',
    },
    container: {
        flex: 1,
        flexGrow: 1,
        // backgroundColor: Colors.app_background_color,
        flexDirection: 'column',
        margin: dimen.signup_margin,
        justifyContent: 'space-between',
        alignItems: 'stretch',

    },
    crossContain: {
        alignItems: 'flex-end',
    },
    loginorContain: {
        marginTop: dimen.app_padding * 2,
        alignItems: 'center',
    },
    byInstalmentText: {
        fontSize: 12,
        alignItems: 'center',
        color: Colors.light_text_login,
    },
    loginLogoButtonContainer: {
        width: 154,
        height: 56,
    },
    crossButtonContainer: {
        width: 20,
        height: 20,
    },
    inputContainer: {
        marginTop: 50,
        flexGrow: 1,

    },
    emailOrPhoneContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        borderColor: Colors.primary_color,
        backgroundColor: Colors.app_background_color,
        borderWidth: 1,
        // borderRadius: dimen.login_border_radius,
        paddingRight: dimen.app_padding,
        paddingLeft: dimen.app_padding,
        marginTop:10,

    },
    userIconStyle: {
        width: 12,
        height: 12,
    },
    inputField: {
        marginRight: dimen.app_padding,
        marginLeft: dimen.app_padding - 4,
        fontSize: 13,
        flexGrow:1,

    },
    fpassStyle: {
        flex: 1,
        alignSelf: 'center',
        fontSize:14,
        color:Colors.dark_text_login,


    },
    fpassStyle2: {
        flex: 1,
        alignSelf: 'center',
        fontSize:10,
        marginLeft:20,
        marginRight:20,
        color:Colors.light_text_login,

    },


});

