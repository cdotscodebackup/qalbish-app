import React, {useState} from 'react';
import {
    Alert,
    StyleSheet,
    TouchableOpacity, ScrollView,
    View, StatusBar, Linking,
} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import dimen from '../../Styles/Dimen';
import {
    _storeData, console_log, errorAlert, getDataWithoutToken, isNetConnected, parseError, saveObject,
} from '../../Classes/auth';

import {
    data_key,
    general_network_error,
    login_url, referal_key, referral_discount_key, settings_key, token_key,
    web_view_url,
} from '../../Classes/UrlConstant';
import MyBackgroundImage from '../../Components/MyBackgroundImage';
import Colors from '../../Styles/Colors';
import MyTextButton, {MyTextButtonSimple} from '../../Components/MyTextButton';
import AppText from '../../Components/AppText';
import MyStrings from '../../Classes/MyStrings';
import SignUpLogo from '../../Components/SignUpLogo';
import {Loader} from '../../Components/Loader';
import crashlytics from '@react-native-firebase/crashlytics';
import {InputFieldStyle} from '../../Styles/TextStyles';

const Loginn = ({navigation}) => {

    const [isLoading, setIsLoading] = useState(false);
    const [phone, setPhone] = useState('');
    const [password, setPassword] = useState('');

    const loginPressed = () => {
        if (phone === '' || password === '') {
            errorAlert('Enter email and password');
            return;
        }
        if (phone.length !== 11) {
            errorAlert('Phone number must be of 11 characters.');
            return;
        }
        isNetConnected()
            .then(() => {
                setIsLoading(true);
                let formData = {
                    'phone_number': phone,
                    'password': password,
                };
                getDataWithoutToken(login_url, 'POST', formData)
                    .then((response) => {
                        setIsLoading(false);
                        // console_log(response)
                        let res = JSON.parse(response);

                        if (res.success && res.success.token) {
                            if (res.success.data.user_type === 'customer') {
                                saveObject(token_key, res.success.token).then().catch((res) => console_log('error' + res));
                                saveObject(data_key, res.success.data).then().catch((res) => console_log('error' + res));
                                saveObject(referal_key, res.success.referral_discount).then().catch((res) => console_log('error' + res));
                                saveObject(referral_discount_key, res.success.referral_discount_description).then().catch((res) => console_log('error' + res));
                                saveObject(settings_key, res.success.settings).then().catch((res) => console_log('error' + res));

                                navigation.navigate('CustomerDashboard');
                            } else if (res.success.data.user_type === 'staff') {
                                saveObject(token_key, res.success.token).then().catch((res) => console_log('error' + res));
                                saveObject(data_key, res.success.data).then().catch((res) => console_log('error' + res));
                                saveObject(settings_key, res.success.settings).then().catch((res) => console_log('error' + res));

                                navigation.navigate('StaffDashboard');
                            }
                        } else if (res.message) {
                            // console_log((res.message));
                            errorAlert(res.message);
                        } else if (res.error) {
                            // console_log((res.error));
                            errorAlert(parseError(res.error));
                        }

                    })
                    .catch((err) => {
                        setIsLoading(false);
                        console_log('error: ' + err);
                        // errorAlert('Wrongng Credentials')
                    });
            })
            .catch((err) => {
                errorAlert(general_network_error);
            });
    };
    const signup = () => {
        // console_log(new Date().getTime())
        // return;
        navigation.navigate('SignUpForm');
    };
    const forgetPassword = () => {
        navigation.navigate('ForgetPassword');
    };

    const termsPressed = () => {
        Linking.openURL('https://qalbish.pk/privacy-policy').catch((err) => console.error('An error occurred', err));
    };
    const privacyPolicyPressed = () => {
        return;
        Linking.openURL('http://qalbish.pk/').catch((err) => console.error('An error occurred', err));
    };

    function forceCrash() {
        // console_log('crashing')
        crashlytics().log('Testing crash');
        crashlytics().crash();
    }

    function sendReport() {
        crashlytics().recordError(new Error('Error with a stack trace'));
    }

    return (
        <MyBackgroundImage>
            <StatusBar backgroundColor={Colors.app_background_color} barStyle="dark-content" hidden={false}/>
            <ScrollView bounce={false} style={styles.scrollContainer}>
                <View>
                    <View style={styles.container}>
                        <SignUpLogo/>

                        <View style={styles.inputContainer}>
                            <View style={styles.emailOrPhoneContainer}>
                                <TextInput onChangeText={(email) => setPhone(email)}
                                           value={phone}
                                           placeholder={MyStrings.login_phone_placeholder}
                                           autoCapitalize='none'
                                           maxLength={11}
                                           keyboardType={'phone-pad'}
                                           style={InputFieldStyle.inputField}/>
                            </View>

                            <View style={[styles.emailOrPhoneContainer, {marginTop: 10}]}>
                                <TextInput onChangeText={(email) => setPassword(email)}
                                           value={password}
                                           placeholder={MyStrings.login_password_placeholder}
                                           autoCapitalize='none'
                                           secureTextEntry={true}
                                           style={InputFieldStyle.inputField}/>
                            </View>
                            <TouchableOpacity style={{}} onPress={forgetPassword}>
                                <AppText style={[styles.byInstalmentText, {marginTop: 10}]}>
                                    {MyStrings.forgot_password}
                                </AppText>
                            </TouchableOpacity>

                            <MyTextButton onPress={loginPressed}
                                          buttonText={MyStrings.login_button}
                                          buttonContainerStyle={[styles.loginButton, {marginTop: 30}]}/>
                        </View>
                    </View>
                    <View>
                        <View style={styles.signUpContainer}>
                            <AppText style={styles.dontHaveAccountText}>
                                {MyStrings.dontHaveAccount}
                            </AppText>
                            <TouchableOpacity onPress={signup}>
                                <AppText style={styles.signUpTextStyle}>
                                    {MyStrings.signup}
                                </AppText>
                            </TouchableOpacity>
                        </View>

                        <View style={styles.policyDisclaimerContainer}>
                            <AppText style={styles.disclaimerTextStyle}>
                                {MyStrings.disclaimer1}
                            </AppText>
                            <TouchableOpacity onPress={termsPressed}>
                                <AppText style={styles.termsStyle}>
                                    {MyStrings.terms}
                                </AppText>
                            </TouchableOpacity>
                            <AppText style={styles.disclaimerTextStyle}>
                                {MyStrings.and}
                            </AppText>

                            <TouchableOpacity onPress={privacyPolicyPressed}>
                                <AppText style={styles.termsStyle}>
                                    {MyStrings.privacy}
                                </AppText>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </ScrollView>
            {isLoading && <Loader/>}
        </MyBackgroundImage>
    );
};

// Loginn.navigationOptions = ({ navigation }) => {
//     return {
//         title: 'home',
//     }
// }


export default Loginn;
const styles = StyleSheet.create({
    scrollContainer: {
        flex: 1,

    },
    container: {
        flex: 1,
        // backgroundColor: Colors.app_background_color,
        margin: dimen.signup_margin,
        justifyContent: 'flex-start',
        alignItems: 'stretch',

    },
    crossContain: {
        alignItems: 'flex-end',
    },
    loginorContain: {
        marginTop: dimen.app_padding * 2,
        alignItems: 'center',
    },
    byInstalmentText: {
        fontSize: 14,
        alignItems: 'center',
        color: Colors.light_text_login,
    },
    crossButtonContainer: {
        width: 20,
        height: 20,
    },
    loginLogoButtonContainer: {
        width: 154,
        height: 56,
    },
    inputContainer: {
        marginTop: 50,
        flexGrow: 1,

    },
    emailOrPhoneContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        borderColor: Colors.primary_color,
        backgroundColor: Colors.app_background_color,
        borderWidth: 1,
        // borderRadius: dimen.login_border_radius,
        paddingRight: dimen.app_padding,
        paddingLeft: dimen.app_padding,
    },
    userIconStyle: {
        width: 12,
        height: 12,
    },
    inputField: {
        margin: dimen.app_padding,
        marginRight: dimen.app_padding,
        marginLeft: dimen.app_padding - 4,
        fontSize: 13,
        flexGrow: 1,

    },

    orTextStyle: {
        marginTop: 30,
        marginBottom: 30,
        flex: 1,
        alignSelf: 'center',
    },
    quickConnectText: {
        // backgroundColor:'green'
        marginRight: 10,
        marginLeft: 10,

    },
    line: {
        height: 1,
        width: 30,
        flexGrow: 1,
        alignSelf: 'center',
        backgroundColor: Colors.border_color,

    },
    loginButton: {
        marginTop: dimen.app_padding,

    },
    quickConnectContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 0,

    },
    socialContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        margin: dimen.app_padding,

    },
    signUpContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        marginBottom: 20,
    },
    dontHaveAccountText: {
        fontSize: 16,
        alignItems: 'center',
        color: Colors.light_text_login,
    },
    signUpTextStyle: {
        fontSize: 18,
        alignItems: 'center',
        alignSelf: 'center',
        color: Colors.primary_color,
        borderBottomColor: Colors.primary_color,
        borderBottomWidth: 1,
        fontWeight: 'bold',
    },
    policyDisclaimerContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        flexWrap: 'wrap',
        padding: 10,
        // paddingBottom: 0,
        alignSelf: 'flex-end',
        marginRight: dimen.signup_margin,
        marginLeft: dimen.signup_margin,


    },
    disclaimerTextStyle: {
        fontSize: 9,
        alignItems: 'center',
        color: Colors.light_text_login,
    },
    termsStyle: {
        fontSize: 9,
        alignItems: 'center',
        alignSelf: 'center',
        color: Colors.primary_color,
        borderBottomColor: Colors.primary_color,
        borderBottomWidth: 1,
        fontWeight: 'bold',
    },

});

