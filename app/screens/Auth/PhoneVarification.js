import React, {useState,useEffect} from 'react';
import {
    Alert,
    TouchableOpacity,
    View, StyleSheet,
} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import dimen from '../../Styles/Dimen';
import AppText from '../../Components/AppText';
import MyTextButton, {MyTextButtonSimple} from '../../Components/MyTextButton';
import Colors from '../../Styles/Colors';
import MyBackgroundImage from '../../Components/MyBackgroundImage';
import MyStrings from '../../Classes/MyStrings';
import {SignUpCross} from '../../Components/SignUpLogo';
import SignUpLogo from '../../Components/SignUpLogo';
import {console_log, errorAlert, getDataWithoutToken, isNetConnected, parseError} from '../../Classes/auth';
import {general_network_error, get_otp_url, verify_otp_url} from '../../Classes/UrlConstant';
import {Loader} from "../../Components/Loader";

const PhoneVarification = ({navigation}) => {
    const [otp, setOtp] = useState(['', '', '', '']);
    const [phone, setPhone] = useState(['']);
    const [isLoading, setIsLoading] = useState(false);



    useEffect(()=>{
        let ph= navigation.getParam('phone')
        setPhone(ph)
    },[])

    const crossPressed = () => {
        navigation.pop();
    };

    const verifyOtp = () => {
        let otpText = otp[0] + otp[1] + otp[2] + otp[3];
        if (otpText === '') {
            errorAlert('Enter 4 digit otp');
            return;
        }
        console_log()
        isNetConnected()
            .then(() => {
                setIsLoading(true);
                let formData = {
                    'phone_number': phone,
                    'otp_code': otpText,
                };
                getDataWithoutToken(verify_otp_url, 'POST', formData)
                    .then((response) => {
                        setIsLoading(false);
                        let res = JSON.parse(response);
                        console_log(res)

                        if (res.status === true) {
                            Alert.alert(
                                'Success',
                                res.message,
                                [
                                    {
                                        text: 'okay',
                                        onPress: () => {
                                            navigation.navigate('NewPassword',{'phone':phone});
                                        },
                                    },
                                ],
                                {cancelable: false},
                            );

                        } else if (res.error !== undefined) {
                            console_log((res.error));
                            errorAlert(parseError(res.error));
                        }

                    })
                    .catch((err) => {
                        setIsLoading(false);
                        console_log('error: ' + err);
                        // errorAlert('Wrongng Credentials')
                    });
            })
            .catch((err) => {
                errorAlert(general_network_error);
            });
    };


    let textInputFields = [];
    const focusPrevious = (key, index) => {
        if (key === 'Backspace' && index !== 0) {
            textInputFields[index - 1].focus();
        }
    };
    const focusNext = (index, value) => {
        if (index < textInputFields.length - 1 && value) {
            textInputFields[index + 1].focus();
        }
        if (index === textInputFields.length - 1) {
            textInputFields[index].blur();
        }
        // const otp = this.state.otp;
        otp[index] = value;
        setOtp(otp);
        console.log(otp);
        // this.props.getOtp(otp.join(''));
    };


    const renderInputs = () => {
        const inputs = Array(4).fill(0);
        const txtContainer = inputs.map(
            (i, j) =>
                <TextInput onChangeText={v => focusNext(j, v)}
                           onKeyPress={e => focusPrevious(e.nativeEvent.key, j)}
                           key={j}
                           maxLength={1}
                           keyboardType="numeric"
                           autoCapitalize='none'
                           ref={(input) => textInputFields[j] = input}
                           style={styles.textInput}/>,
        );
        return txtContainer;

    };


    return (
        <MyBackgroundImage>
            <View style={styles.container}>
                <SignUpCross onPress={crossPressed}/>
                <SignUpLogo/>

                <View style={styles.mainContainer}>
                    <View>
                        <AppText style={styles.enterCodeTextStyle}>
                            {MyStrings.enterCodeText}
                        </AppText>
                        <AppText style={styles.enterCodeTextStyle}>
                            {MyStrings.sentViaSms}
                        </AppText>


                        <View style={styles.inputContainer}>
                            {renderInputs()}
                        </View>

                        <MyTextButton onPress={verifyOtp}
                                      buttonText={MyStrings.submit}
                                      buttonContainerStyle={[styles.loginButton]}/>
                    </View>

                    <View>
                        <AppText style={styles.enterCodeTextStyle}>
                            {MyStrings.termsAndConditions}
                        </AppText>
                    </View>
                </View>
            </View>
            {isLoading && <Loader/>}
        </MyBackgroundImage>

    );

};
export default PhoneVarification;
const styles = StyleSheet.create({
    scrollContainer: {
        flex: 1,
        // backgroundColor: Colors.app_background_color,

    },
    mainContainer: {
        flex: 1,
        // backgroundColor: Colors.app_background_color,
        marginTop: 30,
        justifyContent: 'space-between',
        alignItems: 'stretch',
    },
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        margin: dimen.signup_margin,

    },
    crossContain: {
        alignItems: 'flex-end',
    },
    loginorContain: {
        marginTop: dimen.app_padding * 2,
        alignItems: 'center',
    },
    byInstalmentText: {
        fontSize: 12,
        alignItems: 'center',
        color: Colors.light_text_login,
    },
    enterCodeTextStyle: {
        fontSize: 14,
        alignItems: 'center',
        alignSelf: 'center',
        color: Colors.light_text_login,
    },
    callNowTextStyle: {
        fontSize: 18,
        alignItems: 'center',
        alignSelf: 'center',
        color: Colors.primary_color,
        borderBottomColor: Colors.primary_color,
        borderBottomWidth: 1,
        fontWeight: 'bold',
    },
    crossButtonContainer: {
        width: 20,
        height: 20,
    },
    loginLogoButtonContainer: {
        width: 154,
        height: 56,
    },
    textInput: {
        backgroundColor: Colors.signup_input_text_background_color,
        padding: 7,
        margin: 7,
        borderRadius: 5,
        textAlign: 'center',
    },
    inputContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 15,
    },
    emailOrPhoneContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        borderColor: Colors.primary_color,
        backgroundColor: Colors.app_background_color,
        borderWidth: 1,
        // borderRadius: dimen.login_border_radius,
        paddingRight: dimen.app_padding,
        paddingLeft: dimen.app_padding,


    },
    userIconStyle: {
        width: 12,
        height: 12,
    },
    inputField: {
        marginRight: dimen.app_padding,
        marginLeft: dimen.app_padding - 4,
        fontSize: 11,

    },
    orTextStyle: {
        marginTop: 20,
        flex: 1,
        alignSelf: 'center',
    },
    quickConnectText: {
        // backgroundColor:'green'
        marginRight: 10,
        marginLeft: 10,

    },
    line: {
        height: 1,
        width: 30,
        flexGrow: 1,
        alignSelf: 'center',
        backgroundColor: Colors.border_color,

    },
    quickConnectContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 20,

    },
    loginButton: {
        // marginTop: dimen.app_padding,
        marginTop: 30,

    },
    forgetPasswordContainer: {
        marginTop: dimen.app_padding,
        flexDirection: 'row',
        justifyContent: 'flex-start',

    },
    forgetPasswordText: {
        fontSize: 14,
        color: Colors.button_text_color,
    },
});

const stylesOld = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'stretch',
    },
    crossContain: {
        flexDirection: 'row',
        justifyContent: 'space-between',

    },
    loginorContain: {
        marginTop: dimen.app_padding * 2,
        alignItems: 'center',
    },
    loginorText: {
        fontSize: 25,
        alignItems: 'center',
    },
    crossButtonContainer: {
        width: 30,
        height: 30,
    },
    inputContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 15,
    },
    textInput: {
        backgroundColor: Colors.signup_input_text_background_color,
        padding: 7,
        margin: 7,
        borderRadius: 5,
        textAlign: 'center',
    },
    line: {
        height: 1,
        width: '100%',
        alignSelf: 'center',
        backgroundColor: Colors.border_color,

    },
    loginButton: {
        marginTop: dimen.app_padding,

    },
    forgetPasswordContainer: {
        marginTop: dimen.app_padding,
        flexDirection: 'row',
        justifyContent: 'flex-start',

    },
    forgetPasswordText: {
        fontSize: 14,
        color: Colors.button_text_color,
    },
});
