import React, {useState, useEffect} from 'react';
import {
    Alert,
    Image,
    Text,
    Platform,
    TouchableOpacity, ScrollView, TouchableWithoutFeedback,
    View, StyleSheet, BackHandler, Linking,
} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import TextStyles, {InputFieldStyle} from '../../Styles/TextStyles';
import dimen from '../../Styles/Dimen';
import {
    _storeData, console_log, errorAlert, getDataWithoutToken, isNetConnected, saveObject, successAlert,
} from '../../Classes/auth';

import {
    area_url,
    cities_url,
    data_key,
    general_network_error,
    internetError,
    login_url,
    main_url,
    referal_key,
    register_url,
    token_key,
    web_view_url,
    web_view_url_key,
    webview_web_url,
} from '../../Classes/UrlConstant';
import {DropDownBoth, Loader, SearchDropDown, ThankyouDialog} from '../../Components/Loader';

import AppText from '../../Components/AppText';
import MyTextButton, {MyTextButtonSimple} from '../../Components/MyTextButton';
import Colors from '../../Styles/Colors';
import MyStrings from '../../Classes/MyStrings';
import MyBackgroundImage from '../../Components/MyBackgroundImage';
import SignUpLogo, {SignUpCross} from '../../Components/SignUpLogo';
import {DropDown} from '../../Components/DropDown';
import {MyImage} from '../../Components/MyImageButton';

const SignUpForm = ({navigation}) => {
    const [isLoading, setIsLoading] = useState(false);
    const [termSelected, setTermSelected] = useState(false);

    const [fname, setFname] = useState('');
    const [lname, setLname] = useState('');
    const [email, setEmail] = useState('');

    const [gender, setGender] = useState([
        {'id': 0, 'title': 'Select Gender'},
        {'id': 1, 'title': 'Female'},
        {'id': 2, 'title': 'Male'}]);
    const [selectedGender, setSelectedGender] = useState(gender[0]);

    const [age, setAge] = useState([
        {'id': 0, 'title': 'Select Age'}, {'id': 1, 'title': 'Below 18'},
        {'id': 2, 'title': '18 - 24'}, {'id': 3, 'title': '25 - 34'},
        {'id': 4, 'title': '35 - 44'}, {'id': 5, 'title': '45 - 60'},
        {'id': 6, 'title': '60+'}]);
    const [selectedAge, setSelectedAge] = useState(age[0]);

    const [phone, setPhone] = useState('');
    const [cnic, setCnic] = useState('');
    const [password, setPassword] = useState('');
    const [cPassword, setCPassword] = useState('');
    const [address, setAddress] = useState('');

    const [areaItems, setAreaItems] = useState([{'id': 0, 'title': 'Select City'}, {'id': 1, 'title': 'Select City'}]);

    const [city, setCity] = useState([{'id': 0, 'title': 'Select City'}]);
    const [selectedCity, setSelectedCity] = useState(city[0]);

    const [showArea, setShowArea] = useState(false);
    const [farea, setfArea] = useState([{'id': 0, 'title': 'Select Area'}]);
    const [area, setArea] = useState([{'id': 0, 'title': 'Select Area'}]);
    const [selectedArea, setSelectedArea] = useState(area[0]);

    const [referalCode, setReferalCode] = useState('');
    const [referalCodeCheck, setReferalCodeCheck] = useState(false);

    const [showDialog, setShowDialog] = useState(false);
    const [genderDialog, setGenderDialog] = useState(true);


    const [cnicBool, setCnicBool] = useState(false);


    useEffect(() => {
        getCities();
    }, []);

    const getCities = () => {
        isNetConnected()
            .then(() => {
                setIsLoading(true);
                getDataWithoutToken(cities_url, 'GET', '')
                    .then((response) => {
                        setIsLoading(false);
                        let data = JSON.parse(response).data;
                        const c = data.map((i, j) => {
                            return {'id': i.id, 'title': i.name};
                        });
                        setCity(city.slice(0, 1).concat(c));
                    })
                    .catch((err) => {
                        setIsLoading(false);
                        console_log('error: ' + err);
                    });
            })
            .catch((err) => {
                errorAlert(general_network_error);
            });
    };

    const citySelected = (item) => {
        getAreasOfCity(item);
    };
    const getAreasOfCity = (item) => {
        isNetConnected()
            .then(() => {
                setIsLoading(true);
                getDataWithoutToken(area_url + item.id, 'GET', '')
                    .then((response) => {
                        setIsLoading(false);
                        let data = JSON.parse(response).data;
                        const c = data.map((i, j) => {
                            return {'id': i.id, 'title': i.name};
                        });
                        setArea(area.slice(0, 1).concat(c));
                        setfArea(area.slice(0, 1).concat(c));
                    })
                    .catch((err) => {
                        setIsLoading(false);
                        console_log('error: ' + err);
                    });
            })
            .catch((err) => {
                errorAlert(general_network_error);
            });
    };

    const termsandConditionsCheckbox = () => {
        setTermSelected(!termSelected);
    };

    const parseError = (error) => {
        var m = error;
        var keys = [];
        for (var k in m) {
            keys.push(k);
        }

        var mforuser = '';
        // console.log('key 0 ' + (m[keys[0]]))

        for (var k in keys) {
            mforuser = mforuser + m[keys[k]] + '\n';
        }

        return mforuser;
    };

    const signupPressed = () => {
        //region validation
        if (fname === '') {
            errorAlert('First name is required.');
            return;
        }
        if (lname === '') {
            errorAlert('Last name is required.');
            return;
        }
        if (email === '') {
            errorAlert('Email is required.');
            return;
        }
        if (selectedGender.id === 0) {
            errorAlert('Select gender');
            return;
        }
        if (selectedAge.id === 0) {
            errorAlert('Select age');
            return;
        }
        if (phone === '') {
            errorAlert('Phone number is required.');
            return;
        }
        if (phone.length !== 11) {
            errorAlert('Phone number must be of 11 characters.');
            return;
        }
        if (cnic === '') {
            errorAlert('CNIC is required.');
            return;
        }
        if (cnic.length !== 15) {
            errorAlert('CNIC must be of 13 digits.');
            return;
        }
        if (password === '') {
            errorAlert('Password is required.');
            return;
        }
        if (cPassword === '') {
            errorAlert('Confirm password is required.');
            return;
        }
        if (address === '') {
            errorAlert('Address is required.');
            return;
        }
        if (selectedCity.id === 0) {
            errorAlert('Select a city.');
            return;
        }
        if (selectedArea.id === 0) {
            errorAlert('Select area.');
            return;
        }
        if (!termSelected) {
            errorAlert('Agree to terms and conditions.');
            return;
        }
        //endregion

        isNetConnected()
            .then(() => {
                setIsLoading(true);
                let formData = {
                    'first_name': fname,
                    'last_name': lname,
                    'city_id': selectedCity.id,
                    'area_id': selectedArea.id,
                    'age': selectedAge.title,
                    'phone_number': phone,
                    'cnic': cnic,
                    'gender': selectedGender.id,
                    'address': address,
                    'email': email,
                    'password': password,
                    'password_confirmation': cPassword,
                    'referral_code': referalCode,
                };
                getDataWithoutToken(register_url, 'POST', formData)
                    .then((response) => {
                        setIsLoading(false);
                        let res = JSON.parse(response);
                        if (res.message) {
                            setShowDialog(true);
                            // return;
                            // Alert.alert('Success', res.message,
                            //     [
                            //         // {
                            //         //     text: 'Cancel',
                            //         //     onPress: () => console.log('Cancel Pressed'),
                            //         //     style: 'cancel',
                            //         // },
                            //         {
                            //             text: 'Okay', onPress: () => {
                            //                 backPressed()
                            //             }
                            //         },
                            //     ],
                            //     {cancelable: false},)
                        } else {
                            // console_log(res)
                            errorAlert(parseError(res.error));
                        }


                    })
                    .catch((err) => {
                        setIsLoading(false);
                        console_log('error: ' + err);
                        // errorAlert('Wrongng Credentials')
                    });
            })
            .catch((err) => {
                errorAlert(general_network_error);
            });
    };

    const backPressed = () => {
        navigation.goBack();
    };

    const setReferalCodeCheckFunction = () => {
        setReferalCodeCheck(!referalCodeCheck);
    };

    const termsPressed = () => {
        Linking.openURL('https://qalbish.pk/privacy-policy').catch((err) => console.error('An error occurred', err));
    };
    const privacyPolicyPressed = () => {
        return;
        Linking.openURL('http://qalbish.pk/').catch((err) => console.error('An error occurred', err));
    };


    return (
        <MyBackgroundImage>
            <ScrollView style={styles.scrollContainer}>
                <View style={styles.container}>
                    <SignUpCross onPress={backPressed}/>
                    <SignUpLogo/>
                    <View style={styles.inputContainer}>
                        <AppText style={styles.welcomeText}>
                            Sign up
                        </AppText>

                        <View style={styles.emailOrPhoneContainer}>
                            <TextInput onChangeText={(email) => setFname(email)}
                                       value={fname}
                                       placeholder={MyStrings.firstname}
                                       autoCapitalize='none'
                                       style={InputFieldStyle.inputField}/>
                        </View>

                        <View style={[styles.emailOrPhoneContainer, {marginTop: 10}]}>
                            <TextInput onChangeText={(email) => setLname(email)}
                                       value={lname}
                                       placeholder={MyStrings.lastname}
                                       autoCapitalize='none'
                                       style={InputFieldStyle.inputField}/>
                        </View>

                        <View style={[styles.emailOrPhoneContainer, {marginTop: 10}]}>
                            <TextInput onChangeText={(email) => setEmail(email)}
                                       value={email}
                                       placeholder={MyStrings.email}
                                       keyboardType={'email-address'}
                                       autoCapitalize='none'
                                       style={InputFieldStyle.inputField}/>
                        </View>

                        <DropDown itemsArray={gender}
                                  selectedItem={selectedGender}
                                  setSelectedItems={setSelectedGender}
                        />

                        <DropDown itemsArray={age}
                                  selectedItem={selectedAge}
                                  setSelectedItems={setSelectedAge}
                        />

                        <View style={[styles.emailOrPhoneContainer, {marginTop: 10}]}>
                            <TextInput onChangeText={(email) => setPhone(email)}
                                       value={phone}
                                       placeholder={MyStrings.mobile_number}
                                       autoCapitalize='none'
                                       maxLength={11}
                                       keyboardType={'phone-pad'}
                                       style={InputFieldStyle.inputField}/>
                        </View>

                        <View style={[styles.emailOrPhoneContainer, {marginTop: 10}]}>
                            <TextInput onChangeText={(email) => {
                                // if (cnicBool){
                                //     email=cnic;
                                //     setCnicBool(false)
                                // }
                                if (email.length === 5 || email.length === 13) {
                                    if (cnic.length === 4 || cnic.length === 12) {
                                        setCnic(email + '-');
                                    }
                                } else {
                                    setCnic(email);
                                }
                            }}
                                       onKeyPress={e => {
                                           if (e.nativeEvent.key === 'Backspace') {
                                               if (cnic.length === 6) {
                                                   setCnicBool(true)
                                                   setCnic(cnic.slice(0, 5));
                                               }
                                               if (cnic.length === 14) {
                                                   setCnicBool(true)
                                                   setCnic(cnic.slice(0, 13));
                                               }
                                           }
                                       }}
                                       maxLength={15}
                                       value={cnic}
                                       placeholder={MyStrings.cnic}
                                       autoCapitalize='none'
                                       keyboardType={'numeric'}
                                       style={InputFieldStyle.inputField}/>
                        </View>
                        <View style={[styles.emailOrPhoneContainer, {marginTop: 10}]}>
                            <TextInput onChangeText={(email) => setPassword(email)}
                                       value={password}
                                       secureTextEntry={true}
                                       placeholder={MyStrings.password}
                                       autoCapitalize='none'
                                       style={InputFieldStyle.inputField}/>
                        </View>
                        <View style={[styles.emailOrPhoneContainer, {marginTop: 10}]}>
                            <TextInput onChangeText={(email) => setCPassword(email)}
                                       value={cPassword}
                                       secureTextEntry={true}
                                       placeholder={MyStrings.confirm_password}
                                       autoCapitalize='none'
                                       style={InputFieldStyle.inputField}/>
                        </View>
                        <View style={[styles.emailOrPhoneContainer, {marginTop: 10}]}>
                            <TextInput onChangeText={(email) => setAddress(email)}
                                       value={address}
                                       placeholder={MyStrings.address}
                                       autoCapitalize='none'
                                       style={InputFieldStyle.inputField}/>
                        </View>

                        <DropDown itemsArray={city}
                                  selectedItem={selectedCity}
                                  setSelectedItems={setSelectedCity}
                                  onSelection={citySelected}
                        />

                        {/*<DropDown itemsArray={area}*/}
                        {/*          selectedItem={selectedArea}*/}
                        {/*          setSelectedItems={setSelectedArea}*/}
                        {/*/>*/}
                        <TouchableWithoutFeedback onPress={() => setShowArea(true)}>
                            <View style={[styles.emailOrPhoneContainer, {
                                marginTop: 10,
                                justifyContent: 'space-between',
                            }]}>
                                <AppText style={styles.selectedAreaText}>
                                    {selectedArea.title}
                                </AppText>
                                <MyImage source={require('../../Asset/down.png')}
                                         imageContainerStyle={styles.dropdownStyle}/>
                            </View>

                        </TouchableWithoutFeedback>

                        {false && <View style={styles.alternateAddressContainer}>
                            <View style={styles.checkBoxMainContainer2}>
                                {!referalCodeCheck &&
                                <TouchableOpacity onPress={setReferalCodeCheckFunction}>
                                    <View style={styles.checboxContainer2}>
                                    </View>
                                </TouchableOpacity>
                                }
                                {referalCodeCheck &&
                                <TouchableOpacity onPress={setReferalCodeCheckFunction}>
                                    <View style={styles.checkboxContainerNotselected2}>
                                        <Image source={require('../../Asset/tick.png')}
                                               style={styles.tickImageStyle2}/>
                                    </View>
                                </TouchableOpacity>
                                }
                            </View>

                            <AppText style={[styles.inputFieldHeading]}>
                                User referral code.
                            </AppText>
                        </View>}

                        {!referalCodeCheck &&
                        <View style={[styles.emailOrPhoneContainer, {marginTop: 10}]}>
                            <TextInput onChangeText={(email) => setReferalCode(email)}
                                       value={referalCode}
                                       placeholder={MyStrings.referal}
                                       autoCapitalize='none'
                                       style={InputFieldStyle.inputField}/>
                        </View>}


                        <View style={styles.termAndConditionStyle}>
                            <View style={styles.checkBoxMainContainer}>
                                {!termSelected &&
                                <TouchableOpacity onPress={termsandConditionsCheckbox}>
                                    <View style={styles.checboxContainer}>
                                    </View>
                                </TouchableOpacity>
                                }
                                {termSelected &&
                                <TouchableOpacity onPress={termsandConditionsCheckbox}>
                                    <View style={styles.checkboxContainerNotselected}>
                                        <Image source={require('../../Asset/tick.png')}
                                               style={styles.tickImageStyle}/>
                                    </View>
                                </TouchableOpacity>
                                }
                            </View>

                            <View style={styles.termsContainer}>
                                <AppText style={[styles.agreeTermsStyle]}>
                                    {MyStrings.disclaimertext}
                                </AppText>

                                <TouchableOpacity onPress={termsPressed}>
                                    <AppText style={[styles.termsandConditionsStyle]}>
                                        {MyStrings.terms}
                                    </AppText>
                                </TouchableOpacity>
                                <AppText style={[styles.agreeTermsStyle]}>
                                    {MyStrings.and}
                                </AppText>
                                <TouchableOpacity onPress={privacyPolicyPressed}>
                                    <AppText style={[styles.termsandConditionsStyle]}>
                                        {MyStrings.privacy}
                                    </AppText>
                                </TouchableOpacity>
                            </View>

                        </View>

                        <MyTextButton onPress={signupPressed}
                                      buttonText={'Submit'}
                                      buttonContainerStyle={[styles.loginButton, {marginTop: 30}]}/>

                    </View>
                </View>
            </ScrollView>
            {isLoading && <Loader/>}
            <SearchDropDown
                show={showArea}
                setShow={setShowArea}
                setFiltered={setfArea}
                itemsArray={farea}
                itemsArrayOrignal={area}
                selectedItem={selectedArea}
                setSelectedItems={setSelectedArea}/>

            <ThankyouDialog
                show={showDialog}
                setShow={() => {
                    setShowDialog(false);
                    backPressed();
                }}
                text={'Thank you for registering with Qalbish'}
            />


            {/*<DropDownBoth*/}
            {/*    show={genderDialog}*/}
            {/*    setShow={()=>{setGenderDialog(false)}}*/}
            {/*    itemsArray={gender}*/}
            {/*    selectedItem={selectedGender}*/}
            {/*    setSelectedItems={setSelectedGender}*/}
            {/*/>*/}
        </MyBackgroundImage>
    );

};
export default SignUpForm;
const styles = StyleSheet.create({
    scrollContainer: {
        flex: 1,

    },
    container: {
        flex: 1,
        // backgroundColor: Colors.app_background_color,
        margin: dimen.signup_margin,
        justifyContent: 'flex-start',
        alignItems: 'stretch',

    },
    crossContain: {
        alignItems: 'flex-end',
    },
    crossButtonContainer: {
        width: 20,
        height: 20,
    },
    loginorContain: {
        marginTop: dimen.app_padding * 2,
        alignItems: 'center',
    },
    byInstalmentText: {
        fontSize: 12,
        alignItems: 'center',
        color: Colors.light_text_login,

    },
    welcomeText: {
        fontSize: 22,
        alignItems: 'center',
        color: Colors.primary_color,
        marginBottom: 10,
        textAlign: 'center',
    },
    loginLogoButtonContainer: {
        width: 154,
        height: 56,
    },
    inputContainer: {
        marginTop: 30,
        flexGrow: 1,
        justifyContent: 'center',

    },
    emailOrPhoneContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        borderColor: Colors.primary_color,
        backgroundColor: Colors.app_background_color,
        borderWidth: 1,
        // borderRadius: dimen.login_border_radius,
        paddingRight: dimen.app_padding,
        paddingLeft: dimen.app_padding,


    },
    userIconStyle: {
        width: 12,
        height: 12,
    },
    inputField: {
        marginRight: dimen.app_padding,
        marginLeft: dimen.home_item_padding,
        fontSize: 16,
        flexGrow: 1,

    },

    orTextStyle: {
        marginTop: 30,
        marginBottom: 30,
        flex: 1,
        alignSelf: 'center',
    },
    quickConnectText: {
        // backgroundColor:'green'
        marginRight: 10,
        marginLeft: 10,

    },
    line: {
        height: 1,
        width: 30,
        flexGrow: 1,
        alignSelf: 'center',
        backgroundColor: Colors.border_color,

    },
    loginButton: {
        marginTop: dimen.app_padding,

    },
    quickConnectContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 0,

    },
    socialContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        margin: dimen.app_padding,

    },
    signUpContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        marginBottom: 20,
    },
    dontHaveAccountText: {
        fontSize: 16,
        alignItems: 'center',
        color: Colors.light_text_login,
    },
    signUpTextStyle: {
        fontSize: 18,
        alignItems: 'center',
        alignSelf: 'center',
        color: Colors.primary_color,
        borderBottomColor: Colors.primary_color,
        borderBottomWidth: 1,
        fontWeight: 'bold',
    },
    policyDisclaimerContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        flexWrap: 'wrap',
        padding: 10,
        // paddingBottom: 0,
        alignSelf: 'flex-end',
        marginRight: dimen.signup_margin,
        marginLeft: dimen.signup_margin,


    },
    disclaimerTextStyle: {
        fontSize: 9,
        alignItems: 'center',
        color: Colors.light_text_login,
    },
    termsStyle: {
        fontSize: 9,
        alignItems: 'center',
        alignSelf: 'center',
        color: Colors.primary_color,
        borderBottomColor: Colors.primary_color,
        borderBottomWidth: 1,
        fontWeight: 'bold',
    },


    termAndConditionStyle: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginTop: 25,
        marginBottom: 25,
    },
    termsContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        flex: 1,
        flexWrap: 'wrap',
    },
    checboxContainer: {
        width: 30,
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        // borderRadius: 5,
        borderColor: Colors.primary_color,
    },
    checkboxContainerNotselected: {
        width: 30,
        height: 30,
        // borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.primary_color,
    },
    tickImageStyle: {width: 13, height: 13, alignSelf: 'center', resizeMode: 'contain'},
    checkBoxMainContainer: {marginRight: 10, justifyContent: 'center', alignItems: 'center'},
    agreeTermsStyle: {
        fontSize: 14,
        alignItems: 'center',
        color: Colors.light_text_login,
    },
    termsandConditionsStyle: {
        fontSize: 14,
        alignItems: 'center',
        color: Colors.primary_color,
        fontWeight: 'bold',
    },

    dropdownStyle: {
        width: 14,
        height: 14,
    },
    selectedAreaText: {
        padding: 8,
        paddingTop: dimen.app_padding,
        paddingBottom: dimen.app_padding,
        fontSize: 16,
    },


    alternateAddressContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginTop: 10,
        marginBottom: 5,
        // marginLeft:dimen.app_padding
    },
    termsContainer2: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        flex: 1,
        flexWrap: 'wrap',
    },
    checboxContainer2: {
        width: 20,
        height: 20,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        // borderRadius: 5,
        borderColor: Colors.primary_color,
    },
    checkboxContainerNotselected2: {
        width: 20,
        height: 20,
        // borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.primary_color,
    },
    tickImageStyle2: {width: 10, height: 10, alignSelf: 'center', resizeMode: 'contain'},
    checkBoxMainContainer2: {marginRight: 10, justifyContent: 'center', alignItems: 'center'},

});

