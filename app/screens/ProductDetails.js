import React, {useState, useEffect} from 'react';
import {
    Alert,
    Image,
    KeyboardAvoidingView,
    StyleSheet, SafeAreaView,
    Text, SectionList,
    Platform,
    TouchableOpacity, ScrollView,
    StatusBar,
    View,
} from 'react-native';
import {FlatList, TextInput} from 'react-native-gesture-handler';
import dimen from '../Styles/Dimen';

import Colors from '../Styles/Colors';
import AppText from '../Components/AppText';
import MyImageButton, {MyImage} from '../Components/MyImageButton';
import MyBackgroundImage from '../Components/MyBackgroundImage';
import MyStrings from '../Classes/MyStrings';
import ProductBanner from '../Components/ProductBanner';
import {FeatureCard} from '../Components/FeatureCard';
import TopBar from '../Components/TopBar';


export const ProductDetails = ({navigation}) => {

    const [categories1, setCategories1] = useState([{'id': 1}, {'id': 2}, {'id': 3}, {'id': 4}]);
    const [categories, setCategories] = useState([{'id': 1},
        {'id': 2}, {'id': 3}, {'id': 4}, {'id': 5}, {'id': 6}, {'id': 7}, {'id': 8},
        {'id': 9}, {'id': 10}, {'id': 11}, {'id': 12}, {'id': 13}
        , {'id': 14}, {'id': 15}, {'id': 16}, {'id': 17}, {'id': 18}
        , {'id': 19}, {'id': 20}, {'id': 21}, {'id': 22}, {'id': 23}]);
    const [isloading, setIsLoading] = useState(false);
    const [add, setAdd] = useState('Instalment.pk add here');
    const [email, setEmail] = useState('');


    useEffect(() => {
        setAdd('Instalment.pk new add here');
    });

    const subCatPress = (item) => {
        navigation.navigate('ProductListS');
    };
    const locationButtonPressed = (item) => {
        // navigation.navigate('ProductListS');
    };
    const goback = () => {
        navigation.goBack();
    };
    const renderSubCat = (cat) => {
        const txtContainer = cat.map(
            (i, j) =>
                <FeatureCard subCatPress={subCatPress} data={i}/>,
        );
        return (
            <View style={styles.subCategoriesMainContainer}>
                <View style={styles.featureContainer}>
                    {txtContainer}
                </View>
            </View>
        );

    };
    const SearchField = () => {
        return (
            <View style={styles.searchContainer}>
                <View style={styles.halfView}/>
                <View style={styles.searchbarContainer}>
                    <MyImage source={require('../Asset/search.png')}
                             imageContainerStyle={styles.searchIcon}/>
                    <TextInput onChangeText={(email) => setEmail({email})}
                               value={email}
                               placeholder={MyStrings.searchFieldPlaceHolder}
                               placeholderTextColor={Colors.light_border_color}
                               autoCapitalize='none'
                               style={styles.searchField}/>

                </View>
            </View>
        );
    };
    const Banner = () => {
        return (
            <View style={styles.carousalContainer}>
                <ProductBanner/>
            </View>
        );
    };
    const ProductDiscriptionHeader = () => {
        return (
            <View style={styles.subCategoriesMainContainer}>
                <View style={styles.categoryHeader}>
                    <View style={styles.headerStartStyle}/>
                    <AppText style={styles.headerTextStyle}>
                        Product Description
                    </AppText>
                </View>
            </View>
        );
    };
    const ProductName = () => {
        return (
            <View style={styles.textContainer}>
                <AppText style={styles.textStyle} >
                    Microsoft Surface Book 2 15 i7 Quad Core-8th-gen
                </AppText>
            </View>
        );
    };

    const FlatListHeader = () => {
        return (
            <View style={{flex: 1}}>
                {banner()}
                <View style={styles.subCategoriesMainContainer}>
                    <View style={styles.categoryHeader}>
                        <View style={styles.headerStartStyle}/>
                        <AppText style={styles.headerTextStyle}>
                            Laptops
                        </AppText>
                        <View style={styles.filterIconContainer}>
                            <MyImageButton onPress={locationButtonPressed}
                                           source={require('../Asset/filter.png')}
                                           imageContainerStyle={styles.filterIconStyle}/>
                            <MyImageButton onPress={locationButtonPressed}
                                           source={require('../Asset/sort.png')}
                                           imageContainerStyle={styles.filterIconStyle}/>

                        </View>
                    </View>
                </View>
            </View>
        );
    };


    return (

        <MyBackgroundImage>
            <TopBar backButton={goback} title={'Product'}/>
            <SearchField/>
            <ScrollView style={styles.scrollViewContainer}>
                <View style={styles.scrollViewInnerContainer}>
                    <Banner/>
                    <ProductName/>
                    <View style={styles.feacturesContainer}>
                        <AppText style={styles.featureHeaderTextStyle}>Features</AppText>
                        {renderSubCat(categories1)}
                    </View>


                    <ProductDiscriptionHeader/>

                    <View style={styles.feacturesContainer}>
                        <AppText style={styles.featureHeaderTextStyle}>Features</AppText>
                        {renderSubCat(categories)}
                    </View>


                </View>
            </ScrollView>

        </MyBackgroundImage>
    );

};

const styles = StyleSheet.create({
    dashboardButtonText: {fontFamily: 'roboto-bold', fontSize: 16},
    dashboardButton: {flex: 1, padding: 8, justifyContent: 'center', alignItems: 'center'},
    dashboardButtonContainer: {
        borderWidth: 1,
        borderColor: Colors.border_color,
        borderRadius: dimen.border_radius,
        overflow: 'hidden',
        backgroundColor: Colors.home_widget_background_color,
        margin: dimen.app_padding,
        justifyContent: 'space-evenly',
        alignItems: 'flex-start',
        flexDirection: 'row',

    },
    bannerAdStyle: {
        height: 90, backgroundColor: Colors.primary_color,
        padding: 20, margin: dimen.app_padding,
        justifyContent: 'center',
    },
    flatlistStyle: {
        // width: '100%',
        // backgroundColor: Colors.home_widget_background_color,
        // marginBottom: dimen.bottom_margin_for_bottom_menu + 20,
        padding: dimen.app_padding,
    },
    productFlatList: {
        width: '100%',
        backgroundColor: Colors.primary_color,
        // marginBottom: dimen.bottom_margin_for_bottom_menu + 20,
        // padding: dimen.app_padding,
    },
    flatlistContainerStyle: {
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        backgroundColor: Colors.primary_dark_color,
    },
    emptyComponentStyle: {
        justifyContent: 'flex-start',
        alignItems: 'center',
        // backgroundColor:Colors.primary_dark_color
    },

    //top bar
    topContainer: {
        height: 40,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.primary_color,
    },
    locationButtonContainer: {
        width: 15,
        height: 15,
        marginLeft: 10,
    },
    backArrowContainer: {
        width: 15,
        height: 15,
        // marginLeft: 10,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        position: 'absolute',
        left: 0,
        right: dimen.app_padding,
    },
    topHeading: {
        fontSize: 18,
        color: Colors.button_text_color,
    },
    locationHeading: {
        fontSize: 13,
        color: Colors.button_text_color,
    },
    locationContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        position: 'absolute',
        left: dimen.app_padding,
        right: dimen.app_padding,
    },

    //search bar
    searchContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    searchbarContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginLeft: dimen.app_padding * 3,
        marginRight: dimen.app_padding * 3,
        borderWidth: 1,
        borderRadius: 5,
        borderColor: Colors.light_border_color,
        backgroundColor: Colors.home_widget_background_color,
    },
    searchIcon: {
        width: 12,
        height: 12,
        margin: 5,
        marginLeft: 15,
    },
    searchField: {
        fontSize: 13,
        padding: 5,
        flexGrow: 1,
        color: Colors.light_border_color,
    },
    halfView: {
        height: 18,
        width: '100%',
        backgroundColor: Colors.primary_color,
        position: 'absolute',
        top: 0, left: 0, right: 0,

    },

    //banner
    carousalContainer: {
        height: 280,
        marginTop: 10,
        marginBottom: 10,
        marginLeft: dimen.app_padding * 2,
        marginRight: dimen.app_padding * 2,
    },

    scrollViewContainer: {flex: 1, marginBottom: dimen.bottom_tab_height},
    scrollViewInnerContainer: {flex: 1, justifyContent: 'flex-start', alignItems: 'stretch'},
    productListStyle: {
        flex: 1,
        padding: dimen.app_padding - dimen.home_item_padding,
    },

    //sub categories
    subCategoriesMainContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'stretch',
    },
    categoryHeader: {
        height: 45,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: Colors.primary_color,

    },
    headerTextStyle: {
        fontSize: 16,
        marginLeft: 15,
        color: Colors.button_text_color,
    },
    filterIconContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 15,
    },
    filterIconStyle: {
        width: 20,
        height: 20,
        marginLeft: 10,
    },
    headerStartStyle: {
        width: 8,
        borderTopRightRadius: 4,
        borderBottomRightRadius: 4,
        backgroundColor: Colors.header_start_text,
        position: 'absolute',
        height: '100%',
        left: 0,
    },
    subCategoryContainer: {
        flexDirection: 'row',
        marginTop: 20,
        marginBottom: 20,
        marginLeft: 10,
        marginRight: 10,
        flexWrap: 'wrap',
        alignContent: 'flex-start',
    },
    featureContainer: {
        flexDirection: 'row',
        // marginTop: 20,
        // marginBottom: 20,
        marginLeft: 10,
        marginRight: 10,
        flexWrap: 'wrap',
        alignContent: 'flex-start',
    },

    textContainer: {
        paddingTop: 10,
        paddingBottom: 10,
        paddingRight: 20,
        paddingLeft: 20,
    },
    textStyle: {
        fontSize: 16,
        color: Colors.primary_color,
        fontWeight: 'bold',
        alignSelf: 'center',
        textAlign: 'center',
    },
    featureHeaderTextStyle: {
        fontSize: 16,
        color: Colors.primary_color,
        fontWeight: 'bold',
        alignSelf: 'center',
        textAlign: 'center',
        marginBottom: dimen.app_padding,
        marginTop: 5,
    },
    feacturesContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        borderWidth: 1,
        borderColor: Colors.light_border_color,
        borderRadius: 10,
        margin: dimen.app_padding,
    },


});


{/*<SectionList*/
}
{/*sections={DATA}*/
}
{/*keyExtractor={(item, index) => item + index}*/
}
{/*// keyExtractor={(item, index) => item.id.toString()}*/
}
{/*renderItem={() => <Text>Title</Text>}*/
}
{/*renderSectionHeader={() => (*/
}
{/*<Text>Header</Text>*/
}
{/*)}*/
}
{/*ListHeaderComponent={() => (*/
}
{/*searchField()*/
}
{/*)}*/
}
{/*/>*/
}
