import React, {useState, useEffect, useCallback} from 'react';
import {
    Alert,
    Image,
    KeyboardAvoidingView,
    StyleSheet, SafeAreaView,
    Text, SectionList,
    Platform,
    TouchableOpacity, ScrollView,
    StatusBar,
    View,
} from 'react-native';
import {FlatList, TextInput} from 'react-native-gesture-handler';
import TextStyles from '../Styles/TextStyles';
import dimen from '../Styles/Dimen';
import {
    _storeData, console_log, successAlert,
} from '../Classes/auth';

import {
    web_view_url,
} from '../Classes/UrlConstant';
import {Loader} from '../Components/Loader';

import NetInfo from '@react-native-community/netinfo';
import Colors from '../Styles/Colors';
import AppText from '../Components/AppText';
import {CategoriesCard} from '../Components/Categories';
import MyImageButton, {MyImage} from '../Components/MyImageButton';
import MyBackgroundImage from '../Components/MyBackgroundImage';
import MyStrings from '../Classes/MyStrings';
import MyCarousel from '../Components/Carousel';
import BannerHome from '../Components/BannerHome';
import {ProductsCard} from '../Components/ProductsCard';
import TopBar from '../Components/TopBar';
import {shallowEqual, useDispatch, useSelector} from 'react-redux';
import {getCategoriesActionCreator} from '../Actions/actions';
import {getProductsActionCreator} from '../Actions/productActions';


export const ProductList = ({navigation}) => {

    const [categories, setCategories] = useState([{'id': 1},
        {'id': 2}, {'id': 3}, {'id': 4}, {'id': 5}, {'id': 6}, {'id': 7}, {'id': 8},
        {'id': 9}]);
    const [isloading, setIsLoading] = useState(false);
    const [email, setEmail] = useState('');


    const [products, next_page_url, isLoading] = useSelector(state => {
        console_log('product response');
        console_log(state.productsResponse.products);

        if (state.productsResponse.products.product.data !== undefined) {
            return [
                state.productsResponse.products.product.data,
                state.productsResponse.products.product.next_page_url ,
                state.productsResponse.products.loading,
            ];
        } else {
            return [
                [],
                state.productsResponse.products.product.next_page_url ,
                state.productsResponse.products.loading];
        }
    }, shallowEqual);

    const dispatch = useDispatch();

    const getProducts = useCallback(
        () => dispatch(getProductsActionCreator()),
        [dispatch],
    );

    useEffect(() => {
        getProducts();
    }, []);

    const subCatPress = (item) => {
        navigation.navigate('ProductListS');
    };
    const offersPress = (item) => {
        navigation.navigate('Offers');
    };
    const productDetailsPress = (item) => {
        navigation.navigate('ProductDetails');
    };
    const locationButtonPressed = (item) => {
        // navigation.navigate('ProductListS');
    };
    const goback = () => {
        navigation.goBack();
    };
    const renderSubCat = (cat) => {
        const txtContainer = cat.map(
            (i, j) =>
                <ProductsCard subCatPress={subCatPress} data={i}/>,
        );
        return (
            <View style={styles.subCategoriesMainContainer}>
                <View style={styles.subCategoryContainer}>
                    {txtContainer}
                </View>
            </View>
        );

    };
    const SearchField = () => {
        return (
            <View style={styles.searchContainer}>
                <View style={styles.halfView}/>
                <View style={styles.searchbarContainer}>
                    <MyImage source={require('../Asset/search.png')}
                             imageContainerStyle={styles.searchIcon}/>
                    <TextInput onChangeText={(email) => setEmail({email})}
                               value={email}
                               placeholder={MyStrings.searchFieldPlaceHolder}
                               placeholderTextColor={Colors.light_border_color}
                               autoCapitalize='none'
                               style={styles.searchField}/>

                </View>
            </View>
        );
    };
    const ProductListHeader = () => {
        return (
            <View style={styles.subCategoriesMainContainer}>
                <View style={styles.categoryHeader}>
                    <View style={styles.headerStartStyle}/>
                    <AppText style={styles.headerTextStyle}>
                        Laptops
                    </AppText>
                    <View style={styles.filterIconContainer}>
                        <MyImageButton onPress={locationButtonPressed}
                                       source={require('../Asset/filter.png')}
                                       imageContainerStyle={styles.filterIconStyle}/>
                        <MyImageButton onPress={locationButtonPressed}
                                       source={require('../Asset/sort.png')}
                                       imageContainerStyle={styles.filterIconStyle}/>

                    </View>
                </View>
            </View>
        );
    };

    const FlatListHeader = () => {
        return (
            <View style={{flex: 1}}>
                {banner()}
                <View style={styles.subCategoriesMainContainer}>
                    <View style={styles.categoryHeader}>
                        <View style={styles.headerStartStyle}/>
                        <AppText style={styles.headerTextStyle}>
                            Laptops
                        </AppText>
                        <View style={styles.filterIconContainer}>
                            <MyImageButton onPress={locationButtonPressed}
                                           source={require('../Asset/filter.png')}
                                           imageContainerStyle={styles.filterIconStyle}/>
                            <MyImageButton onPress={locationButtonPressed}
                                           source={require('../Asset/sort.png')}
                                           imageContainerStyle={styles.filterIconStyle}/>

                        </View>
                    </View>
                </View>
            </View>
        );
    };

    const loadMoreProducts = () => {
        console_log('load more products');
    };


    return (
        <MyBackgroundImage>
            <TopBar backButton={goback} title={'Products'}/>
            <SearchField/>
            <ScrollView style={styles.scrollViewContainer}>
                <View style={styles.scrollViewInnerContainer}>
                    <BannerHome/>

                    <ProductListHeader/>

                    <FlatList
                        style={styles.productListStyle}
                        data={products}
                        keyExtractor={(item, index) => item.id.toString()}
                        // contentContainerStyle={{margin:dimen.app_padding}}
                        numColumns={3}
                        bounces={false}
                        renderItem={({item}) => (<ProductsCard productDetailsPress={productDetailsPress}
                                                               offersPress={offersPress}
                                                               data={item}/>)}
                        // ListHeaderComponent={() => {return (<FlatListHeader/>)}}
                        ListFooterComponent={() => {
                            if(next_page_url!==null) {
                                return (
                                    <TouchableOpacity onPress={loadMoreProducts}
                                                      style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                                        <AppText
                                            style={{fontSize: 18, color: Colors.primary_color, fontWeight: 'bold'}}>
                                            Load More..
                                        </AppText>
                                    </TouchableOpacity>
                                )
                            }
                            else {
                                return null;
                            }
                        }}
                    />

                </View>
            </ScrollView>
        </MyBackgroundImage>
    );

};

const styles = StyleSheet.create({
    dashboardButtonText: {fontFamily: 'roboto-bold', fontSize: 16},
    dashboardButton: {flex: 1, padding: 8, justifyContent: 'center', alignItems: 'center'},
    dashboardButtonContainer: {
        borderWidth: 1,
        borderColor: Colors.border_color,
        borderRadius: dimen.border_radius,
        overflow: 'hidden',
        backgroundColor: Colors.home_widget_background_color,
        margin: dimen.app_padding,
        justifyContent: 'space-evenly',
        alignItems: 'flex-start',
        flexDirection: 'row',

    },
    bannerAdStyle: {
        height: 90, backgroundColor: Colors.primary_color,
        padding: 20, margin: dimen.app_padding,
        justifyContent: 'center',
    },
    flatlistStyle: {
        // width: '100%',
        // backgroundColor: Colors.home_widget_background_color,
        // marginBottom: dimen.bottom_margin_for_bottom_menu + 20,
        padding: dimen.app_padding,
    },
    productFlatList: {
        width: '100%',
        backgroundColor: Colors.primary_color,
        // marginBottom: dimen.bottom_margin_for_bottom_menu + 20,
        // padding: dimen.app_padding,
    },
    flatlistContainerStyle: {
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        backgroundColor: Colors.primary_dark_color,
    },
    emptyComponentStyle: {
        justifyContent: 'flex-start',
        alignItems: 'center',
        // backgroundColor:Colors.primary_dark_color
    },

    //top bar
    topContainer: {
        height: 40,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.primary_color,
    },
    locationButtonContainer: {
        width: 15,
        height: 15,
        marginLeft: 10,
    },
    backArrowContainer: {
        width: 15,
        height: 15,
        // marginLeft: 10,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        position: 'absolute',
        left: 0,
        right: dimen.app_padding,
    },
    topHeading: {
        fontSize: 18,
        color: Colors.button_text_color,
    },
    locationHeading: {
        fontSize: 13,
        color: Colors.button_text_color,
    },
    locationContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        position: 'absolute',
        left: dimen.app_padding,
        right: dimen.app_padding,
    },

    //search bar
    searchContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    searchbarContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginLeft: dimen.app_padding * 3,
        marginRight: dimen.app_padding * 3,
        borderWidth: 1,
        borderRadius: 5,
        borderColor: Colors.light_border_color,
        backgroundColor: Colors.home_widget_background_color,
    },
    searchIcon: {
        width: 12,
        height: 12,
        margin: 5,
        marginLeft: 15,
    },
    searchField: {
        fontSize: 13,
        padding: 5,
        flexGrow: 1,
        color: Colors.light_border_color,
    },
    halfView: {
        height: 18,
        width: '100%',
        backgroundColor: Colors.primary_color,
        position: 'absolute',
        top: 0, left: 0, right: 0,

    },

    //banner
    carousalContainer: {
        height: 130,
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: Colors.home_widget_background_color,
        // margin: dimen.app_padding,
        // padding:dimen.app_padding,
        // paddingBottom:0

    },

    scrollViewContainer: {flex: 1, marginBottom: dimen.bottom_tab_height},
    scrollViewInnerContainer: {flex: 1, justifyContent: 'flex-start', alignItems: 'stretch'},
    productListStyle: {
        flex: 1,
        padding: dimen.app_padding - dimen.home_item_padding,
    },

    //sub categories
    subCategoriesMainContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'stretch',
    },
    categoryHeader: {
        height: 45,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: Colors.primary_color,

    },
    headerTextStyle: {
        fontSize: 16,
        marginLeft: 15,
        color: Colors.button_text_color,
    },
    filterIconContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 15,
    },
    filterIconStyle: {
        width: 20,
        height: 20,
        marginLeft: 10,
    },
    headerStartStyle: {
        width: 8,
        borderTopRightRadius: 4,
        borderBottomRightRadius: 4,
        backgroundColor: Colors.header_start_text,
        position: 'absolute',
        height: '100%',
        left: 0,
    },
    subCategoryContainer: {
        flexDirection: 'row',
        marginTop: 20,
        marginBottom: 20,
        marginLeft: 10,
        marginRight: 10,
        flexWrap: 'wrap',
        alignContent: 'flex-start',
    },


});


{/*<SectionList*/
}
{/*sections={DATA}*/
}
{/*keyExtractor={(item, index) => item + index}*/
}
{/*// keyExtractor={(item, index) => item.id.toString()}*/
}
{/*renderItem={() => <Text>Title</Text>}*/
}
{/*renderSectionHeader={() => (*/
}
{/*<Text>Header</Text>*/
}
{/*)}*/
}
{/*ListHeaderComponent={() => (*/
}
{/*searchField()*/
}
{/*)}*/
}
{/*/>*/
}
