


import {categories_url, products_url, services_url} from '../Classes/UrlConstant';
import {GET_SERVICES, GET_SERVICES_FAIL, GET_SERVICES_SUCCESS} from './types';
import {console_log, getDataWithoutTokenAsync} from '../Classes/auth';


//Define your action create that set your loading state.
export const getService= (bool) => {
    //return a action type and a loading state indicating it is getting data.
    return {
        type: GET_SERVICES,
        payload: bool,
    };
}

//Define a action creator to set your loading state to false, and return the data when the promise is resolved
export const getServicesSuccess = (data) => {
    //Return a action type and a loading to false, and the data.
    return {
        type: GET_SERVICES_SUCCESS,
        payload: data,
        loading: false,
    };
}

//Define a action creator that catches a error and sets an errorMessage
export const getServicesFail = (error) => {
    //Return a action type and a payload with a error
    return {
        type: GET_SERVICES_FAIL,
        payload: error,
        loading: false,
    };
}


export const getServicesActionCreator= () => {
    //IN order to use await your callback must be asynchronous using async keyword.
    return async dispatch => {
        //Then perform your asynchronous operations.
        try {
            dispatch(getService(true));



            const products = await getDataWithoutTokenAsync(services_url,'POST','');

            const productsNew=products
            console_log('products object')
            console_log(productsNew)
            // const categoriesNew=Object.assign({},state,)
            //Now when the data is retrieved dispatch an action altering redux state.
            dispatch(getServicesSuccess(productsNew))
        } catch(error) {
            console.log('Getting People Error---------', error);
            dispatch(getServicesFail(error))
        }
    }
}




















