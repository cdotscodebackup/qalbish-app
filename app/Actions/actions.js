

import {ADD_PLACE, FETCH_POSTS, GET_CATEGORIES, GET_CATEGORIES_FAIL, GET_CATEGORIES_SUCCESS} from './types';
import {categories_url} from '../Classes/UrlConstant';
import {getDataWithoutTokenAsync} from '../Classes/auth';
export const addPlace=placeName=>{
    return{
        type:ADD_PLACE,
        payload:placeName
    }
}


export const addFriend = friendIndex => (
    {
        type: 'ADD_FRIEND',
        payload: friendIndex,
    }
);
export const changeName= friendIndex => (
    {
        type: 'RENAME',
        payload: friendIndex,
    }
);

export const getUser= friendIndex => (
    {
        type: 'USER',
        payload: friendIndex,
    }
);




//Define your action create that set your loading state.
export const getCategories = (bool) => {
    //return a action type and a loading state indicating it is getting data.
    return {
        type: GET_CATEGORIES,
        payload: bool,
    };
}

//Define a action creator to set your loading state to false, and return the data when the promise is resolved
export const getCategoriesSuccess = (data) => {
    //Return a action type and a loading to false, and the data.
    return {
        type: GET_CATEGORIES_SUCCESS,
        payload: data,
        loading: false,
    };
}

//Define a action creator that catches a error and sets an errorMessage
export const getCategoriesFail = (error) => {
    //Return a action type and a payload with a error
    return {
        type: GET_CATEGORIES_FAIL,
        payload: error,
        loading: false,
    };
}


export const getCategoriesActionCreator= () => {
    //IN order to use await your callback must be asynchronous using async keyword.
    return async dispatch => {
        //Then perform your asynchronous operations.
        try {
            dispatch(getCategories(true));
            //Have it first fetch data from our starwars url.
            // const categoriesPromise = await fetch(categories_url);
            // const categories = await categoriesPromise.json();

            const categories= await getDataWithoutTokenAsync(categories_url,'GET',null)

            // console.log('');
            // console.log(categories);
            // const categoriesNew=['first value', 'second value']
            const categoriesNew=categories
            // const categoriesNew=Object.assign({},state,)
            //Now when the data is retrieved dispatch an action altering redux state.
            dispatch(getCategoriesSuccess(categories))
        } catch(error) {
            console.log('Getting People Error---------', error);
            dispatch(getCategoriesFail(error))
        }
    }
}














