


import {categories_url, products_url} from '../Classes/UrlConstant';
import {GET_PRODUCTS, GET_PRODUCTS_FAIL, GET_PRODUCTS_SUCCESS} from './types';
import {console_log, getDataWithoutTokenAsync} from '../Classes/auth';


//Define your action create that set your loading state.
export const getProducts= (bool) => {
    //return a action type and a loading state indicating it is getting data.
    return {
        type: GET_PRODUCTS,
        payload: bool,
    };
}

//Define a action creator to set your loading state to false, and return the data when the promise is resolved
export const getProductsSuccess = (data) => {
    //Return a action type and a loading to false, and the data.
    return {
        type: GET_PRODUCTS_SUCCESS,
        payload: data,
        loading: false,
    };
}

//Define a action creator that catches a error and sets an errorMessage
export const getProductsFail = (error) => {
    //Return a action type and a payload with a error
    return {
        type: GET_PRODUCTS_FAIL,
        payload: error,
        loading: false,
    };
}


export const getProductsActionCreator= () => {
    //IN order to use await your callback must be asynchronous using async keyword.
    return async dispatch => {
        //Then perform your asynchronous operations.
        try {
            dispatch(getProducts(true));

            let formdata = new FormData();
            formdata.append("sub_category_id", 1)
            formdata.append("type", '123')

            const products = await getDataWithoutTokenAsync(products_url,'POST',formdata);

            const productsNew=products
            console_log('products object')
            console_log(productsNew)
            // const categoriesNew=Object.assign({},state,)
            //Now when the data is retrieved dispatch an action altering redux state.
            dispatch(getProductsSuccess(productsNew))
        } catch(error) {
            console.log('Getting People Error---------', error);
            dispatch(getProductsFail(error))
        }
    }
}




















