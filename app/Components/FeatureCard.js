import * as React from 'react';
import {View, Image, StyleSheet, TouchableOpacity} from 'react-native';
import AppText from './AppText';
import dimen from '../Styles/Dimen';
import Colors from '../Styles/Colors';
import {MyImage} from './MyImageButton';


export const FeatureCard = (props) => {
    const _onPress = () => {
        props.subCatPress(props.data);
    };

    return (
        <View style={styles.containerStyle}>
            <View style={styles.buttonContainer}>
                <AppText style={styles.buttonTextStyle}>
                    RAM
                </AppText>
            </View>
        </View>
    );
};
export const FeatureCardProductOffers = (props) => {
    const _onPress = () => {
        props.subCatPress(props.data);
    };

    return (
        <View style={styles.containerStyleOffer}>
            <View style={styles.buttonContainerOffer}>
                <AppText style={styles.buttonTextStyleOffer}>
                    RAM
                </AppText>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    containerStyle: {
        width: '25%',
        // margin:10,
        // flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // borderWidth: 1,borderColor: 'black'
        // margin: dimen.home_item_padding,
    },
    buttonContainer:{
        backgroundColor: Colors.primary_color,
        borderRadius: 10,
        width:'80%',
        marginBottom:10,
        // marginLeft:10,
        // marginRight:10,

    },
    buttonTextStyle:{
        fontSize: 13,
        color:Colors.button_text_color,
        textAlign: 'center',
        // backgroundColor:'red',
        margin:3,
    },
    containerStyleOffer: {
        // width: '25%',
        // margin:10,
        // flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        // borderWidth: 1,
        // margin: dimen.home_item_padding,
    },
    buttonContainerOffer:{
        backgroundColor: Colors.primary_color,
        borderRadius: 10,
        // width:'80%',
        marginBottom:10,
        margin:5,
        // marginLeft:10,
        // marginRight:10,

    },
    buttonTextStyleOffer:{
        fontSize: 11,
        color:Colors.button_text_color,
        textAlign: 'center',
        // backgroundColor:'red',
        margin:3,
        padding:4,
    }


});


