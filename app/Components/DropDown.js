import {
    ActivityIndicator,
    View,
    StyleSheet,
    TouchableWithoutFeedback,
    ActionSheetIOS,
    Platform,
    Picker,
    TouchableOpacity,
} from 'react-native';
import React from 'react';
import Colors from '../Styles/Colors';
import {console_log} from '../Classes/auth';
import dimen from '../Styles/Dimen';
import {MyImage} from './MyImageButton';
import AppText from './AppText';

export const DropDown = (props) => {

    let dropDownItems = props.itemsArray.map((s, i) => {
        return <Picker.Item key={i} value={s} label={s.title}/>;
    });

    let dropDownItemsIOS = [];
    props.itemsArray.forEach(item => {
        dropDownItemsIOS.push(item.title);
    });

    const onPress = () =>
        ActionSheetIOS.showActionSheetWithOptions(
            {
                options: dropDownItemsIOS,
                // destructiveButtonIndex: 2,
                // cancelButtonIndex: 0
            },
            buttonIndex => {
                props.itemsArray.forEach((item, j) => {
                    if (buttonIndex === j) {
                        props.setSelectedItems(item);
                        if (props.onSelection !== undefined) {
                            props.onSelection(item);
                        }
                    }
                });
            },
        );

    if (Platform.OS !== 'ios') {
        return (
            <View style={[styles.dropDownMainContainer, props.style]}>
                <Picker
                    style={[{flex: 1, backgroundColor: Colors.app_background_color, marginBottom: 2}]}
                    // prompt={'Select Value'}
                    mode={'dialog'}
                    onValueChange={(value, index) => {
                        props.setSelectedItems(value);
                        if (props.onSelection !== undefined) {
                            props.onSelection(value);
                        }
                    }}
                    selectedValue={props.selectedItem}
                    itemStyle={{backgroundColor: Colors.primary_color}}
                >
                    {dropDownItems}
                </Picker>
                <MyImage source={require('../Asset/down.png')}
                         imageContainerStyle={styles.dropdownStyle}/>
            </View>
        );
    } else {
        return (
            <TouchableWithoutFeedback onPress={onPress}>
                <View style={[styles.dropDownMainContainer, props.style]}>
                    <AppText style={styles.selectedAreaText}>
                        {props.selectedItem.title}
                    </AppText>

                    <MyImage source={require('../Asset/down.png')}
                             imageContainerStyle={styles.dropdownStyle}/>
                </View>
            </TouchableWithoutFeedback>
        );
    }

};


export const DropDownTime = (props) => {

    let dropDownItems = props.itemsArray.map((s, i) => {
        return <Picker.Item style={{backgroundColor: 'pink'}} key={i} value={s} label={s.title}/>;
    });

    let dropDownItemsIOS = [];
    props.itemsArray.forEach(item => {
        dropDownItemsIOS.push(item.title);
    });

    const onPress = () =>
        ActionSheetIOS.showActionSheetWithOptions(
            {
                options: dropDownItemsIOS,
                // destructiveButtonIndex: 2,
                // cancelButtonIndex: 0
            },
            buttonIndex => {
                props.itemsArray.forEach((item, j) => {
                    if (buttonIndex === j) {
                        props.setSelectedItems(item);
                        if (props.onSelection !== undefined) {
                            props.onSelection(item);
                        }
                    }
                });
            },
        );



    if (Platform.OS !== 'ios') {
        return (
            <View style={[styles.timedropDownMainContainer, props.style]}>
                <Picker
                    style={[{width: 90, height: 20, marginTop: dimen.app_padding, marginBottom: dimen.app_padding}]}
                    // prompt={'Select Value'}
                    mode={'dialog'}
                    onValueChange={(value, index) => {
                        props.setSelectedItems(value);
                        if (props.onSelection !== undefined) {
                            props.onSelection(value);
                        }
                    }}
                    selectedValue={props.selectedItem}
                    itemStyle={{}}
                >
                    {dropDownItems}
                </Picker>
                {/*<MyImage source={require('../Asset/down.png')}*/}
                {/*         imageContainerStyle={styles.dropdownStyle}/>*/}
            </View>
        );
    } else {
        return (
            <TouchableWithoutFeedback onPress={onPress}>
                <View style={[styles.timedropDownMainContainer,{justifyContent:'space-between'}, props.style]}>
                    <AppText style={styles.selectedAreaText}>
                        {props.selectedItem.title}
                    </AppText>

                    <MyImage source={require('../Asset/down.png')}
                             imageContainerStyle={[styles.dropdownStyle,{marginRight:10,marginLeft:10}]}/>
                </View>
            </TouchableWithoutFeedback>
        );
    }

};


const styles = StyleSheet.create({
    dropDownMainContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderColor: Colors.primary_color,
        backgroundColor: Colors.app_background_color,
        borderWidth: 1,
        // borderRadius: dimen.login_border_radius,
        paddingRight: dimen.app_padding,
        paddingLeft: dimen.app_padding,
        marginTop: 10,
        overflow: 'hidden',

    },
    dropdownStyle: {
        width: 14,
        height: 14,
    },

    usernameContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        borderColor: Colors.primary_color,
        backgroundColor: Colors.home_widget_background_color,
        borderWidth: 1,
        borderRadius: dimen.login_border_radius,
        paddingRight: dimen.app_padding,
        paddingLeft: dimen.app_padding,
    },

    timedropDownMainContainer: {
        // flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        // borderColor: Colors.primary_color,
        // backgroundColor: Colors.image_background_color,
        // borderWidth: 1,
        // borderRadius: dimen.login_border_radius,
        // paddingRight: dimen.app_padding,
        // paddingLeft: dimen.app_padding,
        // marginTop: 10,

    },
    timedropdownStyle: {
        width: 14,
        height: 14,
    },

    timeusernameContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        borderColor: Colors.primary_color,
        backgroundColor: Colors.home_widget_background_color,
        borderWidth: 1,
        // borderRadius: dimen.login_border_radius,
        // paddingRight: dimen.app_padding,
        // paddingLeft: dimen.app_padding,
    },
    selectedAreaText: {
        padding: 8,
        paddingTop: dimen.app_padding,
        paddingBottom: dimen.app_padding,
        fontSize: 16,
    },
});

