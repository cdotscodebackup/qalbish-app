import React from 'react';
import {Image, ImageBackground, Text, View} from 'react-native';
import Colors from '../Styles/Colors';

const MyBackgroundImage = (props) => {
    return (
        <View source={require('../Asset/background.png')}
                         style={{flex:1,backgroundColor:Colors.app_background_color,width: '100%', height: '100%'}}>
            {props.children}
        </View>
    );
};
export default MyBackgroundImage;
