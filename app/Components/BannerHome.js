import React from 'react';
import {
    Alert,
    Image,
    KeyboardAvoidingView,
    StyleSheet,
    Text,
    Platform,
    TouchableOpacity, ScrollView, Dimensions,
    View,
} from 'react-native';

import Carousel, {Pagination} from 'react-native-snap-carousel';
import AppText from './AppText';
import dimen from '../Styles/Dimen';
import Colors from '../Styles/Colors';
import MyImageButton, {MyImage} from './MyImageButton';

const {width: viewportWidth, height: viewportHeight} = Dimensions.get('window');
export default class BannerHome extends React.Component {
    constructor(props) {
        super(props);
        this.state = ({
            entries: [{'id': '1', 'title': 'first'}, {'id': '2', 'title': 'second'}, {'id': '3', 'title': 'third'}],
            activeSlide: 0,
        });
        this._carousel = null;
        this.width = dimen.carouselContainerWidth;
    }

    _renderItem({item, index}) {
        return (
            <View style={styles.slide}>
                <MyImage source={require('../Asset/banner.png')}
                         imageContainerStyle={styles.bannerImageStyle}/>
            </View>
        );
    }

    get pagination() {
        const {entries, activeSlide} = this.state;
        return (
            <Pagination
                dotsLength={entries.length}
                activeDotIndex={activeSlide}
                containerStyle={styles.dotContainerStyle}
                dotStyle={styles.activeDotStyle}
                inactiveDotStyle={styles.inactiveDotStyle}
                inactiveDotOpacity={0.4}
                inactiveDotScale={0.6}
            />
        );
    }

    render() {
        return (

            <View style={styles.mainContainer}>
                <View style={styles.container}>
                    <Carousel
                        ref={(c) => {
                            this._carousel = c;
                        }}
                        data={this.state.entries}
                        renderItem={this._renderItem}
                        sliderWidth={viewportWidth}
                        itemWidth={viewportWidth}
                        slideStyle={{width: viewportWidth}}
                        onSnapToItem={(index) => this.setState({activeSlide: index})}
                    />
                    {this.pagination}
                </View>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,

    },
    mainContainer: {
        height: 130,
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: Colors.home_widget_background_color,
        // margin: dimen.app_padding,
        // padding:dimen.app_padding,
        // paddingBottom:0

    },
    slide: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: 'yellow',
    },
    bannerImageStyle: {
        // flex:1,
        width: '100%',
        resizeMode: 'contain',
        // backgroundColor: 'red',
    },
    dotContainerStyle: {
        width: '100%',
        paddingVertical: 5,
        position: 'absolute',
        bottom: 5,
        // backgroundColor: 'red',
    },
    activeDotStyle: {
        width: 10,
        height: 10,
        borderRadius: 5,
        // marginHorizontal: 8,
        backgroundColor: Colors.primary_color,
    },
    inactiveDotStyle: {
        // Define styles for inactive dots here
        width: 10,
        height: 10,
        borderRadius: 5,
    },

});
