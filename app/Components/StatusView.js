import {StyleSheet, View} from 'react-native';
import AppText from './AppText';
import Colors from '../Styles/Colors';
import React from 'react';
import dimen from '../Styles/Dimen';


const Counter = (props) => {
    if (props.type === 'done') {
        return (

            <View style={[styles.counterNumberContainer, props.colorStyle]}>
                <AppText style={styles.counterNumberTextStyle}>{props.number}</AppText>
            </View>
        );
    }
    if (props.type === 'active') {
        return (
            <View style={[styles.counterNumberContainer, {
                backgroundColor: Colors.app_background_color,
                borderWidth: 2,
                borderColor: Colors.primary_color,
            }]}>
                <AppText
                    style={[styles.counterNumberTextStyle, {color: Colors.primary_color}]}>{props.number}</AppText>
            </View>
        );
    }
    if (props.type === 'dull') {
        return (
            <View style={[styles.counterNumberContainer, {backgroundColor: Colors.light_text_login}]}>
                <AppText style={styles.counterNumberTextStyle}>{props.number}</AppText>
            </View>
        );
    }
};
const Line = (props) => {
    return (
        <View style={[styles.line, props.color]}/>
    );
};

const StatusLine = (props) => {
    if (props.status === 'pending') {
        return (
            <View style={styles.counterContainer}>
                <View style={[styles.statusContainer, {}]}>
                    <Line type={'active'}/>
                    <Counter type={'done'} number={1} colorStyle={{backgroundColor: '#ffd200'}}/>
                    <Line type={'active'}/>
                </View>
                <View style={styles.statusContainer}>
                    <Line type={'active'} color={styles.lightBackgroundLine}/>
                    {/*<Counter type={'done'} number={1}/>*/}
                    <Line type={'active'} color={styles.lightBackgroundLine}/>
                </View>
                <View style={styles.statusContainer}>
                    <Line type={'active'} color={styles.lightBackgroundLine}/>
                    {/*<Counter type={'done'} number={1}/>*/}
                    <Line type={'active'} color={styles.lightBackgroundLine}/>
                </View>
            </View>
        );
    }
    else if (props.status === 'confirmed') {
        return (
            <View style={styles.counterContainer}>
                <View style={[styles.statusContainer, {}]}>
                    <Line type={'active'}/>
                    {/*<Counter type={'done'} number={1}/>*/}
                    <Line type={'active'}/>
                </View>
                <View style={styles.statusContainer}>
                    <Line type={'active'}/>
                    <Counter type={'done'} number={1} colorStyle={{backgroundColor: Colors.primary_color}}/>
                    <Line type={'active'}/>
                </View>
                <View style={styles.statusContainer}>
                    <Line type={'active'} color={styles.lightBackgroundLine}/>
                    {/*<Counter type={'done'} number={1}/>*/}
                    <Line type={'active'} color={styles.lightBackgroundLine}/>
                </View>
            </View>
        );
    }
    else {
        return (
            <View style={styles.counterContainer}>
                <View style={[styles.statusContainer, {}]}>
                    <Line type={'active'}/>
                    {/*<Counter type={'done'} number={1}/>*/}
                    <Line type={'active'}/>
                </View>
                <View style={styles.statusContainer}>
                    <Line type={'active'}/>
                    {/*<Counter type={'done'} number={1}/>*/}
                    <Line type={'active'}/>
                </View>
                <View style={styles.statusContainer}>
                    <Line type={'active'}/>
                    <Counter type={'done'} number={1} colorStyle={{backgroundColor: 'green'}}/>
                    <Line type={'active'}/>
                </View>
            </View>
        );
    }
};
const StatusText = (props) => {
    if (props.status === 'pending') {
        return (
            <View style={styles.counterContainer}>
                <View style={[styles.statusContainer]}>
                    <AppText style={styles.statusTitle}>Pending</AppText>
                </View>
                <View style={styles.statusContainer}>
                    <AppText style={styles.statusTitle}></AppText>
                </View>
                <View style={styles.statusContainer}>
                    <AppText style={styles.statusTitle}></AppText>
                </View>
            </View>
        );
    }
    else if (props.status === 'confirmed') {
        return (
            <View style={styles.counterContainer}>
                <View style={[styles.statusContainer]}>
                    <AppText style={styles.statusTitle}></AppText>
                </View>
                <View style={styles.statusContainer}>
                    <AppText style={styles.statusTitle}>Confirmed</AppText>
                </View>
                <View style={styles.statusContainer}>
                    <AppText style={styles.statusTitle}></AppText>
                </View>
            </View>
        );
    }
    else {
        return (
            <View style={styles.counterContainer}>
                <View style={[styles.statusContainer]}>
                    <AppText style={styles.statusTitle}></AppText>
                </View>
                <View style={styles.statusContainer}>
                    <AppText style={styles.statusTitle}></AppText>
                </View>
                <View style={styles.statusContainer}>
                    <AppText style={styles.statusTitle}>Completed</AppText>
                </View>
            </View>
        );
    }
};


export const StatusView = (props) => {
    return (
        <View>
            <StatusLine status={props.status}/>
            <StatusText status={props.status}/>
        </View>
    );
};

const styles = StyleSheet.create({

    counterContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop: 5,
        marginBottom: 5,
        marginLeft: dimen.app_padding,
        marginRight: dimen.app_padding,

    },
    counterNumberTextStyle: {
        fontSize: 1,
        color: Colors.app_background_color,
        textAlign: 'center',
        fontWeight: 'bold',

    },
    counterNumberContainer: {
        width: 14,
        height: 14,
        // padding: 5,
        borderRadius: 7,
        justifyContent: 'center',

    },
    line: {
        borderWidth:1,
        borderColor:Colors.normal_text_color,
        flexGrow: 1,
        alignSelf: 'center',
        borderStyle: 'dashed',
        borderRadius: 5,
    },
    lightBackgroundLine:{
        borderColor:Colors.light_border_color
    },
    statusContainer: {
        width: '33.33%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    statusTitle:{textAlign:'center'},


});
