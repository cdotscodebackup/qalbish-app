// import * as React from 'react';
import React, {useState, useEffect, useCallback, useContext} from 'react';

import {
    View,
    Image,
    StyleSheet,
    TouchableOpacity,
    TouchableNativeFeedback,
    TouchableWithoutFeedback,
} from 'react-native';
import AppText from './AppText';
import dimen from '../Styles/Dimen';
import Colors from '../Styles/Colors';
import {MyImage} from './MyImageButton';
import MyTextButton from './MyTextButton';
import {console_log, getObject, saveObject} from '../Classes/auth';
import {CartContext, MyContext} from '../Classes/AppContext';
import {cart_key, services_key} from '../Classes/UrlConstant';

const CATEGORY = [
    'Dresses',
    'Shoes',
    'Shorts',
];

const CheckBox = (props) => {
    return (
        <View>
            {!props.status &&
            <TouchableOpacity onPress={props.setStatus}>
                <View style={[styles.checboxContainer, props.style !== null ? props.style : {}]}>
                </View>
            </TouchableOpacity>
            }
            {props.status &&
            <TouchableOpacity onPress={props.setStatus}>
                <View style={[styles.checkboxContainerSelected, props.style !== null ? props.style : {}]}>
                    <Image source={require('../Asset/tick.png')}
                           style={styles.tickImageStyle}/>
                </View>
            </TouchableOpacity>
            }
        </View>
    );
};

const AddOnItemCard = (props) => {
    const [adOnQuantity, setAdOnQuantity] = useState(1);
    const plusPress = () => {
        setAdOnQuantity(adOnQuantity + 1);
    };
    const minusPress = () => {
        if (adOnQuantity > 1) {
            setAdOnQuantity(adOnQuantity - 1);
        }
    };
    const [checkbox2, setCheckbox2] = useState(false);
    const myContext = useContext(MyContext);

    // console_log('props.data')
    // console_log(props.data.type)


    useEffect(() => {
        getObject(cart_key).then((res) => {
            if (!res) {
            } else {
                let itemfromCart = res.find((i) => {
                    return i.id === props.parentId
                })
                if (itemfromCart !== undefined) {
                    let adonfromCart = itemfromCart.addOns.find((j) => {
                        return j.id === props.data.id
                    })

                    if (adonfromCart !== undefined) {
                        setCheckbox2(true)
                        setAdOnQuantity(adonfromCart.quantity)
                    }
                }
            }
        })
    }, [])


    useEffect(() => {
        updateCartAddOnCount()
    }, [adOnQuantity])


    const updateCartAddOnCount = () => {
        const cartAddOn = Object.assign({}, props.data);
        cartAddOn.quantity = adOnQuantity;
        getObject(cart_key)
            .then((res) => {
                if (!res) {
                } else {
                    // console_log(res)
                    // console_log(cartAddOn)
                    let item = res.find((item) => item.id === props.parentId);
                    if (item !== undefined) {
                        const cartAddOnItem = item.addOns.filter((item) => item.id === props.data.id);
                        // console_log(cartAddOnItem)
                        if (cartAddOnItem === undefined || cartAddOnItem.length === 0) {// add value
                            if (checkbox2) {
                                item.addOns.push(cartAddOn);
                            }
                        } else { //cart has value of same add on so remove it
                            item.addOns = item.addOns.filter((item) => item.id !== props.data.id);
                            item.addOns.push(cartAddOn)
                        }
                        saveObject(cart_key, res).then(() => {
                            setSelected()

                        }).catch();
                    } else {
                    }
                }
            })
            .catch((err) => {
                console_log(err);
            });


    };
    const updateCartAddOn = () => {
        const cartAddOn = Object.assign({}, props.data);

        cartAddOn.quantity = adOnQuantity;

        getObject(cart_key)
            .then((res) => {
                if (!res) {
                } else {
                    let item = res.find((item) => item.id === props.parentId);

                    if (item !== undefined) {
                        const cartAddOnItem = item.addOns.filter((item) => item.id === props.data.id);
                        if (cartAddOnItem === undefined || cartAddOnItem.length === 0) {// add value
                            item.addOns.push(cartAddOn);
                        } else { //cart has value of same add on so remove it
                            item.addOns = item.addOns.filter((item) => item.id !== props.data.id);
                        }
                        saveObject(cart_key, res).then(() => {
                            setSelected()
                        }).catch();
                    } else {
                    }
                }
            })
            .catch((err) => {
                console_log(err);
            });


    };


    const setSelected = () => {
        props.menuSelected(props.data)
    }


    return (
        <View style={[styles.containerStyle, {flex: 1}]}>
            {props.type !== 'Package' &&
            <View style={styles.menuItemAdOnContainer}>
                <CheckBox style={{width: 16, height: 16}} status={checkbox2}
                          setStatus={() => {
                              setCheckbox2(!checkbox2);
                              updateCartAddOn();
                          }}
                />
                <View style={{flex: 3, marginLeft: 10, marginRight: 10}}>
                    <AppText style={{}} numberOfLines={1}>
                        {props.data.name}
                    </AppText>
                </View>
                <AppText style={{flex: 1, marginRight: 10}}>Rs. {props.data.price}</AppText>
                <View style={styles.quantityContainer}>
                    <MyTextButton onPress={minusPress}
                                  buttonText={'-'}
                                  buttonTextStyle={{fontSize: 15, color: Colors.primary_color}}
                                  buttonContainerStyle={styles.plusButtonAddOn}/>

                    <AppText style={{margin: 5}}>{adOnQuantity}</AppText>

                    <MyTextButton onPress={plusPress}
                                  buttonText={'+'}
                                  buttonTextStyle={{fontSize: 15, color: Colors.primary_color}}
                                  buttonContainerStyle={styles.plusButtonAddOn}/>
                </View>
            </View>
            }
            {props.type === 'Package' &&
            <View style={[{flexDirection:'row', justifyContent:'space-between'}]}>
                <View style={{ marginLeft: 10, marginRight: 10}}>
                    <AppText style={{}} numberOfLines={1}>
                        {props.data.name}
                    </AppText>
                </View>
                {/*<AppText style={{ marginRight: 10,alignSelf:'flex-end'}}>*/}
                {/*    Rs. {props.data.price}*/}
                {/*</AppText>*/}
            </View>
            }
        </View>
    );
};
const renderAddonList = (addons, id, menuSelected, type) => {
    return addons.map((item, i) => {
        return (
            <AddOnItemCard key={i} data={item} parentId={id} menuSelected={menuSelected} type={type}/>
        );
    });
};


export const MenuItemCard = (props) => {

    const myContext = useContext(MyContext);
    const cartContext = useContext(CartContext);

    const [quantity, setQuantity] = useState(1);
    const updateCountCart = () => {
        const cartItem = Object.assign({}, props.data);
        cartItem.quantity = quantity;
        console_log(cartItem)

        getObject(cart_key)
            .then((res) => {
                if (!res) {
                } else {
                    let item = res.find((item) => item.id === props.data.id);
                    // console_log(item)
                    if (item !== undefined) {
                        const cart = res.filter((item) => item.id !== props.data.id);
                        cartItem.addOns= item.addOns;

                        cart.push(cartItem);


                        console_log(cart)
                        saveObject(cart_key, cart).then(() => {
                            setSelected()
                        }).catch();

                    } else {
                    }

                }
            })
            .catch((err) => {
                console_log(err);
            });
    };

    useEffect(() => {
        // if (props.data.shouldUncheck !== undefined){
        //     setCheckbox(false);
        // }
        getObject(cart_key).then((res) => {
            if (!res) {

            } else {
                let itemfromCart = res.find((i) => i.id === props.data.id)
                if (itemfromCart !== undefined) {
                    setCheckbox(true);
                    setQuantity(itemfromCart.quantity)

                }
            }
        })
    }, [])


    const plusPress = () => {
        setQuantity(quantity + 1);
    };
    const minusPress = () => {
        if (quantity > 1) {
            setQuantity(quantity - 1);
        }

    };
    const [checkbox, setCheckbox] = useState(false);
    const updateCart = () => {
        const cartItem = Object.assign({}, props.data);
        cartItem.quantity = quantity;
        cartItem.addOns = [];

        getObject(cart_key)
            .then((res) => {
                if (!res) {
                    saveObject(cart_key, [cartItem]).then(() => {
                        setSelected()
                    });

                } else {
                    let item = res.find((item) => item.id === props.data.id);
                    if (item !== undefined) {
                        const cart = res.filter((item) => item.id !== props.data.id);
                        saveObject(cart_key, cart).then(() => {
                            setSelected()
                        }).catch();
                    } else {
                        res.push(cartItem);
                        saveObject(cart_key, res).then(() => {
                            setSelected()
                        }).catch();
                    }
                }
                setSelected()

            })
            .catch((err) => {
                console_log(err);
            });
    };

    useEffect(() => {
        updateCountCart();
    }, [quantity]);

    useEffect(() => {
        // updateCart();
    }, [checkbox]);


    const setSelected = () => {
        props.menuSelected(props.data)

    }
    return (
        <View style={styles.containerStyle}>
            <View style={styles.menuItemContainer}>
                <CheckBox
                    status={checkbox}
                    setStatus={() => {
                        setCheckbox(!checkbox);
                        updateCart();

                    }}
                />
                <View style={{flex: 3, marginLeft: 10, marginRight: 10}}>
                    <AppText style={{}} numberOfLines={2}>
                        {props.data.name}
                    </AppText>
                </View>
                <AppText style={{flex: 1, marginRight: 10}}>Rs. {props.data.price}</AppText>
                <View style={styles.quantityContainer}>
                    <MyTextButton onPress={minusPress}
                                  buttonText={'-'}
                                  buttonTextStyle={{fontSize: 18}}
                                  buttonContainerStyle={styles.plusButton}/>

                    <AppText style={{margin: 5}}>{quantity}</AppText>

                    <MyTextButton onPress={plusPress}
                                  buttonText={'+'}
                                  buttonTextStyle={{fontSize: 18}}
                                  buttonContainerStyle={styles.plusButton}/>
                </View>
            </View>
            {checkbox && props.data.addOns.length !== 0 &&
            <View style={styles.adOnContainer}>
                {props.data.type !== 'Package' &&
                <AppText style={styles.addonTitle}>Add On</AppText>}
                {/*{props.data.type === 'Package' &&*/}
                {/*<AppText style={styles.addonTitle}>Items</AppText>}*/}
                {renderAddonList(props.data.addOns, props.data.id, props.menuSelected, props.data.type)}
            </View>}

        </View>
    );
};

const styles = StyleSheet.create({
    containerStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'stretch',
        margin: dimen.home_item_padding,
        // backgroundColor: 'red'
    },
    menuItemContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
    },
    menuItemAdOnContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        // backgroundColor:'red'
    },
    quantityContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',

    },
    plusButton: {
        height: 20,
        width: 20,
        padding: 0,
        justifyContent: 'center',
        alignItems: 'center',
    },
    plusButtonAddOn: {
        height: 16,
        width: 16,
        padding: 0,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.addon_background_color,
    },

    adOnContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        marginTop: dimen.app_padding,
        padding: 5,
        backgroundColor: Colors.addon_background_color,
    },
    addonTitle: {
        borderBottomWidth: 2,
        borderColor: 'green',
        fontWeight: 'bold',
        alignSelf: 'flex-start',

    },
    checboxContainer: {
        width: 20,
        height: 20,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        // borderRadius: 5,
        borderColor: Colors.primary_color,
    },
    checkboxContainerSelected: {
        width: 20,
        height: 20,
        // borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.primary_color,
    },
    tickImageStyle: {width: 9, height: 9, alignSelf: 'center', resizeMode: 'contain'},
    checkBoxMainContainer: {marginRight: 5, justifyContent: 'center', alignItems: 'center'},

});


