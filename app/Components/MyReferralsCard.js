import * as React from 'react';
import {View, Image, StyleSheet, TouchableOpacity,TouchableNativeFeedback} from 'react-native';
import AppText from './AppText';
import dimen from '../Styles/Dimen';
import Colors from '../Styles/Colors';
import {MyImage} from './MyImageButton';


export const MyReferralsCard = (props) => {
    const _onPress = () => {
        props.productOffersPress(props.data);
    };

    return (
        <TouchableOpacity onPress={_onPress}>
            <View style={styles.containerStyle}>
                <View style={styles.nameContainer}>
                    <AppText style={styles.nameTextStyle} numberOfLines={1}>
                        Discount
                    </AppText>
                    <AppText style={[styles.nameTextStyle,{marginTop:5}]} numberOfLines={1}>
                        10 %
                    </AppText>
                </View>
                <View style={styles.offerContainer}>
                    <AppText style={styles.nameTextStyle} numberOfLines={1}>
                        Date/Time
                    </AppText>
                    <AppText style={[styles.nameTextStyle,{marginTop:5}]} numberOfLines={1}>
                        Jan 22, 2020. 6:43 PM
                    </AppText>
                </View>
                <View style={styles.moreContainer}>
                    {/*<AppText style={styles.moreTextStyle}>More</AppText>*/}
                    <MyImage source={require('../Asset/right_arrow.png')}
                             tintColor={Colors.button_text_color}
                             imageContainerStyle={styles.arrowImageStyle}
                    />
                </View>
            </View>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    containerStyle: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        // borderWidth: 1,borderColor: 'black'
        margin: dimen.home_item_padding,
        padding: dimen.home_item_padding,
        backgroundColor: Colors.primary_color,
    },
    nameContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        padding: 7,
    },
    moreContainer: {
        flex: 0.5,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    offerContainer: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'flex-start',
        padding: 7,
        paddingLeft: 0,
        // backgroundColor:'yellow'

    },
    moreTextStyle: {
        color: Colors.primary_color,
        fontSize: 12,
        borderBottomWidth: 1,
        borderColor: Colors.primary_color,
    },
    nameTextStyle: {
        color: Colors.button_text_color,
        fontSize: 12,
        // textAlign:'center',
        // padding:10,
    },
    newOfferTextStyle: {
        color: 'red',
        fontSize: 10,
        textAlign: 'center',
        // padding:10,
    },
    imageStyle: {
        flex: 1,
        // width: 30,
        height: 100,
        resizeMode: 'contain',
        padding: 5,
        // margin: dimen.app_padding,

    },
    arrowImageStyle: {
        width: 10,
        height: 10,
        resizeMode: 'contain',
        marginLeft: 5,
        marginRight: 5,
        // margin: dimen.app_padding,

    },
    textContainer: {
        paddingTop: 10,
        paddingBottom: 5,
        // paddingRight:10,
    },
    textStyle: {
        fontSize: 10,
        color: Colors.primary_color,
        fontWeight: 'bold',
        alignSelf: 'center',
        textAlign: 'center',
    },
    buttonContainer: {
        backgroundColor: Colors.primary_color,
        borderRadius: 10,
        width: '80%',
        // marginLeft:10,
        // marginRight:10,

    },
    buttonTextStyle: {
        fontSize: 13,
        color: Colors.button_text_color,
        textAlign: 'center',
        // backgroundColor:'red',
        margin: 3,
    },

});


