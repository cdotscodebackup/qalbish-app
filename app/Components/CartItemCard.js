// import * as React from 'react';
import React, {useState, useEffect, useCallback} from 'react';

import {
    View,
    Image,
    StyleSheet,
    TouchableOpacity,
    TouchableNativeFeedback,
    TouchableWithoutFeedback, Animated,
} from 'react-native';
import AppText from './AppText';
import dimen from '../Styles/Dimen';
import Colors from '../Styles/Colors';
import {MyImage} from './MyImageButton';
import MyTextButton from './MyTextButton';
import {console_log} from '../Classes/auth';

const CATEGORY = [
    'Dresses',
    'Shoes',
    'Shorts',
];

const CheckBox = (props) => {
    const termsandConditionsCheckbox = () => {
        props.setStatus;
    };

    return (
        <View>
            {!props.status &&
            <TouchableOpacity onPress={props.setStatus}>
                <View style={styles.checboxContainer}>
                </View>
            </TouchableOpacity>
            }
            {props.status &&
            <TouchableOpacity onPress={props.setStatus}>
                <View style={styles.checkboxContainerSelected}>
                    <Image source={require('../Asset/tick.png')}
                           style={styles.tickImageStyle}/>
                </View>
            </TouchableOpacity>
            }
        </View>
    );
};
const renderAddonList = (addons) => {
    return addons.map((item, i) => {
        return (
            <View key={i} style={[styles.containerStyle, {flex: 1}]}>
                <View style={styles.menuItemContainer}>
                    <View style={{flex: 3, marginLeft: 10, marginRight: 10}}>
                        <AppText style={{}} numberOfLines={1}>
                            Ayurvedic Deep
                        </AppText>
                    </View>
                    <AppText style={{flex: 1, marginRight: 10}}>Rs. 2000</AppText>
                    <View style={styles.quantityContainer}>
                        <MyTextButton onPress={() => {
                        }}
                                      buttonText={'-'}
                                      buttonTextStyle={{fontSize: 18}}
                                      buttonContainerStyle={styles.plusButton}/>

                        <AppText style={{margin: 5}}>1</AppText>

                        <MyTextButton onPress={() => {
                        }}
                                      buttonText={'+'}
                                      buttonTextStyle={{fontSize: 18}}
                                      buttonContainerStyle={styles.plusButton}/>
                    </View>
                </View>
            </View>

        );
    });
};
const addonCard = (CATEGORY) => {
    return (
        <View style={{backgroundColor: Colors.addon_background_color}}>
            {renderAddonList(CATEGORY)}
        </View>
    );
};

const renderItems = (props, parentId, removerAdOnItem) => {
    return props.map((item, index) => {
        return (
            <View key={index}>
                <AddOnItem data={item} parentId={parentId} removerAdOnItem={removerAdOnItem}/>
            </View>
        );
    });
};
const AddOnItem = (props) => {
    const _onPress = () => {
        // console_log(props.data)
        // console_log(props.parentId)
        props.removerAdOnItem(props.data, props.parentId);
    };
    return (
        <View key={props.index} style={{marginTop: 10,width:'100%', flexDirection: 'row', justifyContent:'space-between'}}>
            <View>
                <AppText style={{}} numberOfLines={2}>
                    {props.data.name} x {props.data.quantity}
                </AppText>
                <AppText style={{}}>Rs. {props.data.price}</AppText>
            </View>
            {props.removerAdOnItem !== undefined && true &&
            <MyTextButton onPress={_onPress}
                          buttonText={'X'}
                          buttonTextStyle={{
                              fontSize: 8,
                          }}
                          buttonContainerStyle={styles.plusButton}/>}
        </View>
    );
};

export const CartItemCard = (props) => {
    const _onPress = () => {
        props.removerItem(props.data);
    };

    return (
        <View style={styles.containerStyle}>
            <View style={styles.menuItemContainer}>
                <View style={{flex: 3}}>
                    <AppText style={{}} numberOfLines={2}>
                        {props.data.name} x {props.data.quantity}
                    </AppText>
                    <AppText style={{
                        flex: 1,
                        marginRight: 10,
                        fontWeight: 'bold'
                    }}>Rs. {props.data.price * props.data.quantity}</AppText>
                </View>
                {props.removerItem !== undefined &&
                <MyTextButton onPress={_onPress}
                              buttonText={'X'}
                              buttonTextStyle={{
                                  fontSize: 10,
                              }}
                              buttonContainerStyle={styles.plusButton}/>}
            </View>
            {props.data.addOns !== undefined && props.data.addOns.length !== 0 &&
            <View>
                <View style={styles.addonContainer}>
                    <AppText style={styles.addonHeadingText}>Add on</AppText>
                    {renderItems(props.data.addOns, props.data.id, props.removerAdOnItem)}
                </View>
            </View>}
            <View style={{height: 1, marginTop: 5, backgroundColor: Colors.add_on_divider_color}}/>
        </View>
    );
};

const styles = StyleSheet.create({
    containerStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'stretch',
        marginLeft: dimen.app_padding,
        marginRight: dimen.app_padding,
        marginTop: 10,
        marginBottom: dimen.home_item_padding,
        // backgroundColor: 'red'
    },
    menuItemContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
    },
    quantityContainer: {
        justifyContent: 'center',
        alignItems: 'center',

    },
    plusButton: {
        height: 16,
        width: 16,
        padding: 0,
        justifyContent: 'center',
        alignItems: 'center',
    },

    adOnContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'stretch',
    },
    addonTitle: {
        borderBottomWidth: 1,
        borderColor: Colors.primary_color,
        fontWeight: 'bold',

    },
    checboxContainer: {
        width: 20,
        height: 20,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        // borderRadius: 5,
        borderColor: Colors.primary_color,
    },
    checkboxContainerSelected: {
        width: 20,
        height: 20,
        // borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.primary_color,
    },
    tickImageStyle: {width: 9, height: 9, alignSelf: 'center', resizeMode: 'contain'},
    checkBoxMainContainer: {marginRight: 5, justifyContent: 'center', alignItems: 'center'},

    addonContainer: {
        backgroundColor: Colors.addon_background_color,
        padding: 3,
        marginTop: 5,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },
    addonHeadingText: {marginRight: 10, fontWeight: 'bold', borderBottomWidth: 1, borderColor: 'green'},

});


