import React, {Component, useState} from 'react';
import {
    Animated,
    Dimensions,
    ScrollView,
    StyleSheet,
    TouchableWithoutFeedback,
    View, Text, TouchableOpacity,
} from 'react-native';

import * as Easing from 'react-native/Libraries/Animated/src/Easing';
import Colors from '../Styles/Colors';
import dimen from '../Styles/Dimen';
import AppText from './AppText';
import {MyImage} from './MyImageButton';
import {FlatList} from 'react-native-gesture-handler';
import {CartItemCard} from './CartItemCard';
import {EmptyList} from './Loader';
import {console_log, getObject, saveObject} from '../Classes/auth';
import {MyContext} from '../Classes/AppContext';
import {cart_key} from '../Classes/UrlConstant';

const {width, height} = Dimensions.get('window');
const panelWidth = width - dimen.app_padding * 2;
// const [categories, setCategories] = useState([{'id': 1}, {'id': 2}]);

export default class BottomUpPanel extends Component {

    static defaultProps = {
        isOpen: false,
    };

    // Define state
    state = {
        open: false,
        spinValue: new Animated.Value(0),
        categories: [],
        total: 0
    };

    static contextType = MyContext;


    config = {
        position: {
            max: height,
            start: height - this.props.startHeight,
            end: this.props.topEnd,
            min: this.props.topEnd,
            animates: [
                () => this._animatedHeight,
            ],
        },
        width: {
            end: panelWidth,
            start: panelWidth,
        },
        height: {
            end: height,
            start: this.props.startHeight,
        },

    };

    _animatedHeight = new Animated.Value(this.props.isOpen ? this.config.height.end : this.config.height.start);

    _animatedPosition = new Animated.Value(this.props.isOpen
        ? this.config.position.end
        : this.config.position.start);

    componentWillMount() {
        this._animatedPosition.addListener((value) => {
            //Every time that position changes then actualize the related properties. I.e: height, so the view
            //has the interpolated height
            this.config.position.animates.map(item => {
                item().setValue(value.value);
            });
        });
        // Reset value once listener is registered to update depending animations
        this._animatedPosition.setValue(this._animatedPosition._value);
    }


    // Handle isOpen prop changes to either open or close the window
    UNSAFE_componentWillReceiveProps(nextProps) {
        // isOpen prop changed to true from false
        if (!this.props.isOpen && nextProps.isOpen) {
            this.open();
        }
        // isOpen prop changed to false from true
        else if (this.props.isOpen && !nextProps.isOpen) {
            this.close();
        }
    }

    open = () => {
        this.setState({open: true}, () => {
            Animated.timing(this._animatedPosition, {
                toValue: this.config.position.end,
                duration: 400,
                useNativeDriver: true
            }).start();
            // Animated.timing(
            //     this.state.spinValue,
            //     {
            //         toValue: 1,
            //         duration: 600,
            //         easing: Easing.linear,
            //         useNativeDriver: true
            //     }
            // ).start()
        });

    };


    close = () => {
        // this._scrollView.scrollTo({ y: 0 });
        Animated.timing(this._animatedPosition, {
            toValue: this.config.position.start,
            duration: 400,
            useNativeDriver: true
        }).start(() => this.setState({
            open: false,
        }));
        // Animated.timing(
        //     this.state.spinValue,
        //     {
        //         toValue: 0,
        //         duration: 600,
        //         easing: Easing.linear,
        //         useNativeDriver: true
        //     }
        // ).start();
    };

    toggle = () => {
        if (!this.state.open) {
            this.open();
        } else {
            this.close();
            let res= this.state.categories;
            let length=0;
            res.forEach((item)=>{
                length=length+1;
                item.addOns.forEach((ite)=>{
                    length=length+1;
                })

            })
            this.props.updateParent(length)
        }
    };

    updateCart = () => {
        getObject(cart_key)
            .then((res) => {
                if (!res) {

                } else {

                    this.setState({categories: res})
                    let t = 0;
                    res.forEach((item) => {
                        t = t + ((parseInt(item.price) * item.quantity))
                        item.addOns.forEach((ite) => {
                            t = t + ((parseInt(ite.price) * ite.quantity))
                        })
                    })
                    this.setState({total: t})
                }
            })
            .catch((err) => console_log(err))
    }

    removeCartItem = (prop) => {

        getObject(cart_key)
            .then((res) => {
                if (!res) {
                } else {
                    let item = res.find((item) => item.id === prop.id );
                    if (item !== undefined) {
                        const cart = res.filter((item) => item.id !== prop.id);
                        saveObject(cart_key, cart).then(()=>{

                            this.updateCart();
                        }).catch();
                    } else {

                    }
                }
            })
            .catch((err) => {
                console_log(err);
            });


    }
    removeCartAdOnItem = (props,parentId) => {

        getObject(cart_key)
            .then((res) => {
                if (!res) {
                } else {
                    let item = res.find((item) => item.id === parentId);

                    if (item !== undefined) {
                        const cartAddOnItem = item.addOns.filter((item) => item.id === props.id);
                        if (cartAddOnItem === undefined || cartAddOnItem.length === 0) {// add value
                            // item.addOns.push(cartAddOn);
                        } else { //cart has value of same add on so remove it
                            item.addOns = item.addOns.filter((item) => item.id !== props.id);
                        }
                        saveObject(cart_key, res).then((res)=>{
                            console_log('removed add on and update')
                            console_log(res)
                            this.updateCart();
                        }).catch((err)=>console_log(err));
                    } else {
                    }
                }
            })
            .catch((err) => {
                console_log(err);
            });



    }

    render() {

        let animatedHeight = this._animatedHeight.interpolate({
            inputRange: [this.config.position.end, this.config.position.start],
            outputRange: [this.config.height.end, this.config.height.start],
        });

        return (
            <Animated.View style={[styles.buttonUpPanelView, {height: animatedHeight}]}>
                <Animated.View
                    style={[styles.content, {
                        // Add padding at the bottom to fit all content on the screen
                        paddingBottom: this.props.topEnd,
                        width: panelWidth,
                        // Animate position on the screen
                        transform: [{translateY: this._animatedPosition}, {translateX: 0}],
                    }]}
                >
                    <View style={{}}>
                        <TouchableWithoutFeedback style={{backgroundColor: Colors.primary_color}} onPress={() => {
                            this.toggle();
                            this.updateCart()
                        }}>
                            <View style={styles.totalContainerStyle}>
                                {!this.state.open && true &&
                                <AppText style={styles.totalTextStyle}>Total Rs.{this.props.total}</AppText>}
                                {this.state.open && <View></View>}
                                <View style={styles.cartSummaryContainer}>
                                    <AppText style={styles.totalTextStyle}>Order Summary</AppText>
                                    <MyImage
                                        source={!this.state.open ? require('../Asset/up_arrow.png') : require('../Asset/down.png')}
                                        imageContainerStyle={[styles.dropdownIcon, {marginLeft: 5}]}
                                    />
                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                        <View style={{backgroundColor: Colors.add_on_divider_color, height: 1, width: '100%'}}></View>

                        {/* Scrollable content */}
                        {/*<ScrollView*/}
                        {/*    ref={(scrollView) => { this._scrollView = scrollView; }}*/}
                        {/*    // Enable scrolling only when the window is open*/}
                        {/*    scrollEnabled={this.state.open}*/}
                        {/*    // Hide all scrolling indicators*/}
                        {/*    showsHorizontalScrollIndicator={false}*/}
                        {/*    showsVerticalScrollIndicator={false}*/}
                        {/*    // Trigger onScroll often*/}
                        {/*    scrollEventThrottle={16}*/}
                        {/*    onScroll={this._handleScroll}*/}
                        {/*>*/}
                        {/*    /!* Render content components *!/*/}


                        <FlatList
                            style={{marginBottom: dimen.app_padding}}
                            data={this.state.categories}
                            keyExtractor={(item, index) => item.id.toString()}
                            numColumns={1}
                            bounces={false}
                            renderItem={({item}) => (
                                <CartItemCard
                                    removerItem={(item) => {
                                        this.removeCartItem(item)
                                    }}
                                    removerAdOnItem={(item, parentId) => {
                                        // console_log(item)
                                        this.removeCartAdOnItem(item,parentId)
                                    }}
                                    data={item}/>)}
                            ListEmptyComponent={<EmptyList text={'No item selected'}/>}
                            ListFooterComponent={
                                <View style={{
                                    justifyContent: 'flex-end', alignItems: 'flex-end', padding: dimen.app_padding,
                                }}>
                                    <AppText style={styles.totalTextStyle}>Total Rs. {this.state.total}</AppText>
                                </View>
                            }
                        />
                    </View>

                    {/*<View style={{justifyContent:'flex-end',alignItems:'flex-end',padding:dimen.app_padding,backgroundColor:'yellow'}}>*/}
                    {/*    <AppText style={styles.totalTextStyle}>Total Rs. {this.state.total}</AppText>*/}
                    {/*</View>*/}


                    {/*</ScrollView>*/}
                </Animated.View>
            </Animated.View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'flex-end',
        backgroundColor: Colors.primary_color,
    },
    content: {
        backgroundColor: Colors.app_background_color,
        borderColor: Colors.primary_color,
        borderWidth: 1,
        height: height,
        justifyContent: 'space-between'
    },
    buttonUpPanelView: {
        position: 'absolute',
        bottom: 65,

        // left:dimen.app_padding,
        // right:dimen.app_padding,
        // width: width-dimen.app_padding,
        justifyContent: 'flex-end',
        alignItems: 'center',
        // backgroundColor: 'transparent',
    },

    cartSummaryContainer: {
        flexDirection: 'row',
        alignSelf: 'flex-end',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    dropdownIcon: {
        width: 10,
        height: 10,
        resizeMode: 'contain',
    },
    totalContainerStyle: {
        padding: 5,
        paddingLeft: dimen.app_padding - dimen.home_item_padding,
        paddingRight: dimen.app_padding - dimen.home_item_padding,
        flexDirection: 'row',
        justifyContent: 'space-between',
        // justifyContent: 'flex-end',
        alignItems: 'flex-start',
    },
    totalTextStyle: {
        fontSize: 12,
        fontWeight: 'bold',
    },

});
