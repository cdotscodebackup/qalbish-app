import * as React from 'react';
import {View, Image, StyleSheet, TouchableOpacity} from 'react-native';
import AppText from './AppText';
import dimen from '../Styles/Dimen';
import Colors from '../Styles/Colors';
import {console_log} from '../Classes/auth';


export const CategoriesCard = (props) => {
    const _onPress = () => {
        props.subCatPress(props.data);
        // console_log( props.data.sub_catagory_img_path );
    };

    return (
        <TouchableOpacity onPress={_onPress} style={styles.containerStyle}>
            <View style={styles.innerContainer}>
                <Image
                    source={{uri:props.data.sub_catagory_img_path}}
                    // source={require('../Asset/car.png')}
                       style={styles.imageStyle}
                />
                <View style={styles.textContainer}>
                    <AppText style={styles.textStyle}>
                      {props.data.sub_catagory_name}
                    </AppText>
                </View>
            </View>
        </TouchableOpacity>

    );
};

const styles = StyleSheet.create({
    containerStyle: {
        width: '20%',
        // margin:10,
        justifyContent: 'center',
        alignItems: 'center',
        padding: dimen.home_item_padding,
    },
    innerContainer:{
        justifyContent: 'flex-start',
        alignItems: 'center',
        width:'100%',
    },
    imageStyle: {
        width:'100%',
        height: 30,
        resizeMode: 'contain',

    },
    textContainer: {
        paddingTop: 10,
        paddingBottom: 5,
        // paddingRight:10,
    },
    textStyle: {
        fontSize: 10,
        color: Colors.primary_color,
        fontWeight: 'bold',
        alignSelf: 'center',
        textAlign: 'center',
    },
});
