import dimen from '../Styles/Dimen';
import TextStyles from '../Styles/TextStyles';
import {Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import AppText from './AppText';


const MyTextButton = (props) => {
    return (
        <MyTextButtonSimple onPress={props.onPress}
                            buttonText={props.buttonText}
                            activeOpacity={props.activeOpacity!==undefined ? props.activeOpacity:1}
                            buttonContainerStyle={[TextStyles.buttonStyle, props.buttonContainerStyle]}
                            buttonTextStyle={[TextStyles.buttonTextStyle, props.buttonTextStyle]}
        />

    );
};
const MyTextButton1 = (props) => {
    return (
        <View>
            <TouchableOpacity style={[TextStyles.buttonStyle, props.buttonContainerStyle]}
                              onPress={props.onPress}
            >
                <AppText style={[TextStyles.buttonTextStyle, props.buttonTextStyle]}>{props.buttonText}</AppText>
            </TouchableOpacity>
        </View>
    );
};

export const MyTextButtonSimple = (props) => {
    return (
        <View>
            <TouchableOpacity style={[props.buttonContainerStyle]}
                              activeOpacity={props.activeOpacity}
                              onPress={props.onPress}>
                <AppText style={[props.buttonTextStyle]}>{props.buttonText}</AppText>
            </TouchableOpacity>
        </View>
    );
};

export default MyTextButton;
