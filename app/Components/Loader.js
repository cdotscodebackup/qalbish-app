import {
    ActivityIndicator,
    View,
    StyleSheet,
    Dimensions,
    TouchableWithoutFeedback, BackHandler,
    TouchableOpacity, Alert
} from "react-native";
import React, {useState, useEffect} from "react";
import Colors from "../Styles/Colors";
import dimen from '../Styles/Dimen';
import AppText from './AppText';
import MyImageButton, {MyImage} from './MyImageButton';
import {FlatList, TextInput} from "react-native-gesture-handler";
import {console_log} from "../Classes/auth";
import MyTextButton from "./MyTextButton";
import {InputFieldStyle} from '../Styles/TextStyles';


export const Loader = () => (
    <View style={{
        justifyContent: 'center', alignItems: 'center', position: 'absolute',
        left: 0,
        right: 0,
        opacity: 0.4,
        // backgroundColor: Colors.app_background_color,
        backgroundColor: 'black',
        top: 0,
        bottom: 0,
    }}>
        <ActivityIndicator size="large" color={Colors.primary_color}/>
    </View>
)
export const EmptyList = (props) => {


    return (

        <View style={{flex: 1, justifyContent: 'flex-start', alignItems: 'center', margin: dimen.app_padding}}>
            {props.text === undefined &&
            <AppText style={{fontSize: 15, color: Colors.normal_text_color}}>No Items. Tap to reload.</AppText>}
            {props.text !== undefined &&
            <AppText style={{fontSize: 15, color: Colors.normal_text_color}}>{props.text}</AppText>}
            {props.reload !== undefined &&
            <TouchableOpacity onPress={() => props.reload()}>
                <MyImage
                    source={require('../Asset/reload.png')}
                    tintColor={Colors.primary_color}
                    imageContainerStyle={{width: 30, height: 30, marginTop: dimen.app_padding}}/>
            </TouchableOpacity>
            }
        </View>
    )
}


const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;
const dialogWidth = (deviceWidth - dimen.app_padding * 2);
const dialogHeight = (deviceHeight - dimen.app_padding * 4);


export const SearchDropDown = (props) => {


    const [searchText, setSearchText] = useState('');

    useEffect(() => {
        const backAction = () => {
            if (props.show) {
                props.setShow(false)
                return true;
            } else
                return false;
        };

        const backHandler = BackHandler.addEventListener(
            "hardwareBackPress",
            backAction
        );

        return () => backHandler.remove();
    }, [props.show]);


    const closeDialog = () => {
        props.setShow(false)
    }
    const setSelectedItem = (item) => {
        props.setSelectedItems(item);
        closeDialog()
    }


    if (!props.show)
        return (<View></View>)
    else
        return (
            <View style={styles.mainContainer}>
                <View style={styles.itemsContainer}>
                    <View style={styles.crossContain}>
                        <MyImageButton onPress={closeDialog}
                                       source={require('../Asset/cros.png')}
                                       imageContainerStyle={styles.crossButtonContainer}/>
                    </View>
                    <View style={styles.searchInputContainer}>
                        <TextInput onChangeText={(email) => {
                            setSearchText(email)
                            props.setFiltered(props.itemsArrayOrignal.filter((item) => item.title.toLowerCase().includes(email.toLowerCase())));
                        }}
                                   value={searchText}
                                   placeholder={'Search here'}
                                   autoCapitalize='none'
                            // maxLength={11}
                            // keyboardType={'phone-pad'}
                                   style={InputFieldStyle.inputField}/>
                    </View>

                    {/*{props.itemsArrayOrignal.length ===1 &&*/}
                    {/*<EmptyList text={'Select City first'}/>*/}
                    {/*}*/}

                    <FlatList
                        style={{marginTop: 10}}
                        data={props.itemsArray}
                        extraData={props.itemsArray}
                        keyExtractor={(item, index) => item.id.toString()}
                        numColumns={1}
                        bounces={false}
                        renderItem={({item}) => (
                            <TouchableOpacity onPress={() => setSelectedItem(item)}>
                                <View style={styles.dropdownItemContainer}>
                                    <AppText style={styles.itemTextStyle}>{item.title}</AppText>
                                </View>
                            </TouchableOpacity>
                        )}

                        ListEmptyComponent={<EmptyList text={'No item found.'}/>}

                        // ListHeaderComponent={() => {return (<FlatListHeader/>)}}
                    />


                </View>
            </View>
        )
}
export const ThankyouDialog = (props) => {

    const closeDialog = () => {
        props.setShow(false)
    }

    if (!props.show)
        return (<View></View>)
    else
        return (
            <View style={thankStyles.mainContainer}>
                <View style={thankStyles.itemsContainer}>
                    {/*<View style={thankStyles.crossContain}>*/}
                    {/*    <MyImageButton onPress={closeDialog}*/}
                    {/*                   source={require('../Asset/cros.png')}*/}
                    {/*                   imageContainerStyle={thankStyles.crossButtonContainer}/>*/}
                    {/*</View>*/}

                    <AppText style={{textAlign: 'center', fontSize: 25, color: Colors.primary_color}}>
                        {props.text}
                    </AppText>
                    <View style={{}}>
                        <MyTextButton onPress={closeDialog}
                                      buttonText={'Continue Booking'}
                                      // buttonTextStyle={{color:Colors.primary_color}}
                                      buttonContainerStyle={[{marginTop: 30}]}/>
                    </View>


                </View>
            </View>
        )
}

export const DropDownBoth = (props) => {

    const closeDialog = () => {
        props.setShow(false)
    }

    if (!props.show)
        return (<View></View>)
    else
        return (
            <View style={DropDownStyles.mainContainer}>
                <View style={DropDownStyles.itemsContainer}>
                    <FlatList
                        style={{marginTop: 0}}
                        data={props.itemsArray}
                        keyExtractor={(item, index) => item.id.toString()}
                        numColumns={1}
                        bounces={false}
                        renderItem={({item}) => (
                            <TouchableOpacity onPress={() => {props.setSelectedItems(item);closeDialog()}}>
                                <View style={DropDownStyles.dropdownItemContainer}>
                                    <AppText style={DropDownStyles.itemTextStyle}>{item.title}</AppText>
                                </View>
                            </TouchableOpacity>
                        )}

                        ListEmptyComponent={<EmptyList text={'No item found.'}/>}
                    />

                </View>
            </View>
        )
}

const styles = StyleSheet.create({
    mainContainer: {
        justifyContent: 'center', alignItems: 'center',
        position: 'absolute',
        left: 0,
        right: 0,
        opacity: 1,
        backgroundColor: 'rgba(52, 52, 52, 0.8)',
        // backgroundColor: Colors.home_widget_background_color,
        top: 0,
        bottom: 0,
        zIndex: 5,
    },
    itemsContainer: {
        width: dialogWidth,
        height: dialogHeight,
        backgroundColor: Colors.home_widget_background_color,
        padding: dimen.app_padding,
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        borderRadius: 2,
        // marginTop: 70,
        // marginBottom: 70
    },
    searchInputContainer: {
        // flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        // flexGrow: 1,
        borderColor: Colors.primary_color,
        // backgroundColor: Colors.app_background_color,
        borderWidth: 1,
        // borderRadius: dimen.login_border_radius,
        paddingRight: dimen.app_padding,
        paddingLeft: dimen.app_padding,
    },

    itemTextStyle: {
        fontSize: 16,
        // padding: dimen.app_padding,

    },
    inputField: {
        fontSize: 16,
        flexGrow: 1,
        // padding: dimen.app_padding,

    },
    dropdownItemContainer: {
        paddingTop: dimen.app_padding,
        paddingBottom: dimen.app_padding,
    },

    crossContain: {
        alignItems: 'flex-end',
        marginBottom: dimen.app_padding
    },
    crossButtonContainer: {
        width: 20,
        height: 20,
    },


})
const thankStyles = StyleSheet.create({
    mainContainer: {
        justifyContent: 'center', alignItems: 'center',
        position: 'absolute',
        left: 0,
        right: 0,
        opacity: 1,
        backgroundColor: 'rgba(52, 52, 52, 0.8)',
        // backgroundColor: Colors.home_widget_background_color,
        top: 0,
        bottom: 0,
        zIndex: 5,
    },
    itemsContainer: {
        width: deviceWidth - dimen.app_padding * 2,
        // height: deviceWidth,
        backgroundColor: Colors.home_widget_background_color,
        padding: dimen.app_padding,
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        borderRadius: 2,
        // marginTop: 70,
        // marginBottom: 70
    },
    searchInputContainer: {
        // flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        // flexGrow: 1,
        borderColor: Colors.primary_color,
        // backgroundColor: Colors.app_background_color,
        borderWidth: 1,
        // borderRadius: dimen.login_border_radius,
        paddingRight: dimen.app_padding,
        paddingLeft: dimen.app_padding,
    },

    itemTextStyle: {
        fontSize: 16,
        // padding: dimen.app_padding,

    },
    inputField: {
        fontSize: 16,
        flexGrow: 1,
        // padding: dimen.app_padding,

    },
    dropdownItemContainer: {
        paddingTop: dimen.app_padding,
        paddingBottom: dimen.app_padding,
    },

    crossContain: {
        alignItems: 'flex-end',
        marginBottom: dimen.app_padding
    },
    crossButtonContainer: {
        width: 20,
        height: 20,
    },


})
const DropDownStyles = StyleSheet.create({
    mainContainer: {
        justifyContent: 'center', alignItems: 'center',
        position: 'absolute',
        left: 0,
        right: 0,
        opacity: 1,
        backgroundColor: 'rgba(52, 52, 52, 0.8)',
        // backgroundColor: Colors.home_widget_background_color,
        top: 0,
        bottom: 0,
        zIndex: 5,
    },
    itemsContainer: {
        width: deviceWidth - dimen.app_padding * 2,
        // height: deviceWidth,
        backgroundColor: Colors.home_widget_background_color,
        padding: dimen.app_padding,
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        borderRadius: 2,
        // marginTop: 70,
        // marginBottom: 70
    },
    searchInputContainer: {
        // flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        // flexGrow: 1,
        borderColor: Colors.primary_color,
        // backgroundColor: Colors.app_background_color,
        borderWidth: 1,
        // borderRadius: dimen.login_border_radius,
        paddingRight: dimen.app_padding,
        paddingLeft: dimen.app_padding,
    },

    itemTextStyle: {
        fontSize: 16,
        // padding: dimen.app_padding,

    },
    inputField: {
        fontSize: 16,
        flexGrow: 1,
        // padding: dimen.app_padding,

    },
    dropdownItemContainer: {
        paddingTop: dimen.home_item_padding,
        paddingBottom: dimen.app_padding,
    },

    crossContain: {
        alignItems: 'flex-end',
        marginBottom: dimen.app_padding
    },
    crossButtonContainer: {
        width: 20,
        height: 20,
    },


})

