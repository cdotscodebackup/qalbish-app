import * as React from 'react';
import {
    View,
    Image,
    StyleSheet,
    TouchableOpacity,
    TouchableNativeFeedback,
    TouchableWithoutFeedback,
} from 'react-native';
import AppText from './AppText';
import dimen from '../Styles/Dimen';
import Colors from '../Styles/Colors';
import {MyImage} from './MyImageButton';
import {console_log} from '../Classes/auth';
import {Dimensions} from 'react-native';


export const ServicesCard = (props) => {
    const _onPress = () => {
        // console_log(props.data)
        props.productOffersPress(props.data);
    };

    return (
        <TouchableOpacity onPress={_onPress} style={styles.containerStyle}>
            <View style={styles.imageContainer}>
                <MyImage
                    source={{uri:props.data.thumbnail}}
                    // source={require('../Asset/sevice_image.jpg')}
                         imageContainerStyle={styles.imageStyle}
                />
                <View style={styles.serviceTitle}>
                    <TouchableNativeFeedback onPress={_onPress}>
                        <AppText style={styles.titleText}>{props.data.name}</AppText>
                    </TouchableNativeFeedback>
                </View>
            </View>
        </TouchableOpacity>
    );
};


const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;
let w=deviceWidth*0.5-(dimen.app_padding*2);

const styles = StyleSheet.create({
    containerStyle: {
        // width: '50%',
        // flex: 1,
        height: w,
        width: w,
        justifyContent: 'space-between',
        alignItems: 'center',
        margin: dimen.app_padding * 0.69,
        // borderColor: Colors.light_border_color,
        backgroundColor:Colors.image_background_color,
    },
    imageContainer: {
        // width:'80%',
        // height:'80%',
        // flex: 1,
        width: w,
        height: w,
        justifyContent: 'center',
        alignItems: 'center',
        overflow: 'hidden',


    },
    imageStyle: {
        // flex: 1,
        // width:'100%',
        // height: 150,
        resizeMode: 'cover',
        // padding:5,
        // borderWidth:5,
        // borderColor:'black'

    },
    serviceTitle: {
        position: 'absolute',
        bottom: 0, left: 0, right: 0,
        height: 40,
        backgroundColor: 'rgba(255,108,43, 0.7)',
        justifyContent: 'center',
        alignItems: 'center',
    },
    titleText: {
        // backgroundColor: Colors.primary_color,
        textAlign: 'center',
        color: Colors.button_text_color,
        fontSize: 14,
    },

});


