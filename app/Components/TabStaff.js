import * as React from 'react';
import {Image,Text,Animated, StyleSheet, TouchableOpacity,TouchableNativeFeedback, TouchableWithoutFeedback, View} from 'react-native';
import AppText from './AppText';
import Colors from '../Styles/Colors';
import {useState} from 'react';
import {useEffect} from 'react';
import {console_log, console_log_message} from '../Classes/auth';


const TabStaff = (props) => {
    return (
        <View style={[props.focused ? styles.acctiveTab : styles.deActiveTab, {flex: 1}]}>
            <TouchableNativeFeedback
                onPress={props.onPress}>
                <View>

                    {(props.id == 0) &&
                    <View style={[styles.tabcontainer]}>
                        <View style={styles.tabIconContainer}>
                            <Image
                                tintColor={props.focused ? Colors.primary_color : Colors.inactive_menu_color}
                                source={require('../Asset/staff_orders.png') }
                                style={[styles.tabiconstyle,props.focused ? {tintColor:Colors.primary_color} : {tintColor:Colors.inactive_menu_color}]}/>
                        </View>
                        {props.focused && <AppText style={  styles.activeTabTextStyle }>
                            Orders</AppText>}
                        {!props.focused && <AppText style={styles.tabTextStyle}>
                            Orders</AppText>}
                    </View>}

                    {(props.id == 1) &&
                    <View style={[styles.tabcontainer]}>
                        <View style={styles.tabIconContainer}>

                            <Image
                                tintColor={props.focused ? Colors.primary_color : Colors.inactive_menu_color}
                                source={require('../Asset/staff_processing.png') }
                                style={[styles.tabiconstyle,props.focused ? {tintColor:Colors.primary_color} : {tintColor:Colors.inactive_menu_color}]}/>
                        </View>
                        {props.focused && <AppText style={  styles.activeTabTextStyle }>
                            Processing</AppText>}
                        {!props.focused && <AppText style={styles.tabTextStyle}>
                            Processing</AppText>}
                    </View>}

                    {(props.id == 2) &&
                    <View style={[styles.tabcontainer]}>
                        <View style={styles.tabIconContainer}>

                            <Image
                                tintColor={props.focused ? Colors.primary_color : Colors.inactive_menu_color}
                                source={require('../Asset/staff_contact_us.png') }
                                style={[styles.tabiconstyle,props.focused ? {tintColor:Colors.primary_color} : {tintColor:Colors.inactive_menu_color}]}/>
                        </View>
                        {props.focused && <AppText style={  styles.activeTabTextStyle }>
                            Contact Us</AppText>}
                        {!props.focused && <AppText style={styles.tabTextStyle}>
                            Contact Us</AppText>}
                    </View>}

                    {(props.id == 3) &&
                    <View style={[styles.tabcontainer]}>
                        <View style={styles.tabIconContainer}>
                            <Image
                                tintColor={props.focused ? Colors.primary_color : Colors.inactive_menu_color}
                                source={require('../Asset/staff_user.png')}
                                style={[styles.tabiconstyle,props.focused ? {tintColor:Colors.primary_color} : {tintColor:Colors.inactive_menu_color}]}/>
                        </View>
                        {props.focused && <AppText style={  styles.activeTabTextStyle }>
                            Profile</AppText>}
                        {!props.focused && <AppText style={styles.tabTextStyle}>
                            Profile</AppText>}
                    </View>}


                </View>
            </TouchableNativeFeedback>
        </View>

    );
};
export default TabStaff;

const styles = StyleSheet.create({
    tabiconstyle: {
        width: 28,
        height: 28,
        padding: 2,
        // flex: 1,flexGrow:1,
        resizeMode: 'contain',
    },
    tabIconContainer: {
        justifyContent:'center',
        alignItems:'center',
        height: '70%',
    },
    tabTextStyle: {
        width: '100%',
        height: '30%',
        textAlign: 'center',
        fontSize: 13,
        color: Colors.home_widget_background_color,

    },
    activeTabTextStyle: {
        width: '100%',
        height: '30%',
        textAlign: 'center',
        fontSize: 11,
        color: Colors.primary_color,


    },
    newMessageIcon: {
        width: 6,
        height: 6,
        // padding: 1,
        borderRadius: 3,
        position: 'absolute',
        resizeMode: 'contain',
        backgroundColor: 'red',
        top: 0,
        right: 0,
        // left: 0,
        // bottom: 0,
    },
    tabcontainer: {
        justifyContent: 'center', alignItems: 'center', flexGrow: 1, padding: 2,

    },
    acctiveTab: {backgroundColor: Colors.app_background_color},
    deActiveTab: {backgroundColor: Colors.primary_color},

});


