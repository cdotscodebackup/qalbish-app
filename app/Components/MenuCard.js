import * as React from 'react';
import {View, Image, StyleSheet, TouchableOpacity,TouchableWithoutFeedback} from 'react-native';
import AppText from './AppText';
import dimen from '../Styles/Dimen';
import Colors from '../Styles/Colors';
import {MyImage} from './MyImageButton';


export const MenuCard = (props) => {
    const _onPress = () => {
        props.subCatPress(props.data);
    };

    return (
        <TouchableWithoutFeedback onPress={_onPress}>
        <View style={styles.containerStyle}>
            <View style={styles.imageContainer}>
                <AppText style={styles.itemTextStyle}>{props.data.title}</AppText>
            </View>
        </View>
        </TouchableWithoutFeedback>
    );
};

const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        // margin:10,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-start',
        borderBottomWidth: 1,borderColor: 'black',
        margin: dimen.home_item_padding,
    },
    imageContainer: {
        width:'100%',
        justifyContent:'center',
        alignItems:'flex-start',
        borderColor: Colors.light_border_color,
        borderRadius: dimen.app_padding,
        // borderWidth:1,
        backgroundColor:Colors.home_widget_background_color,
        overflow:'hidden',

    },
    imageStyle: {
        width: 80,
        height: 100,
        resizeMode: 'contain',
        // margin: dimen.app_padding,

    },
    textContainer: {
        paddingTop:10,
        paddingBottom:5,
        // paddingRight:10,
    },
    textStyle: {
        fontSize:10,
        color:Colors.primary_color,
        fontWeight: 'bold',
        alignSelf:'center',
        textAlign: 'center'
    },
    buttonContainer:{
        backgroundColor: Colors.primary_color,
        borderRadius: 10,
        width:'80%',
        // marginLeft:10,
        // marginRight:10,

    },
    buttonTextStyle:{
        fontSize: 13,
        color:Colors.button_text_color,
        textAlign: 'center',
        // backgroundColor:'red',
        margin:3,
    },
    itemTextStyle:{
        fontSize:22,
        color:Colors.primary_color,
    }


});


