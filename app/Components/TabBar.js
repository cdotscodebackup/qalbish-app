import * as React from "react"
import {
    View,
    Text,
    TouchableOpacity,
    TouchableHighlight,
    TouchableWithoutFeedback,
    StyleSheet,
    Image
} from "react-native"
import Tab from "./Tab"
import AsyncStorage from '@react-native-community/async-storage';

import Colors from "../Styles/Colors";
import dimen from "../Styles/Dimen";

const S = StyleSheet.create({
    container: {
        flexDirection: "row",
        borderColor: Colors.bottom_menu_border,
        // borderTopWidth:1,
        position: 'absolute',
        bottom: 0,
        width: "100%",
        height: dimen.bottom_tab_height,
        elevation: 20,
        backgroundColor: Colors.primary_color,
        justifyContent: 'space-evenly',
        alignItems: 'stretch'
    },

    tabButton: {flex: 1, justifyContent: "center", alignItems: "center", backgroundColor: 'red'}
});

// transparent color. rgba(52, 52, 52, 0.8)


export default class TabBar extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {
        const {
            renderIcon,
            getLabelText,
            activeTintColor,
            inactiveTintColor,
            onTabPress,
            onTabLongPress,
            getAccessibilityLabel,
            navigation,
        } = this.props;
        const {routes, index: activeRouteIndex} = navigation.state;

        return (
            <View style={[S.container]}>

                {routes.map((route, routeIndex) => {
                    const isRouteActive = routeIndex === activeRouteIndex;
                    const tintColor = isRouteActive ? activeTintColor : inactiveTintColor;
                    return (

                        <Tab key={routeIndex}
                             id={routeIndex}
                             onPress={() => onTabPress({route})}
                             accessibilityLable={getAccessibilityLabel({route})}
                             focused={isRouteActive}
                             // currentid={this.idd}
                        />

                    );
                })}
            </View>
        );

    }


}

const TabBar1 = (props) => {
    const {
        renderIcon,
        getLabelText,
        activeTintColor,
        inactiveTintColor,
        onTabPress,
        onTabLongPress,
        getAccessibilityLabel,
        navigation,
    } = props;
    const {routes, index: activeRouteIndex} = navigation.state;

    return (
        <View style={[S.container]}>

            {routes.map((route, routeIndex) => {
                const isRouteActive = routeIndex === activeRouteIndex;
                const tintColor = isRouteActive ? activeTintColor : inactiveTintColor;

                return (

                    <Tab key={routeIndex}
                         id={routeIndex}
                         onPress={() => onTabPress({route})}
                         accessibilityLable={getAccessibilityLabel({route})}
                         focused={isRouteActive}
                         // currentid={'16'}
                    />

                );
            })}
        </View>
    );
};

