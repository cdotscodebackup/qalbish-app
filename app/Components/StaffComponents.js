import React, {useState, useEffect, useCallback} from 'react';

import {
    View,
    Image,
    StyleSheet,
    TouchableOpacity,
} from 'react-native';
import AppText from './AppText';
import dimen from '../Styles/Dimen';
import Colors from '../Styles/Colors';
import MyTextButton from './MyTextButton';
import {MyImage} from './MyImageButton';
import {console_log} from '../Classes/auth';


export const CurrentOrderCard = (props) => {
    if (props.data === null) {
        return <View/>;
    }
    let order = props.data;
    let orderDetails = order.order_detail;

    const items = orderDetails.map((item, index) => {
        return (
            <CurrentOrderItemCard key={index} data={item}/>
        );
    });

    return (
        <View style={currentOrderStyles.orderItemsMainContainer}>
            <AppText style={[currentOrderStyles.textStyle, {fontSize: 11}]}>
                Services Ordered
            </AppText>
            <View style={{height: 1, marginTop: 5, backgroundColor: Colors.add_on_divider_color}}/>

            <View>
                {items}
            </View>

            <View style={{height: 1, marginTop: 10, marginBottom: 5, backgroundColor: Colors.add_on_divider_color}}/>
            {order.total_discount !== undefined && order.total_discount !== null &&
            <View style={{justifyContent: 'space-between', flexDirection: 'row', marginBottom: 5,}}>
                <AppText>Total Discount</AppText>
                <AppText>Rs. {order.total_discount}</AppText>
            </View>}

            <View style={{justifyContent: 'space-between', flexDirection: 'row'}}>
                <AppText>Delivery Charges</AppText>
                <AppText>Rs. {order.customer.delivery_charges}</AppText>
            </View>

            <View style={{height: 1, marginTop: 15, marginBottom: 10, backgroundColor: Colors.add_on_divider_color}}/>

            <View style={{justifyContent: 'space-between', flexDirection: 'row'}}>
                <AppText style={currentOrderStyles.totalPriceText}>Total</AppText>
                <AppText style={currentOrderStyles.totalPriceText}>Rs. {order.grand_total}</AppText>
            </View>
        </View>
    );
};
const CurrentOrderItemCard = (props) => {
    // console_log(props.data);
    const addOnItems = props.data.addOns.map((item, index) => {
        return (
            <View style={{justifyContent: 'space-between', flexDirection: 'row'}}>
                <AppText>Classic Manicure x 1</AppText>
                <AppText>Rs. 2000</AppText>
            </View>
        );
    });
    return (
        <View style={{}}>

            <View style={currentOrderStyles.currentOrderServiceItemContainer}>
                <View style={{flex:1,}}>
                    <AppText numberOfLines={2}
                             style={currentOrderStyles.textStyle}>{props.data.name} x {props.data.quantity}</AppText>
                </View>
                <View style={{marginLeft:10}}>
                    <AppText style={currentOrderStyles.textStyle}>Rs. {props.data.amount}</AppText>
                </View>

            </View>

            {props.data.addOns.length > 0 &&
            <View style={currentOrderStyles.addOnContainer}>
                <AppText style={currentOrderStyles.textStyle}>Add on</AppText>
                <View>
                    {addOnItems}
                </View>
            </View>}

        </View>
    );
};
export const DriverComponent = (props) => {
    if (props.data === null) {
        return <View/>;
    }
    let driver = props.data.driver;
    return (
        <View style={currentOrderStyles.staffContainer}>
            <View style={currentOrderStyles.profileImageContainer}>
                <MyImage
                    source={{uri: driver.profile_pic}}
                    // source={require('../Asset/vendor.png')}
                    imageContainerStyle={currentOrderStyles.profileImage}/>
            </View>
            <View>
                <AppText
                    style={[currentOrderStyles.headingTextStyle, {marginLeft: dimen.app_padding, fontWeight: 'bold'}]}>
                    Driver Name
                </AppText>
                <AppText style={[currentOrderStyles.headingTextStyle, {marginLeft: dimen.app_padding}]}>
                    {driver.name}
                </AppText>
                <AppText style={[currentOrderStyles.headingTextStyle, {marginLeft: dimen.app_padding}]}>
                    Phone Number
                </AppText>
                <AppText style={[currentOrderStyles.headingTextStyle, {marginLeft: dimen.app_padding}]}>
                    {driver.phone_number}
                </AppText>
            </View>

        </View>
    );
};
export const CustomerComponent = (props) => {
    if (props.data === null) {
        return <View/>;
    }

    let order = props.data;
    let customer = order.customer;

    return (
        <View>
            <View style={styles.staffContainer}>
                <View style={styles.profileImageContainer}>
                    <MyImage
                        source={{uri: customer.profile_pic}}
                        // source={require('../Asset/vendor.png')}
                        imageContainerStyle={styles.profileImage}/>
                </View>
                <View>
                    <AppText
                        style={[styles.headingTextStyle, {marginLeft: dimen.app_padding, fontWeight: 'bold'}]}>
                        {customer.name + ' ' + customer.last_name}
                    </AppText>
                    <AppText style={[styles.headingTextStyle, {marginLeft: dimen.app_padding}]}>
                        {order.order_id}
                    </AppText>
                </View>

                <View>
                    <AppText style={[styles.headingTextStyle, {marginLeft: dimen.app_padding}]}>
                        Phone Number: {customer.phone_number}
                    </AppText>
                    <AppText style={[styles.headingTextStyle, {marginLeft: dimen.app_padding}]}>
                        Date/Time: {order.order_date + ' ' + order.order_time}
                    </AppText>
                </View>
            </View>

            <View style={{margin: dimen.app_padding}}>
                <AppText style={[styles.headingTextStyle, {marginBottom: 5, fontWeight: 'bold'}]}>
                    Address
                </AppText>
                <AppText style={[styles.headingTextStyle, {}]}>
                    {order.full_address}
                </AppText>
            </View>

            {order.alternate_address!== undefined && order.alternate_address!== '' &&
            <View style={{margin: dimen.app_padding}}>
                <AppText style={[styles.headingTextStyle, {marginBottom: 5, fontWeight: 'bold'}]}>
                   Alternate Address
                </AppText>
                <AppText style={[styles.headingTextStyle, {}]}>
                    {order.alternate_address}
                </AppText>
            </View>}

            {order.special_instruction !== null &&
            <View style={{margin: dimen.app_padding, marginTop: 0}}>
                <AppText style={[styles.headingTextStyle, {marginBottom: 5, fontWeight: 'bold'}]}>
                    Special Instructions
                </AppText>
                <AppText style={[styles.headingTextStyle, {}]}>
                    {order.special_instruction}
                </AppText>
            </View>}
        </View>
    );
};


const currentOrderStyles = StyleSheet.create({
    textStyle: {fontSize: 10, fontWeight: 'bold', flexGrow: 1,},
    addOnContainer: {
        backgroundColor: Colors.addon_background_color,
        padding: dimen.home_item_padding,
        marginTop: dimen.home_item_padding,
    },
    currentOrderServiceItemContainer: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        marginBottom: dimen.home_item_padding,
        marginTop: dimen.home_item_padding,
    },
    orderItemsMainContainer: {
        borderWidth: 1,
        borderColor: Colors.primary_color,
        margin: dimen.app_padding,
        padding: dimen.home_item_padding,
        paddingRight: dimen.home_item_padding,
    },


    staffContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        padding: dimen.app_padding,
    },
    profileImageContainer: {
        alignSelf: 'center',
        overflow: 'hidden',
        backgroundColor: Colors.image_background_color,
        borderRadius: 25,
        width: 50,
        height: 50
    },
    profileImage: {
        width: 50,
        height: 50,
        overflow: 'hidden',
        borderRadius: 25,
        alignSelf: 'center',
        borderWidth: 1,
    },

    totalPriceText: {fontSize: 12, fontWeight: 'bold'},
});


const styles = StyleSheet.create({
    mainContainer: {
        // flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        margin: dimen.app_padding,
        borderWidth: 1,
        borderColor: Colors.primary_color,

        // marginBottom: dimen.bottom_tab_height,
    },
    servicesScrollbar: {
        height: 40,
        backgroundColor: Colors.primary_color,
        paddingLeft: dimen.app_padding,
        paddingRight: dimen.app_padding,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',

    },
    headerImageContainer: {
        height: 150,
        width: '100%',
        marginTop: 10,
    },
    imageStyle: {
        resizeMode: 'cover',
    },
    dropdownIcon: {
        width: 10,
        height: 10,
        resizeMode: 'contain',
    },
    orderSummaryContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        // margin: dimen.app_padding,
        marginBottom: 0,
        padding: 10,

    },
    serviceNameItemContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    serviceNameText: {
        fontSize: 18,
        // color: this.state.currentIndex === i ? "#F08C4F" : "white",
        paddingHorizontal: 10,
        color: Colors.button_text_color,
    },
    serviceNameSelectedBottomLine: {
        width: '80%',
        height: 1,
        backgroundColor: Colors.button_text_color,
        marginLeft: 10,
        marginRight: 10,
        alignSelf: 'center',
    },
    serviceNameHeading: {
        fontSize: 18,
        fontWeight: 'bold',
        color: Colors.normal_text_color,
        margin: dimen.app_padding,
    },

    menuItemList: {
        flex: 1,
        padding: dimen.app_padding - dimen.home_item_padding,
        borderWidth: 1,
        borderColor: Colors.primary_color,
        marginLeft: dimen.app_padding,
        marginRight: dimen.app_padding,
        marginTop: dimen.app_padding,
    },
    orderSummaryPopupContainer: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: Colors.app_background_color,
        borderWidth: 1,
        borderColor: Colors.primary_color,
        height: dimen.orderPopupHeight,

    },

    staffContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        padding: dimen.app_padding,
    },
    profileImageContainer: {
        alignSelf: 'center',
        overflow: 'hidden',
        borderRadius: 25,
        width: 50,
        height: 50,
        backgroundColor:Colors.image_background_color
    },
    profileImage: {
        width: 50,
        height: 50,
        overflow: 'hidden',
        borderRadius: 25,
        alignSelf: 'center',
        borderWidth: 1,
    },

    orderItemsMainContainer: {
        borderWidth: 1,
        borderColor: Colors.primary_color,
        margin: dimen.app_padding,
        padding: dimen.home_item_padding,
        paddingRight: dimen.home_item_padding,
    },


});


