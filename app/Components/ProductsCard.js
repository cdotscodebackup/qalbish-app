import * as React from 'react';
import {View, Image, StyleSheet, TouchableOpacity} from 'react-native';
import AppText from './AppText';
import dimen from '../Styles/Dimen';
import Colors from '../Styles/Colors';
import {MyImage} from './MyImageButton';
import {console_log} from '../Classes/auth';


export const ProductsCard = (props) => {
    const offers = () => {
        props.offersPress(props.data);
    };
    const productDetails = () => {
        console_log(props.data.product_name)
        props.productDetailsPress(props.data);
    };

    return (
        <View style={styles.containerStyle}>
            <TouchableOpacity onPress={productDetails} style={styles.imageContainer}>
                <View style={{}}>
                    <MyImage source={require('../Asset/car.png')}
                             imageContainerStyle={styles.imageStyle}
                    />
                </View>
            </TouchableOpacity>

            <View style={styles.textContainer}>
                <AppText style={styles.textStyle} numberOfLines={3}>
                    {props.data.product_name}
                </AppText>
                <AppText style={styles.textStyle} numberOfLines={3}>
                    {props.data.info}
                </AppText>
                <AppText style={styles.textStyle} numberOfLines={3}>
                    {props.data.info}
                </AppText>
            </View>
            {/*<View style={styles.buttonContainer}>*/}
                {/*<TouchableOpacity onPress={offers}>*/}
                    {/*<AppText style={styles.buttonTextStyle}>*/}
                        {/*Offers*/}
                    {/*</AppText>*/}
                {/*</TouchableOpacity>*/}
            {/*</View>*/}
        </View>
    );
};

const styles = StyleSheet.create({
    containerStyle: {
        width: '33%',
        // margin:10,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // borderWidth: 1,borderColor: 'black'
        margin: dimen.home_item_padding,
    },
    imageContainer: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: Colors.light_border_color,
        borderRadius: dimen.app_padding,
        borderWidth: 1,
        backgroundColor: Colors.home_widget_background_color,
        overflow: 'hidden',

    },
    imageStyle: {
        width: 80,
        height: 100,
        resizeMode: 'contain',
        // margin: dimen.app_padding,

    },
    textContainer: {
        paddingTop: 10,
        paddingBottom: 5,
        // paddingRight:10,
    },
    textStyle: {
        fontSize: 10,
        color: Colors.primary_color,
        fontWeight: 'bold',
        alignSelf: 'center',
        textAlign: 'center',
    },
    buttonContainer: {
        backgroundColor: Colors.primary_color,
        borderRadius: 10,
        width: '80%',
        // marginLeft:10,
        // marginRight:10,

    },
    buttonTextStyle: {
        fontSize: 13,
        color: Colors.button_text_color,
        textAlign: 'center',
        // backgroundColor:'red',
        margin: 3,
    },


});


