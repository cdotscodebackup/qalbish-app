import * as React from 'react';
import {View, Image, StyleSheet, TouchableOpacity,TouchableNativeFeedback} from 'react-native';
import AppText from './AppText';
import dimen from '../Styles/Dimen';
import Colors from '../Styles/Colors';
import {MyImage} from './MyImageButton';


export const OffersCard = (props) => {
    const _onPress = () => {
        props.productOffersPress(props.data);
    };

    return (
        <TouchableOpacity onPress={_onPress}>
            <View style={styles.containerStyle}>
                <View style={styles.imageContainer}>
                    <MyImage source={require('../Asset/car.png')}
                             imageContainerStyle={styles.imageStyle}
                    />
                </View>
                <View style={styles.nameContainer}>
                    <AppText style={styles.nameTextStyle} numberOfLines={4}>
                        Microsoft Surface Book 2 15 i7 Quad Core-8th-gen
                    </AppText>
                </View>
                <View style={styles.offerContainer}>
                    <AppText style={styles.nameTextStyle} numberOfLines={4}>
                        Total offers: 50
                    </AppText>
                    <AppText style={styles.newOfferTextStyle} numberOfLines={4}>
                        New offers: 3
                    </AppText>
                </View>
                <View style={styles.moreContainer}>
                    <AppText style={styles.moreTextStyle}>More</AppText>
                    <MyImage source={require('../Asset/right_arrow.png')}
                             imageContainerStyle={styles.arrowImageStyle}
                    />
                </View>
            </View>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    containerStyle: {
        // width: '100%',
        // margin:10,
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        // borderWidth: 1,borderColor: 'black'
        margin: dimen.home_item_padding,
        borderColor: Colors.light_border_color,
        borderRadius: dimen.app_padding,
        borderWidth: 1,
        padding: 5,
        backgroundColor: Colors.home_widget_background_color,
    },
    imageContainer: {
        // width:'100%',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: Colors.light_border_color,
        borderRadius: dimen.app_padding,
        borderWidth: 1,
        overflow: 'hidden',
        padding: 5,


    },
    nameContainer: {
        flex: 1.3,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 7,
    },
    moreContainer: {
        flex: 0.5,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    offerContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-start',
        padding: 7,
        paddingLeft: 0,
        // backgroundColor:'yellow'

    },
    moreTextStyle: {
        color: Colors.primary_color,
        fontSize: 10,
        borderBottomWidth: 1,
        borderColor: Colors.primary_color,
    },
    nameTextStyle: {
        color: Colors.normal_text_color,
        fontSize: 10,
        // textAlign:'center',
        // padding:10,
    },
    newOfferTextStyle: {
        color: 'red',
        fontSize: 10,
        textAlign: 'center',
        // padding:10,
    },
    imageStyle: {
        flex: 1,
        // width: 30,
        height: 100,
        resizeMode: 'contain',
        padding: 5,
        // margin: dimen.app_padding,

    },
    arrowImageStyle: {
        width: 10,
        height: 10,
        resizeMode: 'contain',
        marginLeft: 5,
        // margin: dimen.app_padding,

    },
    textContainer: {
        paddingTop: 10,
        paddingBottom: 5,
        // paddingRight:10,
    },
    textStyle: {
        fontSize: 10,
        color: Colors.primary_color,
        fontWeight: 'bold',
        alignSelf: 'center',
        textAlign: 'center',
    },
    buttonContainer: {
        backgroundColor: Colors.primary_color,
        borderRadius: 10,
        width: '80%',
        // marginLeft:10,
        // marginRight:10,

    },
    buttonTextStyle: {
        fontSize: 13,
        color: Colors.button_text_color,
        textAlign: 'center',
        // backgroundColor:'red',
        margin: 3,
    },


});


