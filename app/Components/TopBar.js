import {View, StyleSheet, TouchableWithoutFeedback, StatusBar} from 'react-native';
import {MyImage} from './MyImageButton';
import {TextInput} from 'react-native-gesture-handler';
import MyStrings from '../Classes/MyStrings';
import Colors from '../Styles/Colors';
import React, {useState} from 'react';
import dimen from '../Styles/Dimen';
import {console_log, getObject} from '../Classes/auth';
import AppText from './AppText';
import MyImageButton from './MyImageButton';
import {cart_key} from '../Classes/UrlConstant';


const TopBar = (props) => {
    const [searchText, setSearchText] = useState('');
    const [cartCount, setCartCount] = useState(0);
    getObject((cart_key)).then((res) => {
        if (!res) {
            setCartCount(0);

        } else {
            setCartCount(res.length);
        }
    });


    const locationPress = () => {
        alert('Select location');
    };
    const crossPress = () => {
        props.isCross();
    };
    const backButtonPress = () => {
        props.backButton();
    };
    const goToCartPress = () => {
        props.GoToCart();
    };

    return (
        <View style={styles.topContainer}>
            <StatusBar backgroundColor={Colors.primary_color} barStyle="light-content" hidden={false}/>
            <AppText style={styles.topHeading}>{props.title}</AppText>
            <View style={styles.locationContainer}>
                {/*{props.isCross === undefined &&*/}
                {/*<TouchableWithoutFeedback onPress={locationPress}>*/}
                {/*<View style={{flexDirection: 'row'}}>*/}
                {/*<AppText style={styles.locationHeading}>Location</AppText>*/}
                {/*<MyImage*/}
                {/*source={require('../Asset/location.png')}*/}
                {/*imageContainerStyle={styles.locationButtonContainer}/>*/}
                {/*</View>*/}
                {/*</TouchableWithoutFeedback>*/}
                {/*}*/}

                {props.isCross !== undefined &&
                <TouchableWithoutFeedback onPress={crossPress}>
                    <View style={{flexDirection: 'row'}}>
                        <AppText style={[styles.locationHeading, {paddingLeft: 10}]}> X </AppText>

                        {/*<MyImage*/}
                        {/*source={require('../Asset/cros.png')}*/}
                        {/*imageContainerStyle={styles.locationButtonContainer}/>*/}
                    </View>
                </TouchableWithoutFeedback>
                }

                {props.GoToCart !== undefined &&
                <View>

                    <MyImageButton onPress={goToCartPress}
                                   source={require('../Asset/cart.png')}
                                   imageContainerStyle={styles.cartContainer}/>
                    <View style={styles.cartCounterContainerStyle}>
                        <AppText style={styles.cartCounterStyle}>
                            {cartCount}
                        </AppText>
                    </View>
                </View>
                }


                {props.backButton !== undefined &&
                <MyImageButton onPress={backButtonPress}
                               source={require('../Asset/back_arrow.png')}
                               imageContainerStyle={styles.backArrowContainer}/>}
            </View>
        </View>
    );
};
export default TopBar;


const styles = StyleSheet.create({
    cartCounterStyle: {
        color: Colors.primary_color,
        fontSize: 6,
    },
    cartCounterContainerStyle: {
        position: 'absolute',
        left: 0, top: 0,
        width: 14, height: 14,
        justifyContent: 'center', alignItems: 'center',
        backgroundColor: Colors.app_background_color,
        color: Colors.primary_color,
        borderRadius: 7,
        padding: 2,
        fontSize: 7,
    },
    //top bar
    topContainer: {
        height: 45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',

        backgroundColor: Colors.primary_color,
    },
    locationButtonContainer: {
        width: 15,
        height: 15,
        marginLeft: 10,
    },
    backArrowContainer: {
        width: 15,
        height: 15,
        // marginLeft: 10,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        position: 'absolute',
        left: 0,
        right: dimen.app_padding,
    },
    cartContainer: {
        width: 30,
        height: 30,
        // marginLeft: 10,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        // position: 'absolute',
        // right: 0,
        // left: dimen.app_padding,
    },
    topHeading: {
        fontSize: 24,
        color: Colors.button_text_color,
    },
    locationHeading: {
        fontSize: 13,
        color: Colors.button_text_color,
    },
    locationContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        position: 'absolute',
        left: dimen.app_padding,
        right: dimen.app_padding,
    },
});
