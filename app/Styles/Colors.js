const Colors = {

    primary_color: '#ff6c2b',
    primary_dark_color: '#144d77',
    app_background_color: '#fbeddd',
    accent_color: '#0ebebe',
    image_background_color: '#f7ab8a',
    inactive_menu_color: '#ffffff',
    add_on_divider_color: '#fbd5bd',
    current_order_color: '#169242',
    canceled_color: '#f81700',
    not_active_text_color: '#acacac',


    addon_background_color: '#fce7cf',

    normal_text_color: '#363636',
    highlight_text_color: '#ff0000',
    heading_text_color: '#231f20',
    button_text_color: '#ffffff',
    tab_text_color: '#144d88',
    signup_input_text_background_color: '#e4e4e4',

    border_color: '#d6d6d6',
    bottom_menu_border: '#d0d0d0',
    bottom_menu_text_color: '#707070',

    light_text_login: '#8f8f8f',
    dark_text_login: '#8f8f8f',
    homelighbackground: '#eceff8',


    home_widget_background_color: '#ffffff',
    home_background_color: '#f8f8f8',

    header_text_color: '#ffffff',
    header_start_text: '#35bfae',
    light_background_color: '#e6e8e8',
    light_border_color: '#e5e5e5',
    text_prominent_color: '#25437d',
    fbcolor: '#25437d',
    gmailcolor: '#963D32',
    tabbar_background_color: '#333333',

    black: '#000000',
    night: '#333333',
    charcoal: '#474747',
    gray: '#7D7D7D',
    lightishgray: '#9D9D9D',
    lightgray: '#D6D6D6',
    smoke: '#EEEEEE',
    white: '#FFFFFF',
    ypsDark: '#47546E',
    yps: '#637599',
    ypsLight: '#7B92BF',
    cosmic: '#963D32',

};
export default Colors;
