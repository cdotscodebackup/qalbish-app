// RootNavigation.js

import * as React from 'react';
import {console_log} from './auth';

export const isMountedRef = React.createRef();

export const navigationRef = React.createRef();


export function navigate(name, params) {
    console_log(isMountedRef);
    if (isMountedRef.current ) {
        console_log(navigationRef);
        // Perform navigation if the app has mounted
        // navigationRef.current.navigate(name, params);
        navigationRef.current.navigate('Bookings','');
        console_log('navigate logout')
    } else {
        console_log('in else navigate logout')
        // You can decide what to do if the app hasn't mounted
        // You can ignore this, or add these actions to a queue you can call later
    }
}

// add other navigation functions that you need and export them
