import React from "react";

export const contextInitialState = {
    services: [],
    discounts:[],
    cart:[],
};

export const MyContext = React.createContext(contextInitialState );
export const CartContext = React.createContext([] );


export const ProviderContext = MyContext.Provider;
export const Consumer = MyContext.Consumer;

export const CartProvider = CartContext.Provider;
export const CartConsumer = CartContext.Consumer;
