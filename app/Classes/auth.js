import {Alert} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {all_trips_key, internetError, notification_key, token_key, trip_key} from './UrlConstant';
import NetInfo from '@react-native-community/netinfo';

import * as RootNavigation from './RootNavigation';

export const USER_KEY = 'auth-key';
export const USER = 'user-key';

export const onSignIn = () => AsyncStorage.setItem(USER_KEY, 'true');
export const onSignOut = () => AsyncStorage.removeItem(USER_KEY);

export const onSignInUser = (user) => {
    AsyncStorage.setItem(USER_KEY, 'true');
    AsyncStorage.setItem(USER, user);
};

export const onSignOutUser = () => {
    AsyncStorage.removeItem(USER_KEY);
    AsyncStorage.removeItem(USER);
    AsyncStorage.removeItem(trip_key);
    AsyncStorage.removeItem(all_trips_key);
};

export const isSignedIn = () => {
    return new Promise((resolve, reject) => {
        AsyncStorage.getItem(USER_KEY)
            .then(res => {
                if (res !== null) {
                    resolve(true);
                } else {
                    resolve(false);
                }
            })
            .catch(err => reject(err));
    });
};

export const console_log = (message, isProduction = false) => {
    if (!isProduction) {
        console.log(message);
    }
};
export const console_log_message = (text, message, isProduction = false) => {
    if (!isProduction) {
        console.log(text + ' : ' + message);
    }
};

export const successAlert = (message) => {
    Alert.alert('Success', message);
};
export const errorAlert = (message) => {
    Alert.alert('Error', message);
};
export const simpleAlert = (message) => {
    Alert.alert('Alert', message);
};


export const getUser = () => {
    return new Promise((resolve, reject) => {
        AsyncStorage.getItem(USER)
            .then(res => {
                if (res !== null) {
                    // console.log("user found")
                    resolve('Bearer ' + JSON.parse(res).token);
                }
                // else {
                //     resolve(false);
                // }
            })
            .catch(err => {
                // console.log("User not found. Session expired")
                reject(err);
            });
    });
};
export const getUserObject = () => {
    return new Promise((resolve, reject) => {
        AsyncStorage.getItem(USER)
            .then(res => {
                if (res !== null) {
                    // console.log("user found")
                    resolve(res);
                }
                // else {
                //     resolve(false);
                // }
            })
            .catch(err => {
                // console.log("User not found. Session expired")
                reject(err);
            });
    });
};

export const getLocalizedDate = (str) => {
    // var str = "2019-08-02 05:50:00";

    var year = str.substring(0, 4);
    var month = str.substring(5, 7);
    var day = str.substring(8, 10);
    var hour = str.substring(11, 13);
    var minute = str.substring(14, 16);
    var second = str.substring(17, 19);

    // 2019-01-01T00:00:00
    // var ns = year + '-' + month + '-' + day + 'T' + hour + ':' + minute + ':' + second

    var date = Date.UTC(year, month - 1, day, hour, minute, second);


    // console.log('Date String: ' + ns)
    // console.log('Date utc seconds: ' + date)
    // console.log('Date from seconds: ' + new Date(date).toString())

    return new Date(date);

};
export const getLocalizedDate2 = (str) => {
    // var str = "2019-08-02 05:50:00";
    // 2019-08-03T14:24:06.000000Z

    var year = str.substring(0, 4);
    var month = str.substring(5, 7);
    var day = str.substring(8, 10);
    var hour = str.substring(11, 13);
    var minute = str.substring(14, 16);
    var second = str.substring(17, 19);

    var date = Date.UTC(str);


    // console.log('Date String: ' + ns)
    // console.log('Date utc seconds: ' + date)
    // console.log('Date from seconds: ' + new Date(date).toString())

    return new Date(date);

};

export const strip_html_tags = (str) => {
    if ((str === null) || (str === '')) {
        return false;
    } else {
        str = str.toString();
    }
    return str.replace(/<[^>]*>/g, '');
};


export const isNetConnected = () => {
    return new Promise((resolve, reject) => {
        NetInfo.fetch().then(state => {
            // console_log(state.isConnected);
            if (state.isConnected) {
                resolve(true);
            } else {
                resolve(false);
            }
        }).catch(() => {
            reject(false);
        });

    });
};


export const saveObject = (key, object) => {
    let str = JSON.stringify(object);
    return new Promise((resolve, reject) => {
        AsyncStorage.setItem(key, str)
            .then(() => {
                resolve(true);
            })
            .catch((err) => {
                console_log('error while saving' + err);
                reject(false);
            });

    });
};
export const getObject = (key) => {
    return new Promise((resolve, reject) => {
        AsyncStorage.getItem(key)
            .then(res => {
                if (res !== null) {
                    resolve(JSON.parse(res));
                } else {
                    resolve(false);
                }
            })
            .catch(err => {
                reject(err);
            });
    });
};
export const clearAllData = () => {
    AsyncStorage.getAllKeys()
        .then(keys => AsyncStorage.multiRemove(keys))
        .then(() => {
            console_log('success');
        });
};

export const parseError = (error) => {
    var m = error;
    var keys = [];
    for (var k in m) {
        keys.push(k);
    }

    var mforuser = '';
    // console.log('key 0 ' + (m[keys[0]]))

    for (var k in keys) {
        mforuser = mforuser + m[keys[k]] + '\n';
    }

    return mforuser;
};


export const getData = (url, method, formdata) => {
    console.log('Url: ' + url);
    // console.log('form data: ' + JSON.stringify(formdata));
    return new Promise((resolve, reject) => {
        var s = 404;
        getObject(token_key).then(token => {
            // console.log(url, method, token)
            fetch(url, {
                method: method,
                body: formdata,
                headers: {
                    'Authorization': 'Bearer ' + (token),
                    'Accept': 'application/json',
                },
            })
                .then((response) => {
                        const statusCode = response.status;
                        var data = response.json();

                        console.log('Status code: ' + statusCode);
                        // console.log("Status data: "+ JSON.stringify(data));
                        s = statusCode;
                        return Promise.all([statusCode, data]);
                    },
                ).then(([status, data]) => {

                console.log('Status data: ' + JSON.stringify(status));
                if (status ===500) {
                    reject('Server Error');
                } else if (status === 422) {
                    resolve(data);
                }

                resolve(data);

            })
                .catch((error) => {
                    if (s === 200) {
                        resolve({'status': true});
                        s === 401;
                    } else {
                        console.log('API Error: ' + error);
                        if (error === 'TypeError: Network request failed') {
                            reject('Network request failed. Try again.');
                        } else {
                            reject(error);
                        }

                    }
                });
        })
            .catch(err => {
                console.log('user not found ' + err);
                reject('User logged out');
            });


    });

};
export const postMultipartData = (url, method, formdata) => {
    console.log('Url: ' + url);
    console.log('form data: ' + JSON.stringify(formdata));
    return new Promise((resolve, reject) => {
        var s = 404;
        getUser().then(token => {
            // console.log(url, method, token)
            fetch(url, {
                method: method,
                body: formdata,
                headers: {
                    Accept: 'application/json',
                    Authorization: token,
                },
            })
                .then((response) => {
                        const statusCode = response.status;
                        var data = response.json();

                        console.log('Status code: ' + statusCode);
                        // console.log("Status data: "+ JSON.stringify(data));
                        s = statusCode;
                        return Promise.all([statusCode, data]);
                    },
                ).then(([status, data]) => {
                // console.log("Status data: "+ JSON.stringify(data));
                resolve(data);
            })
                .catch((error) => {
                    if (s == 200) {
                        resolve({'status': true});
                        s == 401;
                    } else {
                        console.log('API Error: ' + error);
                        reject(error);
                    }
                });
        })
            .catch(err => {
                console.log('user not found ' + err);
                reject('User logged out');
            });
    });
};

export const getDataWithoutTokenNotEncoded = (url, method, formdata) => {
    console.log('Url: ' + url);
    console.log('form data: ' + JSON.stringify(formdata));

    return new Promise((resolve, reject) => {

        fetch('https://www.salloumdesign.com/path/job-laravel/api/login', {
            method: method,
            headers: {
                // 'Content-Type': 'application/x-www-form-urlencode',
            },
            body: formdata,

        })
            .then((response) => {
                    const statusCode = response.status;
                    var data = response.text();
                    console.log('Status code: ' + statusCode);
                    console.log('Status data: ' + JSON.stringify(data));

                    return Promise.all([statusCode, data]);
                },
            ).then(([status, data]) => {

            if (status === 500) {
                reject('Server Error');
            } else if (status === 422) {
                resolve(data);
            }

            resolve(data);
        })
            .catch((error) => {
                console.log('API Error: ' + error);
                reject(error);
            });

    });

};

export const getDataWithToken = (url, method, formdata) => {

    console.log('Url: ' + url);
    // console.log('form data: ' + JSON.stringify(formdata));

    const formBody = Object.keys(formdata)
        .map(key => encodeURIComponent(key) + '=' + encodeURIComponent(formdata[key])).join('&');

    // console.log('body'+formBody);

    return new Promise((resolve, reject) => {
        var s = 404;
        getObject(token_key).then(token => {
            let header = {
                'Authorization': 'Bearer ' + (token),
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded',
            };

            fetch(url, {
                method: method,
                body: formBody,
                headers: header,
            })
                .then((response) => {
                        const statusCode = response.status;
                        var data = response.json();

                        console.log('Status code: ' + statusCode);
                        // console.log("Status data: "+ JSON.stringify(data));
                        s = statusCode;
                        return Promise.all([statusCode, data]);
                    },
                ).then(([status, data]) => {

                // console.log('Status code: ' + JSON.stringify(status));
                // console.log('Status data: ' + JSON.stringify(data));
                // RootNavigation.navigate('Authentication','')

                if (status === 500) {
                    reject('Server Error');
                } else if (status === 422) {
                    resolve(data);
                } else if (status === 401) {
                    clearAllData();
                    resolve(data);
                }
                resolve(data);

            })
                .catch((error) => {
                    if (s === 200) {
                        resolve({'status': true});
                        s = 401;
                    } else if (s === 401) {
                        resolve({'status': true});
                        clearAllData();
                        // console.log('API Error: ' + error);
                        s = 401;
                    } else {
                        console.log('API Error: ' + error);
                        if (error === 'TypeError: Network request failed') {
                            reject('Network request failed. Try again.');
                        } else {
                            reject(error);
                        }
                    }
                });
        })
            .catch(err => {
                console.log('user not found ' + err);
                reject('User logged out');
            });


    });

};
export const getDataWithoutToken = (url, method, formdata) => {
    console.log('Url: ' + url);
    // console.log('form data: ' + JSON.stringify(formdata));

    const formBody = Object.keys(formdata)
        .map(key => encodeURIComponent(key) + '=' + encodeURIComponent(formdata[key])).join('&');

    // console.log('form data: ' + (formBody));

    return new Promise((resolve, reject) => {

        fetch(url, {
            method: method,
            headers: {
                'Accept': 'application/json',
                // 'Authorization': 'Bearer token',
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            body: formBody,

        })
            .then((response) => {
                    const statusCode = response.status;
                    var data = response.text();
                    console.log('Status code: ' + statusCode);
                    // console.log('Status data: ' + JSON.stringify(data));
                    return Promise.all([statusCode, data]);
                },
            ).then(([status, data]) => {

            if (status === 500) {
                reject('Server Error');
            } else if (status === 422) {
                resolve(data);
            }

            resolve(data);
        })
            .catch((error) => {
                console.log('API Error: ' + error);
                reject(error);
            });

    });

};

export const getDataWithoutTokenAsxync = async (url, method, formdata) => {
    // console.log('Url: ' + url);
    // console.log("form data: " + JSON.stringify(formdata))
    try {
        let response = await fetch(url, {
            method: method,
            body: formdata,
            headers: {
                Accept: 'application/json',
            },
        });
        let body = await response.json();

        return body;
    } catch (e) {
        return e;
    }

};
























