const MyStrings = {

    login_phone_placeholder: 'Phone Number (03-- -------)',
    login_phone_email_placeholder: 'Phone No or Email',
    login_password_placeholder: 'Password',
    forgot_password: 'Forgot your password?',
    login_button: 'Sign in',
    orText: 'Or',
    quickConnect: 'Quick connect with',
    enterCodeText:'Please Enter the Four Digit Code',
    sentViaSms:'Sent via SMS',
    submit:'Submit',
    didNotReceiveCode:'Did Not Receive Code?',
    callNow:'Call Now',
    termsAndConditions:'Terms and conditions apply.',
    dontHaveAccount:'Don\'t have an account? ',
    signup:'Sign Up',
    disclaimer1:'By signing in or creating an account, you agree with our ',
    terms:'Terms & Conditions',
    and:' and ',
    privacy:'Privacy Statement',

    //signup form
    firstname:'First Name',
    lastname:'Last Name',
    email:'Email',
    dateofbirth:'Date of Birth',
    mobile_number:'Mobile Number (03-- -------)',
    cnic:'CNIC',
    password:'Password',
    confirm_password:'Confirm Password',
    address:'Address',
    referal:'Referral Code If Available',


    disclaimertext:'I agree to the ',

    //forget password
    forgotPassword:'Forgot password?',
    otptext:'Please enter your phone number so that we will',
    otptext2:' send you an OTP code?',

    //new password
    createNP:'Forgot password?',
    newPassToContinue:'Please enter your new password to continue',
    newPassPlaceholder:'Enter your new password',
    rePassPlaceholder:'Re-enter your new password',


    //user ProductCategories
    searchFieldPlaceHolder:'Search anything...',




};
export default MyStrings;
