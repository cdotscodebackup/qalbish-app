export const BASE_URL = 'https://qalbish.pk/';
export const api_key = 'AIzaSyAZQiMEU1xYMEVTgch8O5WmL-iZVfQjko0';

export const categories_url = BASE_URL + '/api/category';
export const products_url = BASE_URL + '/api/productdetail';


export const login_url = BASE_URL + '/api/login';
export const register_url = BASE_URL + '/api/register';
export const logout_url = BASE_URL + '/api/logout';

export const cities_url = BASE_URL + '/api/cities';
export const area_url = BASE_URL + '/api/areas/';

export const services_url = BASE_URL + '/api/services';
export const orders_url = BASE_URL + '/api/orders';

export const store_order_url = BASE_URL + '/api/orders/store';
export const coupon_apply_url = BASE_URL + '/api/coupon';
export const change_password_url = BASE_URL + '/api/update-password';


export const staff_all_orders_url = BASE_URL + '/api/staff/appointments';
export const update_new_order_status_url = BASE_URL + '/api/staff/update-order-staff-status';
export const update_new_order_progress_url = BASE_URL + '/api/staff/update-order-progress-status';
export const update_profile_url = BASE_URL + '/api/update-profile';
export const update_profile_image_url = BASE_URL + '/api/update-profile-image';

export const staff_rating_url = BASE_URL + '/api/orders/rating';
export const get_otp_url = BASE_URL + '/api/otp';
export const verify_otp_url = BASE_URL + '/api/verify/otp';
export const set_new_password_url = BASE_URL + '/api/forgot-password-request';


export const staff_save_lat_lon_url = BASE_URL + '/api/store-lat-lng';
export const get_staff_lat_lon_url = BASE_URL + '/api/get-lat-lng';





export const internetError = 'No internet connection';
export const general_network_error = 'Network error. Try again.';


export const trip_key = 'trip_key';
export const token_key = 'token_key';
export const data_key = 'data_key';
export const referal_key = 'referal_key';
export const referral_discount_key = 'referral_discount_key';
export const settings_key = 'settings_key';
export const last_location_time = 'last_location_time';
export const on_off_key = 'on_off_key';


export const user_key = 'user_key';
export const services_key = 'services_key';
export const cart_key = 'cart_key';
export const web_view_url_key = 'web_view_url_key';
export const all_trips_key = 'all_trips_key';
export const notification_key = 'notification_key';
