import { combineReducers } from 'redux';
import {console_log} from '../Classes/auth';
import {GET_CATEGORIES, GET_CATEGORIES_FAIL, GET_CATEGORIES_SUCCESS} from '../Actions/types';
import {productReducer} from './productReducer';

const INITIAL_STATE = {
    current: [],
    possible: [
        'Allie',
        'Gator',
        'Lizzie',
        'Reptar',
    ],
};

const friendReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'ADD_FRIEND':
            // Pulls current and possible out of previous state
            // We do not want to alter state directly in case
            // another action is altering it at the same time
            const {
                current,
                possible,
            } = state;

            // Pull friend out of friends.possible
            // Note that action.payload === friendIndex
            const addedFriend = possible.splice(action.payload, 1);

            // And put friend in friends.current
            current.push(addedFriend);

            // Finally, update our redux state
            // const newState = { current:['ali'], possible };
            const newState = ({},state ,{ current, possible});
            console_log('changed state: ')
            console_log(newState);

            return newState;
        default:
            return state
    }
};
const nameReducer=(state='ali' ,action)=>{
    switch (action.type) {
        case 'RENAME':

            return 'husnain'+action.payload

        default:
            return state

    }
}
const userReducer=(state={name:'ali',des:'software engineer'} ,action)=>{
    switch (action.type) {
        case 'USER':
            return {name:'HUSNAIN',des:'software 1'}
        default:
            return state

    }
}


const categoryInitialState={
    categories:[],
    loading:false,
    errorMessage:''
}

const categoryReducer=(state=categoryInitialState ,action)=>{
    switch (action.type) {
        case GET_CATEGORIES:
            return {...state, loading: action.payload};
        case GET_CATEGORIES_SUCCESS:
            return {...state, categories: action.payload, loading: action.loading};
        case GET_CATEGORIES_FAIL:
            return {...state, errorMessage: action.payload, loading: action.loading};
        default:
            return state;
    }
}

export default combineReducers({
    friends: friendReducer,
    name: nameReducer,
    user: userReducer,
    categories:categoryReducer,
    productsResponse:productReducer,

});
