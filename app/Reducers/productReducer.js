import {
    GET_PRODUCTS,
    GET_PRODUCTS_FAIL,
    GET_PRODUCTS_SUCCESS,
} from '../Actions/types';




const productsInitialState={
    products:{},
    loading:false,
    errorMessage:''
}

export const productReducer=(state=productsInitialState ,action)=>{
    switch (action.type) {
        case GET_PRODUCTS:
            return {...state, loading: action.payload};
        case GET_PRODUCTS_SUCCESS:
            return {...state, products: action.payload, loading: action.loading};
        case GET_PRODUCTS_FAIL:
            return {...state, errorMessage: action.payload, loading: action.loading};
        default:
            return state;
    }
}
