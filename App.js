import 'react-native-gesture-handler';
import React, {useState} from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
} from 'react-native';

import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import Loginn from './app/screens/Auth/Login';
import SplashScreen from 'react-native-splash-screen';
import Services from './app/screens/Services';
import {ProductDetails} from './app/screens/ProductDetails';
import {ProductList} from './app/screens/ProductList';
import Authentication from './app/screens/Authentication';
import TabBar from './app/Components/TabBar';
import SignUp from './app/screens/Auth/SignUp';
import IntroSlides from './app/screens/IntroSlides';
import PhoneVarification from './app/screens/Auth/PhoneVarification';
import SignUpForm from './app/screens/Auth/SignUpForm';
import ForgetPassword from './app/screens/Auth/ForgetPassword';
import NewPassword from './app/screens/Auth/NewPassword';
import {Bookings} from './app/screens/Bookings';
import {ProductOffers} from './app/screens/ProductOffers';
import {Refer} from './app/screens/Refer';
import {Menu} from './app/screens/Menu';
import {SearchResults} from './app/screens/SearchResults';

import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import friendReducer from './app/Reducers/friendsReducer';
import ServiceDetail from './app/screens/ServiceDetail';
import CartDetails from './app/screens/CartDetails';
import ScheduleOrder from './app/screens/ScheduleOrder';
import {OrderDetails} from './app/screens/OrderDetails';
import {CartProvider, contextInitialState, ProviderContext} from './app/Classes/AppContext';
import {Profile} from './app/screens/Profile';
import {ContactUs} from './app/screens/ContactUs';
import {MyReferrals} from './app/screens/MyReferrals';

import {NavigationContainer} from '@react-navigation/native';
import {navigationRef,isMountedRef} from './app/Classes/RootNavigation';
import {StaffOrders} from './app/screens/StaffOrders';
import {StaffProcessing} from './app/screens/StaffProcessing';
import TabBarStaff from './app/Components/TabBarStaff';
import {StaffMenu} from './app/screens/StaffMenu';
import StaffOrderNew from './app/screens/StaffOrderNew';
import StaffOrderProcessing from './app/screens/StaffOrderProcessing';
import SubmitOrder from './app/screens/SubmitOrder';
import ChangePassword from './app/screens/Auth/ChangePassword';
import {CustomerContactUs} from './app/screens/CustomerContactUs';
import crashlytics from '@react-native-firebase/crashlytics';
import {TrackStaff} from "./app/screens/TrackStaff";
import Colors from './app/Styles/Colors';
import MyBackgroundImage from './app/Components/MyBackgroundImage';

const store = createStore(friendReducer, applyMiddleware(thunk));

class App extends React.Component {
    constructor() {
        super();
        this.state={
            cartItemsArray:[],
        }
    }
    componentDidMount() {
        SplashScreen.hide();
        isMountedRef.current = true;


    }

    // addCartItem=(item)=>{
    //     console.log(item)
    //     this.setState({
    //         cartItemsArray:[{'id':1}]
    //     })
    // }

    render() {
        return (
            <ProviderContext value={{
                // cartItem : this.state.cartItemsArray, addCartItem:(item)=>this.addCartItem(item),
                services: [],
                discounts:[],
                cart:[],
            }}>
                <SafeAreaView style={{flex:1,backgroundColor:Colors.primary_color}}>
                    <StatusBar backgroundColor={Colors.primary_color} barStyle="dark-content" hidden={false}/>
                    <Provider store={store}>
                        <AppContainer/>
                    </Provider>
                </SafeAreaView>
            </ProviderContext>

        );
    }
};

const AuthStack = createStackNavigator({
        Login: {
            screen: Loginn,
        },
        Signup: {
            screen: SignUp,
        },
        SignUpForm: {
            screen: SignUpForm,
        },
        PhoneVarification: {
            screen: PhoneVarification,
        },
        IntroSlides: {
            screen: IntroSlides,
        },
        ForgetPassword: {
            screen: ForgetPassword,
        },
        NewPassword: {
            screen: NewPassword,
        },
    },
    {
        initialRouteName: 'Login',
        headerMode: 'none',
        // headerLayoutPreset: 'left',
    },
);

const ReferStack = createStackNavigator({
        Refer: {
            screen: Refer,
        },

    },
    {
        headerMode: 'none',
        // headerLayoutPreset: 'left',

    },
);
const MenuStack = createStackNavigator({
        Menu: {
            screen: Menu,
        },

    },
    {
        headerMode: 'none',
        // headerLayoutPreset: 'left',
    },
);
const ServiceStack = createStackNavigator({
        Services: {
            screen: Services,
        },
        ProductList: {
            screen: ProductList,
        },
        ProductDetails: {
            screen: ProductDetails,
        },
    },
    {
        headerMode: 'none',
        // headerLayoutPreset: 'left',
        initialRouteName: 'Services',
    },
);
const BookingStack = createStackNavigator({
        Bookings: {
            screen: Bookings,
        },

        ProductOffers: {
            screen: ProductOffers,
        },
    },
    {
        headerMode: 'none',
        // headerLayoutPreset: 'left',
        initialRouteName: 'Bookings',
    },
);

const CustomerTabNav = createBottomTabNavigator(
    {
        Home: {
            screen: ServiceStack,
        },
        Bookings: {
            screen: BookingStack,
        },
        Refer: {
            screen: ReferStack,
        },
        Menu: {
            screen: MenuStack,
        },

    },
    {
        tabBarComponent: TabBar,
        lazy: true,

    },
);


const CustomerStack = createStackNavigator({
        CustomerTabNav: {
            screen: CustomerTabNav,
        },
        ServiceDetail: {
            screen: ServiceDetail,
        },
        CartDetails: {
            screen: CartDetails,
        },
        ScheduleOrder: {
            screen: ScheduleOrder,
        },
        OrderDetails: {
            screen: OrderDetails,
        },
        TrackStaff: {
            screen: TrackStaff,
        },
        Profile: {
            screen: Profile,
        },
        MyReferrals: {
            screen: MyReferrals,
        },
        ChangePassword: {
            screen: ChangePassword,
        },
        ContactUs: {
            screen: CustomerContactUs,
        },
        SearchResults: SearchResults,
    },
    {
        headerMode: 'none',
        // headerLayoutPreset: 'left',
        initialRouteName: 'CustomerTabNav',


    },
);




const StaffOrderStack = createStackNavigator({
        Bookings: {
            screen: StaffOrders,
        },
        OrderDetails: {
            screen: OrderDetails,
        },
        ProductOffers: {
            screen: ProductOffers,
        },
    },
    {
        headerMode: 'none',
        // headerLayoutPreset: 'left',
        initialRouteName: 'Bookings',
    },
);
const StaffProcessingStack = createStackNavigator({
        Bookings: {
            screen: StaffProcessing,
        },
        ProductOffers: {
            screen: ProductOffers,
        },
        OrderDetails: {
            screen: OrderDetails,
        },
    },
    {
        headerMode: 'none',
        // headerLayoutPreset: 'left',
        initialRouteName: 'Bookings',
    },
);

const StaffContactStack = createStackNavigator({
        ContactUs: {
            screen: ContactUs,
        },

    },
    {
        headerMode: 'none',
        // headerLayoutPreset: 'left',

    },
);
const StaffMenuStack = createStackNavigator({
        Menu: {
            screen: StaffMenu,
        },
    },
    {
        headerMode: 'none',
        // headerLayoutPreset: 'left',
    },
);
const StaffTabNav = createBottomTabNavigator(
    {
        Home: {
            screen: StaffOrderStack,
        },
        Bookings: {
            screen: StaffProcessingStack,
        },
        Refer: {
            screen: StaffContactStack,
        },
        Menu: {
            screen: StaffMenuStack,
        },

    },
    {
        tabBarComponent: TabBarStaff,
        lazy: true,

    },
);


const StaffStack = createStackNavigator({
        CustomerTabNav: {
            screen: StaffTabNav,
        },
        StaffOrderNew: {
            screen: StaffOrderNew,
        },
        StaffOrderProcessing: {
            screen: StaffOrderProcessing,
        },
        ServiceDetail: {
            screen: ServiceDetail,
        },
        CartDetails: {
            screen: CartDetails,
        },
        SubmitOrder: {
            screen: SubmitOrder,
        },
        ScheduleOrder: {
            screen: ScheduleOrder,
        },
        Profile: {
            screen: Profile,
        },
        ContactUs: {
            screen: ContactUs,
        },
        MyReferrals: {
            screen: MyReferrals,
        },
        ChangePassword: {
            screen: ChangePassword,
        },
        SearchResults: SearchResults,
    },
    {
        headerMode: 'none',
        // headerLayoutPreset: 'left',
        initialRouteName: 'CustomerTabNav',
    },
);

const SwitchNavigator = createSwitchNavigator(
    {
        Authentication: Authentication,
        CustomerDashboard: CustomerStack,
        StaffDashboard: StaffStack,
        AuthStack: AuthStack,
    },
    {
        initialRouteName: 'Authentication',
    },
);


const AppContainer = createAppContainer(SwitchNavigator);
export default App;






























